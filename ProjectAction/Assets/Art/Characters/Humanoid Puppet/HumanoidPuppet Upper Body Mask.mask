%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: HumanoidPuppet Upper Body Mask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Low Poly - Baked And Modified
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Neck
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Neck/Head
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Fingers_Low.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Fingers_Low.L/Fingers_Mid.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Fingers_Low.L/Fingers_Mid.L/Fingers_High.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Fingers_Low.L/Fingers_Mid.L/Fingers_High.L/Fingers_High.L_end
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Thumb_Low.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Thumb_Low.L/Thumb_Mid.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Thumb_Low.L/Thumb_Mid.L/Thumb_High.L
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.L/Shoulder.L/Elbow.L/Hand.L/Thumb_Low.L/Thumb_Mid.L/Thumb_High.L/Thumb_High.L_end
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Fingers_Low.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Fingers_Low.R/Fingers_Mid.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Fingers_Low.R/Fingers_Mid.R/Fingers_High.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Fingers_Low.R/Fingers_Mid.R/Fingers_High.R/Fingers_High.R_end
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Thumb_Low.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Thumb_Low.R/Thumb_Mid.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Thumb_Low.R/Thumb_Mid.R/Thumb_High.R
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Lower Chest/Chest/Collarbone.R/Shoulder.R/Elbow.R/Hand.R/Thumb_Low.R/Thumb_Mid.R/Thumb_High.R/Thumb_High.R_end
    m_Weight: 1
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.L/Shin.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.L/Shin.L/Foot.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.L/Shin.L/Foot.L/Toes.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.L/Shin.L/Foot.L/Toes.L/Toes.L_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.R/Shin.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.R/Shin.R/Foot.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.R/Shin.R/Foot.R/Toes.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/Pelvis/Hip.R/Shin.R/Foot.R/Toes.R/Toes.R_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/CTRL_Foot_Roll.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/CTRL_Foot_Roll.L/IK_Leg.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/CTRL_Foot_Roll.L/IK_Leg.L/IK_Leg.L_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/CTRL_Foot_Roll.L/IK_Foot.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/CTRL_Foot_Roll.L/IK_Foot.L/IK_Foot.L_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/IK_Toe.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.L/CTRL_Heel_Roll.L/IK_Toe.L/IK_Toe.L_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/IK_Target_Knee.L
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/IK_Target_Knee.L/IK_Target_Knee.L_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/CTRL_Foot_Roll.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/CTRL_Foot_Roll.R/IK_Leg.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/CTRL_Foot_Roll.R/IK_Leg.R/IK_Leg.R_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/CTRL_Foot_Roll.R/IK_Foot.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/CTRL_Foot_Roll.R/IK_Foot.R/IK_Foot.R_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/IK_Toe.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/CTRL_Toes_Roll.R/CTRL_Heel_Roll.R/IK_Toe.R/IK_Toe.R_end
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/IK_Target_Knee.R
    m_Weight: 0
  - m_Path: AR_HumanoidPuppet/AnimationRoot/IK_Target_Knee.R/IK_Target_Knee.R_end
    m_Weight: 0
