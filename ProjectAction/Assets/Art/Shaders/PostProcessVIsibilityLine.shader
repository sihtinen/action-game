﻿Shader "Sihtinen/VisibilityLine"
{
    Properties
    {
        [HideInInspector] _MainTex ("Texture", 2D) = "white" {}
		[HDR] _CustomColor("Custom Color", Color) = (1,1,1,1)
		_LineWidth ("Line Width", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

		Cull Off
		Lighting Off
		//ZWrite Off
		Fog { Mode Off }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 ray : TEXCOORD1;
				float3 normal : NORMAL;
            };

            struct v2f
            {
				float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
				float2 uv_depth : TEXCOORD1;
				float4 interpolatedRay : TEXCOORD2;
				float3 normal : NORMAL;
            };

			struct fragOut 
			{
				float depth : DEPTH;
			};

            sampler2D _MainTex;
			float4 _CustomColor;
			float _LineWidth;

            float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			sampler2D _CameraDepthTexture;
			sampler2D _CameraDepthNormalsTexture;

			sampler2D _LastCameraDepthTexture;

			float3 _PlayerWorldPosition;
			float _VisibilityRange;
			float4x4 _ViewToWorld;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv_depth = v.uv.xy;

#if UNITY_UV_STARTS_AT_TOP
				if (_MainTex_TexelSize.y < 0)
					o.uv.y = 1 - o.uv.y;
#endif			
				o.interpolatedRay = v.ray;
				o.normal = UnityObjectToWorldNormal(v.normal);

                return o;
            }

			float In(float k)
			{
				if ((k *= 2) < 1) return 0.5*k*k*k;
				return 0.5*((k -= 2)*k*k + 2);
			}

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);

				float rawDepth = DecodeFloatRG(tex2D(_CameraDepthTexture, i.uv));
				float linearDepth = Linear01Depth(rawDepth);
				float4 worldDir = linearDepth * i.interpolatedRay;
				float3 worldPos = _WorldSpaceCameraPos + worldDir;

				fixed4 depthnormal = tex2D(_CameraDepthNormalsTexture, i.uv);
				fixed3 normal;
				float depth;
				DecodeDepthNormal(depthnormal, depth, normal);
				normal = normal = mul((float3x3)_ViewToWorld, normal);

				float dist = distance(_PlayerWorldPosition, worldPos);

				fixed4 additionalColor = 0;

				if (dist < _VisibilityRange && dist > (_VisibilityRange - _LineWidth) && linearDepth < 1)
				{
					if (dot(float3(0, 1, 0), normal) > 0.8f)
					{
						fixed diff = 1 - (_VisibilityRange - dist) / (_LineWidth);
						additionalColor = In(diff) * _CustomColor;
					}
				}

                return col + additionalColor;
            }

            ENDCG
        }
    }
}
