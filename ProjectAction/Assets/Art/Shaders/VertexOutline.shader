﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sihtinen/VertexOutline"
{
    Properties
    {
		_OutlineColor("Outline Color", Color) = (0, 0, 0, 1)
		_OutlineWidth("Outline Width", Range(0.008, 1)) = 0.03
    }
    SubShader
    {
		Tags
		{
			"RenderType" = "Opaque"
		}

        Pass
        {
			Cull Front

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            struct appdata
            {
                float4 vertex : POSITION;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

			half _OutlineWidth;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

				float4 clipPosition = o.vertex;
				float3 clipNormal = mul((float3x3) UNITY_MATRIX_VP, mul((float3x3) UNITY_MATRIX_M, v.normal));

				clipPosition.xyz += normalize(clipNormal) * _OutlineWidth;
				float2 offset = normalize(clipNormal.xy) / _ScreenParams.xy * _OutlineWidth * clipPosition.w * 2;
				clipPosition.xy += offset;

				o.vertex = clipPosition;
				return o;
            }
            
			half4 _OutlineColor;

            fixed4 frag (v2f i) : SV_Target
            {
                return _OutlineColor;
            }

            ENDCG
        }
    }
}
