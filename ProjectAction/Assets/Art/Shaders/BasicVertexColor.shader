﻿Shader "Sihtinen/Basic Vertex Color"
{
  Properties
  {
      _Color ("Main Color", Color) = (1,1,1,1)
      _SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
      _Shininess ("Shininess", Range (0, 1)) = 0.078125
      _Detail ("Detail (RGB) Gloss (A)", 2D) = "gray" {}
  }
 
  SubShader
  {
      Tags { "RenderType"="Opaque" }
      LOD 300
   
    CGPROGRAM
    #pragma surface surf BlinnPhong vertex:vert

		sampler2D _Detail;
		fixed4 _Color;
		fixed _Shininess;
 
		struct Input
		{
			float2 uv_Detail;
			float3 vertColors;
		};
 
		void vert(inout appdata_full v, out Input o)
		{
			o.vertColors= v.color.rgb;
			o.uv_Detail = v.texcoord;
		}
 
		void surf (Input IN, inout SurfaceOutput o)
		{
			half3 c = GammaToLinearSpace(IN.vertColors.rgb) * _Color.rgb;
			c.rgb *= tex2D(_Detail,IN.uv_Detail).rgb*2;
			o.Albedo = c.rgb;
			o.Gloss = tex2D(_Detail,IN.uv_Detail).a;
			o.Specular = _Shininess;
		}
		ENDCG
	}
 
	Fallback "Specular"
}