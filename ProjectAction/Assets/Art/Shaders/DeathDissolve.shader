// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Sihtinen/StandardDissolve"
{
	Properties
	{
		_AlbedoTexture("Albedo Texture", 2D) = "white" {}
		_AlbedoColor("Albedo Color", Color) = (1,1,1,0)
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_Metallic("Metallic", Range( 0 , 1)) = 0
		[HDR]_EmissionColor("Emission Color", Color) = (4,0.5849056,0.5849056,0)
		_SliceAmount("Slice Amount", Range( -20 , 20)) = 0
		_Range("Range", Range( -10 , 10)) = 0
		_TimeScale("Time Scale", Float) = 1
		_Tiling("Tiling", Vector) = (3,7,0,0)
		_PanSpeed("Pan Speed", Vector) = (0,-1,0,0)
		_NoiseScale("Noise Scale", Float) = 1
		_VertexOffsetStrength("Vertex Offset Strength", Float) = 1
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform float _SliceAmount;
		uniform float _Range;
		uniform float _VertexOffsetStrength;
		uniform float2 _Tiling;
		uniform float _TimeScale;
		uniform float2 _PanSpeed;
		uniform float _NoiseScale;
		uniform sampler2D _AlbedoTexture;
		uniform float4 _AlbedoTexture_ST;
		uniform float4 _AlbedoColor;
		uniform float4 _EmissionColor;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 transform76 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float YGradient17 = saturate( ( ( transform76.y + _SliceAmount ) / _Range ) );
			float mulTime6 = _Time.y * _TimeScale;
			float2 panner5 = ( mulTime6 * _PanSpeed + float2( 0,0 ));
			float2 uv_TexCoord1 = v.texcoord.xy * _Tiling + panner5;
			float simplePerlin2D2 = snoise( uv_TexCoord1*_NoiseScale );
			simplePerlin2D2 = simplePerlin2D2*0.5 + 0.5;
			float Noise11 = simplePerlin2D2;
			float3 VertexOffset55 = ( ase_vertex3Pos * YGradient17 * _VertexOffsetStrength * Noise11 );
			v.vertex.xyz += VertexOffset55;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_AlbedoTexture = i.uv_texcoord * _AlbedoTexture_ST.xy + _AlbedoTexture_ST.zw;
			float4 Albedo46 = ( tex2D( _AlbedoTexture, uv_AlbedoTexture ) * _AlbedoColor );
			o.Albedo = Albedo46.rgb;
			float mulTime6 = _Time.y * _TimeScale;
			float2 panner5 = ( mulTime6 * _PanSpeed + float2( 0,0 ));
			float2 uv_TexCoord1 = i.uv_texcoord * _Tiling + panner5;
			float simplePerlin2D2 = snoise( uv_TexCoord1*_NoiseScale );
			simplePerlin2D2 = simplePerlin2D2*0.5 + 0.5;
			float Noise11 = simplePerlin2D2;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform76 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float YGradient17 = saturate( ( ( transform76.y + _SliceAmount ) / _Range ) );
			float4 Emission38 = ( ( Noise11 * YGradient17 ) * _EmissionColor );
			o.Emission = Emission38.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
			float temp_output_28_0 = ( YGradient17 * 1.0 );
			float OpacityMask27 = ( ( ( ( 1.0 - YGradient17 ) * Noise11 ) - temp_output_28_0 ) + ( 1.0 - temp_output_28_0 ) );
			clip( OpacityMask27 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17500
0;557;1499;802;2633.236;1211.327;1.781908;True;True
Node;AmplifyShaderEditor.CommentaryNode;22;-2253.406,-221.4857;Inherit;False;1218.791;429.7613;;8;15;19;21;17;20;16;75;76;Y Gradient;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;13;-2252.051,-840.1706;Inherit;False;1432.711;529;;9;7;8;6;4;5;1;2;10;11;Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.PosVertexDataNode;75;-2198.898,-144.726;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;76;-1994.898,-154.726;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;7;-2202.051,-427.1702;Inherit;False;Property;_TimeScale;Time Scale;7;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-2203.406,86.50685;Inherit;False;Property;_SliceAmount;Slice Amount;5;0;Create;True;0;0;False;0;0;0;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-1720.765,-157.6619;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;8;-2182.051,-611.1705;Inherit;False;Property;_PanSpeed;Pan Speed;9;0;Create;True;0;0;False;0;0,-1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleTimeNode;6;-2044.051,-424.1702;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1884.475,92.2756;Inherit;False;Property;_Range;Range;6;0;Create;True;0;0;False;0;0;0;-10;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;19;-1507.705,-4.549671;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;5;-1825.051,-561.1705;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;4;-1918.051,-727.1706;Inherit;False;Property;_Tiling;Tiling;8;0;Create;True;0;0;False;0;3,7;3,7;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;10;-1522.941,-493.3838;Inherit;False;Property;_NoiseScale;Noise Scale;10;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;-1581.344,-786.6493;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;21;-1388.528,-7.993519;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;32;-2240.927,344.1875;Inherit;False;1548.187;668.7675;;9;24;28;30;31;29;25;26;23;27;Opacity Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;2;-1318.051,-790.1706;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1.55;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-1258.614,-19.23005;Inherit;False;YGradient;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;-1043.34,-762.0707;Inherit;False;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;23;-2190.927,611.1912;Inherit;True;17;YGradient;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;26;-1831.307,610.9758;Inherit;True;11;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;41;-741.7043,-914.4159;Inherit;False;1121.52;611.1041;;6;35;36;37;39;40;38;Emission;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;24;-1917.824,394.1875;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;36;-691.7043,-572.9357;Inherit;True;17;YGradient;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1542.507,402.6755;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;35;-685.6674,-864.4161;Inherit;True;11;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-1885.803,851.2881;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;72;-1267.247,1139.492;Inherit;False;934.3209;714.2039;;6;66;53;64;65;54;55;Vertex Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;47;-2232.704,1116.569;Inherit;False;840.7789;469.6;;4;44;43;45;46;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;39;-319.0687,-515.3116;Inherit;False;Property;_EmissionColor;Emission Color;4;1;[HDR];Create;True;0;0;False;0;4,0.5849056,0.5849056,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-412.6673,-757.4158;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;53;-1193.882,1336.761;Inherit;True;17;YGradient;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;64;-1169.612,1189.492;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;43;-2182.704,1166.569;Inherit;True;Property;_AlbedoTexture;Albedo Texture;0;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;65;-1217.247,1537.527;Inherit;False;Property;_VertexOffsetStrength;Vertex Offset Strength;11;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;30;-1510.591,901.955;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;29;-1296.704,689.588;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;44;-2121.904,1374.169;Inherit;False;Property;_AlbedoColor;Albedo Color;1;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;66;-1197.235,1623.696;Inherit;True;11;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-1088.191,749.955;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-11.10578,-718.9792;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-1838.324,1325.43;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-741.6841,1329.442;Inherit;False;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;55;-556.9261,1325.582;Inherit;True;VertexOffset;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;155.8158,-725.4385;Inherit;True;Emission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;27;-916.741,585.2581;Inherit;True;OpacityMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;46;-1615.924,1279.03;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-79.08658,330.1551;Inherit;False;27;OpacityMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;56;-43.54388,484.1532;Inherit;False;55;VertexOffset;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;12;-269.6486,34.96391;Inherit;False;38;Emission;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;50;-375.6687,215.3934;Inherit;False;Property;_Smoothness;Smoothness;2;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-374.6687,139.3934;Inherit;False;Property;_Metallic;Metallic;3;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;48;-264.1022,-79.62182;Inherit;False;46;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;261.9744,68.4137;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Sihtinen/StandardDissolve;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;12;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;76;0;75;0
WireConnection;15;0;76;2
WireConnection;15;1;16;0
WireConnection;6;0;7;0
WireConnection;19;0;15;0
WireConnection;19;1;20;0
WireConnection;5;2;8;0
WireConnection;5;1;6;0
WireConnection;1;0;4;0
WireConnection;1;1;5;0
WireConnection;21;0;19;0
WireConnection;2;0;1;0
WireConnection;2;1;10;0
WireConnection;17;0;21;0
WireConnection;11;0;2;0
WireConnection;24;0;23;0
WireConnection;25;0;24;0
WireConnection;25;1;26;0
WireConnection;28;0;23;0
WireConnection;37;0;35;0
WireConnection;37;1;36;0
WireConnection;30;0;28;0
WireConnection;29;0;25;0
WireConnection;29;1;28;0
WireConnection;31;0;29;0
WireConnection;31;1;30;0
WireConnection;40;0;37;0
WireConnection;40;1;39;0
WireConnection;45;0;43;0
WireConnection;45;1;44;0
WireConnection;54;0;64;0
WireConnection;54;1;53;0
WireConnection;54;2;65;0
WireConnection;54;3;66;0
WireConnection;55;0;54;0
WireConnection;38;0;40;0
WireConnection;27;0;31;0
WireConnection;46;0;45;0
WireConnection;0;0;48;0
WireConnection;0;2;12;0
WireConnection;0;3;49;0
WireConnection;0;4;50;0
WireConnection;0;10;18;0
WireConnection;0;11;56;0
ASEEND*/
//CHKSM=A5C9F70DC44C1A2A0A006D63CDCF2DBA1DA937BF