﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class PlayerLookDirectionIKController : MonoBehaviour
{
    public bool Enabled = true;

    [SerializeField] private float m_maxWeight = 1.0f;
    [SerializeField] private float m_maxAngle = 90.0f;

    [SerializeField] private float m_lerpSpeed = 5.0f;

    [Header("Object References")]
    [SerializeField] private Transform m_IKTarget;
    [SerializeField] private Transform m_rotationObject;
    [SerializeField] private Transform m_rotationTarget;
    [SerializeField] private Transform m_boneTransform;
    [SerializeField] private Transform m_playerTransform;

    private float m_weight = 0.0f;

    private PlayerAnimationController m_animController;
    private MultiAimConstraint m_multiAimConstraint = null;
    private MultiRotationConstraint m_multiRotationConstraint = null;

    private void Awake()
    {
        m_multiAimConstraint = GetComponent<MultiAimConstraint>();
        m_multiRotationConstraint = GetComponent<MultiRotationConstraint>();

        m_animController = transform.root.GetComponentInChildren<PlayerAnimationController>();
    }

    private void LateUpdate()
    {
        if (Enabled && m_animController.IsLocked() == false)
        {
            float _lookAngle = Vector3.Angle(MainCameraComponent.GetMovementForward(), m_playerTransform.forward);
            if (Mathf.Abs(_lookAngle) > m_maxAngle)
            {
                if (m_IKTarget)
                    m_IKTarget.position = Vector3.Lerp(m_IKTarget.position, m_boneTransform.position + m_playerTransform.forward * 2.0f, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeed);

                m_weight = Mathf.Lerp(m_weight, 0, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeed);

                if (m_rotationObject && m_rotationTarget)
                {
                    m_rotationObject.rotation = Quaternion.Lerp(m_rotationObject.rotation, m_boneTransform.rotation, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeed);
                }
            }
            else
            {
                if (m_IKTarget)
                    m_IKTarget.position = Vector3.Lerp(m_IKTarget.position, m_boneTransform.position + MainCameraComponent.GetMovementForward() * 2, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeed);

                m_weight = Mathf.Lerp(m_weight, m_maxWeight, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeed);

                if (m_rotationObject && m_rotationTarget)
                {
                    m_rotationObject.rotation = Quaternion.Lerp(m_rotationObject.rotation, m_rotationTarget.rotation, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeed);
                }
            }

            if (m_multiAimConstraint) m_multiAimConstraint.weight = m_weight;
            if (m_multiRotationConstraint) m_multiRotationConstraint.weight = m_weight;
        }
        else
        {
            if (m_multiAimConstraint) m_multiAimConstraint.weight = 0.0f;
            if (m_multiRotationConstraint) m_multiRotationConstraint.weight = 0.0f;
        }
    }
}
