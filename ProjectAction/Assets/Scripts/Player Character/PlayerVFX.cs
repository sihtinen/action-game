﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerVFX : MonoBehaviour
{
    [Header("Screen Shake")]
    [SerializeField] private float m_screenShakeAmount = 8;

    [Header("Object References")]
    [SerializeField] private Transform m_footLeft;
    [SerializeField] private Transform m_footRight;
    [SerializeField] private SkinnedMeshRenderer m_renderer;
    [SerializeField] private ParticleSystem m_particlesFootstep;
    [SerializeField] private CinemachineImpulseSource m_cameraImpulse;

    private Rigidbody m_rigid = null;

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody>();
    }

    public void PlayFootstepVFX(int foot)
    {
        if (m_particlesFootstep == null) return;

        switch (foot)
        {
            case -1:
                m_particlesFootstep.transform.position = m_footLeft.position + Vector3.up * 0.1f;
                break;
            case 1:
                m_particlesFootstep.transform.position = m_footRight.position + Vector3.up * 0.1f;
                break;
        }

        m_particlesFootstep.Emit(14);
    }

    public void TriggerScreenShake(float intensity = 1.0f)
    {
        Vector3 _randomImpulse = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));
        _randomImpulse.Normalize();

        m_cameraImpulse.GenerateImpulse(_randomImpulse * m_screenShakeAmount * intensity);
    }
}
