﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CustomPlayerInputManager : MonoBehaviour
{
    public struct ButtonInputStatus
    {
        public ButtonInputStatus(bool pressed, bool pressedDownThisFrame, bool consoleOpen)
        {
            Pressed = pressed;
            PressedDownThisFrame = pressedDownThisFrame;

            if (consoleOpen)
            {
                Pressed = false;
                PressedDownThisFrame = false;
            }
        }

        public bool Pressed;
        public bool PressedDownThisFrame;
    }

    private static CustomPlayerInputManager m_instance = null;
    public static CustomPlayerInputManager GetInstance() { return m_instance; }

    [Header("Input Settings")]
    [SerializeField] private float m_rightStickSensitivity = 20.0f;
    [SerializeField] private AnimationCurve m_rightStickCurve;

    [Header("Object References")]
    [SerializeField] private PlayerInput m_inputInterface;

    private float m_leftAxisVertical, m_leftAxisHorizontal;
    private bool m_resetPlayerButtonPressed;

    private bool m_jumpPressed = false;
    private bool m_jumpPressed_ThisFrame = false;

    private bool m_runPressed = false;
    private bool m_runPressed_ThisFrame = false;

    private bool m_aimPressed = false;
    private bool m_aimPressed_ThisFrame = false;

    private bool m_blockPressed = false;
    private bool m_blockPressed_ThisFrame = false;

    private bool m_attackPressed = false;
    private bool m_attackPressed_ThisFrame = false;

    private bool m_takedownPressed = false;
    private bool m_takedownPressed_ThisFrame = false;

    private bool m_resetPressed = false;
    private bool m_resetPressed_ThisFrame = false;

    private Vector2 m_mouseInput;

    private Vector2 m_leftAnalogue;
    private Vector2 m_rightAnalogue;

    private GameTime m_gameTime = null;

    private void Awake()
    {
        m_instance = this;

        m_inputInterface.actions.FindAction("Jump").started += jumpPressed;
        m_inputInterface.actions.FindAction("Jump").performed += jumpPressed;
        m_inputInterface.actions.FindAction("Jump").canceled += jumpReleased;

        m_inputInterface.actions.FindAction("Run").started += runPressed;
        m_inputInterface.actions.FindAction("Run").performed += runPressed;
        m_inputInterface.actions.FindAction("Run").canceled += runReleased;

        m_inputInterface.actions.FindAction("Aim").started += aimPressed;
        m_inputInterface.actions.FindAction("Aim").performed += aimPressed;
        m_inputInterface.actions.FindAction("Aim").canceled += aimReleased;

        m_inputInterface.actions.FindAction("Block").started += blockPressed;
        m_inputInterface.actions.FindAction("Block").performed += blockPressed;
        m_inputInterface.actions.FindAction("Block").canceled += blockReleased;

        m_inputInterface.actions.FindAction("Attack").started += attackPressed;
        m_inputInterface.actions.FindAction("Attack").performed += attackPressed;
        m_inputInterface.actions.FindAction("Attack").canceled += attackReleased;

        m_inputInterface.actions.FindAction("Takedown").started += takedownPressed;
        m_inputInterface.actions.FindAction("Takedown").performed += takedownPressed;
        m_inputInterface.actions.FindAction("Takedown").canceled += takedownReleased;

        m_inputInterface.actions.FindAction("Reset").started += resetPressed;
        m_inputInterface.actions.FindAction("Reset").performed += resetPressed;
        m_inputInterface.actions.FindAction("Reset").canceled += resetReleased;

        m_inputInterface.actions.FindAction("Controller Left Stick").performed += setLeftStickAnalogue;
        m_inputInterface.actions.FindAction("Controller Right Stick").performed += setRightStickAnalogue;
    }

    private void Start()
    {
        m_gameTime = GameTime.GetInstance();
    }

    private void Update()
    {
        m_mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        if (ResetInputStatus().PressedDownThisFrame)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void LateUpdate()
    {
        m_jumpPressed_ThisFrame = false;
        m_runPressed_ThisFrame = false;
        m_aimPressed_ThisFrame = false;
        m_blockPressed_ThisFrame = false;
        m_attackPressed_ThisFrame = false;
        m_takedownPressed_ThisFrame = false;
        m_resetPressed_ThisFrame = false;
    }

    public Vector2 GetMouseInput()
    {
        return m_mouseInput;
    }

    public Vector2 GetLeftAnalogue()
    {
        return m_leftAnalogue;
    }

    public Vector2 GetRightAnalogue()
    {
        Vector2 _result = m_rightStickCurve.Evaluate(m_rightAnalogue.magnitude) * m_rightAnalogue * m_rightStickSensitivity;
        return _result;
    }

    public bool ResetPlayerButtonPressed()
    {
        return m_resetPlayerButtonPressed;
    }

    private void setLeftStickAnalogue(InputAction.CallbackContext context)
    {
        m_leftAnalogue = context.ReadValue<Vector2>();
        if (context.canceled) m_leftAnalogue = Vector2.zero;
    }

    private void setRightStickAnalogue(InputAction.CallbackContext context)
    {
        m_rightAnalogue = context.ReadValue<Vector2>();
        m_rightAnalogue.y *= -1;
        if (context.canceled) m_rightAnalogue = Vector2.zero;
    }

    public ButtonInputStatus JumpInputStatus()
    {
        return new ButtonInputStatus(m_jumpPressed, m_jumpPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    public ButtonInputStatus RunInputStatus()
    {
        return new ButtonInputStatus(m_runPressed, m_runPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    public ButtonInputStatus AimInputStatus()
    {
        return new ButtonInputStatus(m_aimPressed, m_aimPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    public ButtonInputStatus BlockInputStatus()
    {
        return new ButtonInputStatus(m_blockPressed, m_blockPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    public ButtonInputStatus AttackInputStatus()
    {
        return new ButtonInputStatus(m_attackPressed, m_attackPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    public ButtonInputStatus TakedownInputStatus()
    {
        return new ButtonInputStatus(m_takedownPressed, m_takedownPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    public ButtonInputStatus ResetInputStatus()
    {
        return new ButtonInputStatus(m_resetPressed, m_resetPressed_ThisFrame, m_gameTime.IsConsoleOpen());
    }

    private void jumpPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_jumpPressed_ThisFrame = true;
        m_jumpPressed = true;
    }

    private void jumpReleased(InputAction.CallbackContext context)
    {
        m_jumpPressed = false;
    }

    private void runPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_runPressed_ThisFrame = true;
        m_runPressed = true;
    }

    private void runReleased(InputAction.CallbackContext context)
    {
        m_runPressed = false;
    }

    private void aimPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_aimPressed_ThisFrame = true;
        m_aimPressed = true;
    }

    private void aimReleased(InputAction.CallbackContext context)
    {
        m_aimPressed = false;
    }

    private void blockPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_blockPressed_ThisFrame = true;
        m_blockPressed = true;
    }

    private void blockReleased(InputAction.CallbackContext context)
    {
        m_blockPressed = false;
    }

    private void attackPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_attackPressed_ThisFrame = true;
        m_attackPressed = true;
    }

    private void attackReleased(InputAction.CallbackContext context)
    {
        m_attackPressed = false;
    }

    private void takedownPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_takedownPressed_ThisFrame = true;
        m_takedownPressed = true;
    }

    private void takedownReleased(InputAction.CallbackContext context)
    {
        m_takedownPressed = false;
    }

    private void resetPressed(InputAction.CallbackContext context)
    {
        if (context.started) m_resetPressed_ThisFrame = true;
        m_resetPressed = true;
    }

    private void resetReleased(InputAction.CallbackContext context)
    {
        m_resetPressed = false;
    }
}
