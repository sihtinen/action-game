﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class PlayerAimIKController : MonoBehaviour
{
    public bool Enabled = false;

    [SerializeField] private Rig m_myRig;

    [Space]

    [SerializeField] private float m_pistolPivotDistance = 0.4f;
    [SerializeField] private float m_pistolHeightOffset = 0.0f;

    [Header("Object References")]
    [SerializeField] private Transform m_bone_Pelvis;
    [SerializeField] private Transform m_bone_UpperChest;
    [SerializeField] private Transform m_bone_HandR;
    [SerializeField] private Transform m_bone_ElbowR;

    [Space]

    [SerializeField] private Transform m_aimTarget;
    [SerializeField] private Transform m_pistolPivot;

    [Space]

    [SerializeField] private Transform m_IKTarget_RightHand;
    [SerializeField] private Transform m_IKTarget_LeftHand;

    [Space]

    [SerializeField] private Vector3 m_posOffset_IKTargetRightHand;
    [SerializeField] private Vector3 m_rotOffset_IKTargetRightHand;
    [SerializeField] private Vector3 m_posOffset_IKTargetLeftHand;
    [SerializeField] private Vector3 m_rotOffset_IKTargetLeftHand;

    public void SetAimTargetPosition(Vector3 position)
    {
        m_aimTarget.position = position;
    }

    private void LateUpdate()
    {
        if (Enabled == false)
        {
            m_myRig.weight = 0.0f;
            return;
        }

        m_myRig.weight = 1.0f;

        Vector3 _directionToAimTarget = m_aimTarget.position - m_pistolPivot.position;
        _directionToAimTarget.Normalize();

        float _distanceModifier = Mathf.Abs(MainCameraComponent.NormalizedAimAngleHorizontal) + 0.3f;
        float _pivotDistance = m_pistolPivotDistance * _distanceModifier;

        Vector3 _pivotPosition = m_bone_UpperChest.position;
        _pivotPosition.y = m_bone_Pelvis.position.y;

        float _rightAmount = 0.2f;
        if (MainCameraComponent.NormalizedAimAngleHorizontal > 0.2f)
        {
            _rightAmount -= (MainCameraComponent.NormalizedAimAngleHorizontal * 0.32f);
        }

        m_pistolPivot.position = _pivotPosition
            + Vector3.up * m_pistolHeightOffset
            + transform.up * MainCameraComponent.NormalizedAimAngleVertical * 0.3f
            + _directionToAimTarget * (0.7f + (-0.5f + _pivotDistance))
            + MainCameraComponent.Right * _rightAmount;

        m_IKTarget_RightHand.rotation = m_pistolPivot.rotation;
        m_IKTarget_RightHand.position = m_pistolPivot.position;

        m_IKTarget_LeftHand.position = m_bone_HandR.position;
        m_IKTarget_LeftHand.rotation = m_bone_ElbowR.rotation;

        m_IKTarget_RightHand.position += m_IKTarget_RightHand.right * m_posOffset_IKTargetRightHand.x;
        m_IKTarget_RightHand.position += m_IKTarget_RightHand.up * m_posOffset_IKTargetRightHand.y;
        m_IKTarget_RightHand.position += m_IKTarget_RightHand.forward * m_posOffset_IKTargetRightHand.z;
        m_IKTarget_RightHand.eulerAngles += m_rotOffset_IKTargetRightHand;

        m_IKTarget_LeftHand.position += m_IKTarget_LeftHand.right * m_posOffset_IKTargetLeftHand.x;
        m_IKTarget_LeftHand.position += m_IKTarget_LeftHand.up * m_posOffset_IKTargetLeftHand.y;
        m_IKTarget_LeftHand.position += m_IKTarget_LeftHand.forward * m_posOffset_IKTargetLeftHand.z;
        m_IKTarget_LeftHand.eulerAngles += m_rotOffset_IKTargetLeftHand;
    }
}
