﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimManager : MonoBehaviour
{
    [System.Serializable]
    public class RecoilImpulse
    {
        public float ImpulseTime = 0.5f;
        public float MinVertical = 2.0f, MaxVertical = 4.0f;
        public float MinHorizontal = 0.4f, MaxHorizontal = 2.3f;
        public float RestoreSpeed = 1.3f;

        public AnimationCurve RecoilCurve;
    }

    [SerializeField] private LayerMask m_aimLayers;

    [Header("Object References")]
    [SerializeField] private RectTransform m_crosshair;
    [SerializeField] private PlayerAnimationController m_animController;

    private Camera m_camera = null;
    private PlayerMovementManager m_movementManager = null;
    private CustomPlayerInputManager m_inputManager = null;
    private PlayerParryBlockManager m_blockManager = null;

    private bool m_aiming = false;
    private Vector3 m_aimPosition = Vector3.zero;
    private RaycastHit[] m_raycastHits = new RaycastHit[100];

    private void Start()
    {
        m_camera = MainCameraComponent.MainCamera;
        m_movementManager = GetComponent<PlayerMovementManager>();
        m_inputManager = GetComponent<CustomPlayerInputManager>();
        m_blockManager = GetComponent<PlayerParryBlockManager>();
    }

    private void Update()
    {
        Ray _ray = m_camera.ScreenPointToRay(m_crosshair.position);
        int _hits = Physics.RaycastNonAlloc(
            _ray,
            m_raycastHits,
            200,
            m_aimLayers,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);
            m_aimPosition = _closest.point;
        }
        else
        {
            m_aimPosition = m_camera.transform.position + _ray.direction * 50;
        }

        if (m_animController.IsLocked() || m_blockManager.IsBlocking())
        {
            m_aiming = false;
            return;
        }

        if (m_inputManager.AimInputStatus().PressedDownThisFrame)
        {
            if (m_movementManager.IsRunning())
            {
                m_movementManager.SetRunning(false);
            }
        }

        if (m_movementManager.IsRunning())
        {
            if (m_inputManager.AimInputStatus().Pressed)
            {
                m_aiming = false;
                return;
            }
        }

        m_aiming = m_inputManager.AimInputStatus().Pressed;
    }

    private RaycastHit getClosestHit(int hits)
    {
        RaycastHit _result = m_raycastHits[0];

        for (int i = 0; i < hits; i++)
        {
            RaycastHit _current = m_raycastHits[i];
            if (_current.collider.gameObject == gameObject || _current.point == Vector3.zero)
            {
                continue;
            }

            Vector3 _dirFromPlayer = _current.point - transform.position;
            float _dirDot = Vector3.Dot(_dirFromPlayer, m_camera.transform.forward);
            if (_dirDot < 0)
            {
                continue;
            }

            if (_current.distance < _result.distance)
            {
                _result = _current;
            }
        }

        return _result;
    }

    public bool IsAiming()
    {
        return m_aiming;
    }

    public void ForceStopAiming()
    {
        m_aiming = false;    
    }

    public Vector3 GetAimPosition()
    {
        return m_aimPosition;
    }
}
