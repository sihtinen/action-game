﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementManager : MonoBehaviour
{
    public struct WallRunInfo
    {
        public WallRunInfo(bool wallRunning, int wallRunSide, float wallAngle, Vector3 pointOnWall, Vector3 wallNormal, float wallRunTime)
        {
            WallRunning = wallRunning;
            WallRunSide = wallRunSide;
            WallAngle = wallAngle;
            PointOnWall = pointOnWall;
            WallNormal = wallNormal;
            WallRunTime = wallRunTime;
        }

        public bool WallRunning;
        public int WallRunSide;
        public float WallAngle;
        public Vector3 PointOnWall;
        public Vector3 WallNormal;
        public float WallRunTime;
    }

    private struct JumpStartParameters
    {
        public enum JumpContext
        {
            Ground,
            Wall,
            LedgeClimb,
            LedgeClimbBoosted
        }

        public JumpStartParameters(JumpContext context)
        {
            Context = context;
        }

        public JumpContext Context;
    }

    [SerializeField] private LayerMask m_movementLayer;

    [Header("Sneak Settings")]
    [SerializeField] private float m_maxSneakVelocity = 5.5f;
    [SerializeField] private float m_sneakAcceleration = 150.0f;
    [SerializeField] private float m_sneakAccelerationInAir = 140.0f;

    [Header("Run Settings")]
    [SerializeField] private float m_maxRunVelocity = 9.5f;
    [SerializeField] private float m_runAcceleration = 120.0f;
    [SerializeField] private float m_runAccelerationInAir = 120.0f;

    [Space]

    [SerializeField] private float m_runDecelerationToStationaryFromGround = 320.0f;
    [SerializeField] private float m_runDecelerationToRunFromGround = 320.0f;
    [SerializeField] private float m_runDecelerationToStationaryFromAir = 320.0f;
    [SerializeField] private float m_runDecelerationToRunFromAir = 320.0f;

    [Header("Jump Settings")]
    [SerializeField] private float m_jumpForce_Ground = 770;
    [SerializeField] private float m_jumpForce_Wall = 600;
    [SerializeField] private float m_jumpForce_LedgeBoost = 900;
    [SerializeField] private float m_jumpHoldTime = 0.5f;
    [SerializeField] private float m_jumpEndTime = 0.8f;
    [SerializeField, Range(0, 1)] private float m_jumpInputDirectionSensitivity = 0.6f;
    [SerializeField] private AnimationCurve m_curveJumpGravity;

    [Space]

    [SerializeField] private float m_addedHorizontalVelocityModifierMin = 3.0f;
    [SerializeField] private float m_addedHorizontalVelocityModifierMax = 3.0f;
    [SerializeField] private float m_addedHorizontalVelocityRangeMin = 3.0f;
    [SerializeField] private float m_addedHorizontalVelocityRangeMax = 15.0f;

    [Space]

    [SerializeField, Range(0, 90)] private float m_minJumpFromWallAngle = 30.0f;
    [SerializeField, Range(0, 90)] private float m_maxJumpFromWallAngle = 70.0f;
    [SerializeField] private float m_wallAddedForceTowardsHorizontalNormal = 300f;

    [Header("Vertical Wall Contact Settings")]
    [SerializeField] private float m_minimumJumpTimeBeforeVerticalWallContact = 0.18f;

    [Header("Wall Run Settings")]
    [SerializeField] private bool m_enableWallRun = true;
    [SerializeField] private float m_minimumJumpTimeBeforeWallRun = 0.3f;
    [SerializeField] private float m_minimumWallRunHorizontalVelocity = 4.0f;

    [Header("Input Responsiveness")]
    [SerializeField] private float m_inputResponsivenessGround_Sneak = 1.2f;
    [SerializeField] private float m_inputResponsivenessGround_Run = 0.8f;
    [SerializeField] private float m_inputResponsivenessAir = 0.45f;

    [Header("Gravity Settings")]
    [SerializeField] private float m_gravityScale_Normal = 1.0f;
    [SerializeField] private float m_gravityScale_JumpCancelled = 1.0f;
    [SerializeField] private float m_gravityScale_VerticalWallContact = 1.0f;
    [SerializeField] private float m_gravityScale_WallRun = 1.0f;

    [Header("Ground Check Settings")]
    [SerializeField] private float m_groundCheckRadiusOffset = -0.2f;
    [SerializeField] private float m_groundCheckDistanceOffset = 0.1f;

    [Header("Vertical Wall Check Settings")]
    [SerializeField] private float m_wallRunCheckRadius = 0.2f;
    [SerializeField] private float m_wallRunCheckDistance = 0.7f;
    [SerializeField, Range(0, 1)] private float m_wallRunDirectionDotProductLimit = 0.3f;
    [SerializeField] private float m_wallRunHorizontalVelocityAngleLimit = 30f;

    [Header("Climb Check Settings")]
    [SerializeField] private float m_ledgeCheckAdditionalHeight = 0.4f;
    [SerializeField] private float m_ledgeCheckAddedRadius = -0.1f;
    [SerializeField] private float m_ledgeBoostJumpLimit = 0.85f;

    [Header("Hearable Events")]
    [SerializeField] private HearableEvent m_hearableEvent_Sneak;
    [SerializeField] private HearableEvent m_hearableEvent_Run;
    [SerializeField] private HearableEvent m_hearableEvent_OnLanding;

    [Header("Object References")]
    [SerializeField] private CameraFollowTarget m_cameraFollowTarget;

    private Rigidbody m_rigid = null;
    private CapsuleCollider m_capsuleCollider = null;
    private CustomPlayerInputManager m_inputManager = null;
    private PlayerAnimationController m_animationController = null;
    private PlayerParryBlockManager m_blockManager = null;
    private HearableEventManager m_hearableManager = null;

    private RaycastHit[] m_raycastHits = new RaycastHit[100];

    private float m_currentJumpTime = 0.0f;
    private float m_currentWallRunTime = 0.0f;
    private float m_jumpStartedTimeStamp = 0.0f;
    private float m_previousGroundHeight;

    private bool m_locked = false;

    private bool m_hasGround = false;
    [HideInInspector] public bool JumpedFromGround = false;

    private bool m_running = false;
    private bool m_runInputDownThisFrame = false;

    private bool m_jumping = false;
    private bool m_jumpStartedThisFrame = false;
    private bool m_jumpInputDownThisFrame = false;
    private bool m_jumpPressed = false;

    private bool m_verticalWallContact = false;

    private bool m_wallRunning = false;
    private Vector3 m_wallRunningTargetPoint = Vector3.zero;
    private Vector3 m_wallRunningHorizontalDirection = Vector3.zero;
    private Vector3 m_wallNormal = Vector3.zero;
    private int m_wallRunningSide = 0;

    private Coroutine m_routineLedgeClimb = null;

    private List<ContactPoint> m_collisionContacts = new List<ContactPoint>();

    private void Awake()
    {
        m_rigid = GetComponent<Rigidbody>();
        m_capsuleCollider = GetComponent<CapsuleCollider>();
        m_inputManager = GetComponent<CustomPlayerInputManager>();
        m_animationController = GetComponentInChildren<PlayerAnimationController>();
        m_blockManager = GetComponent<PlayerParryBlockManager>();

        GameState.GetInstance().PlayerObject = gameObject;
    }

    private void Start()
    {
        m_hearableManager = HearableEventManager.GetInstance();
    }

    private void Update()
    {
        if (m_locked) return;
        if (m_routineLedgeClimb != null) return;

        if (m_inputManager.JumpInputStatus().PressedDownThisFrame)
        {
            m_jumpInputDownThisFrame = true;
        }
        if (m_inputManager.RunInputStatus().PressedDownThisFrame)
        {
            m_runInputDownThisFrame = true;
        }

        m_jumpPressed = m_inputManager.JumpInputStatus().Pressed;

        groundCheck();

        if (m_blockManager.IsBlocking() == false)
        {
            climbableLedgeCheck();
            if (m_enableWallRun) wallRunTargetCheck();
            verticalWallContactCheck();
        }
    }

    private void FixedUpdate()
    {
        if (m_locked || m_animationController.IsLocked()) return;
        if (m_routineLedgeClimb != null) return;

        if (m_wallRunning)
        {
            updateWallRunVelocity();
            return;
        }

        if (m_verticalWallContact)
        {
            updateVerticalWallContact();
            return;
        }

        updateVerticalVelocity();
        updateHorizontalVelocity();
    }

    private void groundCheck()
    {
        Vector3 _rayStartPosition = m_rigid.position + Vector3.up * m_capsuleCollider.height;
        float _radius = m_capsuleCollider.radius + m_groundCheckRadiusOffset;

        float _additionalSlopeDistance = 0.0f;
        if (m_hasGround && m_jumping == false) _additionalSlopeDistance = 0.5f;

        float _verticalVelocity = GetVerticalVelocity();

        int _hits = Physics.SphereCastNonAlloc(
            _rayStartPosition,
            _radius,
            Vector3.down,
            m_raycastHits,
            m_capsuleCollider.height + m_groundCheckDistanceOffset + _additionalSlopeDistance,
            m_movementLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);
            Vector3 _horizontalVelocity = m_rigid.velocity;
            _horizontalVelocity.y = 0;
            float _groundAngle = Vector3.Angle(m_animationController.transform.forward, _closest.normal) - 90.0f;

            if (Mathf.Abs(_groundAngle) <= 45.0f)
            {
                if (m_jumping == false)
                {
                    if (Mathf.Abs(m_rigid.velocity.x) > 0.01f || Mathf.Abs(m_rigid.velocity.z) > 0.01f)
                    {
                        m_rigid.velocity = Vector3.ProjectOnPlane(m_rigid.velocity, _closest.normal);
                        Vector3 _targetPos = m_rigid.position;
                        _targetPos.y = _closest.point.y;
                        m_rigid.MovePosition(_targetPos);
                    }
                }

                if (m_jumping == false || m_rigid.position.y < _closest.point.y)
                {
                    m_previousGroundHeight = m_rigid.position.y;
                    m_jumpStartedTimeStamp = Time.time;

                    JumpedFromGround = false;

                    if (m_hasGround == false)
                    {
                        m_animationController.OnLandedToGround(_verticalVelocity);

                        m_hearableEvent_OnLanding.Origin = m_rigid.position;
                        m_hearableManager.TriggerHearableEvent(m_hearableEvent_OnLanding);
                    }
                }

                m_hasGround = true;
                m_verticalWallContact = false;
            }
            else
            {
                m_hasGround = false;
            }
        }
        else
        {
            m_hasGround = false;
        }
    }

    private void verticalWallContactCheck()
    {
        if (m_hasGround || m_wallRunning) return;
        if (m_jumping && m_currentJumpTime < m_minimumJumpTimeBeforeVerticalWallContact) return;

        Vector3 _direction = m_rigid.velocity;
        _direction.y = 0;

        if (m_verticalWallContact)
        {
            _direction = m_wallNormal * -1;
        }
        else
        {
            Vector2 _input = roundInputCorrection(m_inputManager.GetLeftAnalogue());
            if (_input != Vector2.zero)
            {
                Vector3 _inputDirection =
                    MainCameraComponent.GetMovementRight() * _input.x +
                    MainCameraComponent.GetMovementForward() * _input.y;

                _direction = Vector3.Lerp(_direction, _inputDirection, 0.5f);
            }
        }

        _direction.y = 0;
        _direction.Normalize();

        Vector3 _wallCheckOrigin = m_rigid.position + Vector3.up * (m_capsuleCollider.height * 0.5f);

        int _hits = Physics.SphereCastNonAlloc(
            _wallCheckOrigin,
            m_wallRunCheckRadius,
            _direction,
            m_raycastHits,
            m_wallRunCheckDistance,
            m_movementLayer,
            QueryTriggerInteraction.Ignore
            );

        if (_hits == 0)
        {
            m_verticalWallContact = false;
            return;
        }

        Vector3 _horizontalVelocity = m_rigid.velocity;
        _horizontalVelocity.y = 0;
        _horizontalVelocity.Normalize();

        RaycastHit _closest = getClosestHit(_hits);
        m_wallNormal = _closest.normal;

        Vector3 _horizontalWallNormal = _closest.normal;
        _horizontalWallNormal.y = 0;
        _horizontalWallNormal.Normalize();

        float _angleToWallDotProduct = Vector3.Dot(_direction, -_horizontalWallNormal);
        if (Mathf.Abs(_angleToWallDotProduct) > m_wallRunDirectionDotProductLimit)
        {
            // Facing wall -> vertical wall climb / slide
        }
        else
        {
            m_verticalWallContact = false;
            return;
        }

        if (m_verticalWallContact == false)
        {
            Vector3 _newHorizontalVelocity = m_rigid.velocity;
            _newHorizontalVelocity.y = 0;
            _newHorizontalVelocity *= 0.13f;
            _newHorizontalVelocity.y = m_rigid.velocity.y;
            m_rigid.velocity = _newHorizontalVelocity;

            m_rigid.position = _closest.point + m_wallNormal * (m_capsuleCollider.radius + 0.01f);
        }

        m_previousGroundHeight = m_rigid.position.y;
        m_verticalWallContact = true;
    }

    private void wallRunTargetCheck()
    {
        bool _previousWallRunningState = m_wallRunning;

        if (m_hasGround)
        {
            m_wallRunning = false;
            m_currentWallRunTime = 0.0f;
            return;
        }
        else
        {
            if (m_wallRunning == false && (Time.time - m_jumpStartedTimeStamp) < m_minimumJumpTimeBeforeWallRun)
            {
                m_currentWallRunTime = 0.0f;
                return;
            }
        }

        if (m_jumping)
        {
            if (m_currentJumpTime < m_minimumJumpTimeBeforeWallRun)
            {
                m_wallRunning = false;
                m_currentWallRunTime = 0.0f;
                return;
            }
        }

        bool _targetFound = false;

        float _verticalDotProduct = 0.0f;
        float _horizontalAngle = 0.0f;

        Vector3 _horizontalVelocityDirection = m_rigid.velocity;
        _horizontalVelocityDirection.y = 0;

        if (m_wallRunning == false && _horizontalVelocityDirection.magnitude < m_minimumWallRunHorizontalVelocity)
        {
            m_currentWallRunTime = 0.0f;
            return;
        }

        _horizontalVelocityDirection.Normalize();

        Vector3 _velocityLeft = Vector3.Cross(_horizontalVelocityDirection, Vector3.up);
        Vector3 _wallCheckOrigin = m_rigid.position + Vector3.up * m_wallRunCheckRadius * 0.5f;

        int _hits = Physics.SphereCastNonAlloc(
            _wallCheckOrigin,
            m_wallRunCheckRadius,
            _velocityLeft,
            m_raycastHits,
            m_wallRunCheckDistance,
            m_movementLayer,
            QueryTriggerInteraction.Ignore
            );

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);

            bool _continue = true;

            Vector3 _diff = _closest.point - m_rigid.position;
            _hits = Physics.RaycastNonAlloc(
                m_rigid.position,
                _diff.normalized,
                m_raycastHits,
                _diff.magnitude - 0.01f,
                m_movementLayer,
                QueryTriggerInteraction.Ignore
                );

            if (_hits > 0)
            {
                _continue = false;
            }

            if (_continue && _closest.point != Vector3.zero)
            {
                _verticalDotProduct = Vector3.Dot(_horizontalVelocityDirection, _closest.normal);

                if (Mathf.Abs(_verticalDotProduct) <= m_wallRunDirectionDotProductLimit)
                {
                    Vector3 _temp = Vector3.Cross(_closest.normal, _horizontalVelocityDirection);
                    Vector3 _directionAlongWall = Vector3.Cross(_temp, _closest.normal);

                    _horizontalAngle = Vector3.Angle(_horizontalVelocityDirection, _directionAlongWall);

                    if (_horizontalAngle < m_wallRunHorizontalVelocityAngleLimit)
                    {
                        m_wallRunning = true;

                        m_wallRunningTargetPoint = _closest.point;
                        m_wallRunningHorizontalDirection = _directionAlongWall;
                        m_wallRunningSide = -1;
                        m_wallNormal = _closest.normal;
                        _targetFound = true;
                    }
                }
            }
        }

        _hits = Physics.SphereCastNonAlloc(
            m_rigid.position + Vector3.up * m_wallRunCheckRadius * 0.5f,
            m_wallRunCheckRadius,
            -_velocityLeft,
            m_raycastHits,
            m_wallRunCheckDistance,
            m_movementLayer,
            QueryTriggerInteraction.Ignore
            );

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);

            bool _continue = true;

            Vector3 _diff = _closest.point - m_rigid.position;
            _hits = Physics.RaycastNonAlloc(
                m_rigid.position,
                _diff.normalized,
                m_raycastHits,
                _diff.magnitude - 0.01f,
                m_movementLayer,
                QueryTriggerInteraction.Ignore
                );

            if (_hits > 0)
            {
                _continue = false;
            }

            if (_continue && _closest.point != Vector3.zero)
            {
                _verticalDotProduct = Vector3.Dot(_horizontalVelocityDirection, _closest.normal);

                if (Mathf.Abs(_verticalDotProduct) <= m_wallRunDirectionDotProductLimit)
                {
                    Vector3 _temp = Vector3.Cross(_closest.normal, _horizontalVelocityDirection);
                    Vector3 _directionAlongWall = Vector3.Cross(_temp, _closest.normal);

                    float _horizontalAngle1 = Vector3.Angle(_horizontalVelocityDirection, _directionAlongWall);

                    if (_targetFound)
                    {
                        if (_horizontalAngle1 > _horizontalAngle)
                        {
                            return;
                        }
                    }

                    _horizontalAngle = _horizontalAngle1;

                    if (_horizontalAngle < m_wallRunHorizontalVelocityAngleLimit)
                    {
                        m_wallRunning = true;

                        m_wallRunningTargetPoint = _closest.point;
                        m_wallRunningHorizontalDirection = _directionAlongWall;
                        m_wallRunningSide = 1;
                        m_wallNormal = _closest.normal;
                        _targetFound = true;
                    }
                }
            }
        }

        if (m_wallRunning)
        {
            if (m_wallRunningTargetPoint == Vector3.zero)
            {
                m_wallRunning = false;
            }
        }

        if (_targetFound == false)
        {
            m_wallRunning = false;
            m_currentWallRunTime = 0.0f;
        }
        else
        {
            if (m_wallRunning == true && _previousWallRunningState == false)
            {
                m_previousGroundHeight = m_wallRunningTargetPoint.y;

                Vector3 _velocity = m_rigid.velocity;
                _velocity.y *= 0.82f;

                _velocity *= 1.03f;

                if (_velocity.y < 2.5f)
                {
                    _velocity.y = 2.5f;
                }

                m_rigid.velocity = _velocity;
            }
        }
    }

    private void climbableLedgeCheck()
    {
        if (m_hasGround) return;

        int _hits = Physics.CapsuleCastNonAlloc(
            m_rigid.position, 
            m_rigid.position + Vector3.up * m_capsuleCollider.height,
            m_capsuleCollider.radius + m_ledgeCheckAddedRadius,
            Vector3.up,
            m_raycastHits,
            m_capsuleCollider.height + m_ledgeCheckAdditionalHeight,
            m_movementLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0) return;

        Vector3 _forwardAboveStart = m_rigid.position + Vector3.up * m_capsuleCollider.height + Vector3.up * m_ledgeCheckAdditionalHeight;
        Vector3 _horizontalVelocity = m_rigid.velocity;
        _horizontalVelocity.y = 0;

        Vector3 _checkDirection = _horizontalVelocity.normalized;

        if (m_verticalWallContact)
        {
            _checkDirection = m_wallNormal * -1;
        }
        else
        {
            Vector2 _input = roundInputCorrection(m_inputManager.GetLeftAnalogue());
            if (_input != Vector2.zero)
            {
                Vector3 _inputDirection =
                    MainCameraComponent.GetMovementRight() * _input.x +
                    MainCameraComponent.GetMovementForward() * _input.y;
                _inputDirection.y = 0.0f;
                _inputDirection.Normalize();
                _checkDirection = _inputDirection;
            }
            else
            {
                _checkDirection = transform.GetChild(0).forward;
                _checkDirection.y = 0;
                _checkDirection.Normalize();
            }
        }

        _hits = Physics.CapsuleCastNonAlloc(
            _forwardAboveStart,
            _forwardAboveStart + Vector3.up * m_capsuleCollider.height,
            m_capsuleCollider.radius + m_ledgeCheckAddedRadius,
            _checkDirection,
            m_raycastHits,
            m_capsuleCollider.radius * 2,
            m_movementLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0) return;

        _forwardAboveStart += _checkDirection * m_capsuleCollider.radius * 2;

        _hits = Physics.CapsuleCastNonAlloc(
            _forwardAboveStart,
            _forwardAboveStart + Vector3.up * m_capsuleCollider.height,
            m_capsuleCollider.radius + m_ledgeCheckAddedRadius,
            Vector3.down,
            m_raycastHits,
            m_capsuleCollider.height - 0.3f + m_ledgeCheckAdditionalHeight,
            m_movementLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits == 0) return;

        RaycastHit _closest = getClosestHit(_hits);
        if (_closest.point == Vector3.zero) return;

        float _angleToUp = Vector3.Angle(_closest.normal, Vector3.up);
        if (Mathf.Abs(_angleToUp) < 45.0f)
        {
            float _heightDifference = _closest.point.y - m_previousGroundHeight;

            if (m_jumping && _heightDifference < m_ledgeBoostJumpLimit)
            {
                m_routineLedgeClimb = StartCoroutine(routine_LedgeJumpBoost(_closest.point));
            }
            else
            {
                m_routineLedgeClimb = StartCoroutine(routine_ClimbToLedge(_closest.point));
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (m_wallRunning)
        {
            Vector3 _wallCheckOrigin = m_rigid.position + Vector3.up * m_wallRunCheckRadius;
            Vector3 _toRigidFromWall = _wallCheckOrigin - m_wallRunningTargetPoint;

            Gizmos.color = Color.yellow;

            Gizmos.DrawLine(_wallCheckOrigin, m_wallRunningTargetPoint);
            Gizmos.DrawWireSphere(m_wallRunningTargetPoint + _toRigidFromWall.normalized * m_wallRunCheckRadius, m_wallRunCheckRadius);
        }
    }

    private void updateVerticalVelocity()
    {
        float _gravityScale = m_gravityScale_Normal;

        if (m_jumpInputDownThisFrame && m_jumping == false)
        {
            if (m_hasGround)
            {
                startJump(new JumpStartParameters(JumpStartParameters.JumpContext.Ground));
            }
        }      
        else if (m_jumping)
        {
            m_currentJumpTime += Time.fixedDeltaTime * GameTime.GameTimeScale;

            if (m_currentJumpTime < m_jumpEndTime)
            {
                if (m_jumpPressed == false && m_currentJumpTime < m_jumpHoldTime)
                {
                    _gravityScale = m_gravityScale_JumpCancelled;
                }
                else
                {
                    float _normalizedJumpTime = m_currentJumpTime / m_jumpEndTime;
                    _gravityScale = Mathf.Lerp(0, 1, m_curveJumpGravity.Evaluate(_normalizedJumpTime)) * m_gravityScale_Normal;
                }
            }
            else
            {
                m_jumping = false;
            }

            if (m_currentJumpTime > Time.fixedDeltaTime * GameTime.GameTimeScale && m_hasGround)
            {
                m_jumping = false;
            }
        }

        if (m_hasGround == false)
        {
            m_rigid.AddForce(Physics.gravity * _gravityScale, ForceMode.Acceleration);
        }

        m_jumpInputDownThisFrame = false;
    }

    private void updateHorizontalVelocity()
    {
        if (m_runInputDownThisFrame)
        {
            if (m_running == false && m_blockManager.IsBlocking() == false)
            {
                if (m_hasGround == true && m_jumping == false && GetNormalizedMovementSpeed() > 0.1f)
                {
                    m_running = true;
                }
            }
            else
            {
                m_running = false;
                if (GetNormalizedMovementSpeed() >= 1.0f && m_hasGround && m_jumping == false)
                {
                    Vector3 _tempVelocity = m_rigid.velocity;
                    _tempVelocity.y = 0;
                    _tempVelocity = _tempVelocity.normalized * m_maxSneakVelocity;
                    _tempVelocity.y = m_rigid.velocity.y;
                    m_rigid.velocity = _tempVelocity;
                }
            }
        }

        m_runInputDownThisFrame = false;

        Vector3 _currentHorizontalVelocity = m_rigid.velocity;
        _currentHorizontalVelocity.y = 0;
        float _horizontalVelocityMagnitude = _currentHorizontalVelocity.magnitude;

        float _inputResponsivenessGround = m_inputResponsivenessGround_Sneak;
        float _accelerationGround = m_sneakAcceleration;
        float _accelerationAir = m_sneakAccelerationInAir;
        float _targetVelocityGround = m_maxSneakVelocity;

        if (m_running && m_blockManager.IsBlocking() == false)
        {
            _inputResponsivenessGround = m_inputResponsivenessGround_Run;
            _accelerationGround = m_runAcceleration;
            _accelerationAir = m_runAccelerationInAir;
            _targetVelocityGround = m_maxRunVelocity;
        }

        Vector3 _targetHorizontalVelocity;

        Vector2 _input = roundInputCorrection( m_inputManager.GetLeftAnalogue() );
        if (_input != Vector2.zero)
        {
            Vector3 _inputSensitivityDirection;
            Vector3 _inputDirection =
                MainCameraComponent.GetMovementRight() * _input.x +
                MainCameraComponent.GetMovementForward() * _input.y;

            _inputDirection = m_inputManager.GetLeftAnalogue().magnitude * _inputDirection.normalized;

            float _acceleration = 0.0f;
            float _targetVelocity = _targetVelocityGround;
            float _inputResponsiveness = _inputResponsivenessGround;

            if (_currentHorizontalVelocity.magnitude < _targetVelocityGround)
            {
                if (m_hasGround)
                {
                    _inputResponsiveness = _inputResponsivenessGround;
                    _acceleration = _accelerationGround;
                }
                else
                {
                    _inputResponsiveness = m_inputResponsivenessAir;
                    _acceleration = _accelerationAir;
                }
            }
            else
            {
                if (m_hasGround)
                {
                    _inputResponsiveness = _inputResponsivenessGround;
                    _acceleration = m_runDecelerationToRunFromGround;
                }
                else
                {
                    _inputResponsiveness = m_inputResponsivenessAir;
                    _acceleration = m_runDecelerationToRunFromAir;
                }
            }

            _inputSensitivityDirection = Vector3.Lerp(_currentHorizontalVelocity.normalized, _inputDirection.normalized, Time.fixedDeltaTime * GameTime.GameTimeScale * _inputResponsiveness);
            _inputSensitivityDirection.Normalize();

            _targetHorizontalVelocity = getHorizontalVelocityChangeVector(
                _horizontalVelocityMagnitude,
                _inputDirection,
               _targetVelocity,
               _acceleration);

            _targetHorizontalVelocity = _targetHorizontalVelocity.magnitude * _inputSensitivityDirection;

            _currentHorizontalVelocity.x = _targetHorizontalVelocity.x;
            _currentHorizontalVelocity.z = _targetHorizontalVelocity.z;

            if (m_hasGround)
            {
                if (m_running)
                {
                    m_hearableEvent_Run.Origin = m_rigid.position;
                    m_hearableManager.TriggerHearableEvent(m_hearableEvent_Run);

                }
                else
                {
                    m_hearableEvent_Sneak.Origin = m_rigid.position;
                    m_hearableManager.TriggerHearableEvent(m_hearableEvent_Sneak);
                }
            }
        }
        else
        {
            Vector3 _noHorizontalVelocity = m_rigid.velocity;
            _noHorizontalVelocity.x = 0;
            _noHorizontalVelocity.z = 0;

            if (m_hasGround)
            {
                _targetHorizontalVelocity = Vector3.MoveTowards(m_rigid.velocity, _noHorizontalVelocity, Time.fixedDeltaTime * GameTime.GameTimeScale * m_runDecelerationToStationaryFromGround);
            }
            else
            {
                _targetHorizontalVelocity = Vector3.MoveTowards(m_rigid.velocity, _noHorizontalVelocity, Time.fixedDeltaTime * GameTime.GameTimeScale * m_runDecelerationToStationaryFromAir);
            }

            _currentHorizontalVelocity.x = _targetHorizontalVelocity.x;
            _currentHorizontalVelocity.z = _targetHorizontalVelocity.z;
        }

        if (m_blockManager.IsBlocking())
        {
            _currentHorizontalVelocity *= 0.2f;
        }

        _currentHorizontalVelocity.y = m_rigid.velocity.y;
        m_rigid.velocity = _currentHorizontalVelocity;

        if (m_hasGround == true && m_jumping == false && GetNormalizedMovementSpeed() < 0.01f)
        {
            m_running = false;
        }
    }

    private void updateWallRunVelocity()
    {
        Vector3 _horizontalVelocity;

        if (m_jumpInputDownThisFrame)
        {
            startJump(new JumpStartParameters(JumpStartParameters.JumpContext.Wall));
            m_jumpInputDownThisFrame = false;
            return;
        }

        m_jumpInputDownThisFrame = false;
        float _gravityScale = m_gravityScale_WallRun;

        if (m_currentWallRunTime == 0.0f)
        {
            m_previousGroundHeight = m_rigid.position.y;
        }

        m_currentWallRunTime += Time.fixedDeltaTime * GameTime.GameTimeScale;

        _horizontalVelocity = m_rigid.velocity;
        _horizontalVelocity.y = 0;
        _horizontalVelocity = m_wallRunningHorizontalDirection.normalized * _horizontalVelocity.magnitude;
        _horizontalVelocity.y = m_rigid.velocity.y;
        m_rigid.velocity = _horizontalVelocity;

        m_rigid.AddForce(Physics.gravity * m_gravityScale_WallRun, ForceMode.Acceleration);
    }

    private void updateVerticalWallContact()
    {
        Vector3 _newHorizontalVelocity = m_rigid.velocity;
        _newHorizontalVelocity.y = 0;
        _newHorizontalVelocity = Vector3.MoveTowards(_newHorizontalVelocity, Vector3.zero, Time.fixedDeltaTime * GameTime.GameTimeScale * 1.3f);
        _newHorizontalVelocity.y = m_rigid.velocity.y;
        m_rigid.velocity = _newHorizontalVelocity;

        m_rigid.AddForce(Physics.gravity * m_gravityScale_VerticalWallContact, ForceMode.Acceleration);
    }

    private void startJump(JumpStartParameters parameters)
    {
        m_jumping = true;
        m_jumpStartedThisFrame = true;
        m_currentJumpTime = 0.0f;
        m_jumpStartedTimeStamp = Time.time;

        Vector2 _input = roundInputCorrection(m_inputManager.GetLeftAnalogue());
        Vector3 _inputDirection;
        Vector3 _correctedJumpDirection;

        float _currentVelocityMagnitude;
        float _normalizedBoostVelocity;

        switch (parameters.Context)
        {
            case JumpStartParameters.JumpContext.Ground:
            case JumpStartParameters.JumpContext.LedgeClimb:
            case JumpStartParameters.JumpContext.LedgeClimbBoosted:

                JumpedFromGround = true;

                Vector3 _horizontalVelocity = m_rigid.velocity;
                _horizontalVelocity.y = 0;
                m_rigid.velocity = _horizontalVelocity;

                switch (parameters.Context)
                {
                    case JumpStartParameters.JumpContext.Ground:
                    case JumpStartParameters.JumpContext.LedgeClimb:
                        m_rigid.AddForce(Vector3.up * m_jumpForce_Ground, ForceMode.Impulse);
                        break;

                    case JumpStartParameters.JumpContext.LedgeClimbBoosted:
                        m_rigid.AddForce(Vector3.up * m_jumpForce_LedgeBoost, ForceMode.Impulse);
                        break;
                }

                _currentVelocityMagnitude = _horizontalVelocity.magnitude;
                _normalizedBoostVelocity = (_currentVelocityMagnitude - m_addedHorizontalVelocityRangeMin) / (m_addedHorizontalVelocityRangeMax - m_addedHorizontalVelocityRangeMin);
                _normalizedBoostVelocity = Mathf.Clamp01(_normalizedBoostVelocity);
                _horizontalVelocity *= Mathf.Lerp(m_addedHorizontalVelocityModifierMin, m_addedHorizontalVelocityModifierMax, _normalizedBoostVelocity);

                if (_input != Vector2.zero)
                {
                    _inputDirection =
                        MainCameraComponent.GetMovementRight() * _input.x +
                        MainCameraComponent.GetMovementForward() * _input.y;

                    _correctedJumpDirection = Vector3.Lerp(_horizontalVelocity, _inputDirection, m_jumpInputDirectionSensitivity);
                    _horizontalVelocity = _horizontalVelocity.magnitude * _correctedJumpDirection.normalized;
                }

                _horizontalVelocity.y = m_rigid.velocity.y;
                m_rigid.velocity = _horizontalVelocity;

                break;

            case JumpStartParameters.JumpContext.Wall:

                JumpedFromGround = false;

                m_jumping = true;
                m_jumpStartedThisFrame = true;
                m_wallRunning = false;

                m_previousGroundHeight = m_rigid.position.y;

                m_currentWallRunTime = 0.0f;
                m_currentJumpTime = 0.0f;
                m_jumpStartedTimeStamp = Time.time;

                m_rigid.AddForce(Vector3.up * m_jumpForce_Wall, ForceMode.Impulse);

                _horizontalVelocity = m_rigid.velocity;
                _horizontalVelocity.y = 0;
                _currentVelocityMagnitude = _horizontalVelocity.magnitude;
                _normalizedBoostVelocity = (_currentVelocityMagnitude - m_addedHorizontalVelocityRangeMin) / (m_addedHorizontalVelocityRangeMax - m_addedHorizontalVelocityRangeMin);
                _normalizedBoostVelocity = Mathf.Clamp01(_normalizedBoostVelocity);

                float _targetAngle = 30.0f;

                if (_input != Vector2.zero)
                {
                    _inputDirection =
                        MainCameraComponent.GetMovementRight() * _input.x +
                        MainCameraComponent.GetMovementForward() * _input.y;

                    _inputDirection = m_inputManager.GetLeftAnalogue().magnitude * _inputDirection.normalized;

                    bool _lookingTowardsRunDirection = Vector3.Dot(_inputDirection, m_wallRunningHorizontalDirection) > 0;
                    bool _lookingAwayFromWall = Vector3.Dot(_inputDirection, m_wallNormal) > 0;

                    if (_lookingTowardsRunDirection)
                    {
                        if (_lookingAwayFromWall)
                        {
                            float _currentAngle = Vector3.Angle(_inputDirection, m_wallNormal);
                            _targetAngle = Mathf.Clamp(_currentAngle, m_minJumpFromWallAngle, m_maxJumpFromWallAngle);
                        }
                        else
                        {
                            _targetAngle = m_minJumpFromWallAngle;
                        }
                    }
                    else
                    {
                        if (_lookingAwayFromWall)
                        {
                            _targetAngle = m_maxJumpFromWallAngle;
                        }
                        else
                        {
                            _targetAngle = m_minJumpFromWallAngle;
                        }
                    }
                }

                _correctedJumpDirection = Quaternion.AngleAxis(_targetAngle * m_wallRunningSide, Vector3.up) * m_wallNormal;
                _correctedJumpDirection.y = 0;
                _correctedJumpDirection.Normalize();

                _horizontalVelocity *= Mathf.Lerp(m_addedHorizontalVelocityModifierMin, m_addedHorizontalVelocityModifierMax, _normalizedBoostVelocity);

                _horizontalVelocity = _horizontalVelocity.magnitude * _correctedJumpDirection.normalized;
                _horizontalVelocity.y = m_rigid.velocity.y;
                m_rigid.velocity = _horizontalVelocity;

                Vector3 _horizontalWallNormal = m_wallNormal;
                _horizontalWallNormal.y = 0;
                _horizontalWallNormal.Normalize();
                m_rigid.AddForce(_horizontalWallNormal * m_wallAddedForceTowardsHorizontalNormal, ForceMode.Impulse);

                break;
        }
    }

    private IEnumerator routine_ClimbToLedge(Vector3 targetPosition)
    {
        Vector3 _horizontalDir = targetPosition - m_rigid.position;
        _horizontalDir.y = 0;
        _horizontalDir.Normalize();

        Vector3 _startPosition = m_rigid.position - _horizontalDir * 0.5f;
        Vector3 _currentPosition = _startPosition;
        Vector3 _currentHorizontalVelocity = m_rigid.velocity;
        _currentHorizontalVelocity.y = 0;

        m_previousGroundHeight = targetPosition.y;

        m_animationController.StartClimbAnimation(1, targetPosition);

        m_rigid.isKinematic = true;

        float _timer = 0.0f;
        float _speed = 2.8f;

        bool _jumpHeld = m_inputManager.JumpInputStatus().Pressed;

        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale * _speed;

            _currentPosition = Vector3.Lerp(_startPosition, targetPosition, _timer);
            _currentPosition.y = Mathf.Lerp(_startPosition.y, targetPosition.y, _timer * 2.3f);
            m_rigid.position = _currentPosition;

            m_previousGroundHeight = m_rigid.position.y;

            if (_jumpHeld)
            {
                if (m_inputManager.JumpInputStatus().Pressed == false)
                {
                    _jumpHeld = false;
                }
            }

            yield return null;
        }

        m_rigid.position = targetPosition + Vector3.up * 0.01f;
        m_rigid.isKinematic = false;

        if (m_inputManager.GetLeftAnalogue().sqrMagnitude < 0.2f)
        {
            m_rigid.velocity = Vector3.zero;
            m_running = false;
        }
        else
        {
            m_rigid.velocity = _currentHorizontalVelocity * 0.4f;
        }

        m_animationController.EndLockedAnimation();

        if (_jumpHeld == false)
        {
            if (m_inputManager.JumpInputStatus().Pressed)
            {
                startJump(new JumpStartParameters(JumpStartParameters.JumpContext.LedgeClimb));
            }
        }

        m_routineLedgeClimb = null;

        yield return null;
        m_animationController.ResetLandingDeform();
    }

    private IEnumerator routine_LedgeJumpBoost(Vector3 targetPosition)
    {
        Debug.ClearDeveloperConsole();

        Vector3 _horizontalDir = targetPosition - m_rigid.position;
        _horizontalDir.y = 0;
        _horizontalDir.Normalize();

        Vector3 _startPosition = m_rigid.position - _horizontalDir * 0.5f;
        Vector3 _currentPosition = _startPosition;
        Vector3 _currentHorizontalVelocity = m_rigid.velocity;
        _currentHorizontalVelocity.y = 0;

        m_previousGroundHeight = targetPosition.y;

        m_animationController.StartClimbAnimation(0, targetPosition);

        m_rigid.isKinematic = true;

        float _timer = 0.0f;
        float _speed = 6f;

        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale * _speed;

            _currentPosition = Vector3.Lerp(_startPosition, targetPosition, _timer);
            m_rigid.position = _currentPosition;

            m_previousGroundHeight = m_rigid.position.y;

            yield return null;
        }

        m_rigid.position = targetPosition + Vector3.up * 0.01f;
        m_rigid.isKinematic = false;

        m_rigid.velocity = _currentHorizontalVelocity;

        m_animationController.EndLockedAnimation();

        startJump(new JumpStartParameters(JumpStartParameters.JumpContext.LedgeClimbBoosted));

        m_routineLedgeClimb = null;

        yield return null;
        m_animationController.ResetLandingDeform();
    }

    private RaycastHit getClosestHit(int hits)
    {
        RaycastHit _result = m_raycastHits[0];

        for (int i = 0; i < hits; i++)
        {
            RaycastHit _current = m_raycastHits[i];
            if (_current.collider.gameObject == gameObject || _current.point == Vector3.zero)
            {
                continue;
            }

            if (_current.distance < _result.distance)
            {
                _result = _current;
            }
        }

        return _result;
    }

    private Vector2 roundInputCorrection(Vector2 rawInput)
    {
        Vector2 _inputCircle = new Vector2(
                rawInput.x * Mathf.Sqrt(1 - rawInput.y * rawInput.y * 0.5f),
                rawInput.y * Mathf.Sqrt(1 - rawInput.x * rawInput.x * 0.5f));

        return _inputCircle;
    }

    private Vector3 getHorizontalVelocityChangeVector(
        float currentVelocity, 
        Vector3 direction, 
        float targetVelocity,
        float acceleration)
    {
        return Vector3.MoveTowards(
            currentVelocity * direction,
            targetVelocity * direction,
            Time.fixedDeltaTime * GameTime.GameTimeScale * acceleration);
    }

    public void AnimationMovement(Vector3 targetForce)
    {
        m_rigid.velocity = targetForce;
    }

    public void SetPhysicsEnabled(bool newState)
    {
        m_locked = !newState;
        m_rigid.isKinematic = !newState;

        if (newState == false)
        {
            m_rigid.velocity = Vector3.zero;

            if (m_routineLedgeClimb != null)
            {
                StopCoroutine(m_routineLedgeClimb);
                m_routineLedgeClimb = null;
            }

            m_jumping = false;
            m_wallRunning = false;
        }
    }

    public void SetRunning(bool newState)
    {
        m_running = newState;

        if (m_running == false)
        {
            if (GetNormalizedMovementSpeed() >= 1.0f && m_hasGround && m_jumping == false)
            {
                Vector3 _tempVelocity = m_rigid.velocity;
                _tempVelocity.y = 0;
                _tempVelocity = _tempVelocity.normalized * m_maxSneakVelocity;
                _tempVelocity.y = m_rigid.velocity.y;
                m_rigid.velocity = _tempVelocity;
            }
        }
    }

    public bool IsJumping()
    {
        if (m_jumping)
        {
            return true;
        }

        if (m_rigid.position.y > m_previousGroundHeight)
        {
            return true;
        }

        return false;
    }

    public bool JumpStartedThisFrame()
    {
        bool _result = false;

        if (m_jumpInputDownThisFrame && m_hasGround && m_jumping == false)
        {
            _result = true;
        }

        if (m_jumpStartedThisFrame)
        {
            _result = true;
        }

        m_jumpStartedThisFrame = false;
        return _result;
    }

    public Vector3 GetHorizontalVelocity()
    {
        return new Vector3(m_rigid.velocity.x, 0, m_rigid.velocity.z);
    }

    public float GetVerticalVelocity()
    {
        return m_rigid.velocity.y;
    }

    public bool HasGround()
    {
        return m_hasGround;
    }

    public bool IsRunning()
    {
        return m_running;
    }

    public bool HasVerticalWallContact()
    {
        return m_verticalWallContact;
    }

    public Vector3 GetMovementInputVector()
    {
        Vector3 _result = Vector3.zero;
        Vector2 _input = roundInputCorrection(m_inputManager.GetLeftAnalogue());
        if (_input != Vector2.zero)
        {
            _result =
                MainCameraComponent.GetMovementRight() * _input.x +
                MainCameraComponent.GetMovementForward() * _input.y;
        }

        return _result;
    }

    public float GetNormalizedMovementSpeed()
    {
        Vector3 _horizontalVelocity = m_rigid.velocity;
        _horizontalVelocity.y = 0;

        if (m_running)
        {
            return Mathf.Clamp01( _horizontalVelocity.magnitude / m_maxRunVelocity );
        }
        else
        {
            return Mathf.Clamp01( _horizontalVelocity.magnitude / m_maxSneakVelocity );
        }
    }

    public WallRunInfo GetWallRunInfo()
    {
        if (m_wallRunning)
        {
            return new WallRunInfo(true, m_wallRunningSide, 90, m_wallRunningTargetPoint, m_wallNormal, m_currentWallRunTime);
        }
        else
        {
            return new WallRunInfo(false, 0, 0, Vector3.zero, Vector3.zero, 0);
        }
    }

    public Vector3 GetWallNormal()
    {
        return m_wallNormal;
    }

    public float GetPreviousGroundHeight()
    {
        return m_previousGroundHeight;
    }
}
