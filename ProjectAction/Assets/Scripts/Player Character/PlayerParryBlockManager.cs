﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParryBlockManager : MonoBehaviour
{
    [SerializeField] private GameTime.TimeScaleManipulatorParameters m_onParryHitFreeze;
    [SerializeField] private GameTime.TimeScaleManipulatorParameters m_onParryCounterHitFreeze;

    [Header("Object References")]
    [SerializeField] private ParryCamera m_camera_Parry = null;
    [SerializeField] private ParryCamera m_camera_ParryCounter_LightPistol = null;

    private bool m_blocking = false;
    private bool m_negateDamageWindow = false;
    private bool m_parryWindow = false;
    private bool m_parryTriggered = false;

    private CustomPlayerInputManager m_inputManager = null;
    private PlayerMovementManager m_movementManager = null;
    private PlayerAimManager m_aimManager = null;
    private PlayerAnimationController m_animController = null;
    private PlayerVFX m_playerVFX = null;
    private UI_ParryBlockCanvas m_uiComponent = null;


    private Coroutine m_currentRoutine = null;

    private void Awake()
    {
        m_inputManager = GetComponent<CustomPlayerInputManager>();
        m_movementManager = GetComponent<PlayerMovementManager>();
        m_aimManager = GetComponent<PlayerAimManager>();
        m_playerVFX = GetComponent<PlayerVFX>();
        m_animController = GetComponentInChildren<PlayerAnimationController>();
    }

    private void Start()
    {
        m_uiComponent = UI_ParryBlockCanvas.GetInstance();
        m_uiComponent.ParryWindowActive(false);
    }

    private void Update()
    {
        if (m_blocking == false)
        {
            if (m_animController.IsCinematicAnimationRunning())
            {
                return;
            }

            m_negateDamageWindow = false;
            m_parryWindow = false;
            m_parryTriggered = false;

            if (m_animController.IsLocked())
            {
                return;
            }
        }
        else
        {
            if (m_animController.IsLocked())
            {
                return;
            }
        }

        CustomPlayerInputManager.ButtonInputStatus _inputStatus = m_inputManager.BlockInputStatus();
        if (_inputStatus.Pressed)
        {
            m_blocking = true;
            m_movementManager.SetRunning(false);

            if (_inputStatus.PressedDownThisFrame)
            {
                if (m_currentRoutine == null)
                {
                    m_animController.StartBlockStanceAnimation();
                    m_currentRoutine = StartCoroutine(routine_BlockLockWindow());
                }
            }
        }
        else
        {
            if (m_currentRoutine == null)
            {
                m_blocking = false;
                m_negateDamageWindow = false;
                m_parryWindow = false;
                m_parryTriggered = false;
            }
        }
    }

    public bool IsBlocking()
    {
        return m_blocking;
    }

    public bool IsParryWindow()
    {
        return m_parryWindow;
    }

    public bool IsParryTriggered()
    {
        return m_parryTriggered;
    }

    public void ParryTriggered(EnemyEntity parryTarget)
    {
        if (m_currentRoutine != null) StopCoroutine(m_currentRoutine);

        m_blocking = true;
        m_parryWindow = false;
        m_negateDamageWindow = true;
        m_uiComponent.ParryWindowActive(false);

        m_currentRoutine = StartCoroutine(routine_ParryTriggered(parryTarget));
    }

    private IEnumerator routine_BlockLockWindow()
    {
        float _timer = 0.0f;
        m_negateDamageWindow = false;

        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale;

            if (_timer > 0.14f && _timer < 0.44f)
            {
                m_parryWindow = true;
                m_negateDamageWindow = true;

                m_uiComponent.ParryWindowActive(true);
            }
            else
            {
                m_parryWindow = false;
                m_uiComponent.ParryWindowActive(false);
            }

            yield return null;
        }

        m_currentRoutine = null;
    }

    private IEnumerator routine_ParryTriggered(EnemyEntity parryTarget)
    {
        m_parryTriggered = true;

        m_playerVFX.TriggerScreenShake(1.4f);
        m_camera_Parry.SetActive(true, parryTarget.transform.position);
        GameTime.GetInstance().TriggerHitFreeze(this, ref m_onParryHitFreeze);

        Quaternion _startRotation = m_animController.transform.rotation;
        Vector3 _targetDir = parryTarget.transform.position - m_animController.transform.position;
        _targetDir.y = 0;
        Quaternion _targetDirection = Quaternion.LookRotation(_targetDir);

        bool _newRotationApplied = false;

        float _timer = 0.0f;
        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale * 1.4f;

            parryTarget.UIComponent.AttackElementVisible = true;
            parryTarget.UIComponent.OverrideVerticalOffset = -0.4f;

            m_camera_Parry.SetCinematicNormalizedPosition(_timer);

            m_animController.transform.rotation = Quaternion.Slerp(_startRotation, _targetDirection, _timer * 9);

            if (m_inputManager.AttackInputStatus().PressedDownThisFrame)
            {
                startParryCounter(parryTarget);
            }

            if (_timer > 0.5f)
            {
                _newRotationApplied = true;
                m_camera_ParryCounter_LightPistol.SetFollowCameraDirection((parryTarget.transform.position - transform.position).normalized, 0);
            }

            yield return null;
        }

        m_camera_Parry.SetActive(false);

        m_blocking = false;
        m_parryTriggered = false;
        m_currentRoutine = null;
    }

    private void startParryCounter(EnemyEntity parryTarget)
    {
        StopCoroutine(m_currentRoutine);

        Quaternion _startRotation = m_animController.transform.rotation;
        Vector3 _targetDir = parryTarget.transform.position - m_animController.transform.position;
        _targetDir.y = 0;
        Quaternion _targetDirection = Quaternion.LookRotation(_targetDir);
        m_animController.transform.rotation = _targetDirection;

        m_camera_ParryCounter_LightPistol.SetFollowCameraDirection( (parryTarget.transform.position - transform.position).normalized , 0);

        m_currentRoutine = StartCoroutine(routine_ParryCounter_LightPistol(parryTarget));
    }

    private IEnumerator routine_ParryCounter_LightPistol(EnemyEntity parryTarget)
    {
        m_animController.StartParryCounterAnimation(parryTarget);
        m_camera_ParryCounter_LightPistol.SetActive(true, parryTarget.transform.position);
        GameTime.GetInstance().TriggerHitFreeze(this, ref m_onParryCounterHitFreeze);

        while (m_animController.IsLocked())
        {
            yield return null;
        }

        m_camera_Parry.SetActive(false);
        m_camera_ParryCounter_LightPistol.SetActive(false);

        m_blocking = false;
        m_parryTriggered = false;
        m_currentRoutine = null;
    }

    public void SetParryCounterAnimationState(float normalizedTime)
    {
        m_camera_ParryCounter_LightPistol.SetCinematicNormalizedPosition(normalizedTime);
    }
}
