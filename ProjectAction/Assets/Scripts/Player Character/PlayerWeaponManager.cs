﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class PlayerWeaponManager : MonoBehaviour
{
    public bool Debug_OverrideAim = false;

    [Header("Light Pistol")]
    [SerializeField] private WeaponComponent m_lightPistolWeaponComponent;
    [SerializeField] private float m_weaponDrawnTime = 3.5f;

    [Header("Object References")]
    [SerializeField] private PlayerAnimationController m_animController;
    [SerializeField] private Rig m_movementRig;
    [SerializeField] private Rig m_aimRig;
    [SerializeField] private PlayerAimIKController m_aimIKController;

    private CustomPlayerInputManager m_inputManager = null;
    private PlayerAimManager m_aimManager = null;
    private PlayerMovementManager m_movementManager = null;
    private WeaponComponent m_equippedWeapon = null;

    private Vector3 m_aimPosition = Vector3.zero;

    private float m_timer_weaponDrawn = 0.0f;

    private void Awake()
    {
        m_inputManager = GetComponent<CustomPlayerInputManager>();
        m_aimManager = GetComponent<PlayerAimManager>();
        m_movementManager = GetComponent<PlayerMovementManager>();

        if (m_lightPistolWeaponComponent) m_equippedWeapon = m_lightPistolWeaponComponent;
    }

    private void Update()
    {
        if (m_animController.IsLocked() || m_animController.IsCinematicAnimationRunning())
        {
            m_aimIKController.Enabled = false;
            return;
        }
        if (m_movementManager.IsRunning() && m_movementManager.HasGround())
        {
            m_timer_weaponDrawn = 0.0f;
            m_aimIKController.Enabled = false;
        }

        if (m_inputManager.AttackInputStatus().PressedDownThisFrame || m_aimManager.IsAiming())
        {
            m_movementRig.weight = 0.0f;
            m_aimRig.weight = 1.0f;

            m_aimPosition = m_aimManager.GetAimPosition();

            m_aimIKController.Enabled = true;
            m_aimIKController.SetAimTargetPosition(m_aimPosition);

            m_timer_weaponDrawn = m_weaponDrawnTime;

            if (m_inputManager.AttackInputStatus().PressedDownThisFrame)
            {
                if (m_movementManager.HasGround())
                {
                    m_movementManager.SetRunning(false);
                }

                WeaponComponent.FireParameters _parameters;
                _parameters.AimTarget = m_aimPosition;
                _parameters.TriggerStunState = false;
                _parameters.WaitOneFrameForVFX = true;
                _parameters.OverrideHitTarget = null;

                m_equippedWeapon.Fire(_parameters);
                FollowCamera.GetInstance().RecoilImpulse(m_equippedWeapon.RecoilImpulse);
            }
        }

        if (m_timer_weaponDrawn > 0.0f)
        {
            m_timer_weaponDrawn -= Time.deltaTime * GameTime.GameTimeScale;

            m_movementRig.weight = 0.0f;
            m_aimRig.weight = 1.0f;
            m_aimIKController.Enabled = true;

            m_aimPosition = m_aimManager.GetAimPosition();
            m_aimIKController.SetAimTargetPosition(m_aimPosition);
        }
        else
        {
            m_movementRig.weight = 1.0f;
            m_aimRig.weight = 0.0f;
            m_aimIKController.Enabled = false;

            m_aimPosition = Vector3.zero;
        }
    }

    public bool WeaponDrawn()
    {
        if (m_timer_weaponDrawn > 0.0f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ForceUndrawWeapon()
    {
        m_timer_weaponDrawn = 0.0f;
        m_aimIKController.Enabled = false;
    }

    public void ForceFire(WeaponComponent.FireParameters parameters)
    {
        m_equippedWeapon.Fire(parameters);
    }
}
