﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerHealthComponent : MonoBehaviour
{
    [System.Serializable] public class OnHealthUpdateEvent : UnityEvent<int> { }

    public bool CanTakeDamage = true;

    public int CurrentHealth = 100;
    public int MaxHealth = 100;

    public OnHealthUpdateEvent OnHealthUpdate;

    private PlayerAnimationController m_animController = null;
    private PlayerParryBlockManager m_blockManager = null;

    private void Start()
    {
        m_animController = GetComponentInChildren<PlayerAnimationController>();
        m_blockManager = GetComponent<PlayerParryBlockManager>();
    }

    public void DealDamage(int damage, Transform knockbackSource = null)
    {
        if (m_blockManager.IsBlocking())
        {
            if (m_blockManager.IsParryWindow())
            {
                EnemyEntity _enemy = knockbackSource.GetComponent<EnemyEntity>();
                if (_enemy)
                {
                    _enemy.TriggerParry();
                    m_blockManager.ParryTriggered(_enemy);
                }
            }

            return;
        }

        if (CanTakeDamage == false) return;

        CurrentHealth -= damage;
        CurrentHealth = Mathf.Clamp(CurrentHealth, 0, MaxHealth);

        if (CurrentHealth > 0)
        {
            if (knockbackSource)
            {
                m_animController.TriggerKnockbackAnimation(knockbackSource);
            }
        }
        else
        {
            // DO DEATH
        }

        OnHealthUpdate.Invoke(CurrentHealth);
    }
}
