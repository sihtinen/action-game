﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerTakedownManager : MonoBehaviour
{
    public enum TakedownType
    {
        air,
        behindGrabLightMeleeWeapon
    }

    [SerializeField] private float m_takedownMaxGroundHeightDifference = 0.2f;
    [SerializeField] private LayerMask m_movementLayer;

    [Header("Behind Target Settings")]
    [SerializeField, Range(0, 90)] private float m_behindTargetMaxAngle = 90;
    [SerializeField, Range(0, 5)] private float m_behindTargetMaxDistance = 3;

    [Header("Air Takedown Settings")]
    [SerializeField] private float m_airTakedown_FallAngleToTarget = 40.0f;
    [SerializeField] private float m_airTakedown_MinVerticalDistance = 2.0f;
    [SerializeField] private float m_airTakedown_MaxVerticalDistance = 10.0f;

    [Header("Object References")]
    [SerializeField] private PlayerMovementManager m_movementManager;
    [SerializeField] private CustomPlayerInputManager m_inputManager;
    [SerializeField] private PlayerAnimationController m_animationController;

    private Vector3 m_movePosition = Vector3.zero;

    private GameState m_gameState = null;
    private Rigidbody m_rigid = null;
    private CapsuleCollider m_capsuleCollider = null;
    private List<EnemyEntity> m_availableTargets = null;
    private RaycastHit[] m_raycastHits = new RaycastHit[100];

    private void Awake()
    {
        m_gameState = GameState.GetInstance();
        m_rigid = GetComponent<Rigidbody>();
        m_capsuleCollider = GetComponent<CapsuleCollider>();

        m_availableTargets = new List<EnemyEntity>();
    }

    private void Update()
    {
        m_availableTargets.Clear();
        if (m_animationController.IsLocked()) return;

        EnemyEntity _current;
        RaycastHit _closest;

        m_movePosition = m_rigid.position;

        for (int i = 0; i < m_gameState.EnemyEntities.Count; i++)
        {
            _current = m_gameState.EnemyEntities[i];

            if (_current.AIEnabled == false) continue;

            Vector3 _enemyGroundPoint = _current.transform.position + Vector3.up * m_capsuleCollider.height / 2;
            Vector3 _fromEnemyToPlayer = m_rigid.position - _enemyGroundPoint;

            if (_fromEnemyToPlayer.sqrMagnitude > 100) continue;

            int _hits = Physics.CapsuleCastNonAlloc(
                m_rigid.position,
                m_rigid.position + Vector3.up * m_capsuleCollider.height,
                m_capsuleCollider.radius,
                (_fromEnemyToPlayer * -1).normalized,
                m_raycastHits,
                _fromEnemyToPlayer.magnitude - m_capsuleCollider.radius,
                m_movementLayer,
                QueryTriggerInteraction.Ignore
                );

            if (_hits > 0)
            {
                _closest = getClosestHit(_hits);
                if (_closest.point == Vector3.zero) continue;
                continue;
            }

            // CLEAR LINE OF SIGHT TO ENEMY

            if (m_movementManager.HasGround())
            {
                float _directionDotProduct = Vector3.Dot(-_fromEnemyToPlayer, _current.transform.forward);
                if (_directionDotProduct > 0)
                {
                    float _angleToEnemyForward = Vector3.Angle(-_fromEnemyToPlayer, _current.transform.forward);
                    if (Mathf.Abs(_angleToEnemyForward) < m_behindTargetMaxAngle)
                    {
                        if (_fromEnemyToPlayer.magnitude < m_behindTargetMaxDistance)
                        {
                            if (_current.HasLightMelee)
                            {
                                m_movePosition = Vector3.Lerp(m_rigid.position, _current.transform.position, 0.7f);
                                m_movePosition.y = m_rigid.position.y;
                                _current.UIComponent.CurrentTakedownType = TakedownType.behindGrabLightMeleeWeapon;
                                m_availableTargets.Add(_current);
                            }
                        }
                    }
                }

            }
            else
            {
                validateForAirTakedown(_current, _fromEnemyToPlayer, _enemyGroundPoint);
            }
        }

        if (m_availableTargets.Count > 0 && m_availableTargets[0] != null)
        {
            for (int i = 0; i < m_availableTargets.Count; i++)
            {
                if (i == 0)
                {
                    m_availableTargets[i].UIComponent.TakedownElementVisible = true;
                }
                else
                {
                    m_availableTargets[i].UIComponent.TakedownElementVisible = false;
                }
            }
        }

        if (m_inputManager.TakedownInputStatus().PressedDownThisFrame) takedownInputPressed();
    }

    private void takedownInputPressed()
    {
        if (m_availableTargets.Count > 0 && m_availableTargets[0] != null)
        {
            m_movementManager.SetPhysicsEnabled(false);

            m_animationController.StartTakedownAnimation(m_availableTargets[0].UIComponent.CurrentTakedownType, m_movePosition, m_availableTargets[0]);
            m_availableTargets[0].Takedown(m_movePosition);
        }
    }

    private void validateForAirTakedown(EnemyEntity entity, Vector3 fromEnemyToPlayer, Vector3 enemyGroundPosition)
    {
        int _hits = Physics.CapsuleCastNonAlloc(
           m_rigid.position,
           m_rigid.position + Vector3.up * m_capsuleCollider.height,
           m_capsuleCollider.radius,
           Vector3.down,
           m_raycastHits,
           fromEnemyToPlayer.y + m_takedownMaxGroundHeightDifference,
           m_movementLayer,
           QueryTriggerInteraction.Ignore
           );

        Vector3 _currentGroundPosition;

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);

            if (_closest.point == Vector3.zero) return;
            if ((_closest.point.y - enemyGroundPosition.y) > m_takedownMaxGroundHeightDifference) return;

            _currentGroundPosition = _closest.point;

            Vector3 _halfWayPoint = Vector3.Lerp(m_rigid.position, enemyGroundPosition, 0.5f);
            Vector3 _toHalfWayPoint = _halfWayPoint - m_rigid.position;

            _hits = Physics.CapsuleCastNonAlloc(
                m_rigid.position,
                m_rigid.position + Vector3.up * m_capsuleCollider.height,
                m_capsuleCollider.radius,
                _toHalfWayPoint.normalized,
                m_raycastHits,
                _toHalfWayPoint.magnitude + m_takedownMaxGroundHeightDifference,
                m_movementLayer,
                QueryTriggerInteraction.Ignore
                );

            if (_hits > 0)
            {
                _closest = getClosestHit(_hits);
                if (_closest.point == Vector3.zero) return;

                _currentGroundPosition = _closest.point;
            }
        }
        else
        {
            _currentGroundPosition = entity.transform.position;
        }

        if (fromEnemyToPlayer.y > m_airTakedown_MinVerticalDistance &&
            fromEnemyToPlayer.y < m_airTakedown_MaxVerticalDistance)
        {
            float _fallAngleToEnemy = Vector3.Angle(fromEnemyToPlayer, Vector3.up);
            if (Mathf.Abs(_fallAngleToEnemy) < m_airTakedown_FallAngleToTarget)
            {
                entity.UIComponent.CurrentTakedownType = TakedownType.air;
                m_availableTargets.Add(entity);
                m_movePosition = _currentGroundPosition;
            }
        }
    }

    private RaycastHit getClosestHit(int hits)
    {
        RaycastHit _result = m_raycastHits[0];

        for (int i = 0; i < hits; i++)
        {
            RaycastHit _current = m_raycastHits[i];
            if (_current.collider.gameObject == gameObject || _current.point == Vector3.zero)
            {
                continue;
            }

            if (_current.distance < _result.distance)
            {
                _result = _current;
            }
        }

        return _result;
    }
}
