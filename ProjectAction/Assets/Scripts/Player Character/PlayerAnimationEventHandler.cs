﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEventHandler : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private PlayerVFX m_playerVFX;

    public void Footstep(int foot)
    {
        m_playerVFX.PlayFootstepVFX(foot);
    }
}
