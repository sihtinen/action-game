﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVisibilityComponent : MonoBehaviour
{
    [SerializeField] private float m_mod_Movement_Stationary;
    [SerializeField] private float m_mod_Movement_Sneaking;
    [SerializeField] private float m_mod_Movement_Running;
    [SerializeField] private float m_mod_Movement_InAir;

    [Space]

    [SerializeField] private float m_mod_Light_Min;
    [SerializeField] private float m_mod_Light_Max;
    [SerializeField] private AnimationCurve m_curveModLight;

    [Space]

    [SerializeField] private float m_lerpSpeedUp = 3;
    [SerializeField] private float m_lerpSpeedDown = 9;

    [HideInInspector] public LightLevelMeasureMaster LightMeasureMaster = null;

    private float m_currentVisibility = 0;
    private float m_currentVisibilitySquared = 0;
    private float m_targetVisibility = 0;

    private PlayerMovementManager m_movementManager = null;

    private void Awake()
    {
        m_movementManager = GetComponent<PlayerMovementManager>();
    }

    private void Update()
    {
        m_targetVisibility = m_mod_Movement_Stationary;

        if (m_movementManager.HasGround() == false)
        {
            m_targetVisibility = m_mod_Movement_InAir;
        }
        else if (m_movementManager.IsRunning())
        {
            m_targetVisibility = m_mod_Movement_Running;
        }
        else if (m_movementManager.GetHorizontalVelocity().sqrMagnitude > 0.2f)
        {
            m_targetVisibility = m_mod_Movement_Sneaking;
        }

        if (LightMeasureMaster)
        {
            m_targetVisibility += Mathf.Lerp(m_mod_Light_Min, m_mod_Light_Max, m_curveModLight.Evaluate(LightMeasureMaster.CurrentAverageLightLevelNormalized));
        }

        if (m_targetVisibility > m_currentVisibility)
        {
            m_currentVisibility = Mathf.Lerp(m_currentVisibility, m_targetVisibility, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeedUp);
        }
        else
        {
            m_currentVisibility = Mathf.Lerp(m_currentVisibility, m_targetVisibility, Time.deltaTime * GameTime.GameTimeScale * m_lerpSpeedDown);
        }

        m_currentVisibilitySquared = m_currentVisibility * m_currentVisibility;
    }

    public float GetCurrentVisibilityDistance()
    {
        return m_currentVisibility;
    }

    public float GetCurrentVisibilityDistanceSquared()
    {
        return m_currentVisibilitySquared;
    }
}
