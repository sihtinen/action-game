﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHurtbox : MonoBehaviour
{
    private PlayerHealthComponent m_playerHealth = null;

    private void Start()
    {
        m_playerHealth = GetComponentInParent<PlayerHealthComponent>();
    }

    public void DealDamage(int damage, Transform knockbackSource = null)
    {
        m_playerHealth.DealDamage(damage, knockbackSource);
    }
}
