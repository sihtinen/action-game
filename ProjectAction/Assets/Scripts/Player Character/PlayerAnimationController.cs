﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PlayerAnimationController : MonoBehaviour
{
    [Header("WallRun Animation Settings")]
    [SerializeField] private float m_wallRunXOffset = 0.5f;
    [SerializeField] private float m_wallRunXOffsetResponsiveness = 1.3f;
    [SerializeField] private float m_wallRunAngleZ = 11;
    [SerializeField] private float m_wallRunAngleRotationTime = 0.6f;

    [Header("Hit Freeze Settings")]
    [SerializeField] private AnimationCurve m_hitFreeze_AirTakedown_Curve;
    [SerializeField] private float m_hitFreeze_AirTakedown_Length = 0.3f;
    [SerializeField, Range(0, 1)] private float m_hitFreeze_AirTakedown_StartTime = 0.1f;
    [SerializeField, Range(0, 1)] private float m_hitFreeze_AirTakedown_LowestTimeScale = 0.1f;

    [Header("Knockback Settings")]
    [SerializeField] private float m_knockbackLight_RotationSpeed = 18.0f;
    [SerializeField] private float m_knockbackLight_MovementSpeed = 4.0f;
    [SerializeField] private AnimationCurve m_curve_KnockbackLight_Movement;

    [Header("Scale Deform Settings")]
    [SerializeField] private float m_onLandingScaleDeformTime = 0.6f;
    [SerializeField] private AnimationCurve m_curveOnLandingScaleDeform;

    [Header("Object References")]
    [SerializeField] private MeshRenderer m_renderer_Drawn_LightPistol;
    [SerializeField] private Transform m_rootBone;
    [SerializeField] private Transform m_leftHandBone;
    [SerializeField] private PlayerVFX m_playerVFX;
    [SerializeField] private Rigidbody m_rigid;

    [HideInInspector] public CinematicController Cinematic_TakedownBehindGrabLightMeleeWeapon;

    private const string m_animState_Idle = "Idle";
    private const string m_animState_Sneak = "Sneak";
    private const string m_animState_Run = "Run";

    private const string m_animState_Knockback_Light = "Knockback Light";

    private const string m_animState_JumpAscend = "Jump Ascend";
    private const string m_animState_JumpPeak = "Jump Peak";
    private const string m_animState_JumpDescend = "Jump Descend";

    private const string m_animState_WallRunLeft = "WallRun Left";
    private const string m_animState_WallRunRight = "WallRun Right";
    private const string m_animState_WallRunVertical = "WallRun Vertical";
    private const string m_animState_WallRunSlideDown = "WallRun Slide Down";

    private const string m_animState_ClimbBoost = "Climb Boost";
    private const string m_animState_ClimbEasy = "Climb Easy";

    private const string m_animState_TakedownAir = "Takedown Air";
    private const string m_animState_TakedownBehindGrabLightMeleeWeapon = "Takedown Behind GrabLightMeleeWeapon";

    private const string m_animState_AimLightPistol = "Aim Light Pistol";
    private const string m_animState_BlockStance_Start = "Block Stance Start";
    private const string m_animState_ParryCounter_LightPistol = "ParryCounter LightPistol";

    private const string m_param_float_animSpeedMultiplier = "Float_AnimSpeedMultiplier";
    private const string m_param_float_normalizedMovementSpeed = "Float_MovementSpeedNormalized";
    private const string m_param_float_aimAngleHorizontal = "Float_AimAngleHorizontal";
    private const string m_param_float_aimAngleVertical = "Float_AimAngleVertical";

    private Animator m_animator = null;
    private PlayerMovementManager m_movementManager = null;
    private PlayerAimManager m_aimManager = null;
    private PlayerWeaponManager m_weaponManager = null;
    private PlayerParryBlockManager m_blockManager = null;

    private bool m_locked = false;

    private float m_landedTimeRemaining = 0.0f;
    private float m_landedIntensity = 0.0f;

    private string m_currentAnimState_Layer0;
    private string m_currentAnimState_Layer1;

    private float m_timerAim = 0.0f;

    private Coroutine m_animRoutine = null;

    private Vector3 m_smoothedVelocityCross = Vector3.zero;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    private void Start()
    {
        m_movementManager = GetComponentInParent<PlayerMovementManager>();
        m_aimManager = m_movementManager.gameObject.GetComponent<PlayerAimManager>();
        m_weaponManager = m_movementManager.gameObject.GetComponent<PlayerWeaponManager>();
        m_blockManager = m_movementManager.gameObject.GetComponent<PlayerParryBlockManager>();

        m_animator.Play(m_animState_Idle, 0);
        m_currentAnimState_Layer0 = m_animState_Idle;
    }

    private void LateUpdate()
    {
        transform.localScale = Vector3.one;
        bool _deformScale = true;

        if (m_animRoutine != null)
        {
            return;
        }

        if (m_locked)
        {
            startAnimation("Empty", 1, 0.1f);
            m_renderer_Drawn_LightPistol.enabled = false;

            return;
        }

        if (m_blockManager.IsBlocking())
        {
            if (m_blockManager.IsParryTriggered() == false)
            {
                Vector3 _lookDir = MainCameraComponent.GetMovementForward();
                _lookDir.y = 0;
                Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
                transform.rotation = Quaternion.Lerp(transform.rotation, _targetDirection, Time.deltaTime * GameTime.GameTimeScale * 20);
            }

            return;
        }

        if ( (m_aimManager.IsAiming() || m_weaponManager.WeaponDrawn() ) 
            && (m_movementManager.IsRunning() == false || m_movementManager.HasGround() == false) )
        {
            m_animator.SetLayerWeight(1, 1);
            startAnimation(m_animState_AimLightPistol, 1, 0.1f);
            m_renderer_Drawn_LightPistol.enabled = true;

            _deformScale = false;
        }
        else
        {
            startAnimation("Empty", 1, 0.1f);
            m_renderer_Drawn_LightPistol.enabled = false;
        }

        m_animator.SetFloat(m_param_float_aimAngleHorizontal, MainCameraComponent.NormalizedAimAngleHorizontal);
        m_animator.SetFloat(m_param_float_aimAngleVertical, MainCameraComponent.NormalizedAimAngleVertical);

        Vector3 _horizontalVelocity = m_movementManager.GetHorizontalVelocity();
        float _verticalVelocity = m_movementManager.GetVerticalVelocity();
        float _floatValue_horizontalVelocity = 0.0f;

        transform.localPosition = Vector3.zero;

        if (m_movementManager.HasVerticalWallContact())
        {
            verticalWallContact();
            return;
        }

        PlayerMovementManager.WallRunInfo _wallRunInfo = m_movementManager.GetWallRunInfo();
        if (_wallRunInfo.WallRunning)
        {
            wallrunning();
            return;
        }

        Vector3 _eulerAngles = transform.localEulerAngles;
        _eulerAngles.z = 0;
        transform.localEulerAngles = _eulerAngles;

        if ((_horizontalVelocity.magnitude) > 0.1f)
        {
            Vector3 _lookVector = Vector3.Lerp(_horizontalVelocity, m_movementManager.GetMovementInputVector(), 0.7f);
            Vector3 _lookDir = _lookVector.normalized;
            Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
            transform.rotation = Quaternion.Lerp(transform.rotation, _targetDirection, Time.deltaTime * GameTime.GameTimeScale * 11);
        }

        if (m_movementManager.IsJumping())
        {
            if (m_movementManager.JumpStartedThisFrame() || _verticalVelocity > 0)
            {
                if (m_movementManager.JumpStartedThisFrame() || _verticalVelocity >= 2.0f)
                {
                    startAnimation(m_animState_JumpAscend, 0, 0.1f);
                }
                else
                {
                    startAnimation(m_animState_JumpPeak, 0, 0.3f);
                }
            }
            else
            {
                startAnimation(m_animState_JumpPeak, 0, 0.3f);

                if (m_movementManager.GetVerticalVelocity() < -2)
                {
                    startAnimation(m_animState_JumpDescend, 0, 0.25f);
                }
            }
        }
        else
        {
            if (m_movementManager.HasGround() == false)
            {
                startAnimation(m_animState_JumpPeak, 0, 0.16f);

                if (m_movementManager.GetVerticalVelocity() < 0)
                {
                    startAnimation(m_animState_JumpDescend, 0, 0.2f);
                }
            }
        }

        if (m_movementManager.IsJumping() == false && m_movementManager.HasGround())
        {
            float _horizontalVelocitySqrMag = _horizontalVelocity.sqrMagnitude;

            _eulerAngles = transform.localEulerAngles;

            if (_horizontalVelocitySqrMag > 0.1f)
            {
                Vector3 _velocityFacingCross = Vector3.Cross(m_movementManager.GetHorizontalVelocity(), m_movementManager.GetMovementInputVector());
                if (_velocityFacingCross.sqrMagnitude > m_smoothedVelocityCross.sqrMagnitude)
                {
                    m_smoothedVelocityCross = Vector3.Slerp(m_smoothedVelocityCross, _velocityFacingCross, Time.deltaTime * GameTime.GameTimeScale * 2f);
                }
                else
                {
                    m_smoothedVelocityCross = Vector3.Slerp(m_smoothedVelocityCross, _velocityFacingCross, Time.deltaTime * GameTime.GameTimeScale * 4f);
                }

                _eulerAngles.z = m_smoothedVelocityCross.y * -11;
                _eulerAngles.z = Mathf.Clamp(_eulerAngles.z, -15, 15);

                _eulerAngles.x = _horizontalVelocitySqrMag * 0.07f;
            }
            else
            {
                m_smoothedVelocityCross = Vector3.zero;
                _eulerAngles.z = 0;
                _eulerAngles.x = 0;
            }

            transform.localEulerAngles = _eulerAngles;

            if (m_movementManager.IsRunning())
            {
                _floatValue_horizontalVelocity = _horizontalVelocitySqrMag / 2.5f + 0.4f;

                if (_floatValue_horizontalVelocity > 0.4f)
                {
                    startAnimation(m_animState_Run, 0, 0.2f);
                    m_animator.SetFloat(m_param_float_animSpeedMultiplier, _floatValue_horizontalVelocity / 8.3f);
                }
                else
                {
                    startAnimation(m_animState_Idle, 0, 0.3f);
                }
            }
            else
            {
                float _normalizedMovementSpeed = m_movementManager.GetNormalizedMovementSpeed();

                if (_normalizedMovementSpeed > 0.01f)
                {
                    startAnimation(m_animState_Sneak, 0, 0.2f);
                    m_animator.SetFloat(m_param_float_animSpeedMultiplier, _normalizedMovementSpeed * 2.65f);
                    m_animator.SetFloat(m_param_float_normalizedMovementSpeed, _normalizedMovementSpeed);

                }
                else
                {
                    startAnimation(m_animState_Idle, 0, 0.3f);
                }
            }
        }

        if (_deformScale) deformScale();
        else
        {
            m_landedTimeRemaining = 0.0f;
        }
    }

    private void wallrunning()
    {
        Vector3 _horizontalVelocity = m_movementManager.GetHorizontalVelocity();
        float _verticalVelocity = m_movementManager.GetVerticalVelocity();
        float _floatValue_horizontalVelocity = 0.0f;

        PlayerMovementManager.WallRunInfo _wallRunInfo = m_movementManager.GetWallRunInfo();

        Vector3 _lookDir = _horizontalVelocity.normalized;
        Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
        transform.rotation = Quaternion.Lerp(transform.rotation, _targetDirection, Time.deltaTime * GameTime.GameTimeScale * 50);

        float _horizontalVelocitySqrMag = _horizontalVelocity.sqrMagnitude;
        _floatValue_horizontalVelocity = _horizontalVelocitySqrMag / 5.9f + 1.4f;

        Vector3 _position = transform.position;
        Vector3 _targetPosition = _wallRunInfo.PointOnWall + _wallRunInfo.WallNormal * m_wallRunXOffset;
        _position = Vector3.MoveTowards(_position, _targetPosition, m_wallRunXOffsetResponsiveness * Time.deltaTime * GameTime.GameTimeScale);
        transform.position = _position;

        Vector3 _eulerAngles = transform.localEulerAngles;
        float _angleZ = _eulerAngles.z;

        switch (_wallRunInfo.WallRunSide)
        {
            case -1:
                startAnimation(m_animState_WallRunLeft, 0, 0.1f);
                _angleZ = Mathf.Lerp(0, -m_wallRunAngleZ, _wallRunInfo.WallRunTime / m_wallRunAngleRotationTime);
                break;

            case 1:
                startAnimation(m_animState_WallRunRight, 0, 0.1f);
                _angleZ = Mathf.Lerp(0, m_wallRunAngleZ, _wallRunInfo.WallRunTime / m_wallRunAngleRotationTime);
                break;
        }

        _eulerAngles.z = _angleZ;
        transform.localEulerAngles = _eulerAngles;
    }

    private void verticalWallContact()
    {
        Vector3 _horizontalLookDir = m_movementManager.GetWallNormal() * -1;
        _horizontalLookDir.y = 0;
        _horizontalLookDir.Normalize();
        transform.forward = _horizontalLookDir;

        transform.position += m_movementManager.GetWallNormal() * 0.17f;

        float _verticalVelocity = m_movementManager.GetVerticalVelocity();

        if (_verticalVelocity > 0)
        {
            startAnimation(m_animState_WallRunVertical, 0, 0.1f);
            m_animator.SetFloat(m_param_float_animSpeedMultiplier, _verticalVelocity * 1.1f);
        }
        else
        {
            startAnimation(m_animState_WallRunSlideDown, 0, 0.7f);
        }
    }

    private void deformScale()
    {
        if (m_movementManager.HasGround() == false)
        {
            Vector3 _newScale = new Vector3(1, 1, 1);

            float _verticalVelAbs = Mathf.Abs(m_movementManager.GetVerticalVelocity());

            bool _jumpStart = false;
            float _jumpStartThreshold = 6f;

            if (m_movementManager.IsJumping() && m_movementManager.GetVerticalVelocity() > _jumpStartThreshold)
            {
                _jumpStart = true;
            }

            if (_jumpStart)
            {
                _newScale.x += 0.04f * (_verticalVelAbs - _jumpStartThreshold);
                _newScale.z += 0.04f * (_verticalVelAbs - _jumpStartThreshold);
                _newScale.y -= 0.06f * (_verticalVelAbs - _jumpStartThreshold);
            }
            else
            {
                float _threshold = 3.4f;

                if (_verticalVelAbs < _threshold)
                {
                    _newScale.x += 0.01f * (_threshold - _verticalVelAbs);
                    _newScale.z += 0.01f * (_threshold - _verticalVelAbs);
                    _newScale.y -= 0.02f * (_threshold - _verticalVelAbs);
                }
                else
                {
                    _verticalVelAbs = Mathf.Clamp(_verticalVelAbs, 0, 14);

                    _newScale.x -= 0.01f * (_verticalVelAbs - _threshold);
                    _newScale.z -= 0.01f * (_verticalVelAbs - _threshold);
                    _newScale.y += 0.025f * (_verticalVelAbs - _threshold);
                }
            }

            transform.localScale = _newScale;
        }
        else
        {
            if (m_landedTimeRemaining > 0.0f)
            {
                m_landedTimeRemaining -= Time.deltaTime * GameTime.GameTimeScale;
                float _normalized = 1.0f - m_landedTimeRemaining / m_onLandingScaleDeformTime;
                float _pointOnCurve = m_curveOnLandingScaleDeform.Evaluate(_normalized);

                Vector3 _newScale = new Vector3(1, 1, 1);

                float _verticalVelAbs = _pointOnCurve * m_landedIntensity;

                _newScale.x += _verticalVelAbs * 0.13f;
                _newScale.z += _verticalVelAbs * 0.13f;

                _newScale.y -= _verticalVelAbs * 0.112f;

                transform.localScale = _newScale;
            }
        }
    }

    private void startAnimation(string animState, int layer, float crossfadeTime)
    {
        switch (layer)
        {
            case 0:

                if (m_currentAnimState_Layer0 == animState) return;

                if (crossfadeTime == 0.0f) m_animator.Play(animState, layer);
                else m_animator.CrossFadeInFixedTime(animState, crossfadeTime, layer);

                m_currentAnimState_Layer0 = animState;

                break;

            case 1:

                if (m_currentAnimState_Layer1 == animState) return;

                if (crossfadeTime == 0.0f) m_animator.Play(animState, layer);
                else m_animator.CrossFadeInFixedTime(animState, crossfadeTime, layer);

                m_currentAnimState_Layer1 = animState;

                break;
        }
    }

    public void OnLandedToGround(float verticalVelocity)
    {
        m_landedTimeRemaining = m_onLandingScaleDeformTime;
        m_landedIntensity = Mathf.Lerp(Mathf.Abs(verticalVelocity), 2.8f, 0.8f);
    }

    public void ResetLandingDeform()
    {
        m_landedTimeRemaining = 0.0f;
    }

    public void StartBlockStanceAnimation()
    {
        m_aimManager.ForceStopAiming();
        m_weaponManager.ForceUndrawWeapon();
        m_renderer_Drawn_LightPistol.enabled = true;

        m_animator.SetLayerWeight(1, 0);
        startAnimation(m_animState_BlockStance_Start, 0, 0.02f);
    }

    public void StartParryCounterAnimation(EnemyEntity enemyEntity)
    {
        m_animRoutine = StartCoroutine(routine_ParryCounter_LightPistol(enemyEntity));
    }

    public void StartClimbAnimation(int variation, Vector3 targetPosition)
    {
        m_locked = true;

        m_animator.SetLayerWeight(1, 0);

        Vector3 _lookDir = targetPosition - transform.position;
        _lookDir.y = 0;
        _lookDir.Normalize();

        Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
        transform.rotation = _targetDirection;

        switch (variation)
        {
            case 0:
                startAnimation(m_animState_ClimbBoost, 0, 0.0f);
                break;

            case 1:
                startAnimation(m_animState_ClimbEasy, 0, 0.02f);
                break;
        }
    }

    public void StartTakedownAnimation(PlayerTakedownManager.TakedownType takedownType, Vector3 targetPosition, EnemyEntity enemy)
    {
        m_locked = true;

        Vector3 _lookDir = targetPosition - transform.position;
        _lookDir.y = 0;
        _lookDir.Normalize();

        Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
        transform.rotation = _targetDirection;

        switch (takedownType)
        {
            case PlayerTakedownManager.TakedownType.air:
                startAnimation(m_animState_TakedownAir, 0, 0.05f);
                m_animRoutine = StartCoroutine(routine_AirTakedown(targetPosition, enemy));
                CameraTransitionController.GetInstance().ActivateAirTakedownCamera();
                break;

            case PlayerTakedownManager.TakedownType.behindGrabLightMeleeWeapon:
                startAnimation(m_animState_TakedownBehindGrabLightMeleeWeapon, 0, 0.05f);
                m_animRoutine = StartCoroutine(routine_BehindGrabLightMeleeWeapon(targetPosition, enemy));
                break;
        }
    }

    public void EndLockedAnimation()
    {
        m_locked = false;
    }

    public void TriggerKnockbackAnimation(Transform source)
    {
        m_animRoutine = StartCoroutine(routine_Knockback(source));
    }

    public bool IsLocked()
    {
        return m_locked;
    }

    public bool IsCinematicAnimationRunning()
    {
        bool _animRoutine = m_animRoutine != null;
        bool _parryRoutineRunning =  (m_blockManager.IsBlocking() || m_blockManager.IsParryTriggered());

        if (_animRoutine == false && _parryRoutineRunning == false)
        {
            return false;
        }

        return true;
    }

    private IEnumerator routine_AirTakedown(Vector3 targetPosition, EnemyEntity enemy)
    {
        m_animator.SetLayerWeight(1, 0);

        Vector3 _startPosition = m_rigid.position;
        _startPosition.y = targetPosition.y + (_startPosition.y - targetPosition.y) * 0.62f;

        float _timer = 0.0f;
        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale * 2;
            m_rigid.position = Vector3.Lerp(_startPosition, targetPosition, _timer);
            yield return new WaitForEndOfFrame();
        }

        m_rigid.position = targetPosition;

        bool _triggerDeath = false;
        bool _triggerScreenShake = false;
        bool _triggerHitFreeze = false;
        bool _animationRunning = true;

        while (_animationRunning)
        {
            if (_triggerDeath == false && m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.33f)
            {
                _triggerDeath = true;
                enemy.TriggerDeath();
            }

            if (_triggerScreenShake == false && m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.32f)
            {
                _triggerScreenShake = true;
                m_playerVFX.TriggerScreenShake();
            }

            if (_triggerHitFreeze == false && m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= m_hitFreeze_AirTakedown_StartTime)
            {
                _triggerHitFreeze = true;
                GameTime.GetInstance().TriggerHitFreeze(this, 10, m_hitFreeze_AirTakedown_Length, m_hitFreeze_AirTakedown_LowestTimeScale, ref m_hitFreeze_AirTakedown_Curve);
            }

            if (m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.72f)
            {
                _animationRunning = false;
            }

            yield return new WaitForEndOfFrame();
        }

        m_movementManager.SetPhysicsEnabled(true);

        CameraTransitionController.GetInstance().ActivateFollowCamera();

        m_animator.SetLayerWeight(1, 1);

        m_locked = false;
        m_animRoutine = null;
    }

    private IEnumerator routine_BehindGrabLightMeleeWeapon(Vector3 targetPosition, EnemyEntity enemy)
    {
        m_animator.SetLayerWeight(1, 0);

        Vector3 _startPosition = m_rigid.position;

        bool _triggerDeath = false;
        bool _playerGrabbedSword = false;
        bool _swordStuckToEnemy = false;

        Cinematic_TakedownBehindGrabLightMeleeWeapon.SetActive(true);

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(m_animState_TakedownBehindGrabLightMeleeWeapon) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        while (_stateInfo.IsName(m_animState_TakedownBehindGrabLightMeleeWeapon) && _stateInfo.normalizedTime < 0.9f)
        {
            Cinematic_TakedownBehindGrabLightMeleeWeapon.SetTimelinePosition(_stateInfo.normalizedTime);

            if (_stateInfo.normalizedTime < 0.1f)
            {
                m_rigid.position = Vector3.Lerp(_startPosition, targetPosition - transform.forward * 1.0f, _stateInfo.normalizedTime * 10f);
            }

            if (_playerGrabbedSword == false)
            {
                if (_stateInfo.normalizedTime >= 0.4f)
                {
                    enemy.VFXComponent.SetLightMeleeWeaponParent(m_leftHandBone, new Vector3(-0.01f, 0.003f, -0.002f), new Vector3(90, 0, -90), true);
                    _playerGrabbedSword = true;
                }
            }

            if (_swordStuckToEnemy == false)
            {
                if (_stateInfo.normalizedTime >= 0.662f)
                {
                    enemy.VFXComponent.SetLightMeleeWeaponParentToUpperChest();
                    _swordStuckToEnemy = true;
                    m_playerVFX.TriggerScreenShake();
                }
            }

            if (_triggerDeath == false)
            {
                if (_stateInfo.normalizedTime >= 0.78f)
                {
                    _triggerDeath = true;
                    enemy.TriggerDeath();
                }
            }

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        float _timer = 0.0f;
        while (_timer < 1.0f)
        {
            Cinematic_TakedownBehindGrabLightMeleeWeapon.SetTimelinePosition(0.9f + (_timer / 10));

            _timer += Time.deltaTime * GameTime.GameTimeScale * 10;
            yield return new WaitForEndOfFrame();
        }

        Cinematic_TakedownBehindGrabLightMeleeWeapon.SetActive(false);

        m_rigid.position = m_rootBone.position;

        m_animator.SetLayerWeight(1, 1);

        m_movementManager.SetPhysicsEnabled(true);
        m_locked = false;
        m_animRoutine = null;
    }

    private IEnumerator routine_Knockback(Transform source)
    {
        m_locked = true;

        m_animator.SetLayerWeight(1, 0);
        startAnimation(m_animState_Knockback_Light, 0, 0.02f);

        Vector3 _movementDir = source.position - m_rigid.position;
        _movementDir.y = 0.1f;
        _movementDir.Normalize();

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(m_animState_Knockback_Light) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        while (_stateInfo.IsName(m_animState_Knockback_Light) && _stateInfo.normalizedTime < 1.0f)
        {
            Vector3 _dir = source.position - m_rigid.position;
            _dir.y = 0.01f;
            _dir.Normalize();

            float _rotAmount = m_knockbackLight_RotationSpeed * Time.deltaTime * GameTime.GameTimeScale;
            transform.forward = Vector3.Lerp(transform.forward, _dir, m_knockbackLight_RotationSpeed);
            transform.forward = new Vector3(transform.forward.x, 0, transform.forward.z);

            Vector3 _targetForce = -_movementDir * m_knockbackLight_MovementSpeed * Time.fixedDeltaTime * GameTime.GameTimeScale * m_curve_KnockbackLight_Movement.Evaluate(_stateInfo.normalizedTime);
            m_movementManager.AnimationMovement(_targetForce);

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        m_animator.SetLayerWeight(1, 1);

        m_locked = false;
        m_animRoutine = null;
    }

    private IEnumerator routine_ParryCounter_LightPistol(EnemyEntity enemyEntity)
    {
        m_locked = true;

        yield return null;

        m_animator.SetLayerWeight(1, 0);
        startAnimation(m_animState_ParryCounter_LightPistol, 0, 0.02f);

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(m_animState_ParryCounter_LightPistol) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        float _fireTime = 0.13f;
        bool _weaponFired = false;

        while (_stateInfo.IsName(m_animState_ParryCounter_LightPistol) && _stateInfo.normalizedTime < 1.0f)
        {
            if (_weaponFired == false && _stateInfo.normalizedTime > _fireTime)
            {
                _weaponFired = true;

                WeaponComponent.FireParameters _parameters;
                _parameters.AimTarget = enemyEntity.GetCriticalBodyPart().position;
                _parameters.WaitOneFrameForVFX = false;
                _parameters.TriggerStunState = true;
                _parameters.OverrideHitTarget = enemyEntity.GetCriticalBodyPart().gameObject;

                m_weaponManager.ForceFire(_parameters);
            }

            m_blockManager.SetParryCounterAnimationState(_stateInfo.normalizedTime);

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        m_animRoutine = null;
        m_locked = false;
    }
}
