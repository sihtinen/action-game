﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyCameraProperties : MonoBehaviour
{
    [SerializeField] private LayerMask m_renderLayers;
    [SerializeField] private RenderingPath m_renderPath;
    [SerializeField] private CameraClearFlags m_clearFlags;

    [SerializeField] private Camera m_targetCamera;

    private Camera m_camera = null;

    private void Awake()
    {
        m_camera = GetComponent<Camera>();
    }

    private void LateUpdate()
    {
        m_camera.CopyFrom(m_targetCamera);
        m_camera.cullingMask = m_renderLayers;
        m_camera.renderingPath = m_renderPath;
        m_camera.clearFlags = m_clearFlags;
    }
}
