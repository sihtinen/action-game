﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ParryCamera : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private CinemachineVirtualCamera m_followCamera;
    [SerializeField] private CinemachineVirtualCamera m_parryCamera;
    [SerializeField] private Transform m_parryLookTarget;

    private CinematicController m_cinematicController = null;
    private FollowCamera m_followCameraComponent = null;

    private void Awake()
    {
        m_cinematicController = GetComponent<CinematicController>();
        m_parryLookTarget.parent = null;
    }

    private void Start()
    {
        m_followCameraComponent = m_followCamera.gameObject.GetComponent<FollowCamera>();
        SetActive(false);
    }

    public void SetActive(bool newState, Vector3 lookPosition = new Vector3())
    {
        m_cinematicController.SetActive(newState);

        if (newState)
        {
            m_parryLookTarget.position = lookPosition + Vector3.up * 0.1f;

            Vector3 _newForward = m_parryLookTarget.position - GameState.GetInstance().PlayerObject.transform.position;
            _newForward.y = 0;
            _newForward.Normalize();
            m_parryLookTarget.forward = _newForward;
        }
        else
        {
            m_parryCamera.enabled = false;
        }
    }

    public void SetCinematicNormalizedPosition(float position)
    {
        m_cinematicController.SetTimelinePosition(position);
    }

    public void SetFollowCameraDirection(Vector3 newForward, float verticalAngle)
    {
        m_followCamera.transform.forward = newForward;
        m_followCameraComponent.ApplyRotation(m_followCamera.transform.rotation);
    }

    public void ApplyCurrentRotationToFollowCamera()
    {
        m_followCameraComponent.ApplyRotation(m_parryCamera.transform.rotation);
    }

    public void ApplyCurrentPositionToFollowCamera()
    {
        m_followCameraComponent.ApplyPosition(m_parryCamera.transform.position);
    }
}
