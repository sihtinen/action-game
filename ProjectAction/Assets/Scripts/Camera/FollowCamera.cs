﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FollowCamera : MonoBehaviour
{
    [System.Serializable]
    public class FollowSettings
    {
        public float CameraMoveSpeed = 200;
        public float InputSensitivity = 135;

        [Space]

        public Vector3 DistanceToTarget;
        public float DistanceChangeSpeed = 5.0f;

        [Space]

        public Vector2 ClampAngles;
        public float ClampCorrectSpeed = 10.0f;

        [Space]

        public float FieldOfView = 42;
        public float FieldOfView_AdjustSpeed = 6.0f;
    }

    private static FollowCamera m_instance = null;
    public static FollowCamera GetInstance() { return m_instance; }

    [Header("General Settings")]
    public FollowSettings m_settings_Default;
    public FollowSettings m_settings_Run;
    public FollowSettings m_settings_Aim;
    public FollowSettings m_settings_Block;

    [Header("Object References")]
    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private CameraCollision m_cameraCollision;

    private Vector2 m_currentClampAngles;
    private Vector3 m_currentFollowOffset;
    private Vector3 m_recoilRotation = Vector3.zero;

    private FollowSettings m_currentFollowSettings = null;

    private float m_finalInputX, m_finalInputY;
    private float m_currentFOV;

    private float m_rotX = 0.0f;
    private float m_rotY = 0.0f;

    private Quaternion m_finalRotation;

    private float m_normalizedAimAngleHorizontal = 0.0f;
    private float m_normalizedAimAngleVertical = 0.0f;

    private CustomPlayerInputManager m_inputManager = null;
    private PlayerMovementManager m_movementManager = null;
    private PlayerAimManager m_aimManager = null;
    private PlayerParryBlockManager m_blockManager = null;
    private PlayerAnimationController m_animController = null;
    private Transform m_followTarget = null;

    private CinemachineVirtualCamera m_virtualCamera = null;
    private Coroutine m_routineRecoil = null;

    private void Awake()
    {
        m_instance = this;
        m_currentFollowSettings = m_settings_Default;
        m_currentClampAngles = m_currentFollowSettings.ClampAngles;
        m_currentFollowOffset = m_currentFollowSettings.DistanceToTarget;
    }

    private void Start()
    {
        Vector3 _rot = transform.localRotation.eulerAngles;
        m_rotY = _rot.y;
        m_rotX = _rot.x;

        m_finalRotation = transform.rotation;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        m_inputManager = CustomPlayerInputManager.GetInstance();
        m_movementManager = m_inputManager.gameObject.GetComponent<PlayerMovementManager>();
        m_aimManager = m_inputManager.gameObject.GetComponent<PlayerAimManager>();
        m_blockManager = m_inputManager.gameObject.GetComponent<PlayerParryBlockManager>();
        m_animController = m_inputManager.transform.GetChild(0).GetComponent<PlayerAnimationController>();
        m_followTarget = CameraFollowTarget.PlayerFollowTarget;

        m_virtualCamera = GetComponent<CinemachineVirtualCamera>();
        m_currentFOV = m_virtualCamera.m_Lens.FieldOfView;
    }

    private void LateUpdate()
    {
        m_currentFollowSettings = m_settings_Default;

        if (m_animController.IsLocked() == false)
        {
            if (m_aimManager.IsAiming())
            {
                m_currentFollowSettings = m_settings_Aim;
            }
            else if (m_movementManager.IsRunning())
            {
                m_currentFollowSettings = m_settings_Run;
            }
            else if (m_blockManager.IsBlocking())
            {
                m_currentFollowSettings = m_settings_Block;
            }
        }

        Vector2 _mouseInput = m_inputManager.GetMouseInput() * GameTime.GameTimeScale;
        Vector3 _rightAnalogue = m_inputManager.GetRightAnalogue();

        m_finalInputX = _mouseInput.x / 60;
        m_finalInputY = _mouseInput.y * -1 / 60;

        m_finalInputX += _rightAnalogue.x * Time.unscaledDeltaTime * GameTime.GameTimeScale;
        m_finalInputY += _rightAnalogue.y * Time.unscaledDeltaTime * GameTime.GameTimeScale;

        if (m_animController.IsCinematicAnimationRunning() == false)
        {
            m_rotY += m_finalInputX * m_currentFollowSettings.InputSensitivity;
            m_rotX += m_finalInputY * m_currentFollowSettings.InputSensitivity;
        }

        m_currentClampAngles = Vector2.Lerp(m_currentClampAngles, m_currentFollowSettings.ClampAngles, Time.unscaledDeltaTime * GameTime.GameTimeScale * m_currentFollowSettings.ClampCorrectSpeed);

        m_rotX = Mathf.Clamp(m_rotX, m_currentClampAngles.x, m_currentClampAngles.y);

        if (m_rotX >= 0)
            m_normalizedAimAngleVertical = m_rotX / m_currentClampAngles.y * -1;
        else
            m_normalizedAimAngleVertical = (Mathf.Abs(m_rotX) / Mathf.Abs(m_currentClampAngles.x));

        Vector3 _horizontalForward = transform.forward;
        _horizontalForward.y = 0;
        float _rawAngle = Vector3.Angle(m_animController.transform.forward, _horizontalForward);
        float _horizontalAimDirection = angleDir(m_animController.transform.forward, _horizontalForward, Vector3.up);

        if (_horizontalAimDirection == 0.0f) m_normalizedAimAngleHorizontal = 0.0f;
        else
        {
            m_normalizedAimAngleHorizontal = _rawAngle / 180.0f * _horizontalAimDirection;
        }

        MainCameraComponent.NormalizedAimAngleHorizontal = m_normalizedAimAngleHorizontal;
        MainCameraComponent.NormalizedAimAngleVertical = m_normalizedAimAngleVertical;

        m_finalRotation = Quaternion.Euler(m_rotX + m_recoilRotation.x, m_rotY + m_recoilRotation.y, 0.0f);
        transform.rotation = m_finalRotation;

        m_currentFollowOffset = Vector3.Lerp(m_currentFollowOffset, m_currentFollowSettings.DistanceToTarget, Time.unscaledDeltaTime * GameTime.GameTimeScale * m_currentFollowSettings.DistanceChangeSpeed);
        Vector3 _targetPosition = m_followTarget.position
            + transform.right * m_currentFollowOffset.x
            + transform.up * m_currentFollowOffset.y
            + transform.forward * -m_currentFollowOffset.z;

        if (m_cameraCollision)
            _targetPosition = m_cameraCollision.CollisionCorrection(m_followTarget.position, _targetPosition);

        float _moveStep = m_currentFollowSettings.CameraMoveSpeed * Time.unscaledDeltaTime * GameTime.GameTimeScale;
        transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _moveStep);
        transform.position = m_cameraCollision.CollisionCorrection(m_followTarget.position, transform.position);

        PlayerMovementManager.WallRunInfo _wallRunInfo = m_movementManager.GetWallRunInfo();
        if (_wallRunInfo.WallRunning)
        {
            Vector3 _horizontalDir = m_movementManager.GetHorizontalVelocity();
            _horizontalDir.Normalize();

            float _lookingToDirDot = Vector3.Dot(_horizontalDir, MainCameraComponent.GetMovementForward());

            if (Mathf.Abs(_lookingToDirDot) > 0.7f)
            {
                int _dir = 1;
                if (_lookingToDirDot < 0) _dir = -1;

                m_virtualCamera.m_Lens.Dutch = Mathf.Lerp(m_virtualCamera.m_Lens.Dutch, _dir * _wallRunInfo.WallRunSide * 7, Time.unscaledDeltaTime * GameTime.GameTimeScale * 14);
            }
            else
            {
                m_virtualCamera.m_Lens.Dutch = Mathf.Lerp(m_virtualCamera.m_Lens.Dutch, 0, Time.unscaledDeltaTime * GameTime.GameTimeScale * 20);
            }
        }
        else
        {
            m_virtualCamera.m_Lens.Dutch = Mathf.Lerp(m_virtualCamera.m_Lens.Dutch, 0, Time.unscaledDeltaTime * GameTime.GameTimeScale * 20);
        }

        m_currentFOV = Mathf.Lerp(m_currentFOV, m_currentFollowSettings.FieldOfView, Time.unscaledDeltaTime * GameTime.GameTimeScale * m_currentFollowSettings.FieldOfView_AdjustSpeed);
        m_virtualCamera.m_Lens.FieldOfView = m_currentFOV;
    }

    private float angleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 right = Vector3.Cross(up, fwd);
        float dir = Vector3.Dot(right, targetDir);

        if (dir > 0f)
        {
            return 1f;
        }
        else if (dir < 0f)
        {
            return -1f;
        }
        else
        {
            return 0f;
        }
    }

    public void RecoilImpulse(PlayerAimManager.RecoilImpulse impulse)
    {
        if (m_routineRecoil != null) StopCoroutine(m_routineRecoil);
        m_routineRecoil = StartCoroutine(routine_Recoil(impulse));
    }

    public void ApplyRotation(Quaternion rotation)
    {
        transform.rotation = rotation;

        m_rotX = transform.eulerAngles.x;
        m_rotY = transform.eulerAngles.y;
        m_finalRotation = Quaternion.Euler(m_rotX, m_rotY, 0.0f);
        transform.rotation = m_finalRotation;

        LateUpdate();
    }

    public void ApplyPosition(Vector3 position)
    {
        transform.position = position;
        LateUpdate();
    }

    private IEnumerator routine_Recoil(PlayerAimManager.RecoilImpulse impulse)
    {
        float _timer = 0.0f;

        float _startX = m_recoilRotation.x;
        float _startY = m_recoilRotation.y;
        float _goalX = _startX + Random.Range(-impulse.MaxVertical, -impulse.MinVertical);
        float _goalY = _startY + Random.Range(impulse.MinHorizontal, impulse.MaxHorizontal);
        int _direction = Random.Range(0, 2) * 2 - 1;
        _goalY *= _direction;

        bool _routineRunning = true;
        while (_routineRunning)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale;

            if (_timer > impulse.ImpulseTime)
            {
                _routineRunning = false;
            }
            else
            {
                float _normalized = _timer / impulse.ImpulseTime;
                float _curveValue = impulse.RecoilCurve.Evaluate(_normalized);

                m_recoilRotation.x = Mathf.Lerp(_startX, _goalX, _curveValue);
                m_recoilRotation.y = Mathf.Lerp(_startY, _goalY, _curveValue);
            }

            yield return new WaitForEndOfFrame();
        }

        m_rotX += m_recoilRotation.x;
        m_rotY += m_recoilRotation.y;
        m_recoilRotation = Vector3.zero;

        m_routineRecoil = null;
    }
}
