﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour
{
    [SerializeField] private LayerMask m_cameraCollisionLayers;

    private Vector3 m_dollyDir;
    private Vector3 m_dollyDirAdjusted;

    private RaycastHit[] m_raycastHits = new RaycastHit[100];

    private float m_distance;

    private void Awake()
    {
        m_dollyDir = transform.localPosition.normalized;
        m_distance = transform.localPosition.magnitude;
    }

    public Vector3 CollisionCorrection(Vector3 cameraFollowTargetPosition, Vector3 targetPosition)
    {
        Vector3 _diff = targetPosition - cameraFollowTargetPosition;

        int _hits = Physics.RaycastNonAlloc(
            cameraFollowTargetPosition,
            _diff.normalized,
            m_raycastHits,
            _diff.magnitude,
            m_cameraCollisionLayers,
            QueryTriggerInteraction.Ignore
            );

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(cameraFollowTargetPosition, _hits);
            targetPosition = _closest.point + _closest.normal * 0.1f;
        }

        return targetPosition;
    }

    private RaycastHit getClosestHit(Vector3 targetPos, int hits)
    {
        RaycastHit _result = m_raycastHits[0];
        float _closestDist;

        for (int i = 0; i < hits; i++)
        {
            _closestDist = Vector3.SqrMagnitude(_result.point - targetPos);

            RaycastHit _current = m_raycastHits[i];
            if (Vector3.SqrMagnitude(_current.point - targetPos) < _closestDist)
            {
                _result = _current;
            }
        }

        return _result;
    }
}
