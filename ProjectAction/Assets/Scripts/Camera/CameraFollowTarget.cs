﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    public static Transform PlayerFollowTarget = null;

    [SerializeField] private Transform m_targetTransform;
    [SerializeField] private float m_followHeight_Grounded = 1.8f;
    [SerializeField] private float m_followHeight_Air = 0.2f;

    [SerializeField] private float m_horizontalResponsiveness = 14.0f;
    [SerializeField] private float m_verticalResponsiveness_VelocityUp = 6.0f;
    [SerializeField] private float m_verticalResponsiveness_VelocityDown = 12.0f;

    [Header("Wall Run Settings")]
    [SerializeField] private Vector3 m_wallRunOffset;

    [Header("Object References")]
    [SerializeField] private Transform m_characterMesh;

    private Vector3 m_targetPosition;

    private PlayerMovementManager m_movementManager = null;
    private PlayerAimManager m_aimManager = null;

    private void Awake()
    {
        transform.parent = null;
        PlayerFollowTarget = transform;
    }

    private void Start()
    {
        m_movementManager = GameState.GetInstance().PlayerObject.GetComponent<PlayerMovementManager>();
        m_aimManager = GameState.GetInstance().PlayerObject.GetComponent<PlayerAimManager>();
    }

    private void LateUpdate()
    {
        transform.rotation = m_characterMesh.rotation;

        float _followHeight = m_followHeight_Grounded;
        if (m_movementManager.HasGround() == false && 
            m_movementManager.GetWallRunInfo().WallRunning == false && 
            m_aimManager.IsAiming() == false &&
            m_movementManager.JumpedFromGround == false)
        {
            _followHeight = m_followHeight_Air;
        }

        m_targetPosition = m_targetTransform.position + Vector3.up * _followHeight;

        if (m_movementManager.IsJumping() && 
            m_movementManager.JumpedFromGround && 
            m_aimManager.IsAiming() == false)
        {
            float _difference = m_targetTransform.position.y - m_movementManager.GetPreviousGroundHeight();
            m_targetPosition.y = m_movementManager.GetPreviousGroundHeight() + _followHeight + _difference * 0.55f;
        }

        PlayerMovementManager.WallRunInfo _wallRunInfo = m_movementManager.GetWallRunInfo();
        if (_wallRunInfo.WallRunning)
        {
            m_targetPosition += _wallRunInfo.WallNormal * 1.2f * Mathf.Clamp01(_wallRunInfo.WallRunTime * 2.4f);
        }

        Vector3 _currentPosition = Vector3.Lerp(transform.position, m_targetPosition, m_horizontalResponsiveness * Time.unscaledDeltaTime * GameTime.GameTimeScale);

        if (m_movementManager.GetVerticalVelocity() >= -2)
        {
            _currentPosition.y = Mathf.Lerp(transform.position.y, m_targetPosition.y, m_verticalResponsiveness_VelocityUp * Time.unscaledDeltaTime * GameTime.GameTimeScale);
        }
        else
        {
            _currentPosition.y = Mathf.Lerp(transform.position.y, m_targetPosition.y, m_verticalResponsiveness_VelocityDown * Time.unscaledDeltaTime * GameTime.GameTimeScale);
        }

        transform.position = _currentPosition;
    }
}
