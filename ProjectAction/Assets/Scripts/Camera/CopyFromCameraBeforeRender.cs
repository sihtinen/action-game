﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyFromCameraBeforeRender : MonoBehaviour
{
    [SerializeField] private Camera m_targetCamera;
    [SerializeField] private LayerMask m_renderMask;
    [SerializeField] private Material m_renderMaterial;

    private Camera m_myCamera;

    private void Awake()
    {
        m_myCamera = GetComponent<Camera>();
    }

    private void Start()
    {
        m_targetCamera = MainCameraComponent.MainCamera;
    }

    public void ManualRender()
    {
        transform.position = m_targetCamera.transform.position;
        transform.rotation = m_targetCamera.transform.rotation;
        m_myCamera.fieldOfView = m_targetCamera.fieldOfView;

        m_myCamera.enabled = true;
        m_myCamera.Render();
        m_myCamera.enabled = false;
    }

    private void OnPreRender()
    {
        transform.position = m_targetCamera.transform.position;
        transform.rotation = m_targetCamera.transform.rotation;
        m_myCamera.fieldOfView = m_targetCamera.fieldOfView;
    }

    [ImageEffectOpaque]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, m_renderMaterial);
    }
}
