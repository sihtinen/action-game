﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraTransitionController : MonoBehaviour
{
    private static CameraTransitionController m_instance = null;
    public static CameraTransitionController GetInstance() { return m_instance; }

    [SerializeField] private Camera m_mainCamera;

    [SerializeField] private CinemachineVirtualCamera m_followCamera;
    [SerializeField] private CinemachineVirtualCamera m_airTakedownCamera;

    private void Awake()
    {
        m_instance = this;
    }

    public void ActivateFollowCamera()
    {
        disableAllCameras();

        m_followCamera.transform.position = m_mainCamera.transform.position;
        m_followCamera.enabled = true;
    }

    public void ActivateAirTakedownCamera()
    {
        disableAllCameras();

        m_airTakedownCamera.transform.position = m_mainCamera.transform.position;
        m_airTakedownCamera.enabled = true;
    }

    private void disableAllCameras()
    {
        m_followCamera.enabled = false;
        m_airTakedownCamera.enabled = false;
    }
}
