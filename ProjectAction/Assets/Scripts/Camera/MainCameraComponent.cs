﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class MainCameraComponent : MonoBehaviour
{
    private static MainCameraComponent m_instance = null;
    public static MainCameraComponent GetInstance() { return m_instance; }

    public static Camera MainCamera;
    public static Vector3 Forward, Right, Up;

    public static float NormalizedAimAngleVertical;
    public static float NormalizedAimAngleHorizontal;

    [SerializeField] private Material m_matVisibilityLine;
    [SerializeField] private LayerMask m_enemyOutlineLayer;

    private Transform m_playerTransform = null;
    private PlayerVisibilityComponent m_playerVisibility = null;

    private void Awake()
    {
        m_instance = this;
        MainCamera = GetComponent<Camera>();

        MainCamera.useOcclusionCulling = false;
        MainCamera.depthTextureMode = DepthTextureMode.Depth | DepthTextureMode.DepthNormals;
        Debug.LogWarning("Main Camera Occlusion Culling = false, please fix if needed");
    }

    private void Start()
    {
        m_playerTransform = GameState.GetInstance().PlayerObject.transform;
        m_playerVisibility = m_playerTransform.GetComponent<PlayerVisibilityComponent>();
    }

    private void LateUpdate()
    {
        Forward = transform.forward;
        Right = transform.right;
        Up = transform.up;
    }

    public static Vector3 GetMovementForward()
    {
        Vector3 _result = Forward;
        _result.y = 0;
        _result.Normalize();

        return _result;
    }

    public static Vector3 GetMovementRight()
    {
        Vector3 _result = Right;
        _result.y = 0;
        _result.Normalize();

        return _result;
    }

    private void OnPreRender()
    {
        m_matVisibilityLine.SetVector("_PlayerWorldPosition", m_playerTransform.position);
        m_matVisibilityLine.SetFloat("_VisibilityRange", m_playerVisibility.GetCurrentVisibilityDistance());
        Matrix4x4 _viewToWorld = MainCamera.cameraToWorldMatrix;
        m_matVisibilityLine.SetMatrix("_ViewToWorld", _viewToWorld);
    }

    [ImageEffectOpaque]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        raycastCornerBlit(source, destination, m_matVisibilityLine);
    }

    private void raycastCornerBlit(RenderTexture source, RenderTexture dest, Material mat)
    {
        // Compute Frustum Corners
        float camFar = MainCamera.farClipPlane;
        float camFov = MainCamera.fieldOfView;
        float camAspect = MainCamera.aspect;

        float fovWHalf = camFov * 0.5f;

        Vector3 toRight = MainCamera.transform.right * Mathf.Tan(fovWHalf * Mathf.Deg2Rad) * camAspect;
        Vector3 toTop = MainCamera.transform.up * Mathf.Tan(fovWHalf * Mathf.Deg2Rad);

        Vector3 topLeft = (MainCamera.transform.forward - toRight + toTop);
        float camScale = topLeft.magnitude * camFar;

        topLeft.Normalize();
        topLeft *= camScale;

        Vector3 topRight = (MainCamera.transform.forward + toRight + toTop);
        topRight.Normalize();
        topRight *= camScale;

        Vector3 bottomRight = (MainCamera.transform.forward + toRight - toTop);
        bottomRight.Normalize();
        bottomRight *= camScale;

        Vector3 bottomLeft = (MainCamera.transform.forward - toRight - toTop);
        bottomLeft.Normalize();
        bottomLeft *= camScale;

        // Custom Blit, encoding Frustum Corners as additional Texture Coordinates
        RenderTexture.active = dest;
        mat.SetTexture("_MainTex", source);

        GL.PushMatrix();
        GL.LoadOrtho();

        mat.SetPass(0);

        GL.Begin(GL.QUADS);

        GL.MultiTexCoord2(0, 0.0f, 0.0f);
        GL.MultiTexCoord(1, bottomLeft);
        GL.Vertex3(0.0f, 0.0f, 0.0f);

        GL.MultiTexCoord2(0, 1.0f, 0.0f);
        GL.MultiTexCoord(1, bottomRight);
        GL.Vertex3(1.0f, 0.0f, 0.0f);

        GL.MultiTexCoord2(0, 1.0f, 1.0f);
        GL.MultiTexCoord(1, topRight);
        GL.Vertex3(1.0f, 1.0f, 0.0f);

        GL.MultiTexCoord2(0, 0.0f, 1.0f);
        GL.MultiTexCoord(1, topLeft);
        GL.Vertex3(0.0f, 1.0f, 0.0f);

        GL.End();
        GL.PopMatrix();
    }
}
