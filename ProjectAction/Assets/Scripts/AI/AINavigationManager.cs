﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace ActionAI
{
    public enum NavigationTarget
    {
        None,
        CombatTarget,
        Cover,
        Investigation,
        Waypoint,
        Flank
    }

    public struct CoverPointPathWrapper
    {
        public CoverPointOctree.CoverPoint CoverPoint;
        public float Score_Normal;
        public float Score_Distance;

        public float FinalScore;

        public void CalculateScore()
        {
            FinalScore = Score_Normal + Score_Distance;
        }
    }

    public class AINavigationManager : MonoBehaviour
    {
        private static AINavigationManager m_instance = null;
        public static AINavigationManager GetInstance() { return m_instance; }

        private List<EnemyEntity> m_allEnemies = new List<EnemyEntity>();
        private List<EnvironmentNodeWrapper> m_potentialNodes = new List<EnvironmentNodeWrapper>();

        [SerializeField] private int m_updateRatePerSecond = 2;
        [SerializeField] private int m_calulationsPerCycle = 3;

        [Header("Debug")]
        [SerializeField] private bool m_drawDebug;

        private System.Random m_random = null;
        private NavMeshHit m_navMeshHit;
        private NavMeshPath m_navMeshPath;
        private Vector3[] m_navMeshPathCorners = new Vector3[100];
        private List<Vector3> m_pathResult = new List<Vector3>();

        private void Awake()
        {
            m_instance = this;
            m_random = new System.Random(DateTime.Now.Millisecond);
            m_navMeshPath = new NavMeshPath();
        }

        public void RegisterEnemy(EnemyEntity entity)
        {
            if (m_allEnemies.Contains(entity)) return;
            m_allEnemies.Add(entity);
        }

        private IEnumerator Start()
        {
            bool _running = true;
            while (_running)
            {
                for (int i = 0; i < m_allEnemies.Count; i++)
                {
                    if (m_allEnemies[i].AIEnabled)
                    {
                        m_allEnemies[i].FramesFromLastNavigationUpdate++;
                    }
                    else
                    {
                        m_allEnemies[i].FramesFromLastNavigationUpdate = 0;
                    }
                }

                m_allEnemies = m_allEnemies.OrderByDescending(o => o.FramesFromLastNavigationUpdate).ToList();

                for (int i = 0; i < m_allEnemies.Count; i++)
                {
                    if (m_allEnemies[i].AIEnabled == false) continue;
                    if (m_allEnemies[i].NavTarget == NavigationTarget.None) continue;

                    int _pathLength;

                    switch (m_allEnemies[i].NavTarget)
                    {
                        case NavigationTarget.CombatTarget:

                            NavMesh.CalculatePath(m_allEnemies[i].transform.position, m_allEnemies[i].CombatTarget.position, NavMesh.AllAreas, m_navMeshPath);
                            _pathLength = m_navMeshPath.GetCornersNonAlloc(m_navMeshPathCorners);

                            m_pathResult.Clear();
                            for (int j = 0; j < _pathLength; j++)
                            {
                                m_pathResult.Add(m_navMeshPathCorners[j]);
                            }

                            m_allEnemies[i].MovementComponent.SetPath(m_pathResult, _pathLength);

                            break;

                        case NavigationTarget.Cover:

                            if (m_allEnemies[i].CurrentCoverPoint == null)
                            {
                                findCoverPoint(m_allEnemies[i]);
                            }

                            break;

                        case NavigationTarget.Investigation:

                            NavMesh.CalculatePath(m_allEnemies[i].transform.position, m_allEnemies[i].InvestigationTargetPoint, NavMesh.AllAreas, m_navMeshPath);
                            _pathLength = m_navMeshPath.GetCornersNonAlloc(m_navMeshPathCorners);

                            m_pathResult.Clear();
                            for (int j = 0; j < _pathLength; j++)
                            {
                                m_pathResult.Add(m_navMeshPathCorners[j]);
                            }

                            m_allEnemies[i].MovementComponent.SetPath(m_pathResult, _pathLength);

                            break;

                        case NavigationTarget.Waypoint:

                            NavMesh.CalculatePath(m_allEnemies[i].transform.position, m_allEnemies[i].CurrentWaypoint.GetPosition(), NavMesh.AllAreas, m_navMeshPath);
                            _pathLength = m_navMeshPath.GetCornersNonAlloc(m_navMeshPathCorners);

                            m_pathResult.Clear();
                            for (int j = 0; j < _pathLength; j++)
                            {
                                m_pathResult.Add(m_navMeshPathCorners[j]);
                            }

                            m_allEnemies[i].MovementComponent.SetPath(m_pathResult, _pathLength);

                            break;

                        case NavigationTarget.Flank:

                            if (m_allEnemies[i].CurrentCoverPoint == null)
                            {
                                findFlankPath(m_allEnemies[i]);
                            }

                            break;
                    }

                    m_allEnemies[i].FramesFromLastNavigationUpdate = 0;

                    if (i >= m_calulationsPerCycle)
                    {
                        break;
                    }
                }

                float _timeToWait = 1.0f / m_updateRatePerSecond;
                while (_timeToWait > 0)
                {
                    _timeToWait -= Time.deltaTime * GameTime.GameTimeScale;
                    yield return null;
                }
            }
        }

        private void findCoverPoint(EnemyEntity entity)
        {
            Vector3 _coverPosition = entity.transform.position;

            CoverPointOctree.CoverPoint _bestCoverPoint = CoverPointOctree.GetInstance().GetValidCoverPoint(
                ref entity.CurrentCoverWeightSettings,
                entity.transform.position,
                entity.transform.position,
                entity.CombatTarget.position);

            if (_bestCoverPoint == null) return;

            entity.CurrentCoverPoint = _bestCoverPoint;
            _coverPosition = _bestCoverPoint.Position;

            NavMesh.CalculatePath(entity.transform.position, _coverPosition, NavMesh.AllAreas, m_navMeshPath);
            int _pathLength = m_navMeshPath.GetCornersNonAlloc(m_navMeshPathCorners);

            m_pathResult.Clear();
            for (int j = 0; j < _pathLength; j++)
            {
                m_pathResult.Add(m_navMeshPathCorners[j]);
            }

            entity.MovementComponent.SetPath(m_pathResult, _pathLength);
        }

        private void findFlankPath(EnemyEntity entity)
        {
            CoverPointOctree.CoverPoint _coverPoint = CoverPointOctree.GetInstance().GetValidCoverPoint(
                ref entity.CurrentCoverWeightSettings, 
                entity.CombatTarget.position,
                entity.transform.position,
                entity.CombatTarget.position);

            if (_coverPoint == null)
            {
                UnityEngine.Debug.Log("No suitable cover point found");
                return;
            }

            m_potentialNodes.Clear();
            m_potentialNodes = EnvironmentProbeManager.GetInstance().GenerateClosestNodes(
                _coverPoint.Position,
                ref entity.CurrentProbeSettings,
                m_potentialNodes);

            EnvironmentNodeWrapper _bestWaypoint = CalculateBestWaypoint(
                entity.transform.position,
                _coverPoint.Position,
                entity.CombatTarget.position,
                ref entity.CurrentFlankWeightSettings,
                ref m_potentialNodes);

            NavMesh.CalculatePath(entity.transform.position, _bestWaypoint.Position, NavMesh.AllAreas, m_navMeshPath);
            int _pathLength = m_navMeshPath.GetCornersNonAlloc(m_navMeshPathCorners);
            int _oldPathLength = _pathLength;

            m_pathResult.Clear();
            for (int j = 0; j < _pathLength; j++)
            {
                m_pathResult.Add(m_navMeshPathCorners[j]);
            }

            NavMesh.CalculatePath(_bestWaypoint.Position, _coverPoint.Position, NavMesh.AllAreas, m_navMeshPath);
            _pathLength = m_navMeshPath.GetCornersNonAlloc(m_navMeshPathCorners);

            for (int j = 0; j < _pathLength; j++)
            {
                m_pathResult.Add(m_navMeshPathCorners[j]);
            }

            entity.MovementComponent.SetPath(m_pathResult, _pathLength + _oldPathLength);
            entity.CurrentCoverPoint = _coverPoint;
        }

        public EnvironmentNodeWrapper CalculateBestWaypoint(
            Vector3 source, 
            Vector3 destination, 
            Vector3 flankTarget, 
            ref FlankPathWeightSettings settings, 
            ref List<EnvironmentNodeWrapper> cachedList)
        {
            float _bestScore = float.NegativeInfinity;
            EnvironmentNodeWrapper _result = new EnvironmentNodeWrapper();

            for (int i = 0; i < cachedList.Count; i++)
            {
                EnvironmentNodeWrapper _current = cachedList[i];

                Vector3 _diff_ToSource = source - _current.Position;
                Vector3 _diff_ToDestination = destination - _current.Position;
                Vector3 _diff_ToFlankTarget = flankTarget - _current.Position;

                float _score_ToSource = _diff_ToSource.sqrMagnitude * settings.Weight_DistanceToSource;
                float _score_ToDestination = _diff_ToDestination.sqrMagnitude * settings.Weight_DistanceToDestination;
                float _score_ToFlankTarget = _diff_ToFlankTarget.sqrMagnitude * settings.Weight_DistanceToCombatTarget;

                float _finalScore = _score_ToSource + _score_ToDestination + _score_ToFlankTarget;
                if (_finalScore > _bestScore)
                {
                    _bestScore = _finalScore;
                    _result = _current;
                }
            }

            return _result;
        }

        private int getDirectionFromEntity(Vector3 forward, Vector3 targetDir, Vector3 up)
        {
            Vector3 right = Vector3.Cross(up, forward);
            float dir = Vector3.Dot(right, targetDir);

            if (dir > 0f)
            {
                return 1;
            }
            else if (dir < 0f)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
