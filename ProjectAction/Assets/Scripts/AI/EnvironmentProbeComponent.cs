﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;
using Unity.Mathematics;

public class EnvironmentProbeComponent : MonoBehaviour
{
    [SerializeField] private LayerMask m_collisionMask;
    public bool DrawDebug = false;
    [HideInInspector] public int FramesFromUpdate = 0;

    public EnvironmentProbeSettings Settings;

    [HideInInspector, System.NonSerialized] public List<Vector3> GridPositions;
    [HideInInspector, System.NonSerialized] public List<EnvironmentNodeWrapper> RuntimeGrid;

    private static RaycastHit[] m_raycastHits = new RaycastHit[10];
    private static Camera m_mainCamera = null;
    private static Transform m_playerTranform = null;

    private void Start()
    {
        //if (m_mainCamera == null) m_mainCamera = MainCameraComponent.GetInstance().gameObject.GetComponent<Camera>();
        //if (m_playerTranform == null) m_playerTranform = GameState.GetInstance().PlayerObject.transform;

        //EnemyEntity _enemyEntity = GetComponent<EnemyEntity>();
        //if (_enemyEntity)
        //{
        //    _enemyEntity.EnvironmentProbeComponent = this;
        //}

        //EnvironmentProbeManager.GetInstance().RegisterComponent(this);

        //GridPositions = new List<Vector3>();
        //RuntimeGrid = new List<EnvironmentNodeWrapper>();

        //for (int x = 0; x < Settings.GridSize; x++)
        //{
        //    int _rawX = x - (Settings.GridSize / 2);

        //    for (int z = 0; z < Settings.GridSize; z++)
        //    {
        //        int _rawZ = z - (Settings.GridSize / 2);

        //        float _posX = _rawX * Settings.CellSize;
        //        float _posZ = _rawZ * Settings.CellSize;

        //        Vector3 _positionVector = new Vector3(_posX, 0, _posZ);
        //        float _positionVectorMagnitude = _positionVector.magnitude;

        //        if (_positionVectorMagnitude < Settings.IgnoreDistance_Min ||
        //            _positionVectorMagnitude > Settings.IgnoreDistance_Max)
        //        {
        //            continue;
        //        }

        //        GridPositions.Add(_positionVector);

        //        EnvironmentNodeWrapper _newWrapper = new EnvironmentNodeWrapper();
        //        RuntimeGrid.Add(_newWrapper);
        //    }
        //}
    }

    //private void OnDrawGizmos()
    //{
    //    if (DrawDebug == false) return;
    //    if (RuntimeGrid == null) return;

    //    for (int i = 0; i < RuntimeGrid.Count; i++)
    //    {
    //        if (RuntimeGrid[i].VisibleToPlayer)
    //        {
    //            Color _targetColor = Color.red;
    //            _targetColor.a = 0.2f;
    //            Gizmos.color = _targetColor;
    //        }
    //        else
    //        {
    //            Gizmos.color = Color.yellow;
    //        }

    //        Gizmos.DrawWireSphere(RuntimeGrid[i].Position, 0.12f);
    //    }
    //}
}
