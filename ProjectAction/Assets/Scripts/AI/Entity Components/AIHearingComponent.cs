﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHearingComponent : MonoBehaviour
{
    [HideInInspector] public HearableEventInstance PriorityHeardEvent = null;
    private EnemyEntity m_entity = null;

    private void Awake()
    {
        m_entity = GetComponent<EnemyEntity>();
    }

    private void Start()
    {
        HearableEventManager.GetInstance().RegisterEntity(this);
    }

    public void OnEventHeard(HearableEventInstance instance)
    {
        if (PriorityHeardEvent != null)
        {
            if ((int)PriorityHeardEvent.Hearable.EventType > (int)instance.Hearable.EventType)
            {
                return; // current hearable instance has a higher priority
            }
        }

        PriorityHeardEvent = instance;
        instance.LinkedComponents.Add(this);
    }
}
