﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIVisionComponent : MonoBehaviour
{
    public float CurrentPlayerVisibility = 0;

    [Space]

    [SerializeField] private AIVisionSettings m_settings;

    private static RaycastHit[] m_hits = new RaycastHit[100];
    private static Transform m_playerTransform = null;
    private static PlayerVisibilityComponent m_playerVisibility = null;

    public bool CalculateVisionToPlayer()
    {
        if (m_playerTransform == null) m_playerTransform = GameState.GetInstance().PlayerObject.transform;
        if (m_playerVisibility == null) m_playerVisibility = m_playerTransform.GetComponent<PlayerVisibilityComponent>();

        CurrentPlayerVisibility = 0;
      
        Vector3 _eyePosition = transform.position + Vector3.up * m_settings.EyesHeightOffset;
        Vector3 _toPlayerUpperBody = (m_playerTransform.position + Vector3.up * m_settings.PlayerUpperBodyHeight) - _eyePosition;
        Vector3 _toPlayerLowerBody = (m_playerTransform.position + Vector3.up * m_settings.PlayerLowerBodyHeight) - _eyePosition;

        Vector3 _angleVector = _toPlayerLowerBody;
        _angleVector.y = transform.position.y;
        float _angleToPlayer = Vector3.Angle(transform.forward, _angleVector.normalized);

        if (_angleToPlayer > m_settings.VisionFieldOfView)
        {
            return false;
        }

        float _angleNormalized = _angleToPlayer / m_settings.VisionFieldOfView;
        float _angleDistanceCurveValue = Mathf.Clamp01(m_settings.CurveAngleMaxDistance.Evaluate(_angleNormalized));
        float _visionDistance = m_settings.VisionMaxDistance * _angleDistanceCurveValue;
        float _visionDistanceSquared = _visionDistance * _visionDistance;

        if (_toPlayerUpperBody.sqrMagnitude > _visionDistanceSquared ||
            _toPlayerLowerBody.sqrMagnitude > _visionDistanceSquared ||
            _toPlayerUpperBody.sqrMagnitude > m_playerVisibility.GetCurrentVisibilityDistanceSquared() ||
            _toPlayerLowerBody.sqrMagnitude > m_playerVisibility.GetCurrentVisibilityDistanceSquared())
        {
            if (m_settings.DrawDebugLines)
                Debug.DrawLine(_eyePosition, _eyePosition + _toPlayerUpperBody.normalized * _visionDistance, m_settings.DebugColor_Occluded);

            return false;
        }

        int _hits = Physics.RaycastNonAlloc(
            _eyePosition,
            _toPlayerUpperBody.normalized,
            m_hits,
            m_settings.VisionMaxDistance,
            m_settings.EnvironmentLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);

            if (_closest.collider.transform == m_playerTransform)
            {
                CurrentPlayerVisibility += 0.5f;

                if (m_settings.DrawDebugLines)
                    Debug.DrawLine(_eyePosition, _closest.point, m_settings.DebugColor_Hit);
            }
            else
            {
                if (m_settings.DrawDebugLines)
                    Debug.DrawLine(_eyePosition, _closest.point, m_settings.DebugColor_Occluded);
            }
        }
        else
        {
            if (m_settings.DrawDebugLines)
                Debug.DrawLine(_eyePosition, _eyePosition + _toPlayerUpperBody, m_settings.DebugColor_Occluded);
        }

        _hits = Physics.RaycastNonAlloc(
            _eyePosition,
            _toPlayerLowerBody.normalized,
            m_hits,
            m_settings.VisionMaxDistance,
            m_settings.EnvironmentLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);

            if (_closest.collider.transform == m_playerTransform)
            {
                CurrentPlayerVisibility += 0.5f;

                if (m_settings.DrawDebugLines)
                    Debug.DrawLine(_eyePosition, _closest.point, m_settings.DebugColor_Hit);
            }
            else
            {
                if (m_settings.DrawDebugLines)
                    Debug.DrawLine(_eyePosition, _closest.point, m_settings.DebugColor_Occluded);
            }
        }
        else
        {
            if (m_settings.DrawDebugLines)
                Debug.DrawLine(_eyePosition, _eyePosition + _toPlayerLowerBody, m_settings.DebugColor_Occluded);
        }

        if (CurrentPlayerVisibility <= 0.0f) return false;

        float _angleMod = 1.0f;
        if (_angleToPlayer > m_settings.ModAngleMin)
        {
            _angleMod = (_angleToPlayer - m_settings.ModAngleMin) / (m_settings.ModAngleMax - m_settings.ModAngleMin);
            _angleMod = Mathf.Lerp(1.0f, m_settings.ModAngleMultiplierFloor, m_settings.CurveModAngle.Evaluate(_angleMod));
        }

        float _rawDistToPlayer = (m_playerTransform.position - transform.position).magnitude;
        _rawDistToPlayer = Mathf.Clamp(_rawDistToPlayer, m_settings.ModDistanceMin, m_settings.ModDistanceMax);
        float _distMod = (_rawDistToPlayer - m_settings.ModDistanceMin) / (m_settings.ModDistanceMax - m_settings.ModDistanceMin);
        _distMod = m_settings.CurveModDistance.Evaluate(_distMod);

        CurrentPlayerVisibility = CurrentPlayerVisibility * _distMod * _angleMod;

        return true;
    }

    public float GetAggressionIncrease()
    {
        return CurrentPlayerVisibility * m_settings.DetectionSpeed * Time.deltaTime * GameTime.GameTimeScale;
    }

    public bool HasLineOfSightToTarget(GameObject target)
    {
        Vector3 _eyePosition = transform.position + Vector3.up * m_settings.EyesHeightOffset;
        Vector3 _targetPos = target.transform.position;

        if (target == m_playerTransform.gameObject)
        {
            _targetPos += Vector3.up * m_settings.PlayerUpperBodyHeight;
        }

        Vector3 _diff = _targetPos - _eyePosition;

        int _hits = Physics.RaycastNonAlloc(
            _eyePosition,
            _diff.normalized,
            m_hits,
            m_settings.VisionMaxDistance,
            m_settings.EnvironmentLayer,
            QueryTriggerInteraction.Ignore);

        if (_hits > 0)
        {
            RaycastHit _closest = getClosestHit(_hits);

            if (_closest.collider.gameObject == target)
            {
                if (m_settings.DrawDebugLines)
                    Debug.DrawLine(_eyePosition, _closest.point, m_settings.DebugColor_Hit);

                return true;
            }
            else
            {
                if (m_settings.DrawDebugLines)
                    Debug.DrawLine(_eyePosition, _closest.point, m_settings.DebugColor_Occluded);
            }
        }
        else
        {
            if (m_settings.DrawDebugLines)
                Debug.DrawLine(_eyePosition, _eyePosition + _diff, m_settings.DebugColor_Occluded);
        }

        return false;
    }

    private RaycastHit getClosestHit(int hits)
    {
        RaycastHit _result = m_hits[0];

        for (int i = 0; i < hits; i++)
        {
            RaycastHit _current = m_hits[i];
            if (_current.collider.gameObject == gameObject || _current.point == Vector3.zero)
            {
                continue;
            }

            if (_current.distance < _result.distance)
            {
                _result = _current;
            }
        }

        return _result;
    }

    private Vector3 getDirectionFromAngle(float angleInDegrees)
    {
        angleInDegrees += transform.eulerAngles.y;
        Vector3 _result = new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        return _result.normalized;
    }

    private void OnDrawGizmos()
    {
        if (m_settings.DrawGizmos == false) return;

        Vector3 _eyePosition = transform.position + Vector3.up * m_settings.EyesHeightOffset;
        Vector3 _leftEdgeDirection = getDirectionFromAngle(-m_settings.VisionFieldOfView);
        Vector3 _rightEdgeDirection = getDirectionFromAngle(m_settings.VisionFieldOfView);

        Gizmos.color = m_settings.GizmoColor;
        Gizmos.DrawLine(_eyePosition, _eyePosition + _leftEdgeDirection * m_settings.VisionMaxDistance);
        Gizmos.DrawLine(_eyePosition, _eyePosition + _rightEdgeDirection * m_settings.VisionMaxDistance);

        for (int i = 1; i < m_settings.GizmoLineIterations; i++)
        {
            Vector3 _current = Vector3.Lerp(_leftEdgeDirection, _rightEdgeDirection, (float)i / (float)m_settings.GizmoLineIterations);
            _current.Normalize();

            float _angle = Vector3.Angle(transform.forward, _current);
            float _angleNormalized = _angle / m_settings.VisionFieldOfView;
            float _angleDistance = m_settings.CurveAngleMaxDistance.Evaluate(_angleNormalized) * m_settings.VisionMaxDistance;

            Gizmos.DrawLine(_eyePosition, _eyePosition + _current * _angleDistance);
        }
    }
}
