﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace ActionAI
{
    public class AIMovementComponent : MonoBehaviour
    {
        public bool Enabled = true;
        public bool DrawDebug = true;

        public float MovementSpeed;

        private static float STOPPING_DISTANCE = 0.3f;

        private Queue<Vector3> m_currentPath = new Queue<Vector3>();
        private int m_currentPathNodeIndex;
        private float m_pathTotalLength;

        public float RemainingDistance;

        private AIComponent m_aiComponent = null;
        private NavMeshAgent m_navMeshAgent = null;
        private static RaycastHit[] m_raycastHits = new RaycastHit[100];

        private void Awake()
        {
            m_aiComponent = GetComponent<AIComponent>();
            m_aiComponent.Entity.MovementComponent = this;

            m_navMeshAgent = GetComponent<NavMeshAgent>();
            m_navMeshAgent.speed = 0;
        }

        private void Update()
        {
            if (Enabled == false ||
                m_aiComponent.Entity.AIEnabled == false ||
                m_aiComponent.Entity.AnimationLock == true)
            {
                RemainingDistance = 0;
                m_navMeshAgent.isStopped = true;
                return;
            }

            if (m_currentPath != null && m_currentPath.Count > 0)
            {
                m_navMeshAgent.isStopped = false;

                Vector3 _targetNode = m_currentPath.Peek();
                Vector3 _diff = _targetNode - transform.position;

                m_navMeshAgent.velocity = _diff.normalized * MovementSpeed;

                if (_diff.magnitude < STOPPING_DISTANCE)
                {
                    m_currentPath.Dequeue();

                    if (m_currentPath.Count > 0)
                    {
                        m_navMeshAgent.SetDestination(m_currentPath.Peek());
                    }
                }

                if (m_navMeshAgent.isOnOffMeshLink && m_aiComponent.Entity.AnimationLock == false)
                {
                    m_aiComponent.Entity.AnimationLock = true;
                    //m_routineOffmeshAnimation = StartCoroutine(routine_OffMeshTransition());
                }
            }

            RemainingDistance = calculateRemainingDistance();
        }

        public float GetMovementSpeedNormalized()
        {
            return m_navMeshAgent.velocity.magnitude / MovementSpeed;
        }

        public bool IsMoving()
        {
            if (RemainingDistance > 0 && m_currentPath.Count > 0) return true;
            return false;
        }

        public void ClearPath()
        {
            m_navMeshAgent.ResetPath();
            m_currentPath.Clear();
            m_currentPathNodeIndex = 0;
        }

        public void SetPath(List<Vector3> newPath, int pathLength)
        {
            m_currentPath = new Queue<Vector3>(newPath);
            m_currentPathNodeIndex = 0;
            m_navMeshAgent.SetDestination(m_currentPath.Peek());
            RemainingDistance = calculateRemainingDistance();
        }

        private float calculateRemainingDistance()
        {
            if (m_currentPath.Count == 0)
            {
                if (m_navMeshAgent.hasPath)
                {
                    return m_navMeshAgent.remainingDistance;
                }

                return 0;
            }

            float _result = 0;
            if (m_currentPath.Count == 1)
            {
                Vector3 _diff = m_currentPath.Peek() - transform.position;
                _result = _diff.magnitude;
            }
            else
            {
                Queue<Vector3> _copy = new Queue<Vector3>(m_currentPath);
                Vector3 _start = transform.position;

                while (_copy.Count > 0)
                {
                    Vector3 _destination = _copy.Dequeue();
                    Vector3 _diff = _destination - _start;
                    _result += _diff.magnitude;
                    _start = _destination;
                }
            }

            return _result;
        }

        private void OnDrawGizmos()
        {
            if (DrawDebug == false || Enabled == false) return;
            if (m_currentPath.Count == 0) return;

            Queue<Vector3> _copy = new Queue<Vector3>(m_currentPath);
            Vector3 _start = transform.position;

            Gizmos.color = Color.yellow;

            while (_copy.Count > 0)
            {
                Vector3 _destination = _copy.Dequeue();
                Gizmos.DrawLine(_start, _destination);
                _start = _destination;
            }
        }
    }
}
