﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "ActionAI/Entity Settings/Vision Settings Asset", fileName = "EnemyEntity Vision Settings")]
public class AIVisionSettings : ScriptableObject
{
    [Header("Base Settings")]
    public float DetectionSpeed = 0.7f;
    public float VisionMaxDistance = 50.0f;
    [Range(0, 180)] public float VisionFieldOfView = 70.0f;

    [Header("Distance Modifiers")]
    public float ModDistanceMin = 8;
    public float ModDistanceMax = 35;
    public AnimationCurve CurveModDistance;

    [Header("Angle Modifiers")]
    public float ModAngleMin = 350;
    public float ModAngleMax = 70;
    public float ModAngleMultiplierFloor = 0.4f;
    public AnimationCurve CurveModAngle;
    public AnimationCurve CurveAngleMaxDistance;

    [Header("Raycast Settings")]
    public float EyesHeightOffset = 0.0f;
    public float PlayerUpperBodyHeight = 1.5f;
    public float PlayerLowerBodyHeight = 0.6f;
    public LayerMask EnvironmentLayer;

    [Header("Debug")]
    public bool DrawDebugLines = false;
    public Color DebugColor_Hit = Color.green;
    public Color DebugColor_Occluded = Color.red;

    [Space]

    public bool DrawGizmos = false;
    public int GizmoLineIterations = 35;
    public Color GizmoColor = Color.white;
}
