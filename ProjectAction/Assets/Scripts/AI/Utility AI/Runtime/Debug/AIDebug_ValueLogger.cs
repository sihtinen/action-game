﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

namespace ActionAI.Debug
{
    [System.Serializable]
    public class AIDebug_ValueLogger : ScriptableObject, IGraphSaveable
    {
        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        public string DebugText = "Value: ";
        [HideInInspector] public FloatProviderWrapper Input = new FloatProviderWrapper();

        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        string IGraphSaveable.GetEditorName()
        {
            return "Debug Value Logger";
        }

        public void Execute(AIComponent aiComponent)
        {
            float _result = Input.GetValue(aiComponent);
            UnityEngine.Debug.Log($"{DebugText}{_result}");
        }

        public void CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {
            if (node is IGraphFloatProvider)
            {
                IGraphFloatProvider _floatProvider = (IGraphFloatProvider)node;
                if (Input.FloatProviders.Contains(_floatProvider) == false)
                {
                    Input.FloatProviders.Add(_floatProvider);
                }
            }
        }
    }
}
