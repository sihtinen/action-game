﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;
using ActionAI.Rules;
using ActionAI.Debug;

namespace ActionAI
{
    [System.Serializable]
    [CreateAssetMenu(menuName = "ActionAI/New Runtime AI Container", fileName = "New AI Graph Runtime Container")]
    public class AIGraphRuntimeContainer : ScriptableObject
    {
        public List<NodeLinkData> NodeLinks = new List<NodeLinkData>();

        public List<AIOptionData> Options = new List<AIOptionData>();
        public List<AIValueBase> Values = new List<AIValueBase>();
        public List<AIActionBase> Actions = new List<AIActionBase>();

        public List<AIRule_Math> MathRules = new List<AIRule_Math>();
        public List<AIRule_Compare> CompareRules = new List<AIRule_Compare>();
        public List<AIRule_AndFilter> AndFilters = new List<AIRule_AndFilter>();
        public List<AIRule_Clamp01> ClampRules = new List<AIRule_Clamp01>();

        public List<AIDebug_ValueLogger> DebugValueLoggers = new List<AIDebug_ValueLogger>();

        [System.NonSerialized] public bool Initialized = false;

        public void ClearConnections()
        {
            for (int i = 0; i < NodeLinks.Count; i++)
            {
                IGraphSaveable _currentNode = GetNodeInstance(NodeLinks[i].GUID_NodeTo);
                if (_currentNode is IGraphFloatProvider)
                {
                    ((IGraphFloatProvider)_currentNode).ClearConnections();
                }

                if (_currentNode is AIOptionData)
                {
                    ((AIOptionData)_currentNode).Actions.Clear();
                    ((AIOptionData)_currentNode).InputValueWrapper.ClearConnections();
                }
            }
        }

        public void SetupConnections()
        {
            ClearConnections();

            for (int i = 0; i < NodeLinks.Count; i++)
            {
                setupDataConnections(NodeLinks[i]);
            }

            Initialized = true;
        }

        private void setupDataConnections(NodeLinkData link)
        {
            IGraphSaveable _from = GetNodeInstance(link.GUID_NodeFrom);
            IGraphSaveable _to = GetNodeInstance(link.GUID_NodeTo);

            _to.CreateDataConnection(_from, link.PortIndex_To, -1);
            _from.CreateDataConnection(_to, link.PortIndex_From, 1);
        }

        private IGraphSaveable GetNodeInstance(string guid)
        {
            for (int i = 0; i < Options.Count; i++)
            {
                if (Options[i].GUID == guid) return Options[i];
            }
            for (int i = 0; i < Values.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)Values[i];
                if (_current.GUID == guid) return _current;
            }
            for (int i = 0; i < Actions.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)Actions[i];
                if (_current.GUID == guid) return _current;
            }
            for (int i = 0; i < MathRules.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)MathRules[i];
                if (_current.GUID == guid) return _current;
            }
            for (int i = 0; i < CompareRules.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)CompareRules[i];
                if (_current.GUID == guid) return _current;
            }
            for (int i = 0; i < AndFilters.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)AndFilters[i];
                if (_current.GUID == guid) return _current;
            }
            for (int i = 0; i < ClampRules.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)ClampRules[i];
                if (_current.GUID == guid) return _current;
            }
            for (int i = 0; i < DebugValueLoggers.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)DebugValueLoggers[i];
                if (_current.GUID == guid) return _current;
            }

            return null;
        }

        public IGraphFloatProvider GetFloatProvider(string guid)
        {
            for (int i = 0; i < Values.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)Values[i];
                if (_current.GUID == guid) return (IGraphFloatProvider)Values[i];
            }
            for (int i = 0; i < MathRules.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)MathRules[i];
                if (_current.GUID == guid) return (IGraphFloatProvider)MathRules[i];
            }
            for (int i = 0; i < CompareRules.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)CompareRules[i];
                if (_current.GUID == guid) return (IGraphFloatProvider)CompareRules[i];
            }
            for (int i = 0; i < AndFilters.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)AndFilters[i];
                if (_current.GUID == guid) return (IGraphFloatProvider)AndFilters[i];
            }
            for (int i = 0; i < ClampRules.Count; i++)
            {
                IGraphSaveable _current = (IGraphSaveable)ClampRules[i];
                if (_current.GUID == guid) return (IGraphFloatProvider)ClampRules[i];
            }

            return null;
        }
    }
}
