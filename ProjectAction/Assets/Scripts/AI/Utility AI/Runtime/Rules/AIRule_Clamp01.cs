﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

namespace ActionAI.Rules
{
    [System.Serializable]
    public class AIRule_Clamp01 : ScriptableObject, IGraphSaveable, IGraphFloatProvider
    {
        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        [HideInInspector] public FloatProviderWrapper Input = new FloatProviderWrapper();

        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        string IGraphSaveable.GetEditorName()
        {
            return "Clamp01";
        }

        void IGraphSaveable.CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {
            if (node is IGraphFloatProvider)
            {
                if (direction == -1)
                {
                    IGraphFloatProvider _floatProvider = (IGraphFloatProvider)node;
                    if (Input.FloatProviders.Contains(_floatProvider) == false)
                    {
                        Input.FloatProviders.Add(_floatProvider);
                    }
                }
            }

            if (node is AIOptionData)
            {
                if (direction == 1)
                {
                    AIOptionData _option = (AIOptionData)node;
                    if (_option.InputValueWrapper.FloatProviders.Contains(this) == false)
                    {
                        _option.InputValueWrapper.FloatProviders.Add(this);
                    }
                }
            }
        }

        float IGraphFloatProvider.GetValue(AIComponent aiComponent)
        {
            return Mathf.Clamp01(Input.GetValue(aiComponent));
        }

        void IGraphFloatProvider.ClearConnections()
        {
            Input.ClearConnections();
        }
    }
}
