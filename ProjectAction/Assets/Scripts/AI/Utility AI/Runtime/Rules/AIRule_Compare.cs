﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

namespace ActionAI.Rules
{
    public enum AICompareOperation
    {
        LessThan = 0,
        LessOrEqual = 1,
        Equal = 2,
        GreaterOrEqual = 3,
        Greater = 4
    }

    [System.Serializable]
    public class AIRule_Compare : ScriptableObject, IGraphSaveable, IGraphFloatProvider
    {
        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        [HideInInspector] public AICompareOperation OperationType = AICompareOperation.Equal;
        [HideInInspector] public FloatProviderWrapper InputA = new FloatProviderWrapper();
        [HideInInspector] public FloatProviderWrapper InputB = new FloatProviderWrapper();

        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        string IGraphSaveable.GetEditorName()
        {
            return "Compare";
        }

        void IGraphSaveable.CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {
            if (node is IGraphFloatProvider)
            {
                if (direction == -1)
                {
                    IGraphFloatProvider _floatProvider = (IGraphFloatProvider)node;

                    if (portIndex == 0)
                    {
                        if (InputA.FloatProviders.Contains(_floatProvider) == false)
                        {
                            InputA.FloatProviders.Add(_floatProvider);
                        }
                    }
                    else
                    {
                        if (InputB.FloatProviders.Contains(_floatProvider) == false)
                        {
                            InputB.FloatProviders.Add(_floatProvider);
                        }
                    }
                }
            }

            if (node is AIOptionData)
            {
                if (direction == 1)
                {
                    AIOptionData _option = (AIOptionData)node;
                    if (_option.InputValueWrapper.FloatProviders.Contains(this) == false)
                    {
                        _option.InputValueWrapper.FloatProviders.Add(this);
                    }
                }
            }
        }

        float IGraphFloatProvider.GetValue(AIComponent aiComponent)
        {
            float _a = InputA.GetValue(aiComponent);
            float _b = InputB.GetValue(aiComponent);

            switch (OperationType)
            {
                case AICompareOperation.LessThan:
                    if (_a < _b) return 1; else return 0;

                case AICompareOperation.LessOrEqual:
                    if (_a <= _b) return 1; else return 0;

                case AICompareOperation.Equal:
                    if (Mathf.Approximately(_a, _b)) return 1; else return 0;

                case AICompareOperation.GreaterOrEqual:
                    if (_a >= _b) return 1; else return 0;

                case AICompareOperation.Greater:
                    if (_a > _b) return 1; else return 0;

            }

            return 0;
        }

        void IGraphFloatProvider.ClearConnections()
        {
            InputA.ClearConnections();
            InputB.ClearConnections();
        }
    }
}
