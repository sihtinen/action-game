﻿using System.Collections;
using System.Collections.Generic;
using ActionAI;
using UnityEngine;

namespace ActionAI.Rules
{
    public enum AIMathOperation
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }

    [System.Serializable]
    public class AIRule_Math : ScriptableObject, IGraphSaveable, IGraphFloatProvider
    {
        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        [HideInInspector] public AIMathOperation OperationType = AIMathOperation.Add;
        [HideInInspector] public FloatProviderWrapper InputA = new FloatProviderWrapper();
        [HideInInspector] public FloatProviderWrapper InputB = new FloatProviderWrapper();

        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        string IGraphSaveable.GetEditorName()
        {
            return "Math";
        }

        void IGraphSaveable.CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {
            if (node is IGraphFloatProvider)
            {
                if (direction == -1)
                {
                    IGraphFloatProvider _floatProvider = (IGraphFloatProvider)node;

                    if (portIndex == 0)
                    {
                        if (InputA.FloatProviders.Contains(_floatProvider) == false)
                        {
                            InputA.FloatProviders.Add(_floatProvider);
                        }
                    }
                    else
                    {
                        if (InputB.FloatProviders.Contains(_floatProvider) == false)
                        {
                            InputB.FloatProviders.Add(_floatProvider);
                        }
                    }
                }
            }

            if (node is AIOptionData)
            {
                if (direction == 1)
                {
                    AIOptionData _option = (AIOptionData)node;
                    if (_option.InputValueWrapper.FloatProviders.Contains(this) == false)
                    {
                        _option.InputValueWrapper.FloatProviders.Add(this);
                    }
                }
            }
        }

        float IGraphFloatProvider.GetValue(AIComponent aiComponent)
        {
            float _result = 0;

            float _a = InputA.GetValue(aiComponent);
            float _b = InputB.GetValue(aiComponent);

            switch (OperationType)
            {
                case AIMathOperation.Add: _result = _a + _b; break;
                case AIMathOperation.Subtract: _result = _a - _b; break;
                case AIMathOperation.Divide: _result = _a / _b; break;
                case AIMathOperation.Multiply: default: _result = _a * _b; break;
            }

            return _result;
        }

        void IGraphFloatProvider.ClearConnections()
        {
            InputA.ClearConnections();
            InputB.ClearConnections();
        }
    }
}
