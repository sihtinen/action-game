﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_IsMovingToCover : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Is Moving To Cover";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.Entity.MovingToCover)
            {
                return 1.0f;
            }
            else
            {
                return 0.0f;
            }
        }
    }
}
