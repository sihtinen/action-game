﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_HasGroup : AIValueBase
    {
        public bool IsLeader = false;

        protected override string _GetEditorName()
        {
            return "Has Group";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.GroupInstance == null)
            {
                return 0.0f;
            }
            else
            {
                bool _isLeader = aiComponent.GroupInstance.Leader == aiComponent;

                if (_isLeader != IsLeader) return 0.0f;
                else return 1.0f;
            }
        }
    }
}
