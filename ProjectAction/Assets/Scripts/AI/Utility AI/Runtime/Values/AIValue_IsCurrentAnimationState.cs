﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_IsCurrentAnimationState : AIValueBase
    {
        public string StateName = "";
        public int AnimatorLayer = 0;

        protected override string _GetEditorName()
        {
            return "Is Current Animation State";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.Entity.IsCurrentAnimationState(StateName, AnimatorLayer))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
