﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_IsCurrentRole : AIValueBase
    {
        public AIRole Role = AIRole.None;

        protected override string _GetEditorName()
        {
            return "Is Current Role";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.CurrentRole == Role) return 1.0f;
            return 0.0f;
        }
    }
}
