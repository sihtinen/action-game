﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_HasLineOfSightToTarget : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Has Line Of Sight To Target";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.Entity.HasLineOfSightToTarget)
            {
                return 1.0f;
            }
            else
            {
                return 0.0f;
            }
        }
    }
}
