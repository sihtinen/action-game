﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_AttackCooldown : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Attack Cooldown";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            return aiComponent.Entity.AttackCooldown;
        }
    }
}
