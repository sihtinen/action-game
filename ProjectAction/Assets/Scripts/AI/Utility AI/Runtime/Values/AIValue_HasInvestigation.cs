﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_HasInvestigation : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Has Investigation";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.Entity.InvestigationEventType == AwarenessEventType.None)
            {
                return 0.0f;
            }
            else
            {
                return 1.0f;
            }
        }
    }
}
