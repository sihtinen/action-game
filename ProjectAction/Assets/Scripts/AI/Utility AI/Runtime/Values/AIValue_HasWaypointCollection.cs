﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_HasWaypointCollection : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Has Waypoint Collection";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.Entity.PatrolWaypointRoute == null)
            {
                return 0.0f;
            }
            else
            {
                return 1.0f;
            }
        }
    }
}
