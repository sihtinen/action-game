﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_TimeSpentInCurrentOption : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Time Spent In Current Option";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            return aiComponent.Entity.TimeSpentInCurrentState;
        }
    }
}
