﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_GetCoordinatorValue : AIValueBase
    {
        public enum ValueType
        {
            ProjectileShooterCount,
            TimeFromLastProjectileFired
        }

        public ValueType TargetValue = ValueType.ProjectileShooterCount;

        private static AICoordinator m_aiCoordinator;

        protected override string _GetEditorName()
        {
            return "AI Coordinator Value";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (m_aiCoordinator == null)
            {
                m_aiCoordinator = AICoordinator.GetInstance();
                if (m_aiCoordinator == null) return 0;
            }

            switch (TargetValue)
            {
                case ValueType.ProjectileShooterCount:
                    return m_aiCoordinator.ProjectileFiringEntities.Count;

                case ValueType.TimeFromLastProjectileFired:
                    return Time.time - m_aiCoordinator.LatestProjectileFiredTimeStamp;
            }

            return 0;
        }
    }
}
