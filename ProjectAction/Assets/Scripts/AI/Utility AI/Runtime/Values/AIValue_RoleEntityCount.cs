﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_RoleEntityCount : AIValueBase
    {
        public AIRole Role = AIRole.None;

        private static AICoordinator m_aiCoordinator = null;

        protected override string _GetEditorName()
        {
            return "AI Entity Amount With Role";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (m_aiCoordinator == null)
            {
                m_aiCoordinator = AICoordinator.GetInstance();
                if (m_aiCoordinator == null) return 0.0f;
            }

            switch (Role)
            {
                case AIRole.MeleePursuer: return m_aiCoordinator.RoleCount_MeleePursuers;
                case AIRole.Flanker: return m_aiCoordinator.RoleCount_Flanker;
            }
           
            return 0.0f;
        }
    }
}

