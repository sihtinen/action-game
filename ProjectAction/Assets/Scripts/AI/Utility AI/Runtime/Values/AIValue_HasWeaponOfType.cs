﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_HasWeaponOfType : AIValueBase
    {
        public EnemyEntity.EquippedWeaponType WeaponType = EnemyEntity.EquippedWeaponType.LightMelee;

        protected override string _GetEditorName()
        {
            return "Has Weapon Of Type";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            switch (WeaponType)
            {
                case EnemyEntity.EquippedWeaponType.LightMelee:
                    if (aiComponent.Entity.HasLightMelee) return 1.0f;
                    break;

                case EnemyEntity.EquippedWeaponType.LightPistol:
                    if (aiComponent.Entity.HasLightPistol) return 1.0f;
                    break;

                default: return 0.0f;
            }

            return 0.0f;
        }
    }
}
