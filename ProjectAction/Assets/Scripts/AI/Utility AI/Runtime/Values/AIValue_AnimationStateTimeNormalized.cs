﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_AnimationStateTimeNormalized : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Animation State Time Normalized";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            return aiComponent.Entity.CurrentAnimationStateTimeNormalized;
        }
    }
}
