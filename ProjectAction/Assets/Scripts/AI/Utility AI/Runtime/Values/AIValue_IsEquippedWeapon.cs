﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_IsEquippedWeapon : AIValueBase
    {
        public EnemyEntity.EquippedWeaponType WeaponType = EnemyEntity.EquippedWeaponType.None;

        protected override string _GetEditorName()
        {
            return "Is Equipped Weapon";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (WeaponType == aiComponent.Entity.CurrentEquippedWeaponType)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
