﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_DistanceToTarget : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Distance To Target";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            return aiComponent.Entity.DistanceToTarget;
        }
    }
}
