﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_CurrentCoverScore : AIValueBase
    {
        [SerializeField] private CoverWeightSettings m_coverWeightSettings;

        protected override string _GetEditorName()
        {
            return "Current Cover Score";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (aiComponent.Entity.CombatTarget == null
                || aiComponent.Entity.CurrentCoverPoint == null
                || aiComponent.Entity.MovingToCover)
                return 0;

            if (Vector3.SqrMagnitude(aiComponent.transform.position - aiComponent.Entity.CurrentCoverPoint.Position) > 0.08f) return 0;

            Vector3 _directionToTarget = aiComponent.Entity.CombatTarget.position - aiComponent.transform.position;
            Vector3 _combatDirectionPerpendicular = Vector3.Cross(Vector3.up, _directionToTarget.normalized);

            return aiComponent.Entity.CurrentCoverPoint.GetScore(
                aiComponent.transform.position,
                aiComponent.Entity.CombatTarget.position,
                _combatDirectionPerpendicular,
                ref m_coverWeightSettings);
        }
    }
}
