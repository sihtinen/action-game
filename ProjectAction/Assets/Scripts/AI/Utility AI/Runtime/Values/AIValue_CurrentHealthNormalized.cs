﻿using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_CurrentHealthNormalized : AIValueBase
    {
        protected override string _GetEditorName()
        {
            return "Current Health Normalized";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            return aiComponent.Entity.CurrentHealth / aiComponent.Entity.BaseSettings.MaxHealth;
        }
    }
}



