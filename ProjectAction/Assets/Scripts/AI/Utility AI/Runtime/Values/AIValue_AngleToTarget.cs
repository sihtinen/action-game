﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Values
{
    [System.Serializable]
    public class AIValue_AngleToTarget : AIValueBase
    {
        public bool Normalize = false;

        protected override string _GetEditorName()
        {
            return "Angle To Target";
        }

        protected override float _GetValue(AIComponent aiComponent)
        {
            if (Normalize)
                return aiComponent.Entity.AngleToTarget / 180.0f;

            return aiComponent.Entity.AngleToTarget;
        }
    }
}
