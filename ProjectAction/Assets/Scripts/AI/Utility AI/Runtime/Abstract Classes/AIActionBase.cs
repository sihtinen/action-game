﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

namespace ActionAI.AbstractClasses
{
    [System.Serializable]
    public abstract class AIActionBase : ScriptableObject, IGraphSaveable
    {
        public bool ExecuteEveryFrame
        {
            get
            {
                return m_executeEveryFrame;
            }
            set
            {
                m_executeEveryFrame = value;
            }
        }

        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        [SerializeField] private bool m_executeEveryFrame;
        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        string IGraphSaveable.GetEditorName()
        {
            return _GetEditorName();
        }

        void IGraphSaveable.CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {
            if (node is AIOptionData)
            {
                AIOptionData _target = (AIOptionData)node;
                if (_target.Actions.Contains(this) == false)
                {
                    _target.Actions.Add(this);
                }
            }
        }

        protected abstract string _GetEditorName();
        protected abstract void _OnActionBegin(AIComponent aiComponent);
        protected abstract void _Execute(AIComponent aiComponent, float deltaTime);
        protected abstract void _OnActionEnd(AIComponent aiComponent);

        public void OnActionBegin(AIComponent aiComponent)
        {
            _OnActionBegin(aiComponent);
        }

        public void Execute(AIComponent aiComponent, float deltaTime)
        {
            _Execute(aiComponent, deltaTime);
        }

        public void OnActionEnd(AIComponent aiComponent)
        {
            _OnActionEnd(aiComponent);
        }


    }
}
