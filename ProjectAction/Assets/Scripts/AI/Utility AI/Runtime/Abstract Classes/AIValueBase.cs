﻿using UnityEngine;
using ActionAI;

namespace ActionAI.AbstractClasses
{
    [System.Serializable]
    public abstract class AIValueBase : ScriptableObject, IGraphSaveable, IGraphFloatProvider
    {
        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        float IGraphFloatProvider.GetValue(AIComponent aiComponent)
        {
            return _GetValue(aiComponent);
        }

        void IGraphFloatProvider.ClearConnections()
        {

        }

        string IGraphSaveable.GetEditorName()
        {
            return _GetEditorName();
        }

        void IGraphSaveable.CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {

        }

        protected abstract string _GetEditorName();
        protected abstract float _GetValue(AIComponent aiComponent);
    }
}
