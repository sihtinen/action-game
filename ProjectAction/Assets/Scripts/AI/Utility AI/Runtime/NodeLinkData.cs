﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodeLinkData
{
    public string GUID_NodeFrom;
    public string PortName;
    public string GUID_NodeTo;

    public int PortIndex_From;
    public int PortIndex_To;
}
