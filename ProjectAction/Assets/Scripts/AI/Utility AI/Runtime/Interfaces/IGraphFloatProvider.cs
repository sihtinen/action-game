﻿using System.Collections.Generic;
using UnityEngine;
using ActionAI;
using System.Linq;

namespace ActionAI
{
    public enum FloatProviderMergeType
    {
        Total,
        Average,
        Min,
        Max
    }

    public interface IGraphFloatProvider
    {
        float GetValue(AIComponent aiComponent);
        void ClearConnections();
    }

    [System.Serializable]
    public class FloatProviderWrapper
    {
        [System.NonSerialized] public List<IGraphFloatProvider> FloatProviders = new List<IGraphFloatProvider>();

        public float DefaultValue = 0;
        public FloatProviderMergeType MergeType = FloatProviderMergeType.Total;

        public void ClearConnections()
        {
            FloatProviders = new List<IGraphFloatProvider>();
        }

        public float GetValue(AIComponent aiComponent)
        {
            if (FloatProviders != null && FloatProviders.Count > 0)
            {
                float _result = 0;
                float[] _inputs;

                switch (MergeType)
                {
                    case FloatProviderMergeType.Total:

                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _result += FloatProviders[i].GetValue(aiComponent);
                        }

                        break;

                    case FloatProviderMergeType.Average:

                        _inputs = new float[FloatProviders.Count];
                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _inputs[i] = FloatProviders[i].GetValue(aiComponent);
                        }
                        _result = _inputs.Average();

                        break;

                    case FloatProviderMergeType.Min:

                        _inputs = new float[FloatProviders.Count];
                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _inputs[i] = FloatProviders[i].GetValue(aiComponent);
                        }
                        _result = _inputs.Min();

                        break;

                    case FloatProviderMergeType.Max:

                        _inputs = new float[FloatProviders.Count];
                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _inputs[i] = FloatProviders[i].GetValue(aiComponent);
                        }
                        _result = _inputs.Max();

                        break;
                }

                return _result;
            }
            else
            {
                return DefaultValue;
            }
        }

        public float GetValue(AIComponent aiComponent, FloatProviderMergeType overrideMergeType)
        {
            if (FloatProviders != null && FloatProviders.Count > 0)
            {
                float _result = 0;
                float[] _inputs;

                switch (overrideMergeType)
                {
                    case FloatProviderMergeType.Total:

                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _result += FloatProviders[i].GetValue(aiComponent);
                        }

                        break;

                    case FloatProviderMergeType.Average:

                        _inputs = new float[FloatProviders.Count];
                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _inputs[i] = FloatProviders[i].GetValue(aiComponent);
                        }
                        _result = _inputs.Average();

                        break;

                    case FloatProviderMergeType.Min:

                        _inputs = new float[FloatProviders.Count];
                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _inputs[i] = FloatProviders[i].GetValue(aiComponent);
                        }
                        _result = _inputs.Min();

                        break;

                    case FloatProviderMergeType.Max:

                        _inputs = new float[FloatProviders.Count];
                        for (int i = 0; i < FloatProviders.Count; i++)
                        {
                            _inputs[i] = FloatProviders[i].GetValue(aiComponent);
                        }
                        _result = _inputs.Max();

                        break;
                }

                return _result;
            }
            else
            {
                return DefaultValue;
            }
        }
    }
}
