﻿public interface IGraphSaveable
{
    string GUID { get; set; }
    Vector2_NonUnity GraphPosition { get; set; }

    string GetEditorName();
    void CreateDataConnection(IGraphSaveable node, int portIndex, int direction);
}

[System.Serializable]
public class Vector2_NonUnity
{
    public float X, Y;
}
