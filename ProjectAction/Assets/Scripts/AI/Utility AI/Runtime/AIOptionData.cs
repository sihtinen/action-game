﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI
{
    [System.Serializable]
    public class AIOptionData : ScriptableObject, IGraphSaveable
    {
        public string GUID
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public Vector2_NonUnity GraphPosition
        {
            get
            {
                return m_graphPosition;
            }
            set
            {
                m_graphPosition = value;
            }
        }

        [SerializeField, HideInInspector] private string m_guid;
        [SerializeField, HideInInspector] private Vector2_NonUnity m_graphPosition;

        public bool Enabled = true;
        public string Description;
        public FloatProviderMergeType MergeType = FloatProviderMergeType.Total;

        [HideInInspector] public FloatProviderWrapper InputValueWrapper = new FloatProviderWrapper();
        [System.NonSerialized] public List<AIActionBase> Actions = new List<AIActionBase>();

        public string GetEditorName()
        {
            return $"{Description}";
        }

        public float GetUtilityScore(AIComponent aiComponent)
        {
            return InputValueWrapper.GetValue(aiComponent, MergeType);
        }

        public void CreateDataConnection(IGraphSaveable node, int portIndex, int direction)
        {
            if (node is IGraphFloatProvider)
            {
                IGraphFloatProvider _floatProvider = (IGraphFloatProvider)node;
                if (InputValueWrapper.FloatProviders.Contains(_floatProvider) == false)
                {
                    InputValueWrapper.FloatProviders.Add(_floatProvider);
                }
            }

            if (node is AIActionBase)
            {
                AIActionBase _action = (AIActionBase)node;
                if (Actions.Contains(_action) == false)
                {
                    Actions.Add(_action);
                }
            }
        }
    }
}
