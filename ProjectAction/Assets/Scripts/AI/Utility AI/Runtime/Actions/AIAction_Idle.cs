﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_Idle : AIActionBase
    {
        protected override string _GetEditorName()
        {
            return "Idle";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = false;
            aiComponent.Entity.NavTarget = NavigationTarget.None;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;

            if (aiComponent.Entity.CurrentCoverPoint != null)
            {
                aiComponent.Entity.transform.position = Vector3.MoveTowards(
                    aiComponent.Entity.transform.position,
                    aiComponent.Entity.CurrentCoverPoint.Position,
                    deltaTime * GameTime.GameTimeScale * 2);

                aiComponent.Entity.TakeCoverAnimation();
            }
            else
            {
                aiComponent.Entity.MovementAnimationLogic();
            }
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
