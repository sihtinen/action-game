﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_SetRoleActionGraph : AIActionBase
    {
        public AIRole Role = AIRole.None;
        public AIGraphRuntimeContainer RoleActionGraph;

        protected override string _GetEditorName()
        {
            return "Set Role Action Graph";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {

        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            aiComponent.CurrentRole = Role;
            aiComponent.SetActionGraph(RoleActionGraph);
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
