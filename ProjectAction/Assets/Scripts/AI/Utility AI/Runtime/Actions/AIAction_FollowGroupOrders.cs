﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_FollowGroupOrders : AIActionBase
    {
        [Header("Debug")]
        public bool DrawDebug = true;
        public Color DebugColor_MovePosition = Color.green;

        private const float MAX_DRIFT = 0.2f;
        private const float MAX_DRIFT_REMAININGDISTANCEMODIFIER = 0.8f;
        private const float VELOCITYMODIFIER = 20;

        private static NavMeshHit m_navMeshHit;

        protected override string _GetEditorName()
        {
            return "Follow Group Orders";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = true;
            aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Run;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;
            if (aiComponent.GroupInstance == null) return;

            Vector3 _movePosition = aiComponent.GroupInstance.GetFollowPosition(aiComponent);
            NavMesh.SamplePosition(_movePosition, out m_navMeshHit, 2.0f, NavMesh.AllAreas);
            _movePosition = m_navMeshHit.position;

            if (DrawDebug)
            {
                drawMarker(_movePosition, DebugColor_MovePosition, deltaTime);
                UnityEngine.Debug.DrawLine(aiComponent.transform.position + Vector3.up * 2, _movePosition, DebugColor_MovePosition, deltaTime);
            }

            Vector3 _diff = _movePosition - aiComponent.transform.position;
            float _differenceMag = _diff.magnitude;

            float _speedModifier = 1.0f;

            if (_differenceMag > 0 && _differenceMag > MAX_DRIFT)
            {
                _speedModifier = _differenceMag / MAX_DRIFT;
            }
            else if (_differenceMag < 0
                && Mathf.Abs(_differenceMag) > MAX_DRIFT
                && aiComponent.Entity.MovementComponent.RemainingDistance < MAX_DRIFT * MAX_DRIFT_REMAININGDISTANCEMODIFIER)
            {
                _speedModifier = MAX_DRIFT / Mathf.Abs(_differenceMag);
            }

            if (_differenceMag > MAX_DRIFT)
            {
                aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Run;
                Vector3 _targetVelocity =
                    _diff
                    * deltaTime
                    * aiComponent.Entity.BaseSettings.MovementSpeed_Run
                    * _speedModifier
                    * VELOCITYMODIFIER;

                if (_targetVelocity.magnitude > aiComponent.Entity.MovementComponent.MovementSpeed)
                {
                    _targetVelocity = _targetVelocity.normalized * aiComponent.Entity.MovementComponent.MovementSpeed;
                }

                //_targetVelocity = Vector3.Lerp(aiComponent.Entity.MovementComponent.MovementSpeed, _targetVelocity, deltaTime * 3);
                //aiComponent.Entity.NavigationMeshAgent.velocity = _targetVelocity;
            }
            else
            {
                //aiComponent.Entity.NavigationMeshAgent.velocity = aiComponent.GroupInstance.Leader.Entity.NavigationMeshAgent.velocity;
            }

            aiComponent.Entity.MovementAnimationLogic();
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }

        private void drawMarker(Vector3 position, Color color, float deltaTime)
        {
            Vector3 _pos01 = position - Vector3.right * 0.25f + Vector3.forward * 0.25f + Vector3.up * 0.25f;
            Vector3 _pos02 = position + Vector3.right * 0.25f - Vector3.forward * 0.25f - Vector3.up * 0.25f;

            Vector3 _pos03 = position - Vector3.right * 0.25f - Vector3.forward * 0.25f + Vector3.up * 0.25f;
            Vector3 _pos04 = position + Vector3.right * 0.25f + Vector3.forward * 0.25f - Vector3.up * 0.25f;

            Vector3 _pos05 = position + Vector3.right * 0.25f + Vector3.forward * 0.25f + Vector3.up * 0.25f;
            Vector3 _pos06 = position - Vector3.right * 0.25f - Vector3.forward * 0.25f - Vector3.up * 0.25f;

            Vector3 _pos07 = position - Vector3.right * 0.25f - Vector3.forward * 0.25f - Vector3.up * 0.25f;
            Vector3 _pos08 = position + Vector3.right * 0.25f + Vector3.forward * 0.25f + Vector3.up * 0.25f;

            UnityEngine.Debug.DrawLine(_pos01, _pos02, color, deltaTime);
            UnityEngine.Debug.DrawLine(_pos03, _pos04, color, deltaTime);
            UnityEngine.Debug.DrawLine(_pos05, _pos06, color, deltaTime);
            UnityEngine.Debug.DrawLine(_pos07, _pos08, color, deltaTime);
        }
    }
}
