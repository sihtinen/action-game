﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_AimAndShootProjectile : AIActionBase
    {
        public float RotationSpeed = 10;
        public float AbortDistance = 8.0f;

        private static AICoordinator m_aiCoordinator = null;

        protected override string _GetEditorName()
        {
            return "Aim And Shoot Projectile";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = false;
            aiComponent.Entity.NavTarget = NavigationTarget.None;

            if (m_aiCoordinator == null)
            {
                m_aiCoordinator = AICoordinator.GetInstance();
                if (m_aiCoordinator == null) return;
            }

            m_aiCoordinator.ProjectileFiringEntities.Add(aiComponent);
            m_aiCoordinator.LatestProjectileFiredTimeStamp = Time.time;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;
            if (aiComponent.Entity.CurrentEquippedWeaponType != EnemyEntity.EquippedWeaponType.LightPistol) return;

            aiComponent.Entity.CurrentRotationSpeed = RotationSpeed;
            aiComponent.Entity.AimAndShootProjectile(AbortDistance * AbortDistance);
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {
            if (m_aiCoordinator == null)
            {
                m_aiCoordinator = AICoordinator.GetInstance();
                if (m_aiCoordinator == null) return;
            }

            m_aiCoordinator.ProjectileFiringEntities.Remove(aiComponent);
        }
    }
}
