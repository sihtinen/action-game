﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_MeleeAttack : AIActionBase
    {
        public EnemyEntity.MeleeAttackType AttackType;
        public bool InterruptCurrentAttack = false;

        protected override string _GetEditorName()
        {
            return "Melee Attack";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = false;
            aiComponent.Entity.NavTarget = NavigationTarget.None;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock)
            {
                if (InterruptCurrentAttack == false) return;
            }

            aiComponent.Entity.MeleeAttack(AttackType, InterruptCurrentAttack);
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
