﻿using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_ChaseTarget : AIActionBase
    {
        protected override string _GetEditorName()
        {
            return "Chase Target";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            if (aiComponent.Entity.CombatTarget == null)
            {
                aiComponent.Entity.CombatTarget = GameState.GetInstance().PlayerObject.transform;
            }

            aiComponent.Entity.MovementComponent.ClearPath();
            aiComponent.Entity.MovementComponent.Enabled = true;
            aiComponent.Entity.NavTarget = NavigationTarget.CombatTarget;
            aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Run;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;

            aiComponent.Entity.MovementAnimationLogic();
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
