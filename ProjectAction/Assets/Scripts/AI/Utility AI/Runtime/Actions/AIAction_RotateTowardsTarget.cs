﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_RotateTowardsTarget : AIActionBase
    {
        public float RotationSpeed;

        protected override string _GetEditorName()
        {
            return "Rotate Towards Target";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            if (aiComponent.Entity.CombatTarget == null) aiComponent.Entity.CombatTarget = GameState.GetInstance().PlayerObject.transform;

            aiComponent.Entity.MovementComponent.Enabled = false;
            aiComponent.Entity.NavTarget = NavigationTarget.None;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;

            aiComponent.Entity.CurrentRotationSpeed = RotationSpeed;

            Vector3 _targetDir = aiComponent.Entity.CombatTarget.position - aiComponent.Entity.transform.position;
            _targetDir.y = 0;
            _targetDir.Normalize();

            aiComponent.Entity.transform.forward = Vector3.RotateTowards(aiComponent.Entity.transform.forward, _targetDir, deltaTime * RotationSpeed * GameTime.GameTimeScale, 0);
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
