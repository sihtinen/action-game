﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_Investigate : AIActionBase
    {
        public float InvestigationIdleTime = 6.0f;
        public float DistanceThresholdSquared = 6.0f;

        [Header("Debug")]
        public bool DrawDebug = false;
        public Color DebugColor = Color.cyan;

        protected override string _GetEditorName()
        {
            return "Investigate";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = true;
            aiComponent.Entity.Investigating = true;
            aiComponent.Entity.TimeSpentInvestigating = 0.0f;
            aiComponent.Entity.NavTarget = NavigationTarget.Investigation;
            aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Walk;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;

            aiComponent.Entity.MovementAnimationLogic();
            aiComponent.Entity.AggressionIncreaseTimeStamp = Time.time;

            if (Vector3.SqrMagnitude(aiComponent.transform.position - aiComponent.Entity.InvestigationTargetPoint) < DistanceThresholdSquared)
            {
                aiComponent.Entity.TimeSpentInvestigating += deltaTime * GameTime.GameTimeScale;
                if (aiComponent.Entity.TimeSpentInvestigating > InvestigationIdleTime)
                {
                    aiComponent.Entity.InvestigationEventType = AwarenessEventType.None;
                }
            }

            if (DrawDebug)
            {
                UnityEngine.Debug.DrawLine(aiComponent.transform.position + Vector3.up * 2.0f, aiComponent.Entity.InvestigationTargetPoint, DebugColor);
            }
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {
            aiComponent.Entity.Investigating = false;
            aiComponent.Entity.TimeSpentInvestigating = 0.0f;
        }
    }
}
