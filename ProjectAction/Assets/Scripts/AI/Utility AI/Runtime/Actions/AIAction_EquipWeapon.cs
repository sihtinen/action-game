﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_EquipWeapon : AIActionBase
    {
        public EnemyEntity.EquippedWeaponType WeaponType = EnemyEntity.EquippedWeaponType.None;

        protected override string _GetEditorName()
        {
            return "Equip Weapon";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = false;
            aiComponent.Entity.NavTarget = NavigationTarget.None;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;
            if (WeaponType == aiComponent.Entity.CurrentEquippedWeaponType) return;

            aiComponent.Entity.NextEquippedWeaponType = WeaponType;
            aiComponent.Entity.EquipWeapon();
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
