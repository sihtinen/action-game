﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_ProcessWaypointCollection : AIActionBase
    {
        public float DistanceThresholdSquared = 6.0f;
        public bool IgnoreMovementNodesWhenSearching = false;

        [Header("Debug")]
        public bool DrawDebug = false;
        public Color DebugColor = Color.cyan;

        protected override string _GetEditorName()
        {
            return "Process Waypoint Collection";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            aiComponent.Entity.MovementComponent.Enabled = true;

            aiComponent.Entity.TimeSpentInCurrentWaypoint = 0.0f;
            aiComponent.Entity.CurrentWaypoint = aiComponent.Entity.PatrolWaypointRoute.FindClosestWaypoint(aiComponent.transform.position, IgnoreMovementNodesWhenSearching);
            aiComponent.Entity.NavTarget = NavigationTarget.Waypoint;
            aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Walk;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;
            if (aiComponent.Entity.CurrentWaypoint == null) return;

            aiComponent.Entity.MovementAnimationLogic();

            if (Vector3.SqrMagnitude(aiComponent.transform.position - aiComponent.Entity.CurrentWaypoint.GetPosition()) < DistanceThresholdSquared)
            {
                processCurrentWaypoint(aiComponent, deltaTime);
            }

            if (DrawDebug)
            {
                UnityEngine.Debug.DrawLine(aiComponent.transform.position + Vector3.up * 2.0f, aiComponent.Entity.CurrentWaypoint.transform.position, DebugColor);
            }
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }

        private void processCurrentWaypoint(AIComponent aiComponent, float deltaTime)
        {
            switch (aiComponent.Entity.CurrentWaypoint.WaypointAction)
            {
                case Waypoint.WaypointActionType.Movement:

                    if (aiComponent.Entity.CurrentWaypoint.To)
                    {
                        aiComponent.Entity.CurrentWaypoint = aiComponent.Entity.CurrentWaypoint.To;
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("Next waypoint missing from [" + aiComponent.Entity.CurrentWaypoint.Collection + "]", aiComponent.Entity.CurrentWaypoint.Collection);
                    }

                    break;

                case Waypoint.WaypointActionType.Idle:
                case Waypoint.WaypointActionType.PerformAction:

                    if (aiComponent.Entity.TimeSpentInCurrentWaypoint < aiComponent.Entity.CurrentWaypoint.IdleWaitTime)
                    {
                        aiComponent.Entity.TimeSpentInCurrentWaypoint += deltaTime * GameTime.GameTimeScale;
                    }
                    else
                    {
                        aiComponent.Entity.TimeSpentInCurrentWaypoint = 0;

                        if (aiComponent.Entity.CurrentWaypoint.To)
                        {
                            aiComponent.Entity.CurrentWaypoint = aiComponent.Entity.CurrentWaypoint.To;
                        }
                        else
                        {
                            UnityEngine.Debug.LogError("Next waypoint missing from [" + aiComponent.Entity.CurrentWaypoint.Collection + "]", aiComponent.Entity.CurrentWaypoint.Collection);
                        }
                    }

                    break;
            }
        }
    }
}
