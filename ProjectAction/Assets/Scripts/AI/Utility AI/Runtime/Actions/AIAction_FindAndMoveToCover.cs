﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_FindAndMoveToCover : AIActionBase
    {
        public CoverWeightSettings NewCoverWeightSettings;

        public float AttackCooldownMultiplierMoving = 0.2f;
        public float AttackCooldownMultiplierInCover = 2.0f;

        protected override string _GetEditorName()
        {
            return "Find And Move To Cover";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            if (aiComponent.Entity.CombatTarget == null) aiComponent.Entity.CombatTarget = GameState.GetInstance().PlayerObject.transform;

            aiComponent.Entity.MovementComponent.ClearPath();
            aiComponent.Entity.MovementComponent.Enabled = true;

            aiComponent.Entity.CurrentCoverWeightSettings = NewCoverWeightSettings;

            if (aiComponent.Entity.CurrentCoverPoint != null) aiComponent.Entity.CurrentCoverPoint.SetOccupied(false);
            aiComponent.Entity.CurrentCoverPoint = null;

            aiComponent.Entity.NavTarget = NavigationTarget.Cover;
            aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Run;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;

            aiComponent.Entity.MovementComponent.Enabled = true;

            if (aiComponent.Entity.CurrentCoverPoint != null)
            {
                if (aiComponent.Entity.CurrentCoverPoint.GetDotProductToPosition(aiComponent.Entity.CombatTarget.position) > 0)
                {
                    resetPathFinding(aiComponent);
                }
            }

            if (aiComponent.Entity.CurrentCoverPoint != null)
            {
                aiComponent.Entity.MovingToCover = true;

                if (Vector3.SqrMagnitude(aiComponent.transform.position - aiComponent.Entity.CurrentCoverPoint.Position) <= 0.08f)
                {
                    aiComponent.Entity.NavTarget = NavigationTarget.None;
                    aiComponent.Entity.MovementComponent.Enabled = false;

                    aiComponent.transform.position = aiComponent.Entity.CurrentCoverPoint.Position;
                    aiComponent.Entity.MovingToCover = false;
                    aiComponent.Entity.AttackCooldownTimerMultiplier = AttackCooldownMultiplierInCover;
                    aiComponent.Entity.TakeCoverAnimation();

                    if (aiComponent.Entity.HasLineOfSightToTarget == false)
                    {
                        resetPathFinding(aiComponent);
                    }

                    return;
                }
            }

            aiComponent.Entity.AttackCooldownTimerMultiplier = AttackCooldownMultiplierMoving;
            aiComponent.Entity.MovementAnimationLogic();
        }

        private void resetPathFinding(AIComponent aiComponent)
        {
            aiComponent.Entity.CurrentCoverPoint.SetOccupied(false);
            aiComponent.Entity.CurrentCoverPoint = null;
            aiComponent.Entity.MovementComponent.ClearPath();
            aiComponent.Entity.NavTarget = NavigationTarget.Cover;
            aiComponent.Entity.MovingToCover = false;
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {
            aiComponent.Entity.MovingToCover = false;
            aiComponent.Entity.AttackCooldownTimerMultiplier = 1.0f;
        }
    }
}
