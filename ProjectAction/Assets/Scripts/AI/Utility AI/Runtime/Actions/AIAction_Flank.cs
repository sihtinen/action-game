﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI.AbstractClasses;

namespace ActionAI.Actions
{
    [System.Serializable]
    public class AIAction_Flank : AIActionBase
    {
        public CoverWeightSettings CoverSettings;
        public EnvironmentProbeSettings ProbeSettings;
        public FlankPathWeightSettings FlankSettings;

        protected override string _GetEditorName()
        {
            return "Flank";
        }

        protected override void _OnActionBegin(AIComponent aiComponent)
        {
            if (aiComponent.Entity.CombatTarget == null) aiComponent.Entity.CombatTarget = GameState.GetInstance().PlayerObject.transform;

            aiComponent.Entity.CurrentCoverWeightSettings = CoverSettings;
            aiComponent.Entity.CurrentProbeSettings = ProbeSettings;
            aiComponent.Entity.CurrentFlankWeightSettings = FlankSettings;

            if (aiComponent.Entity.CurrentCoverPoint != null) aiComponent.Entity.CurrentCoverPoint.SetOccupied(false);
            aiComponent.Entity.CurrentCoverPoint = null;

            aiComponent.Entity.MovementComponent.ClearPath();
            aiComponent.Entity.MovementComponent.Enabled = true;
            aiComponent.Entity.MovementComponent.MovementSpeed = aiComponent.Entity.BaseSettings.MovementSpeed_Run;
            aiComponent.Entity.NavTarget = NavigationTarget.Flank;
        }

        protected override void _Execute(AIComponent aiComponent, float deltaTime)
        {
            if (aiComponent.Entity.AIEnabled == false) return;
            if (aiComponent.Entity.AnimationLock) return;

            aiComponent.Entity.MovementAnimationLogic();

            if (aiComponent.Entity.MovementComponent.RemainingDistance <= 0)
            {
                AICoordinator.GetInstance().RequestNewRole(aiComponent);
            }
        }

        protected override void _OnActionEnd(AIComponent aiComponent)
        {

        }
    }
}
