﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActionAI
{
    public class AIGroupInstance : MonoBehaviour
    {
        public AIComponent Leader = null;
        public List<AIComponent> Followers = new List<AIComponent>();

        private Vector3[] m_groupFollowPositions;

        private const float MOVEMENT_PREDICTION_TIME = 0.3f;
        private const float FOLLOWPOSITION_SMOOTHING = 1.4f;

        public bool IsGroupFull()
        {
            if (Followers.Count >= 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void InitializeGroup(AIComponent leader)
        {
            Leader = leader;
            Followers.Clear();
        }

        public void AddEntity(AIComponent aiComponent)
        {
            if (Followers.Contains(aiComponent)) return;
            Followers.Add(aiComponent);
        }

        public void RemoveEntity(AIComponent aiComponent)
        {
            if (Followers.Contains(aiComponent) == false) return;
            Followers.Remove(aiComponent);
        }

        private void Update()
        {
            if (GameTime.GameTimeScale == 0) return;
            if (Leader == null) return;
            if (Followers.Count == 0) return;

            Vector3 _moveDirection = Leader.transform.forward;
            Vector3 _perpendicular = Vector3.Cross(_moveDirection, Vector3.up).normalized;

            if (m_groupFollowPositions == null || m_groupFollowPositions.Length != Followers.Count)
            {
                m_groupFollowPositions = new Vector3[Followers.Count];

                for (int i = 0; i < Followers.Count; i++)
                {
                    Vector3 _position = Leader.transform.position;

                    switch (i)
                    {
                        case 0:

                            _position -= _moveDirection * 3.0f;
                            _position += _perpendicular * 1.4f;

                            break;

                        case 1:

                            _position -= _moveDirection * 3.0f;
                            _position -= _perpendicular * 1.4f;

                            break;
                    }

                    m_groupFollowPositions[i] = _position;
                }

                return;
            }

            for (int i = 0; i < Followers.Count; i++)
            {
                Vector3 _position = Leader.transform.position;

                switch (i)
                {
                    case 0:

                        _position -= _moveDirection * 1.0f;
                        _position += _perpendicular * 1.4f;

                        break;

                    case 1:

                        _position -= _moveDirection * 1.0f;
                        _position -= _perpendicular * 1.4f;

                        break;
                }

                m_groupFollowPositions[i] = Vector3.Lerp(m_groupFollowPositions[i], _position, Time.deltaTime * GameTime.GameTimeScale * FOLLOWPOSITION_SMOOTHING);
            }
        }

        public Vector3 GetFollowPosition(AIComponent aiComponent)
        {
            Vector3 _result = Leader.transform.position;

            int _index = 0;
            if (Followers.Count > 1)
            {
                for (int i = 0; i < Followers.Count; i++)
                {
                    if (Followers[i] == aiComponent)
                    {
                        _index = i;
                        break;
                    }
                }
            }

            if (m_groupFollowPositions != null)
            {
                if (m_groupFollowPositions.Length == Followers.Count)
                {
                    _result = m_groupFollowPositions[_index];
                }
            }

            return _result;
        }
    }
}
