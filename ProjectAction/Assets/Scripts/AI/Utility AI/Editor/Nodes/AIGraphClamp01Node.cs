﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using ActionAI.Rules;

namespace ActionAI.Editor
{
    public class AIGraphClamp01Node : AIGraphNodeBase
    {
        public override void GenerateLabel(VisualElement labelElement)
        {

        }

        public override System.Type GetImplementationType()
        {
            return typeof(AIRule_Clamp01);
        }

        public override List<PortGeneratorWrapper> GetPorts()
        {
            List<PortGeneratorWrapper> _ports = new List<PortGeneratorWrapper>();
            AIRule_Clamp01 _implementation = (AIRule_Clamp01)Implementation;

            PortGeneratorWrapper _portIn = new PortGeneratorWrapper();
            _portIn.Capacity = Port.Capacity.Single;
            _portIn.Direction = Direction.Input;
            _portIn.PortConnectionType = typeof(float);
            _portIn.Name = "Input";
            _portIn.FloatProvider = _implementation.Input;

            PortGeneratorWrapper _portOut = new PortGeneratorWrapper();
            _portOut.Capacity = Port.Capacity.Multi;
            _portOut.Direction = Direction.Output;
            _portOut.PortConnectionType = typeof(float);
            _portOut.Name = "Output";

            _ports.Add(_portIn);
            _ports.Add(_portOut);

            return _ports;
        }

        protected override string _GetEditorName()
        {
            return "Clamp01";
        }
    }
}
