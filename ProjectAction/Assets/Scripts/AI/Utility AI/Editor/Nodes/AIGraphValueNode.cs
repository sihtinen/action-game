﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;
using UnityEditor.Experimental.GraphView;
using System;
using UnityEngine.UIElements;

namespace ActionAI.Editor
{
    [System.Serializable]
    public class AIGraphValueNode : AIGraphNodeBase
    {
        public override void GenerateLabel(VisualElement labelElement)
        {

        }

        public override Type GetImplementationType()
        {
            return Implementation.GetType();
        }

        public override List<PortGeneratorWrapper> GetPorts()
        {
            List<PortGeneratorWrapper> _ports = new List<PortGeneratorWrapper>();

            PortGeneratorWrapper _portOut = new PortGeneratorWrapper();
            _portOut.Capacity = Port.Capacity.Multi;
            _portOut.Direction = Direction.Output;
            _portOut.PortConnectionType = typeof(float);
            _portOut.Name = "Output";

            _ports.Add(_portOut);

            return _ports;
        }

        protected override string _GetEditorName()
        {
            return ((IGraphSaveable)Implementation).GetEditorName();
        }
    }
}
