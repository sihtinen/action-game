﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;
using UnityEditor.Experimental.GraphView;
using System;
using UnityEngine.UIElements;

namespace ActionAI.Editor
{
    [System.Serializable]
    public class AIGraphActionNode : AIGraphNodeBase
    {
        public override void GenerateLabel(VisualElement labelElement)
        {

        }

        public override Type GetImplementationType()
        {
            return Implementation.GetType();
        }

        public override List<PortGeneratorWrapper> GetPorts()
        {
            List<PortGeneratorWrapper> _ports = new List<PortGeneratorWrapper>();

            PortGeneratorWrapper _portIn = new PortGeneratorWrapper();
            _portIn.Capacity = Port.Capacity.Multi;
            _portIn.Direction = Direction.Input;
            _portIn.PortConnectionType = typeof(AIGraphOptionNode);
            _portIn.Name = "Option";

            _ports.Add(_portIn);

            return _ports;
        }

        protected override string _GetEditorName()
        {
            string _name = ((IGraphSaveable)Implementation).GetEditorName();
            return _name;
        }
    }
}
