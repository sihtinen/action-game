﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;

namespace ActionAI.Editor
{
    [System.Serializable]
    public abstract class AIGraphNodeBase : Node
    {
        public struct PortGeneratorWrapper
        {
            public string Name;
            public Type PortConnectionType;
            public Direction Direction;
            public Port.Capacity Capacity;
            public FloatProviderWrapper FloatProvider;
        }

        public ScriptableObject Implementation;

        public string GetEditorName()
        {
            return _GetEditorName();
        }

        public void SetGUID(string guid)
        {
            IGraphSaveable _implementation = (IGraphSaveable)Implementation;

            if (string.IsNullOrEmpty(guid))
            {
                guid = System.Guid.NewGuid().ToString();
            }

            _implementation.GUID = guid;
        }

        protected abstract string _GetEditorName();
        public abstract List<PortGeneratorWrapper> GetPorts();
        public abstract Type GetImplementationType();
        public abstract void GenerateLabel(VisualElement labelElement);
    }
}
