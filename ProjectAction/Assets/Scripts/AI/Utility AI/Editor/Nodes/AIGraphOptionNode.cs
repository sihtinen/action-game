﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using System;
using UnityEngine.UIElements;

namespace ActionAI.Editor
{
    [System.Serializable]
    public class AIGraphOptionNode : AIGraphNodeBase
    {
        public override List<PortGeneratorWrapper> GetPorts()
        {
            List<PortGeneratorWrapper> _ports = new List<PortGeneratorWrapper>();

            PortGeneratorWrapper _portIn = new PortGeneratorWrapper();
            _portIn.Capacity = Port.Capacity.Multi;
            _portIn.Direction = Direction.Input;
            _portIn.PortConnectionType = typeof(float);
            _portIn.Name = "Utility Score";
            _portIn.FloatProvider = ((AIOptionData)Implementation).InputValueWrapper;

            PortGeneratorWrapper _portOut = new PortGeneratorWrapper();
            _portOut.Capacity = Port.Capacity.Multi;
            _portOut.Direction = Direction.Output;
            _portOut.PortConnectionType = typeof(AIGraphActionNode);
            _portOut.Name = "Actions";

            _ports.Add(_portIn);
            _ports.Add(_portOut);

            return _ports;
        }

        public override Type GetImplementationType()
        {
            return typeof(AIOptionData);
        }

        protected override string _GetEditorName()
        {
            return "Option";
        }

        public override void GenerateLabel(VisualElement labelElement)
        {

        }
    }
}
