﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using System;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using ActionAI.Rules;

namespace ActionAI.Editor
{
    [System.Serializable]
    public class AIGraphMathNode : AIGraphNodeBase
    {
        public override void GenerateLabel(VisualElement labelElement)
        {
            labelElement.Clear();

            SerializedProperty _operationProperty = new SerializedObject(Implementation).FindProperty("OperationType");
            var _propertyField = new EnumField(((AIRule_Math)Implementation).OperationType);
            _propertyField.label = "";
            _propertyField.RegisterValueChangedCallback(evt =>
            {
                ((AIRule_Math)Implementation).OperationType = (AIMathOperation)evt.newValue;
            });
            _propertyField.StretchToParentSize();

            labelElement.Add(_propertyField);
        }

        public override Type GetImplementationType()
        {
            return typeof(AIRule_Math);
        }

        public override List<PortGeneratorWrapper> GetPorts()
        {
            List<PortGeneratorWrapper> _ports = new List<PortGeneratorWrapper>();
            AIRule_Math _implementation = (AIRule_Math)Implementation;

            PortGeneratorWrapper _portInA = new PortGeneratorWrapper();
            _portInA.Capacity = Port.Capacity.Single;
            _portInA.Direction = Direction.Input;
            _portInA.PortConnectionType = typeof(float);
            _portInA.Name = "A";
            _portInA.FloatProvider = _implementation.InputA;

            PortGeneratorWrapper _portInB = new PortGeneratorWrapper();
            _portInB.Capacity = Port.Capacity.Single;
            _portInB.Direction = Direction.Input;
            _portInB.PortConnectionType = typeof(float);
            _portInB.Name = "B";
            _portInB.FloatProvider = _implementation.InputB;

            PortGeneratorWrapper _portOut = new PortGeneratorWrapper();
            _portOut.Capacity = Port.Capacity.Multi;
            _portOut.Direction = Direction.Output;
            _portOut.PortConnectionType = typeof(float);
            _portOut.Name = "Output";

            _ports.Add(_portInA);
            _ports.Add(_portInB);
            _ports.Add(_portOut);

            return _ports;
        }

        protected override string _GetEditorName()
        {
            return "Math";
        }
    }

}