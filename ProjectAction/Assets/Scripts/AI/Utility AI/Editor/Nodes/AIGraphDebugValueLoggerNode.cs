﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using ActionAI.Debug;

namespace ActionAI.Editor
{
    public class AIGraphDebugValueLoggerNode : AIGraphNodeBase
    {
        public override void GenerateLabel(VisualElement labelElement)
        {

        }

        public override System.Type GetImplementationType()
        {
            return typeof(AIDebug_ValueLogger);
        }

        public override List<PortGeneratorWrapper> GetPorts()
        {
            List<PortGeneratorWrapper> _ports = new List<PortGeneratorWrapper>();
            AIDebug_ValueLogger _implementation = (AIDebug_ValueLogger)Implementation;

            PortGeneratorWrapper _portIn = new PortGeneratorWrapper();
            _portIn.Capacity = Port.Capacity.Single;
            _portIn.Direction = Direction.Input;
            _portIn.PortConnectionType = typeof(float);
            _portIn.Name = "Input";
            _portIn.FloatProvider = _implementation.Input;

            _ports.Add(_portIn);

            return _ports;
        }

        protected override string _GetEditorName()
        {
            return "Debug Value Logger";
        }
    }
}
