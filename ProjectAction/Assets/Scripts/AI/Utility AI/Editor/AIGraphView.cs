﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;

namespace ActionAI.Editor
{
    public class AIGraphView : GraphView
    {
        private EditorWindow m_editorWindow;

        public const int NODEWIDTH = 170;
        public const int NODEHEIGHT = 400;

        private Vector2 m_mousePosition;

        public AIGraphView(EditorWindow window)
        {
            m_editorWindow = window;

            styleSheets.Add(Resources.Load<StyleSheet>("StyleSheet_AIGraph"));

            var _grid = new GridBackground();
            Insert(0, _grid);
            _grid.StretchToParentSize();

            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            SetupZoom(0.1f, ContentZoomer.DefaultMaxScale);

            RegisterCallback<MouseDownEvent>(onMouseDownEvent);
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var _compatiblePorts = new List<Port>();

            ports.ForEach((port) =>
            {
                if (startPort != port && startPort.node != port.node)
                {
                    _compatiblePorts.Add(port);
                }
            });

            return _compatiblePorts;
        }

        public AIGraphNodeBase CreateNode(
            Type nodeType, 
            Type implementationType, 
            ScriptableObject implementationInstance, 
            bool isExistingData)
        {
            var _newInstance = Activator.CreateInstance(nodeType);
            var _node = (AIGraphNodeBase)_newInstance;

            if (isExistingData)
            {
                implementationInstance = ScriptableObject.Instantiate(implementationInstance);
            }
            else
            {
                implementationInstance = ScriptableObject.CreateInstance(implementationType);
            }

            _node.Implementation = implementationInstance;
            _node.title = _node.GetEditorName();

            var _labelContainer = _node.titleContainer;
            _node.GenerateLabel(_labelContainer);

            _node.styleSheets.Add(Resources.Load<StyleSheet>("StyleSheet_AIGraphNode"));

            generatePorts(_node);

            if (_node.Implementation != null)
            {
                SerializedObject _serializedImplementation = new SerializedObject(_node.Implementation);
                drawProperties(_node, _serializedImplementation);
            }
            else
            {
                UnityEngine.Debug.Log("Node implementation == null");
            }

            _node.RefreshExpandedState();
            _node.RefreshPorts();

            AddElement(_node);

            if (isExistingData == false)
            {
                _node.SetGUID(null);
                _node.name = _node.GetEditorName();

                _node.SetPosition(new Rect(-viewTransform.position + new Vector3(350, 220, 0), new Vector2(NODEWIDTH, NODEHEIGHT)));
            }
            else
            {
                IGraphSaveable _existingData = (IGraphSaveable)implementationInstance;

                _node.SetGUID(_existingData.GUID);
                _node.name = _existingData.GetEditorName();

                _node.SetPosition(new Rect(new Vector2(_existingData.GraphPosition.X, _existingData.GraphPosition.Y), new Vector2(NODEWIDTH, NODEHEIGHT)));
            }

            return _node;
        }

        private void onMouseDownEvent(MouseEventBase<MouseDownEvent> evt)
        {
            m_mousePosition = evt.mousePosition;
        }

        private void generatePorts(AIGraphNodeBase node)
        {
            List<AIGraphNodeBase.PortGeneratorWrapper> _ports = node.GetPorts();
            for (int i = 0; i < _ports.Count; i++)
            {
                AIGraphNodeBase.PortGeneratorWrapper _currentWrapper = _ports[i];

                var _generatedPort = generatePort(node, Orientation.Horizontal, _currentWrapper.Direction, _currentWrapper.Capacity, _currentWrapper.PortConnectionType);
                _generatedPort.portName = _currentWrapper.Name;

                if (_currentWrapper.FloatProvider != null)
                {
                    var _portLabel = _generatedPort.Q<Label>("type");
                    _generatedPort.contentContainer.Remove(_portLabel);

                    var _floatField = new FloatField();
                    _floatField.label = "";
                    _floatField.RegisterValueChangedCallback(evt =>
                    {
                        _currentWrapper.FloatProvider.DefaultValue = evt.newValue;
                    });
                    _floatField.SetValueWithoutNotify(_currentWrapper.FloatProvider.DefaultValue);
                    _generatedPort.contentContainer.Add(_floatField);

                    RegisterCallback<MouseMoveEvent>(evt =>
                    {
                        if (_generatedPort.connected)
                        {
                            _floatField.visible = false;
                        }
                        else
                        {
                            _floatField.visible = true;
                        }
                    });
                }

                switch (_currentWrapper.Direction)
                {
                    case Direction.Input: node.inputContainer.Add(_generatedPort); break;
                    case Direction.Output: node.outputContainer.Add(_generatedPort); break;
                }
            }
        }

        private Port generatePort(Node node, Orientation orientation, Direction portDirection, Port.Capacity capacity, Type portType)
        {
            return node.InstantiatePort(orientation, portDirection, capacity, portType);
        }

        private void drawProperties(Node node, SerializedObject serializedObject)
        {
            PropertyField _propertyField;

            var _serializedProperty = serializedObject.GetIterator();
            while (_serializedProperty.NextVisible(true))
            {
                if (_serializedProperty.displayName == "Script") continue;

                _propertyField = new PropertyField();
                _propertyField.label = _serializedProperty.displayName;
                _propertyField.bindingPath = _serializedProperty.propertyPath;
                _propertyField.BindProperty(_serializedProperty);

                node.Add(_propertyField);
            }
        }
    }
}