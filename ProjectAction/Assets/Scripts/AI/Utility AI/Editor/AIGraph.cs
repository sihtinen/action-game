﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using ActionAI.AbstractClasses;
using ActionAI.Rules;
using ActionAI.Debug;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using UnityEditor.Experimental.GraphView;

namespace ActionAI.Editor
{
    [System.Serializable]
    public class AIGraph : EditorWindow
    {
        [System.Serializable]
        public class AIGraphContainerEditorWrapper
        {
            public AIGraphRuntimeContainer CurrentContainer = null;
        }

        private AIGraphView m_graphView = null;
        public AIGraphContainerEditorWrapper CurrentContainerWrapper = new AIGraphContainerEditorWrapper();
        public AIGraphRuntimeContainer CurrentContainer = null;

        private IEnumerable<AIValueBase> m_allValueTypes;
        private List<AIValueBase> m_valueTypeList;

        private IEnumerable<AIActionBase> m_allActionTypes;
        private List<AIActionBase> m_actionTypeList;

        [MenuItem("Tools/AI Graph Editor")]
        public static void OpenWindow()
        {
            var _window = GetWindow<AIGraph>();
            _window.titleContent = new GUIContent("AI Graph Editor");
        }

        private void OnEnable()
        {
            generateClassNameReferences();

            constructGraph();
            generateToolbar();
            generateMinimap();
        }

        private void generateClassNameReferences()
        {
            m_allValueTypes = typeof(AIValueBase)
                .Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(AIValueBase)) && !t.IsAbstract)
                .Select(t => (AIValueBase)Activator.CreateInstance(t));
            m_valueTypeList = m_allValueTypes.ToList();
            m_valueTypeList.OrderByDescending(o => o.name);

            m_allActionTypes = typeof(AIActionBase)
                .Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(AIActionBase)) && !t.IsAbstract)
                .Select(t => (AIActionBase)Activator.CreateInstance(t));
            m_actionTypeList = m_allActionTypes.ToList();
            m_actionTypeList.OrderByDescending(o => o.name);
        }

        private void constructGraph()
        {
            m_graphView = new AIGraphView(this)
            {
                name = "AI Graph"
            };

            m_graphView.StretchToParentSize();
            rootVisualElement.Add(m_graphView);
        }

        private void generateToolbar()
        {
            var _toolbar = new Toolbar();

            var _containerField = new ObjectField();
            _containerField.Bind(new SerializedObject(this));
            _containerField.bindingPath = "CurrentContainer";
            _containerField.objectType = typeof(AIGraphRuntimeContainer);
            _containerField.allowSceneObjects = false;
            _containerField.label = "Target Asset";
            _containerField.RegisterCallback
                <ChangeEvent<UnityEngine.Object>>(evt =>
            {
                CurrentContainer = (AIGraphRuntimeContainer)evt.newValue;
                loadFile();
            });
            _containerField.MarkDirtyRepaint();
            _toolbar.Add(_containerField);

            var _saveButton = new Button( () => { saveFile(); });
            _saveButton.text = "Save";
            _toolbar.Add(_saveButton);

            var _loadButton = new Button( () => { loadFile(); });
            _loadButton.text = "Load";
            _toolbar.Add(_loadButton);

            var _dropDownButtonCreate = new ToolbarMenu
            {
                name = "Create",
                text = "Create"
            };

            _dropDownButtonCreate.menu.AppendAction(
                "Default (do nothing)",
                action => { },
                action => DropdownMenuAction.Status.None);

            _dropDownButtonCreate.menu.AppendAction(
                "General/Option Node",
                action => m_graphView.CreateNode(
                    typeof(AIGraphOptionNode),
                    typeof(AIOptionData),
                    null,
                    false));

            _dropDownButtonCreate.menu.AppendAction("Rules/Math Node", a => m_graphView.CreateNode(typeof(AIGraphMathNode), typeof(AIRule_Math), null, false), a => DropdownMenuAction.Status.Normal);
            _dropDownButtonCreate.menu.AppendAction("Rules/Compare Node", a => m_graphView.CreateNode(typeof(AIGraphCompareNode), typeof(AIRule_Compare), null, false), a => DropdownMenuAction.Status.Normal);
            _dropDownButtonCreate.menu.AppendAction("Rules/And Filter Node", a => m_graphView.CreateNode(typeof(AIGraphAndFilterNode), typeof(AIRule_AndFilter), null, false), a => DropdownMenuAction.Status.Normal);
            _dropDownButtonCreate.menu.AppendAction("Rules/Clamp01 Node", a => m_graphView.CreateNode(typeof(AIGraphClamp01Node), typeof(AIRule_Clamp01), null, false), a => DropdownMenuAction.Status.Normal);
            _dropDownButtonCreate.menu.AppendAction("Debug/Value Logger Node", a => m_graphView.CreateNode(typeof(AIGraphDebugValueLoggerNode), typeof(AIDebug_ValueLogger), null, false), a => DropdownMenuAction.Status.Normal);

            for (int i = 0; i < m_valueTypeList.Count; i++)
            {
                IGraphSaveable _saveable = (IGraphSaveable)m_valueTypeList[i];

                _dropDownButtonCreate.menu.AppendAction(
                    $"Value/{_saveable.GetEditorName()}",
                    onButtonCreateValueNode,
                    dropDownMenuAction => DropdownMenuAction.Status.Normal,
                    m_valueTypeList[i]);
            }

            for (int i = 0; i < m_actionTypeList.Count; i++)
            {
                IGraphSaveable _saveable = (IGraphSaveable)m_actionTypeList[i];

                _dropDownButtonCreate.menu.AppendAction(
                    $"Action/{_saveable.GetEditorName()}",
                    onButtonCreateActionNode,
                    a => DropdownMenuAction.Status.Normal,
                    m_actionTypeList[i]);
            }

            _toolbar.Add(_dropDownButtonCreate);

            rootVisualElement.Add(_toolbar);
        }

        private void generateMinimap()
        {
            var _minimap = new MiniMap();
            _minimap.title = "";
            _minimap.anchored = true;
            _minimap.SetPosition(new Rect(this.position.size.x - 260, 30, 250, 160));
            m_graphView.Add(_minimap);
        }

        private void OnDisable()
        {
            rootVisualElement.Remove(m_graphView);
        }

        private void saveFile()
        {
            if (CurrentContainer == null)
            {
                UnityEngine.Debug.Log("Data operation aborted. Current container == null");
                return;
            }

            var _saveUtility = AIGraphSaveUtility.GetInstance(m_graphView);
            _saveUtility.SaveGraph(CurrentContainer);
            CurrentContainer = _saveUtility.GetCurrentContainer();
        }

        private void loadFile()
        {
            if (CurrentContainer == null)
            {
                UnityEngine.Debug.Log("Data operation aborted. Current container == null");
                return;
            }

            var _saveUtility = AIGraphSaveUtility.GetInstance(m_graphView);
            _saveUtility.LoadGraph(CurrentContainer);
        }

        private void onButtonCreateValueNode(DropdownMenuAction action)
        {
            Type _type = action.userData.GetType();
            m_graphView.CreateNode(typeof(AIGraphValueNode), _type, null, false);
        }

        private void onButtonCreateActionNode(DropdownMenuAction action)
        {
            Type _type = action.userData.GetType();
            m_graphView.CreateNode(typeof(AIGraphActionNode), _type, null, false);
        }
    }
}
