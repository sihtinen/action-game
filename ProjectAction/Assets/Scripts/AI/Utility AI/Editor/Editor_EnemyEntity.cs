﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ActionAI.Editor
{
    [CustomEditor(typeof(EnemyEntity))]
    public class Editor_EnemyEntity : UnityEditor.Editor
    {
        private EnemyEntity m_target = null;

        private void OnEnable()
        {
            m_target = (EnemyEntity)target;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            base.OnInspectorGUI();

            EditorGUILayout.BeginVertical("box");
            drawBasicSettings(serializedObject);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical("box");
            drawWaypointSettings(serializedObject);
            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }

        private void drawBasicSettings(SerializedObject serializedObject)
        {
            EditorGUILayout.LabelField("General Settings");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("BaseSettings"), GUIContent.none);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("BarkContainer"), GUIContent.none);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("AIEnabled"));

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("HasLightMelee"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("HasLightPistol"));
        }

        private void drawWaypointSettings(SerializedObject serializedObject)
        {
            EditorGUILayout.LabelField("Waypoint Settings");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PatrolWaypointRoute"), GUIContent.none);
        }
    }
}
