﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using ActionAI.AbstractClasses;
using ActionAI.Debug;
using ActionAI.Rules;

namespace ActionAI.Editor
{
    public class AIGraphSaveUtility
    {
        private AIGraphView m_targetGraphView = null;
        private AIGraphRuntimeContainer m_targetContainer = null;

        private List<Edge> m_edges = null;
        private List<Node> m_nodes = null;

        public static AIGraphSaveUtility GetInstance(AIGraphView graphView)
        {
            return new AIGraphSaveUtility
            {
                m_targetGraphView = graphView
            };
        }

        public void SaveGraph(AIGraphRuntimeContainer container)
        {
            setupReferences();

            if (m_edges.Any() == false) return;
            validateFilePath();

            m_targetContainer = container;
            string _assetPath = AssetDatabase.GetAssetPath(m_targetContainer);

            Object[] _old = AssetDatabase.LoadAllAssetsAtPath(_assetPath);
            for (int i = 0; i < _old.Length; i++)
            {
                if (_old[i] != m_targetContainer)
                    Object.DestroyImmediate(_old[i], true);
            }

            m_targetContainer.Actions.Clear();
            m_targetContainer.AndFilters.Clear();
            m_targetContainer.ClampRules.Clear();
            m_targetContainer.CompareRules.Clear();
            m_targetContainer.DebugValueLoggers.Clear();
            m_targetContainer.MathRules.Clear();
            m_targetContainer.NodeLinks.Clear();
            m_targetContainer.Options.Clear();
            m_targetContainer.Values.Clear();

            for (int i = 0; i < m_edges.Count; i++)
            {
                var _outputNode = m_edges[i].output.node;
                var _inputNode = m_edges[i].input.node;

                int _portIndex_From = m_edges[i].output.node.outputContainer.IndexOf(m_edges[i].output);
                int _portIndex_To = m_edges[i].input.node.inputContainer.IndexOf(m_edges[i].input);

                AIGraphNodeBase _nodeBase_Out = (AIGraphNodeBase)_outputNode;
                AIGraphNodeBase _nodeBase_In = (AIGraphNodeBase)_inputNode;

                m_targetContainer.NodeLinks.Add(new NodeLinkData
                {
                    GUID_NodeFrom = (_nodeBase_Out.Implementation as IGraphSaveable).GUID,
                    PortName = m_edges[i].output.portName,
                    GUID_NodeTo = (_nodeBase_In.Implementation as IGraphSaveable).GUID,

                    PortIndex_From = _portIndex_From,
                    PortIndex_To = _portIndex_To
                });
            }

            for (int i = 0; i < m_nodes.Count; i++)
            {
                switch (m_nodes[i])
                {
                    case AIGraphOptionNode optionNode:

                        AIOptionData _newInstance = (AIOptionData)ScriptableObject.Instantiate(optionNode.Implementation);
                        _newInstance.name = $"Option_{_newInstance.GetEditorName()}";
                        _newInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newInstance.GraphPosition = optionNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newInstance, m_targetContainer);
                        m_targetContainer.Options.Add(_newInstance);

                        break;

                    case AIGraphValueNode valueNode:

                        AIValueBase _newValueInstance = (AIValueBase)ScriptableObject.Instantiate(valueNode.Implementation);
                        _newValueInstance.name = _newValueInstance.GetType().ToString();
                        _newValueInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newValueInstance.GraphPosition = valueNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newValueInstance, m_targetContainer);
                        m_targetContainer.Values.Add(_newValueInstance);

                        break;

                    case AIGraphActionNode actionNode:

                        AIActionBase _newActionInstance = (AIActionBase)ScriptableObject.Instantiate(actionNode.Implementation);
                        _newActionInstance.name = _newActionInstance.GetType().ToString();
                        _newActionInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newActionInstance.GraphPosition = actionNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newActionInstance, m_targetContainer);
                        m_targetContainer.Actions.Add(_newActionInstance);

                        break;

                    case AIGraphMathNode mathNode:

                        AIRule_Math _newMathInstance = (AIRule_Math)ScriptableObject.Instantiate(mathNode.Implementation);
                        _newMathInstance.name = $"MathRule_{_newMathInstance.OperationType.ToString()}";
                        _newMathInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newMathInstance.GraphPosition = mathNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newMathInstance, m_targetContainer);
                        m_targetContainer.MathRules.Add(_newMathInstance);

                        break;

                    case AIGraphCompareNode compareNode:

                        AIRule_Compare _newCompareInstance = (AIRule_Compare)ScriptableObject.Instantiate(compareNode.Implementation);
                        _newCompareInstance.name = $"MathRule_{_newCompareInstance.OperationType.ToString()}";
                        _newCompareInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newCompareInstance.GraphPosition = compareNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newCompareInstance, m_targetContainer);
                        m_targetContainer.CompareRules.Add(_newCompareInstance);

                        break;

                    case AIGraphAndFilterNode andFilterNode:

                        AIRule_AndFilter _newAndFilterInstance = (AIRule_AndFilter)ScriptableObject.Instantiate(andFilterNode.Implementation);
                        _newAndFilterInstance.name = "AndFilter";
                        _newAndFilterInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newAndFilterInstance.GraphPosition = andFilterNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newAndFilterInstance, m_targetContainer);
                        m_targetContainer.AndFilters.Add(_newAndFilterInstance);

                        break;

                    case AIGraphClamp01Node clampNode:

                        AIRule_Clamp01 _newClampRuleInstance = (AIRule_Clamp01)ScriptableObject.Instantiate(clampNode.Implementation);
                        _newClampRuleInstance.name = "Clamp01";
                        _newClampRuleInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newClampRuleInstance.GraphPosition = clampNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newClampRuleInstance, m_targetContainer);
                        m_targetContainer.ClampRules.Add(_newClampRuleInstance);

                        break;

                    case AIGraphDebugValueLoggerNode debugValueLoggerNode:

                        AIDebug_ValueLogger _newDebugValueLoggerInstance = (AIDebug_ValueLogger)ScriptableObject.Instantiate(debugValueLoggerNode.Implementation);
                        _newDebugValueLoggerInstance.name = "DebugValueLogger";
                        _newDebugValueLoggerInstance.hideFlags = HideFlags.HideInHierarchy;

                        _newDebugValueLoggerInstance.GraphPosition = debugValueLoggerNode.GetPosition().position.Convert_Vector2NonUnity();

                        AssetDatabase.AddObjectToAsset(_newDebugValueLoggerInstance, m_targetContainer);
                        m_targetContainer.DebugValueLoggers.Add(_newDebugValueLoggerInstance);

                        break;
                }
            }

            m_targetContainer.ClearConnections();
            EditorUtility.SetDirty(m_targetContainer);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            m_targetContainer = (AIGraphRuntimeContainer)AssetDatabase.LoadAssetAtPath(_assetPath, typeof(AIGraphRuntimeContainer));
        }

        public void LoadGraph(AIGraphRuntimeContainer container)
        {
            m_targetContainer = container;

            setupReferences();
            validateFilePath();

            clearGraph();

            generateNodes();
            setupReferences();
            connectNodes();
        }

        public AIGraphRuntimeContainer GetCurrentContainer()
        {
            return m_targetContainer;
        }

        private void validateFilePath()
        {
            if (AssetDatabase.IsValidFolder("Assets/Presets") == false)
            {
                AssetDatabase.CreateFolder("Assets", "Presets");
            }

            if (AssetDatabase.IsValidFolder("Assets/Presets/AI") == false)
            {
                AssetDatabase.CreateFolder("Assets/Presets", "AI");
            }
        }

        private void clearGraph()
        {
            foreach (var node in m_nodes)
            {
                //Remove connected edges
                m_edges.Where(x => x.input.node == node).ToList()
                    .ForEach(edge => m_targetGraphView.RemoveElement(edge));

                m_targetGraphView.RemoveElement((Node)node);
            }
        }

        private void generateNodes()
        {
            for (int i = 0; i < m_targetContainer.Options.Count; i++)
            {
                if (m_targetContainer.Options[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.Options[{i}] == null");
                    continue;
                }

                AIOptionData _currentOptionData = m_targetContainer.Options[i];
                m_targetGraphView.CreateNode(typeof(AIGraphOptionNode), typeof(AIOptionData), _currentOptionData, true);
            }

            for (int i = 0; i < m_targetContainer.Values.Count; i++)
            {
                if (m_targetContainer.Values[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.Values[{i}] == null");
                    continue;
                }

                AIValueBase _currentValue = m_targetContainer.Values[i];
                m_targetGraphView.CreateNode(typeof(AIGraphValueNode), _currentValue.GetType(), _currentValue, true);
            }

            for (int i = 0; i < m_targetContainer.Actions.Count; i++)
            {
                if (m_targetContainer.Actions[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.Actions[{i}] == null");
                    continue;
                }

                AIActionBase _currentAction = m_targetContainer.Actions[i];
                m_targetGraphView.CreateNode(typeof(AIGraphActionNode), _currentAction.GetType(), _currentAction, true);
            }

            for (int i = 0; i < m_targetContainer.MathRules.Count; i++)
            {
                if (m_targetContainer.MathRules[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.MathRules[{i}] == null");
                    continue;
                }

                AIRule_Math _currentMathRule = m_targetContainer.MathRules[i];
                m_targetGraphView.CreateNode(typeof(AIGraphMathNode), _currentMathRule.GetType(), _currentMathRule, true);
            }

            for (int i = 0; i < m_targetContainer.CompareRules.Count; i++)
            {
                if (m_targetContainer.CompareRules[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.MathRules[{i}] == null");
                    continue;
                }

                AIRule_Compare _currentCompareRule = m_targetContainer.CompareRules[i];
                m_targetGraphView.CreateNode(typeof(AIGraphCompareNode), _currentCompareRule.GetType(), _currentCompareRule, true);
            }

            for (int i = 0; i < m_targetContainer.AndFilters.Count; i++)
            {
                if (m_targetContainer.AndFilters[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.AndFilters[{i}] == null");
                    continue;
                }

                AIRule_AndFilter _currentAndFilterRule = m_targetContainer.AndFilters[i];
                m_targetGraphView.CreateNode(typeof(AIGraphAndFilterNode), _currentAndFilterRule.GetType(), _currentAndFilterRule, true);
            }

            for (int i = 0; i < m_targetContainer.ClampRules.Count; i++)
            {
                if (m_targetContainer.ClampRules[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.ClampRules[{i}] == null");
                    continue;
                }

                AIRule_Clamp01 _currentClampRule = m_targetContainer.ClampRules[i];
                m_targetGraphView.CreateNode(typeof(AIGraphClamp01Node), _currentClampRule.GetType(), _currentClampRule, true);
            }

            for (int i = 0; i < m_targetContainer.DebugValueLoggers.Count; i++)
            {
                if (m_targetContainer.DebugValueLoggers[i] == null)
                {
                    UnityEngine.Debug.Log($"m_targetContainer.DebugValueLoggers[{i}] == null");
                    continue;
                }

                AIDebug_ValueLogger _currentDebugValueLogger = m_targetContainer.DebugValueLoggers[i];
                m_targetGraphView.CreateNode(typeof(AIGraphDebugValueLoggerNode), _currentDebugValueLogger.GetType(), _currentDebugValueLogger, true);
            }
        }

        private void connectNodes()
        {
            for (int i = 0; i < m_targetContainer.NodeLinks.Count; i++)
            {
                Node _nodeFrom = findNode(m_targetContainer.NodeLinks[i].GUID_NodeFrom);
                Node _nodeTo = findNode(m_targetContainer.NodeLinks[i].GUID_NodeTo);

                if (_nodeFrom == null)
                {
                    UnityEngine.Debug.Log("Node From == null");
                    continue;
                }
                if (_nodeTo == null)
                {
                    UnityEngine.Debug.Log("Node To == null");
                    continue;
                }

                Port _portFrom = (Port)_nodeFrom.outputContainer[m_targetContainer.NodeLinks[i].PortIndex_From];
                Port _portTo = (Port)_nodeTo.inputContainer[m_targetContainer.NodeLinks[i].PortIndex_To];

                linkNodes(_portFrom, _portTo, m_targetContainer.NodeLinks[i].PortName);
            }
        }

        private Node findNode(string guid)
        {
            for (int i = 0; i < m_nodes.Count; i++)
            {
                AIGraphNodeBase _currentBaseNode = (AIGraphNodeBase)m_nodes[i];
                IGraphSaveable _currentSaveable = (IGraphSaveable)_currentBaseNode.Implementation;
                if (_currentSaveable.GUID == guid)
                {
                    return m_nodes[i];
                }
            }

            return null;
        }

        private void setupReferences()
        {
            m_edges = m_targetGraphView.edges.ToList();

            if (m_nodes == null) m_nodes = new List<Node>();
            m_nodes.Clear();
            List<Node> _allNodes = m_targetGraphView.nodes.ToList();
            for (int i = 0; i < _allNodes.Count; i++)
            {
                if (m_nodes.Contains(_allNodes[i]) == false)
                {
                    m_nodes.Add(_allNodes[i]);
                }
            }
        }

        private void linkNodes(Port outputPort, Port inputPort, string name)
        {
            var _tempEdge = new Edge
            {
                output = outputPort,
                input = inputPort,
                name = name
            };

            _tempEdge.input.Connect(_tempEdge);
            _tempEdge.output.Connect(_tempEdge);

            m_targetGraphView.AddElement(_tempEdge);
        }
    }
}
