﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ActionAI.Editor
{
    [CustomEditor(typeof(AIComponent))]
    public class AIDebugger : UnityEditor.Editor
    {
        private AIComponent m_target = null;

        private List<AIOptionData> m_options;

        private void OnEnable()
        {
            m_target = (AIComponent)target;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();

            if (Application.isPlaying)
            {
                updateData();

                if (m_options == null || m_options.Count == 0) return;

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("AI Debugger");

                Rect _previousRect = GUILayoutUtility.GetLastRect();
                for (int i = 0; i < m_options.Count; i++)
                {
                    Rect _rect = GUILayoutUtility.GetRect(_previousRect.width, 22);
                    EditorGUI.ProgressBar(_rect, m_options[i].GetUtilityScore(m_target), m_options[i].GetEditorName());
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void updateData()
        {
            m_options = m_target.ActionsSorted;
        }
    }
}