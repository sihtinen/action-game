﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ActionAI
{
    public class AIComponent : MonoBehaviour
    {
        public EnemyEntity Entity;
        public AIGraphRuntimeContainer RoleGraph;
        public AIGraphRuntimeContainer ActionGraph;

        [Range(0.1f, 1f), SerializeField] private float m_updateTickRate = 0.5f;

        [HideInInspector] public AIGroupInstance GroupInstance = null;
        [HideInInspector] public List<AIOptionData> ActionsSorted = new List<AIOptionData>();
        [HideInInspector] public List<AIOptionData> RolesSorted = new List<AIOptionData>();

        [HideInInspector] public AIRole CurrentRole = AIRole.None;
        [HideInInspector] public bool HasPermissionForRequest = false;

        [HideInInspector] public AIOptionData CurrentAction = null;
        [HideInInspector] public float CalculatedPathDistance;

        private float m_lastProbabilityResult;
        private bool m_isThinking;
        private float m_timeSinceLastRefresh;

        private AIOptionData m_previousOption = null;

        private void Start()
        {
            AIGroupManager.GetIntance().RegisterAIComponent(this);

            if (RoleGraph)
            {
                if (RoleGraph.Initialized == false)
                    RoleGraph.SetupConnections();
            }
            if (ActionGraph)
            {
                if (ActionGraph.Initialized == false)
                    ActionGraph.SetupConnections();
            }

            m_timeSinceLastRefresh = Random.Range(0, m_updateTickRate);
        }

        private void Update()
        {
            m_timeSinceLastRefresh += Time.deltaTime * GameTime.GameTimeScale;

            if (m_isThinking || m_timeSinceLastRefresh <= m_updateTickRate)
            {

            }
            else
            {
                StartCoroutine(routineEvaluateAndAct());
                m_timeSinceLastRefresh = 0;
            }

            executeActions(ActionGraph, true, Time.deltaTime * GameTime.GameTimeScale);
        }

        private IEnumerator routineEvaluateAndAct()
        {
            m_isThinking = true;

            Entity.CalculateUtilityAIValues();
            calculateOptions(ActionGraph);
            executeActions(ActionGraph, false, m_updateTickRate * GameTime.GameTimeScale);

            if (GameTime.GameTimeScale == 0.0f)
            {
                while (GameTime.GameTimeScale == 0.0f)
                {
                    yield return null;
                }
            }

            m_isThinking = false;
        }

        public void CalculateRoleScores()
        {
            RolesSorted.Clear();

            for (int i = 0; i < RoleGraph.Options.Count; i++)
            {
                if (RoleGraph.Options[i].Enabled)
                {
                    RolesSorted.Add(RoleGraph.Options[i]);
                }
            }

            for (int i = 0; i < RoleGraph.DebugValueLoggers.Count; i++)
            {
                RoleGraph.DebugValueLoggers[i].Execute(this);
            }

            if (RolesSorted != null && RolesSorted.Count > 0)
                RolesSorted = RolesSorted.OrderByDescending(option => option.GetUtilityScore(this)).ToList();
        }

        public void SetActionGraph(AIGraphRuntimeContainer newActionGraph)
        {
            ActionGraph = newActionGraph;

            if (ActionGraph)
            {
                if (ActionGraph.Initialized == false)
                    ActionGraph.SetupConnections();
            }
        }

        private void calculateOptions(AIGraphRuntimeContainer aiContainer)
        {
            ActionsSorted.Clear();

            for (int i = 0; i < aiContainer.Options.Count; i++)
            {
                if (aiContainer.Options[i].Enabled)
                {
                    ActionsSorted.Add(aiContainer.Options[i]);
                }
            }

            for (int i = 0; i < aiContainer.DebugValueLoggers.Count; i++)
            {
                aiContainer.DebugValueLoggers[i].Execute(this);
            }

            if (ActionsSorted != null && ActionsSorted.Count > 0)
                ActionsSorted = ActionsSorted.OrderByDescending(option => option.GetUtilityScore(this)).ToList();
        }

        private void executeActions(AIGraphRuntimeContainer aiContainer, bool isUnityUpdate, float deltaTime)
        {
            if (ActionsSorted == null || ActionsSorted.Count == 0) return;

            CurrentAction = ActionsSorted[0];

            if (CurrentAction == m_previousOption)
            {
                Entity.TimeSpentInCurrentState += deltaTime;
            }
            else
            {
                if (m_previousOption != null)
                {
                    for (int i = 0; i < m_previousOption.Actions.Count; i++)
                    {
                        if (m_previousOption.Actions[i] == null) continue;

                        m_previousOption.Actions[i].OnActionEnd(this);
                    }
                }

                m_previousOption = CurrentAction;

                for (int i = 0; i < CurrentAction.Actions.Count; i++)
                {
                    if (CurrentAction.Actions[i] == null) continue;

                    CurrentAction.Actions[i].OnActionBegin(this);
                }


                Entity.TimeSpentInCurrentState = 0;
                Entity.OnActionChanged.Invoke();
            }

            for (int i = 0; i < CurrentAction.Actions.Count; i++)
            {
                if (CurrentAction.Actions[i] == null) continue;

                if (CurrentAction.Actions[i].ExecuteEveryFrame == isUnityUpdate)
                {
                    CurrentAction.Actions[i].Execute(this, deltaTime);
                }
            }
        }
    }
}
