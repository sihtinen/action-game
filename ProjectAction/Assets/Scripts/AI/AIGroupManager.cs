﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ActionAI;
using UnityEngine.AI;

public class AIGroupManager : MonoBehaviour
{
    private static AIGroupManager m_instance = null;
    public static AIGroupManager GetIntance() { return m_instance; }

    [SerializeField] private int m_updateRatePerSecond = 2;

    private List<AIComponent> m_activeAIComponents = new List<AIComponent>();

    private NavMeshHit m_navMeshHit;
    private NavMeshPath m_navMeshPath;

    private AIGroupInstance[] m_groupInstancePool = new AIGroupInstance[100];

    private void Awake()
    {
        m_instance = this;
        m_navMeshPath = new NavMeshPath();

        for (int i = 0; i < 100; i++)
        {
            GameObject _g = new GameObject();
            _g.name = $"AI Group Instance {i}";
            m_groupInstancePool[i] = _g.AddComponent<AIGroupInstance>();
            _g.SetActive(false);
        }
    }

    public void RegisterAIComponent(AIComponent component)
    {
        if (m_activeAIComponents.Contains(component) == false)
            m_activeAIComponents.Add(component);
    }

    public AIGroupInstance GetPooledGroupInstance()
    {
        for (int i = 0; i < m_groupInstancePool.Length; i++)
        {
            if (m_groupInstancePool[i].gameObject.activeInHierarchy == false)
            {
                m_groupInstancePool[i].gameObject.SetActive(true);
                return m_groupInstancePool[i];
            }
        }

        return null;
    }

    private IEnumerator Start()
    {
        bool _running = true;
        while (_running)
        {
            for (int i = 0; i < m_activeAIComponents.Count; i++)
            {
                if (m_activeAIComponents[i].GroupInstance)
                    updateGroupInstance(m_activeAIComponents[i]);

                yield return null;
            }

            float _timeToWait = 1.0f / m_updateRatePerSecond;
            while (_timeToWait > 0)
            {
                _timeToWait -= Time.deltaTime * GameTime.GameTimeScale;
                yield return null;
            }
        }
    }

    private void updateGroupInstance(AIComponent aiComponent)
    {
        AIGroupInstance _currentGroup = aiComponent.GroupInstance;
        List<AIComponent> _validComponents = new List<AIComponent>();
        for (int i = 0; i < m_activeAIComponents.Count; i++)
        {
            AIComponent _current = m_activeAIComponents[i];
            if (_current.Entity.AIEnabled == false) continue;
            if (_current == aiComponent) continue;

            if (_current.GroupInstance == null)
            {
                if (_currentGroup.Followers.Contains(_current) == false)
                    _validComponents.Add(_current);
            }
        }

        for (int i = 0; i < _validComponents.Count; i++)
        {
            AIComponent _current = _validComponents[i];

            m_navMeshPath.ClearCorners();
            bool _pathFound = NavMesh.CalculatePath(_current.transform.position, aiComponent.transform.position, NavMesh.AllAreas, m_navMeshPath);
            if (_pathFound)
            {
                _current.CalculatedPathDistance = getPathLength(m_navMeshPath);
            }
        }

        _validComponents = _validComponents.OrderBy(component => component.CalculatedPathDistance).ToList();

        for (int i = 0; i < _validComponents.Count; i++)
        {
            if (_currentGroup.IsGroupFull())
            {
                break;
            }

            AIComponent _current = _validComponents[i];
            if (Mathf.Approximately(_current.CalculatedPathDistance, 0)) continue;

            _current.GroupInstance = _currentGroup;
            _currentGroup.AddEntity(_current);
        }
    }

    private static float getPathLength(NavMeshPath path)
    {
        float _result = 0.0f;

        if ((path.status != NavMeshPathStatus.PathInvalid) && (path.corners.Length > 1))
        {
            for (int i = 1; i < path.corners.Length; ++i)
            {
                _result += Vector3.SqrMagnitude(path.corners[i - 1] - path.corners[i]);
            }
        }

        return _result;
    }
}
