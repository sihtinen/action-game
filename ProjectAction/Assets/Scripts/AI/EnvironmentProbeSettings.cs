﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Environment Probes/New Environment Probe Settings", fileName = "New Environment Probe Settings")]
public class EnvironmentProbeSettings : ScriptableObject
{
    public int GridSize = 14;
    public float CellSize = 2.2f;
    public float IgnoreDistance_Min = 5;
    public int IgnoreDistance_Max = 14;

    public float VisibilitySqrMagThreshold = 900;
    public LayerMask EnvironmentLayer;

    public bool IgnoreNodesVisibleToPlayer = true;
}
