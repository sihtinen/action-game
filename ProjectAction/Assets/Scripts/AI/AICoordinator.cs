﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActionAI
{
    public enum AIRole
    {
        None,
        MeleePursuer,
        Flanker,
        CoverShooter
    }

    public class AICoordinator : MonoBehaviour
    {
        private struct RoleEvaluationWrapper
        {
            public AIRole Role;
            public float Score;
        }

        private static AICoordinator m_instance = null;
        public static AICoordinator GetInstance() { return m_instance; }

        [SerializeField, Range(0, 2)] private float m_roleUpdateTime = 0.5f;

        [HideInInspector, System.NonSerialized] public int RoleCount_MeleePursuers;
        [HideInInspector, System.NonSerialized] public int RoleCount_Flanker;
        [HideInInspector, System.NonSerialized] public int RoleCount_CoverShooter;
        [HideInInspector, System.NonSerialized] public float LatestProjectileFiredTimeStamp = 0;
        [HideInInspector, System.NonSerialized] public List<AIComponent> ProjectileFiringEntities = new List<AIComponent>();

        private List<AIComponent> m_allAIComponents = new List<AIComponent>();
        private List<RoleEvaluationWrapper> m_potentialRoles = new List<RoleEvaluationWrapper>();

        private GameState m_gameState = null;
        private System.Random m_random = new System.Random();

        private void Awake()
        {
            m_instance = this;
        }

        private IEnumerator Start()
        {
            m_gameState = GameState.GetInstance();

            bool _running = true;
            while (_running)
            {
                evaluateAndAssingRoles();

                float _timer = 0.0f;
                while (_timer < m_roleUpdateTime)
                {
                    updateAIValues();

                    _timer += Time.deltaTime * GameTime.GameTimeScale;
                    yield return null;
                }
            }
        }

        private void evaluateAndAssingRoles()
        {
            for (int i = 0; i < m_gameState.EnemyEntities.Count; i++)
            {
                if (m_allAIComponents.Contains(m_gameState.EnemyEntities[i].AIComponent) == false)
                    m_allAIComponents.Add(m_gameState.EnemyEntities[i].AIComponent);
            }

            for (int i = 0; i < m_allAIComponents.Count; i++)
            {
                m_allAIComponents[i].CalculateRoleScores();
            }

            for (int i = 0; i < m_allAIComponents.Count; i++)
            {
                AIComponent _currentAIComponent = m_allAIComponents[i];
                calculateAndAssingNewRole(_currentAIComponent);
            }
        }

        public void RequestNewRole(AIComponent aiComponent)
        {
            aiComponent.CalculateRoleScores();
            calculateAndAssingNewRole(aiComponent);
        }

        private void calculateAndAssingNewRole(AIComponent aiComponent)
        {
            bool _roleFound = false;

            for (int j = 0; j < aiComponent.RolesSorted.Count; j++)
            {
                if (_roleFound) break;

                AIOptionData _currentRole = aiComponent.RolesSorted[j];
                Actions.AIAction_SetRoleActionGraph _roleSetter = (Actions.AIAction_SetRoleActionGraph)_currentRole.Actions[0];

                switch (_roleSetter.Role)
                {
                    case AIRole.None:
                        _roleSetter.Execute(aiComponent, 0);
                        _roleFound = true;
                        break;

                    case AIRole.MeleePursuer:

                        if (RoleCount_MeleePursuers < 2)
                        {
                            RoleCount_MeleePursuers++;
                            _roleSetter.Execute(aiComponent, 0);
                            _roleFound = true;
                        }

                        break;

                    case AIRole.Flanker:

                        if (RoleCount_Flanker < 2)
                        {
                            RoleCount_Flanker++;
                            _roleSetter.Execute(aiComponent, 0);
                            _roleFound = true;
                        }

                        break;

                    case AIRole.CoverShooter:
                        RoleCount_CoverShooter++;
                        _roleSetter.Execute(aiComponent, 0);
                        _roleFound = true;
                        break;
                }
            }
        }

        private void updateAIValues()
        {
            RoleCount_MeleePursuers = 0;
            RoleCount_Flanker = 0;
            RoleCount_CoverShooter = 0;

            for (int i = 0; i < m_allAIComponents.Count; i++)
            {
                switch (m_allAIComponents[i].CurrentRole)
                {
                    case AIRole.MeleePursuer:
                        RoleCount_MeleePursuers++;
                        break;

                    case AIRole.Flanker:
                        RoleCount_Flanker++;
                        break;

                    case AIRole.CoverShooter:
                        RoleCount_CoverShooter++;
                        break;

                    case AIRole.None:
                        break;
                }
            }
        }
    }
}
