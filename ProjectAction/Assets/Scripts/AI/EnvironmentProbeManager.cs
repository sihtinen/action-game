﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public struct EnvironmentNodeWrapper
{
    public Vector3 Position;
    public bool VisibleToPlayer;
}

public class EnvironmentProbeManager : MonoBehaviour
{
    private static EnvironmentProbeManager m_instance = null;
    public static EnvironmentProbeManager GetInstance() { return m_instance; }

    private static Transform m_playerTransform = null;
    private static RaycastHit[] m_raycastHits = new RaycastHit[1];
    private static NavMeshHit m_navMeshHit;

    private List<EnvironmentNodeWrapper> m_cachedWrappers = new List<EnvironmentNodeWrapper>();

    private void Awake()
    {
        m_instance = this;
    }

    private void Start()
    {
        m_playerTransform = GameState.GetInstance().PlayerObject.transform;
    }

    public List<EnvironmentNodeWrapper> GenerateClosestNodes(
        Vector3 sourcePosition,
        ref EnvironmentProbeSettings settings,
        List<EnvironmentNodeWrapper> cachedList)
    {
        calculateEnvironmentProbes(sourcePosition, ref settings);
        calculateVisibility(sourcePosition, ref settings);

        float _sqrDistance = settings.IgnoreDistance_Max * settings.IgnoreDistance_Max;

        for (int i = 0; i < m_cachedWrappers.Count; i++)
        {
            EnvironmentNodeWrapper _currentWrapper = m_cachedWrappers[i];
            if (settings.IgnoreNodesVisibleToPlayer && _currentWrapper.VisibleToPlayer) continue;

            Vector3 _diff = _currentWrapper.Position - sourcePosition;
            if (_diff.sqrMagnitude < _sqrDistance)
            {
                cachedList.Add(_currentWrapper);
            }
        }

        return cachedList;
    }

    private void calculateEnvironmentProbes(Vector3 position, ref EnvironmentProbeSettings settings)
    {
        m_cachedWrappers.Clear();

        float _sqrMag_Min = settings.IgnoreDistance_Min * settings.IgnoreDistance_Min;
        float _sqrMag_Max = settings.IgnoreDistance_Max * settings.IgnoreDistance_Max;

        for (int x = 0; x < settings.GridSize; x++)
        {
            int _rawX = x - (settings.GridSize / 2);

            for (int z = 0; z < settings.GridSize; z++)
            {
                int _rawZ = z - (settings.GridSize / 2);

                float _posX = _rawX * settings.CellSize;
                float _posZ = _rawZ * settings.CellSize;

                Vector3 _positionVector = new Vector3(_posX, 0, _posZ);
                float _positionVectorSqrMagnitude = _positionVector.sqrMagnitude;

                if (_positionVectorSqrMagnitude < _sqrMag_Min ||
                    _positionVectorSqrMagnitude > _sqrMag_Max)
                {
                    continue;
                }

                _positionVector += position;

                bool _hit = NavMesh.SamplePosition(_positionVector, out m_navMeshHit, 2.0f, 0);
                if (_hit)
                {
                    _positionVector = m_navMeshHit.position;
                }

                m_cachedWrappers.Add(new EnvironmentNodeWrapper
                {
                    Position = _positionVector              
                });
            }
        }
    }

    private void calculateVisibility(Vector3 sourcePosition, ref EnvironmentProbeSettings settings)
    {
        Vector3 _playerPosition = m_playerTransform.position + Vector3.up * 1.5f;
        Vector3 _diff = _playerPosition - sourcePosition;

        if (_diff.sqrMagnitude >= settings.VisibilitySqrMagThreshold)
        {
            for (int i = 0; i < m_cachedWrappers.Count; i++)
            {
                m_cachedWrappers[i] = new EnvironmentNodeWrapper
                {
                    Position = m_cachedWrappers[i].Position,
                    VisibleToPlayer = false
                };
            }

            return;
        }

        for (int i = 0; i < m_cachedWrappers.Count; i++)
        {
            _diff = _playerPosition - m_cachedWrappers[i].Position;

            int _hits = Physics.RaycastNonAlloc(
                m_cachedWrappers[i].Position,
                _diff.normalized,
                m_raycastHits,
                _diff.magnitude,
                settings.EnvironmentLayer,
                QueryTriggerInteraction.Ignore);

            bool _visible = true;
            if (_hits > 0)
            {
                _visible = false;
            }

            m_cachedWrappers[i] = new EnvironmentNodeWrapper
            {
                Position = m_cachedWrappers[i].Position,
                VisibleToPlayer = _visible
            };
        }
    }
}
