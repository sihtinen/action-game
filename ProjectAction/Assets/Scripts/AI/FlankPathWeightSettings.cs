﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Environment Probes/New Flank Path Weight Settings", fileName = "New Flank Path Weight Settings")]
public class FlankPathWeightSettings : ScriptableObject
{
    public float Weight_DistanceToSource;
    public float Weight_DistanceToDestination;
    public float Weight_DistanceToCombatTarget;
}
