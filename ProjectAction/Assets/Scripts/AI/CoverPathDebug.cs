﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CoverPathDebug : MonoBehaviour
{
    [SerializeField] private Transform m_coverTarget;

    [Space]

    [SerializeField] private CoverWeightSettings m_coverWeightSettings;
    [SerializeField] private EnvironmentProbeSettings m_probeSettings;
    [SerializeField] private FlankPathWeightSettings m_flankWeightSettings;

    [Header("Gizmos")]
    [SerializeField] private bool m_draw_CoverPoint = true;
    [SerializeField] private Color m_color_CoverPoint = Color.blue;
    [SerializeField] private float m_size_CoverPoint = 0.5f;

    [Space]

    [SerializeField] private bool m_draw_AllPotentialNodes = true;
    [SerializeField] private Color m_color_AllPotentialNodes = Color.yellow;
    [SerializeField] private float m_size_AllPotentialNodes = 0.2f;

    [Space]

    [SerializeField] private bool m_draw_BestWaypoint_Destination = true;
    [SerializeField] private Color m_color_BestWaypoint_Destination = Color.yellow;
    [SerializeField] private float m_size_BestWaypoint_Destination = 0.5f;

    [Space]

    [SerializeField] private bool m_draw_FinalPath = true;
    [SerializeField] private Color m_color_FinalPath = Color.white;

    private CoverPointOctree.CoverPoint m_coverPoint = null;
    private List<EnvironmentNodeWrapper> m_potentialNodes_Source = new List<EnvironmentNodeWrapper>();
    private List<EnvironmentNodeWrapper> m_potentialNodes_Destination = new List<EnvironmentNodeWrapper>();

    private EnvironmentNodeWrapper m_bestWaypoint_Destination;

    private NavMeshPath m_path;
    private NavMeshHit m_navMeshHit;
    private List<Vector3> m_pathCorners = new List<Vector3>();

    private void Awake()
    {
        m_path = new NavMeshPath();
    }

    private void Update()
    {
        m_coverPoint = CoverPointOctree.GetInstance().GetValidCoverPoint(ref m_coverWeightSettings, m_coverTarget.position, transform.position, m_coverTarget.position);
        if (m_coverPoint == null) return;

        m_potentialNodes_Source.Clear();
        m_potentialNodes_Source = EnvironmentProbeManager.GetInstance().GenerateClosestNodes(
            m_coverPoint.Position,
            ref m_probeSettings,
            m_potentialNodes_Source);

        m_potentialNodes_Destination.Clear();
        m_potentialNodes_Destination = EnvironmentProbeManager.GetInstance().GenerateClosestNodes(
            m_coverPoint.Position,
            ref m_probeSettings,
            m_potentialNodes_Destination);

        m_bestWaypoint_Destination = ActionAI.AINavigationManager.GetInstance().CalculateBestWaypoint(
            transform.position, 
            m_coverPoint.Position, 
            m_coverTarget.position, 
            ref m_flankWeightSettings,
            ref m_potentialNodes_Destination);

        m_pathCorners.Clear();
        NavMesh.CalculatePath(transform.position, m_bestWaypoint_Destination.Position, NavMesh.AllAreas, m_path);
        for (int i = 0; i < m_path.corners.Length; i++)
        {
            m_pathCorners.Add(m_path.corners[i]);
        }
        NavMesh.CalculatePath(m_bestWaypoint_Destination.Position, m_coverPoint.Position, NavMesh.AllAreas, m_path);
        for (int i = 0; i < m_path.corners.Length; i++)
        {
            m_pathCorners.Add(m_path.corners[i]);
        }
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying == false) return;
        else if (enabled == false) return;

        if (m_draw_CoverPoint)
        {
            if (m_coverPoint != null)
            {
                Gizmos.color = m_color_CoverPoint;
                Gizmos.DrawWireSphere(m_coverPoint.Position, m_size_CoverPoint);
                DrawArrow.ForGizmo(m_coverPoint.Position, m_coverTarget.position - m_coverPoint.Position, 1.5f, 25.0f, 1.0f);
            }
        }

        if (m_draw_AllPotentialNodes)
        {
            Gizmos.color = m_color_AllPotentialNodes;

            for (int i = 0; i < m_potentialNodes_Destination.Count; i++)
            {
                Gizmos.DrawWireSphere(m_potentialNodes_Destination[i].Position, m_size_AllPotentialNodes);
            }
        }

        if (m_draw_BestWaypoint_Destination)
        {
            Gizmos.color = m_color_BestWaypoint_Destination;
            Gizmos.DrawWireSphere(m_bestWaypoint_Destination.Position, m_size_BestWaypoint_Destination);
            DrawArrow.ForGizmo(transform.position, m_bestWaypoint_Destination.Position - transform.position, 1.5f, 25.0f, 1.0f);

            if (m_coverPoint != null)
            {
                DrawArrow.ForGizmo(m_bestWaypoint_Destination.Position, m_coverPoint.Position - m_bestWaypoint_Destination.Position, 1.5f, 25.0f, 1.0f);
            }
        }

        if (m_draw_FinalPath)
        {
            Gizmos.color = m_color_FinalPath;

            for (int i = 1; i < m_pathCorners.Count; i++)
            {
                Gizmos.DrawLine(m_pathCorners[i], m_pathCorners[i - 1]);
            }
        }
    }
}
