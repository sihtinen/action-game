﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtensions
{
    public static Vector2_NonUnity Convert_Vector2NonUnity(this Vector2 self)
    {
        return new Vector2_NonUnity
        {
            X = self.x,
            Y = self.y
        };
    }

    public static Vector2_NonUnity Convert_Vector2NonUnity(this Vector3 self)
    {
        return new Vector2_NonUnity
        {
            X = self.x,
            Y = self.y
        };
    }
}
