﻿using UnityEngine;

public static class FloatExtensions
{
    public static float Remap(this float value, float fromMin, float fromMax, float toMin, float toMax, bool clamp = false)
    {
        if (clamp) value = Mathf.Max(fromMin, Mathf.Min(value, fromMax));
        return (value - fromMin) * (toMax - toMin) / (fromMax - fromMin) + toMin;
    }
}
