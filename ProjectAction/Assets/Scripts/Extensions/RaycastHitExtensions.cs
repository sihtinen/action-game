﻿using UnityEngine;

public static class RaycastHitExtensions
{
    public static RaycastHit GetClosestHit(this RaycastHit[] array, int hits, GameObject source = null)
    {
        RaycastHit _result = array[0];

        for (int i = 0; i < hits; i++)
        {
            RaycastHit _current = array[i];

            if (source)
            {
                if (_current.collider.gameObject == source) continue;
            }

            if (_current.point == Vector3.zero)
            {
                continue;
            }

            if (_current.distance < _result.distance)
            {
                _result = _current;
            }
        }

        return _result;
    }
}
