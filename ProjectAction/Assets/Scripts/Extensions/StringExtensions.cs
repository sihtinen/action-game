﻿using System.Text;

public static class StringExtensions
{
    public static string AddSpacesBeforeUpperCase(this string source)
    {
        if (string.IsNullOrWhiteSpace(source))
            return "";

        StringBuilder _newText = new StringBuilder(source.Length * 2);
        _newText.Append(source[0]);

        for (int i = 1; i < source.Length; i++)
        {
            if (char.IsUpper(source[i]) && source[i - 1] != ' ')
            {
                _newText.Append(' ');
            }

            _newText.Append(source[i]);
        }
        return _newText.ToString();
    }
}
