﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_EnemyDebugElement : MonoBehaviour
{
    [SerializeField] private Vector2 m_screenPosOffset;

    [SerializeField] private Slider m_sliderCurrentAggression;
    [SerializeField] private Slider m_sliderCurrentVision;

    [SerializeField] private TMP_Text m_textEntityName;
    [SerializeField] private TMP_Text m_textCurrentAction;
    [SerializeField] private TMP_Text m_textCurrentRole;

    private EnemyEntity m_entity = null;
    private Camera m_mainCamera = null;

    private RectTransform m_rectTransform = null;

    public void Initialize(EnemyEntity entity, Camera mainCamera)
    {
        m_entity = entity;
        m_mainCamera = mainCamera;

        m_rectTransform = GetComponent<RectTransform>();

        m_entity.OnActionChanged.AddListener(onActionChanged);

        m_textEntityName.SetText(entity.gameObject.name);

        gameObject.SetActive(true);
    }

    public void ManualUpdate()
    {
        Vector3 _viewPos = m_mainCamera.WorldToViewportPoint(m_entity.transform.position);
        if (_viewPos.x < 0 || _viewPos.x > 1 ||
            _viewPos.y < 0 || _viewPos.y > 1 ||
            _viewPos.z < 0 ||
            m_entity.AIEnabled == false ||
            m_entity.UIComponent.EnableDebugUI == false)
        {
            if (gameObject.activeInHierarchy == true)
                gameObject.SetActive(false);
        }
        else
        {
            if (gameObject.activeInHierarchy == false)
                gameObject.SetActive(true);
        }

        if (gameObject.activeInHierarchy == true)
        {
            m_rectTransform.position = m_mainCamera.WorldToScreenPoint(m_entity.transform.position);
            m_rectTransform.position += (Vector3)m_screenPosOffset;

            m_sliderCurrentAggression.value = m_entity.AggressionLevel;
            m_sliderCurrentVision.value = m_entity.VisionComponent.CurrentPlayerVisibility;

            m_textCurrentRole.SetText($"Role: {m_entity.AIComponent.CurrentRole.ToString()}");
        }
    }

    private void onActionChanged()
    {
        m_textCurrentAction.SetText(m_entity.AIComponent.CurrentAction.GetEditorName());
    }
}
