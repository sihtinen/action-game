﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ActionAI;

public class UI_EnemyElement : MonoBehaviour
{
    [HideInInspector] public EnemyUIComponent UIComponent;
    [HideInInspector] public RectTransform MyRectTransform;

    [SerializeField] private Image m_attackPromptImage = null;
    [SerializeField] private Image m_takedownPromptImage = null;

    private const string m_attackInputAction = "Attack";
    private const string m_takedownInputAction = "Takedown";
    private InputPromptManager m_inputPromptManager = null;

    private void Awake()
    {
        MyRectTransform = GetComponent<RectTransform>();

        m_inputPromptManager = InputPromptManager.GetInstance();
        m_inputPromptManager.OnInputDeviceChanged.AddListener(OnInputDeviceChanged);
    }

    public void OnInputDeviceChanged()
    {
        m_attackPromptImage.sprite = m_inputPromptManager.GetInputBindingSprite(m_attackInputAction);
        m_takedownPromptImage.sprite = m_inputPromptManager.GetInputBindingSprite(m_takedownInputAction);
    }

    public void EnableAttackElement()
    {
        m_attackPromptImage.enabled = true;
        m_takedownPromptImage.enabled = false;
    }

    public void EnableTakedownElement()
    {
        m_attackPromptImage.enabled = false;
        m_takedownPromptImage.enabled = true;
    }
}
