﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "UI/Controller Prompt Sprite Container")]
public class ControllerPromptSpriteContainer : ScriptableObject
{
    public Sprite DpadUp;
    public Sprite DpadDown;
    public Sprite DpadLeft;
    public Sprite DpadRight;

    [Space]

    public Sprite AnalogueRightPress;
    public Sprite AnalogueLeftPress;

    [Space]

    public Sprite FaceButtonUp;
    public Sprite FaceButtonDown;
    public Sprite FaceButtonLeft;
    public Sprite FaceButtonRight;

    [Space]

    public Sprite BumperRight;
    public Sprite BumperLeft;
    public Sprite TriggerRight;
    public Sprite TriggerLeft;

    [Space]

    public Sprite Select;
    public Sprite Start;

    public Sprite GetSprite(string binding)
    {
        switch (binding)
        {
            case "<Gamepad>/buttonNorth": return FaceButtonUp;
            case "<Gamepad>/buttonSouth": return FaceButtonDown;
            case "<Gamepad>/buttonWest": return FaceButtonLeft;
            case "<Gamepad>/buttonEast": return FaceButtonRight;

            case "<Gamepad>/leftStickPress": return AnalogueLeftPress;
            case "<Gamepad>/rightStickPress": return AnalogueRightPress;

            case "<Gamepad>/leftShoulder": return BumperLeft;
            case "<Gamepad>/rightShoulder": return BumperRight;
            case "<Gamepad>/leftTrigger": return TriggerLeft;
            case "<Gamepad>/rightTrigger": return TriggerRight;

            case "<Gamepad>/startButton": return Start;
            case "<Gamepad>/selectButton": return Select;
        }

        Debug.Log(binding);

        return null;
    }
}
