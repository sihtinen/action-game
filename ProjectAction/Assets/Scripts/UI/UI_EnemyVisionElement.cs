﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EnemyVisionElement : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private Image m_imgFill_Right;
    [SerializeField] private Image m_imgFill_Left;

    private Canvas m_canvas = null;
    private EnemyEntity m_entity = null;
    private static Transform m_playerTransform = null;
    private static Transform m_cameraTransform = null;

    private const float m_orbitDistance = 3.5f;
    private const float m_orbitHeightOffset = 0.8f;
    private const float m_distanceSkinWidth = 2.0f;
    private const float m_lookDirDotLimit = 0.88f;
    private const float m_minScale = 0.4f;

    private static Color32 m_colorMin = Color.yellow, m_colorMax = Color.red;

    public void Initialize(EnemyEntity entity)
    {
        m_entity = entity;
        m_canvas = GetComponent<Canvas>();
        m_canvas.enabled = false;
    }

    public void ManualUpdate()
    {
        if (m_playerTransform == null) m_playerTransform = GameState.GetInstance().PlayerObject.transform;
        if (m_cameraTransform == null) m_cameraTransform = MainCameraComponent.GetInstance().transform;

        if (m_entity.AIEnabled == false || m_entity.AggressionLevel <= 0.0f)
        {
            if (m_canvas.enabled)
                m_canvas.enabled = false;

            return;
        }

        m_imgFill_Left.fillAmount = m_entity.AggressionLevel;
        m_imgFill_Right.fillAmount = m_entity.AggressionLevel;

        Color32 _targetColor = Color32.Lerp(m_colorMin, m_colorMax, m_entity.AggressionLevel);
        m_imgFill_Left.color = _targetColor;
        m_imgFill_Right.color = _targetColor;

        Vector3 _toEntity = m_entity.transform.position - m_playerTransform.position;
        float _magnitude = _toEntity.magnitude;

        float _lookDirectionDot = Vector3.Dot(m_cameraTransform.forward, _toEntity.normalized);
        float _lookDirectionModifier = 1.0f;

        float _targetScale = 1.0f;

        if (_lookDirectionDot < m_lookDirDotLimit)
        {
            _lookDirectionModifier = Mathf.Clamp(_lookDirectionDot, 0.0f, m_lookDirDotLimit);
            _lookDirectionModifier = Mathf.Lerp(0.0f, 1.0f, _lookDirectionModifier / m_lookDirDotLimit);

            _targetScale = Mathf.Lerp(m_minScale, 1.0f, _lookDirectionModifier);

            _lookDirectionModifier = Mathf.Clamp(_lookDirectionModifier, 0.33f, 1.0f);
        }

        if (_magnitude <= (m_orbitDistance + m_distanceSkinWidth))
        {
            float _distNormalized = _magnitude / (m_orbitDistance + m_distanceSkinWidth);
            float _distScalar = _distNormalized * _magnitude;
            _distScalar = Mathf.Clamp(_distScalar, 1.0f, m_orbitDistance);

            float _newTargetScale = Mathf.Lerp(m_minScale, 1.0f, _distNormalized);
            if (_newTargetScale < _targetScale)
            {
                _targetScale = _newTargetScale;
            }

            transform.position = m_playerTransform.position + _toEntity.normalized * _distScalar * _lookDirectionModifier;
        }
        else
        {
            transform.position = m_playerTransform.position + _toEntity.normalized * m_orbitDistance * _lookDirectionModifier;
        }

        transform.position += Vector3.up * m_orbitHeightOffset;
        transform.localScale = new Vector3(_targetScale, _targetScale, _targetScale);
        transform.forward = _toEntity.normalized;

        if (m_canvas.enabled == false)
            m_canvas.enabled = true;
    }
}
