﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "UI/Keyboard Mouse Prompt Sprite Container")]
public class KeyboardMousePromptSpriteContainer : ScriptableObject
{
    public Sprite BlankSmall;
    public Sprite BlankWide;

    [Space]

    public Sprite Space;
    public Sprite LeftShift;
    public Sprite LeftControl;
    public Sprite LeftAlt;
    public Sprite Tab;
    public Sprite CapsLock;
    public Sprite Esc;
    public Sprite MarkLeft;

    [Space]

    public Sprite MouseLeftButton;
    public Sprite MouseMiddleButton;
    public Sprite MouseRightButton;
    public Sprite MouseBlank;

    public Sprite GetSprite(string binding)
    {
        switch (binding)
        {
            case "<Mouse>/leftButton": return MouseLeftButton;
            case "<Mouse>/middleButton": return MouseMiddleButton;
            case "<Mouse>/rightButton": return MouseRightButton;
        }

        Debug.Log(binding);

        return null;
    }
}
