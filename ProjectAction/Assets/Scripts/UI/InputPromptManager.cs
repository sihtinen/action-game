﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class InputPromptManager : MonoBehaviour
{
    private enum CurrentInputDevice
    {
        KeyboardMouse,
        DS3,
        DS4,
        Xbox360,
        XboxOne,
        GenericGamepad
    }

    private static InputPromptManager m_instance = null;
    public static InputPromptManager GetInstance() { return m_instance; }

    [SerializeField] private KeyboardMousePromptSpriteContainer m_keyboardMouse;

    [Space]

    [SerializeField] private ControllerPromptSpriteContainer m_dualshock3;
    [SerializeField] private ControllerPromptSpriteContainer m_dualshock4;
    [SerializeField] private ControllerPromptSpriteContainer m_xbox360;
    [SerializeField] private ControllerPromptSpriteContainer m_xboxOne;

    public UnityEvent OnInputDeviceChanged;

    private CurrentInputDevice m_currentDevice = CurrentInputDevice.KeyboardMouse;
    private PlayerInput m_playerInput = null;

    private void Awake()
    {
        m_instance = this;
        InputUser.onChange += onInputDeviceChanged;
    }

    private void Start()
    {
        m_playerInput = FindObjectOfType<PlayerInput>();
        updateControlScheme(m_playerInput.currentControlScheme);
    }

    private void onInputDeviceChanged(InputUser user, InputUserChange change, InputDevice device)
    {
        if (change == InputUserChange.ControlSchemeChanged)
        {
            updateControlScheme(user.controlScheme.Value.name);
        }
    }

    private void updateControlScheme(string deviceName)
    {
        switch(deviceName)
        {
            case "Keyboard&Mouse":
                m_currentDevice = CurrentInputDevice.KeyboardMouse;
                break;

            case "Gamepad":

                for (int i = 0; i < m_playerInput.devices.Count; i++)
                {
                    if (m_playerInput.devices[i].enabled)
                    {
                        switch(m_playerInput.devices[i].name)
                        {
                            case "DualShock3GamepadHID":
                                m_currentDevice = CurrentInputDevice.DS3;
                                break;

                            case "DualShock4GamepadHID":
                                m_currentDevice = CurrentInputDevice.DS4;
                                break;

                            default:
                                m_currentDevice = CurrentInputDevice.GenericGamepad;
                                break;
                        }

                        break;
                    }
                }

                break;
        }

        OnInputDeviceChanged.Invoke();
    }

    public Sprite GetInputBindingSprite(string action)
    {
        switch (m_currentDevice)
        {
            case CurrentInputDevice.DS3:

                for (int i = 0; i < m_playerInput.currentActionMap.actions.Count; i++)
                {
                    if (m_playerInput.currentActionMap.actions[i].name == action)
                    {
                        string _bindingName = m_playerInput.currentActionMap.actions[i].bindings[0].effectivePath;
                        return m_dualshock3.GetSprite(_bindingName);
                    }
                }

                break;

            case CurrentInputDevice.DS4:

                for (int i = 0; i < m_playerInput.currentActionMap.actions.Count; i++)
                {
                    if (m_playerInput.currentActionMap.actions[i].name == action)
                    {
                        string _bindingName = m_playerInput.currentActionMap.actions[i].bindings[0].effectivePath;
                        return m_dualshock4.GetSprite(_bindingName);
                    }
                }

                break;

            case CurrentInputDevice.GenericGamepad:

                for (int i = 0; i < m_playerInput.currentActionMap.actions.Count; i++)
                {
                    if (m_playerInput.currentActionMap.actions[i].name == action)
                    {
                        string _bindingName = m_playerInput.currentActionMap.actions[i].bindings[0].effectivePath;
                        return m_xbox360.GetSprite(_bindingName);
                    }
                }

                break;

            case CurrentInputDevice.KeyboardMouse:

                for (int i = 0; i < m_playerInput.currentActionMap.actions.Count; i++)
                {
                    if (m_playerInput.currentActionMap.actions[i].name == action)
                    {
                        string _bindingName = m_playerInput.currentActionMap.actions[i].bindings[1].effectivePath;
                        return m_keyboardMouse.GetSprite(_bindingName);
                    }
                }

                break;
        }

        return null;
    }
}
