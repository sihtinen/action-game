﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ParryBlockCanvas : MonoBehaviour
{
    private static UI_ParryBlockCanvas m_instance = null;
    public static UI_ParryBlockCanvas GetInstance() { return m_instance; }

    [Header("Object References")]
    [SerializeField] private RectTransform m_parryElementLeft;
    [SerializeField] private RectTransform m_parryElementRight;

    private Canvas m_canvas = null;

    private void Awake()
    {
        m_instance = this;

        m_canvas = GetComponent<Canvas>();
        m_canvas.enabled = false;
    }

    public void ParryWindowActive(bool isActive)
    {
        m_canvas.enabled = isActive;
    }
}
