﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MainCanvasComponent : MonoBehaviour
{
    [Header("Attack Hit Settings")]
    [SerializeField] private float m_attackHitRoutineLength = 0.3f;
    [SerializeField, Range(1, 3)] private float m_attackHitElementMaxSize = 1;
    [SerializeField] private AnimationCurve m_curveAttackHitElementSize;
    [SerializeField] private AnimationCurve m_curveAttackHitElementColor;

    [Header("Object References")]
    [SerializeField] private GameObject m_crosshairObject_Static;
    [SerializeField] private GameObject m_crosshairObject_Moving;
    [SerializeField] private Image m_imgAttackHitElement;

    private Canvas m_canvas = null;
    private PlayerAnimationController m_playerAnimController = null;

    private Coroutine m_routineAttackHit = null;

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();
        m_imgAttackHitElement.enabled = false;
    }

    private void Start()
    {
        GameState.GetInstance().UI_MainCanvas = this;
        GameState.GetInstance().OnPlayerAttackHit.AddListener(OnPlayerAttackHit);
        m_playerAnimController = GameState.GetInstance().PlayerObject.GetComponentInChildren<PlayerAnimationController>();
    }

    private void Update()
    {
        if (m_playerAnimController.IsCinematicAnimationRunning() == m_canvas.enabled)
        {
            m_canvas.enabled = !m_playerAnimController.IsCinematicAnimationRunning();
        }
    }

    public void OnPlayerAttackHit(GameState.PlayerAttackHitInfo info)
    {
        if (m_routineAttackHit != null) StopCoroutine(m_routineAttackHit);
        m_routineAttackHit = StartCoroutine(routine_attackHit(info));
    }

    private IEnumerator routine_attackHit(GameState.PlayerAttackHitInfo info)
    {
        m_imgAttackHitElement.enabled = true;

        Color _startColor = m_imgAttackHitElement.color;
        _startColor.a = 1.0f;
        Color _targetColor = _startColor;
        _targetColor.a = 0.0f;

        float _timer = 0.0f;
        while (_timer < m_attackHitRoutineLength)
        {
            _timer += Time.unscaledDeltaTime * GameTime.GameTimeScale;

            float _normalized = _timer / m_attackHitRoutineLength;
            float _sizeAmount = m_curveAttackHitElementSize.Evaluate(_normalized);
            float _colorAmount = m_curveAttackHitElementColor.Evaluate(_normalized);

            m_imgAttackHitElement.rectTransform.localScale = Vector3.Lerp(Vector3.one, new Vector3(m_attackHitElementMaxSize, m_attackHitElementMaxSize, m_attackHitElementMaxSize), _sizeAmount);
            m_imgAttackHitElement.color = Color.Lerp(_startColor, _targetColor, _colorAmount);

            yield return null;
        }

        m_imgAttackHitElement.enabled = false;
        m_routineAttackHit = null;
    }
}
