﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugAnimationPlayer : MonoBehaviour
{
    [SerializeField] private Vector3 m_debugPosition;
    [SerializeField] private Vector3 m_debugEulerAngles;
    [SerializeField] private string m_debugAnimationState = "";
    [SerializeField] private KeyCode m_debugPlayAnimationInput = KeyCode.P;

    [Space]

    [SerializeField] private Animator m_animator;

    private void Update()
    {
        if (Input.GetKeyDown(m_debugPlayAnimationInput))
        {
            transform.position = m_debugPosition;
            transform.eulerAngles = m_debugEulerAngles;

            m_animator.Play(m_debugAnimationState, 0);
        }
    }
}
