﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshRendererGroupComponent))]
public class Editor_MeshRendererGroupComponent : Editor
{
    private MeshRendererGroupComponent m_target = null;

    private void OnEnable()
    {
        m_target = (MeshRendererGroupComponent)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Update MeshRenderers"))
        {
            m_target.UpdateMeshRenderers();
        }
    }
}
