﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

public class EnemyUIComponent : MonoBehaviour
{
    public bool AttackElementVisible = false;
    public bool TakedownElementVisible = false;

    [HideInInspector] public float OverrideVerticalOffset = 0;

    public PlayerTakedownManager.TakedownType CurrentTakedownType;

    [SerializeField] private Transform m_uiPositionTransform;

    [Header("Debug")]
    public bool EnableDebugUI = false;

    private EnemyEntity m_entity = null;

    private void Start()
    {
        EnemyCanvas.GetInstance().RegisterUIComponent(this);

        m_entity = GetComponent<EnemyEntity>();
        m_entity.UIComponent = this;
    }

    public Vector3 GetWorldPosition()
    {
        if (m_uiPositionTransform)
        {
            return m_uiPositionTransform.position + Vector3.up * OverrideVerticalOffset;
        }

        return transform.position + Vector3.up * OverrideVerticalOffset;
    }

    public Vector3 GetScreenPosition(Camera camera)
    {
        return camera.WorldToScreenPoint(GetWorldPosition());
    }
}
