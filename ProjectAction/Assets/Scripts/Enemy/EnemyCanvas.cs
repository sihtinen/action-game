﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

public class EnemyCanvas : MonoBehaviour
{
    private static EnemyCanvas m_instance = null;
    public static EnemyCanvas GetInstance() { return m_instance; }

    [SerializeField] private int m_elementPoolSize = 32;

    [SerializeField] private GameObject m_takedownElementPrefab;
    [SerializeField] private float m_takedownElementVerticalOffset = 10.0f;

    [Space]

    [SerializeField] private GameObject m_enemyVisionElementPrefab;

    [Space]

    [SerializeField] private GameObject m_enemyDebugUIElementPrefab;

    private int m_enemyCount = 0;

    private UI_EnemyElement[] m_allUIElements;
    private UI_EnemyVisionElement[] m_visionElements;
    private UI_EnemyDebugElement[] m_allDebugUIElements;

    private Camera m_mainCamera;

    private void Awake()
    {
        m_instance = this;

        m_allUIElements = new UI_EnemyElement[m_elementPoolSize];
        m_visionElements = new UI_EnemyVisionElement[m_elementPoolSize];
        m_allDebugUIElements = new UI_EnemyDebugElement[m_elementPoolSize];

        for (int i = 0; i < m_elementPoolSize; i++)
        {
            GameObject _g = Instantiate(m_takedownElementPrefab, transform);
            _g.SetActive(false);
            m_allUIElements[i] = _g.GetComponent<UI_EnemyElement>();

            _g = Instantiate(m_enemyDebugUIElementPrefab, transform);
            _g.SetActive(false);
            m_allDebugUIElements[i] = _g.GetComponent<UI_EnemyDebugElement>();

            _g = Instantiate(m_enemyVisionElementPrefab, null);
            _g.name = "UI_EntityVisionElement_" + i;
            _g.SetActive(false);
            m_visionElements[i] = _g.GetComponent<UI_EnemyVisionElement>();
        }
    }

    private void Start()
    {
        m_mainCamera = MainCameraComponent.GetInstance().gameObject.GetComponent<Camera>();
    }

    public void RegisterUIComponent(EnemyUIComponent component)
    {
        EnemyEntity _entity = component.gameObject.GetComponent<EnemyEntity>();

        m_allUIElements[m_enemyCount].UIComponent = component;
        m_visionElements[m_enemyCount].Initialize(_entity);
        m_allDebugUIElements[m_enemyCount].Initialize(_entity, m_mainCamera);

        m_visionElements[m_enemyCount].gameObject.SetActive(true);

        m_enemyCount++;
    }

    private void Update()
    {
        for (int i = 0; i < m_enemyCount; i++)
        {
            m_allUIElements[i].UIComponent.OverrideVerticalOffset = 0;
            m_allUIElements[i].UIComponent.AttackElementVisible = false;
            m_allUIElements[i].UIComponent.TakedownElementVisible = false;
        }
    }

    private void LateUpdate()
    {
        for (int i = 0; i < m_enemyCount; i++)
        {
            if (m_allUIElements[i].UIComponent.TakedownElementVisible)
            {
                m_allUIElements[i].MyRectTransform.position = m_allUIElements[i].UIComponent.GetScreenPosition(m_mainCamera) + Vector3.up * m_takedownElementVerticalOffset;
                m_allUIElements[i].EnableTakedownElement();
                m_allUIElements[i].gameObject.SetActive(true);
            }
            else if (m_allUIElements[i].UIComponent.AttackElementVisible)
            {
                m_allUIElements[i].MyRectTransform.position = m_allUIElements[i].UIComponent.GetScreenPosition(m_mainCamera) + Vector3.up * m_takedownElementVerticalOffset;
                m_allUIElements[i].EnableAttackElement();
                m_allUIElements[i].gameObject.SetActive(true);
            }
            else
            {
                m_allUIElements[i].gameObject.SetActive(false);
            }

            m_visionElements[i].ManualUpdate();
            m_allDebugUIElements[i].ManualUpdate();
        }
    }
}
