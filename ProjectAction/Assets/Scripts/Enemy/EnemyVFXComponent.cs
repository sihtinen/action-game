﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionAI;

public class EnemyVFXComponent : MonoBehaviour
{
    [Header("Curves")]
    [SerializeField] private AnimationCurve m_curveDeathDissolve;

    [Header("Materials")]
    [SerializeField, ColorUsage(true, true)] private Color m_outlineColor_TakedownHighlight;

    [Header("Object References")]
    [SerializeField] private SkinnedMeshRenderer m_renderer;
    [SerializeField] private ParticleSystem m_particlesDeath;

    [SerializeField] private Transform m_lightMeleeWeaponTransform;
    [SerializeField] private Transform m_upperChestBone;

    private EnemyEntity m_entity = null;
    private Material m_baseMaterial = null;
    private List<Renderer> m_registeredRendererList = new List<Renderer>();

    private bool m_previousFrameVisibility = true;

    private void Awake()
    {
        m_entity = GetComponent<EnemyEntity>();
    }

    private void Start()
    {
        m_baseMaterial = m_renderer.materials[0];
        SetDeathSliceAmount(-10);

        m_registeredRendererList.Add(m_renderer);
    }

    private void LateUpdate()
    {
        if (m_entity.UIComponent.TakedownElementVisible == m_previousFrameVisibility) return;

        if (m_entity.UIComponent.TakedownElementVisible)
        {
            HighlightsFX.GetInstance().AddRenderers(m_entity.InstanceID, m_registeredRendererList, m_outlineColor_TakedownHighlight, HighlightsFX.SortingType.DepthFiltered);
        }
        else
        {
            HighlightsFX.GetInstance().RemoveRenderers(m_entity.InstanceID, m_registeredRendererList);
        }

        m_previousFrameVisibility = m_entity.UIComponent.TakedownElementVisible;
    }

    public void SetDeathSliceAmount(float sliceAmount, bool useCurve = false)
    {
        if (useCurve)
        {
            m_baseMaterial.SetFloat("_SliceAmount", Mathf.Lerp(-2f, 2f, m_curveDeathDissolve.Evaluate(sliceAmount)));
        }
        else
        {
            m_baseMaterial.SetFloat("_SliceAmount", sliceAmount);
        }
    }

    public void SetLightMeleeWeaponParent(Transform newParent, Vector3 localPositionOffset, Vector3 localEulerOffset, bool setNewPosRot = false)
    {
        m_lightMeleeWeaponTransform.parent = newParent;

        if (setNewPosRot)
        {
            m_lightMeleeWeaponTransform.localPosition = localPositionOffset;
            m_lightMeleeWeaponTransform.localEulerAngles = localEulerOffset;
        }
    }

    public void SetLightMeleeWeaponParentToUpperChest()
    {
        m_lightMeleeWeaponTransform.parent = m_upperChestBone;
    }
}
