﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitCollider : MonoBehaviour
{
    public float DamageMultiplier = 1.0f;

    private EnemyEntity m_myEntity = null;

    public void ProjectileHit(float damage, Vector3 point, Vector3 direction)
    {
        bool _death = m_myEntity.DealDamage(damage * DamageMultiplier);
        if (_death)
        {

        }
    }

    public void SetActive(bool newState)
    {
        Physics.IgnoreCollision(
            GetComponent<Collider>(),
            GameState.GetInstance().PlayerObject.GetComponent<Collider>(),
            newState);
    }

    public void RegisterEntity(ref EnemyEntity entity)
    {
        m_myEntity = entity;
    }
}
