﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "ActionAI/Entity Settings/Entity Base Settings Asset", fileName = "EnemyEntity Base Settings")]
public class EnemyEntitySettings : ScriptableObject
{
    [Header("General")]
    public int MaxHealth = 100;

    public float MovementSpeed_Walk;
    public float MovementSpeed_Run;

    [Header("Animation States")]
    public string AnimState_Idle = "Idle";
    public string AnimState_Move = "Move";
    public string AnimState_Climb_Easy = "Climb Easy";
    public string AnimState_Climb_Hard_Begin = "Climb Hard Begin";
    public string AnimState_Climb_Hard = "Climb Hard";
    public string AnimState_Drop = "Drop";

    public string AnimState_TakedownAirTarget = "AirTakedown Target";
    public string AnimState_TakedownBehindGrabLightMeleeWeapon_Target = "Takedown Behind GrabLightMeleeWeapon Target";

    public string AnimState_TakeCover_Full = "Take Cover Full";
    public string AnimState_TakeCover_Half = "Take Cover Half";

    public string AnimState_LightMelee_Equip = "Light Melee Equip";
    public string AnimState_LightMelee_Unequip = "Light Melee Unequip";
    public string AnimState_LightMelee_Idle = "Light Melee Idle";
    public string AnimState_LightMelee_Attack_Light_01 = "Light Melee Attack Light 01";
    public string AnimState_LightMelee_Attack_Light_02 = "Light Melee Attack Light 02";
    public string AnimState_LightMelee_Parried = "Light Melee Parried";

    public string AnimState_LightPistol_Equip = "LightPistol Equip";
    public string AnimState_LightPistol_Unequip = "LightPistol Unequip";
    public string AnimState_LightPistol_Aim_Layer0 = "LightPistol Aim";
    public string AnimState_LightPistol_Aim_Layer1 = "LightPistol Aim";
    public string AnimState_LightPistol_Recoil = "LightPistol Recoil";

    [Header("Aggression")]
    public float AggressionThreshold_Investigation = 0.33f;
    public float AggressionDecreseSpeed = 0.1f;
    public float AggressionDecayTimeStationary = 4.0f;

    [Header("Animation Curves")]
    public AnimationCurve Curve_ClimbHardBeginMovement;

    [Header("Melee Attack Animations")]
    public EnemyEntity.AttackTransformManipulator AttackTransformManipulator_LightMelee_01;
    public EnemyEntity.AttackTransformManipulator AttackTransformManipulator_LightMelee_02;
}
