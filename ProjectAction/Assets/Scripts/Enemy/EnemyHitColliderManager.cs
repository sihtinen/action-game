﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitColliderManager : MonoBehaviour
{
    [Header("Spine")]
    [SerializeField] private EnemyHitCollider m_colliderHead;
    [SerializeField] private EnemyHitCollider m_colliderUpperChest;
    [SerializeField] private EnemyHitCollider m_colliderLowerChest;
    [Header("Arms")]
    [SerializeField] private EnemyHitCollider m_colliderRightArmUpper;
    [SerializeField] private EnemyHitCollider m_colliderRightArmLower;
    [Space]
    [SerializeField] private EnemyHitCollider m_colliderLeftArmUpper;
    [SerializeField] private EnemyHitCollider m_colliderLeftArmLower;
    [Header("Legs")]
    [SerializeField] private EnemyHitCollider m_colliderRightLegUpper;
    [SerializeField] private EnemyHitCollider m_colliderRightLegLower;
    [Space]
    [SerializeField] private EnemyHitCollider m_colliderLeftLegUpper;
    [SerializeField] private EnemyHitCollider m_colliderLeftLegLower;

    private EnemyHitCollider[] m_allColliders;

    private void Awake()
    {
        m_allColliders = new EnemyHitCollider[11];

        m_allColliders[0] = m_colliderHead;
        m_allColliders[1] = m_colliderUpperChest;
        m_allColliders[2] = m_colliderLowerChest;

        m_allColliders[3] = m_colliderRightArmUpper;
        m_allColliders[4] = m_colliderRightArmLower;

        m_allColliders[5] = m_colliderLeftArmUpper;
        m_allColliders[6] = m_colliderLeftArmLower;

        m_allColliders[7] = m_colliderRightLegUpper;
        m_allColliders[8] = m_colliderRightLegLower;

        m_allColliders[9] = m_colliderLeftLegUpper;
        m_allColliders[10] = m_colliderLeftLegLower;
    }

    public void SetActive(bool newState)
    {
        for (int i = 0; i < 11; i++)
        {
            m_allColliders[i].SetActive(newState);
        }
    }

    public void RegisterEntity(ref EnemyEntity entity)
    {
        for (int i = 0; i < 11; i++)
        {
            m_allColliders[i].RegisterEntity(ref entity);
        }
    }

    public Transform GetCriticalBodyPartTransform()
    {
        return m_colliderHead.transform;
    }
}
