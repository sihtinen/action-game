﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using ActionAI;

public class EnemyEntity : MonoBehaviour
{
    private struct MeleeAttackRoutineInfo
    {
        public MeleeAttackRoutineInfo(MeleeAttackType attackType, Coroutine routine)
        {
            AttackType = attackType;
            Routine = routine;
        }

        public MeleeAttackType AttackType;
        public Coroutine Routine;
    }

    private struct InvestigationSettings
    {
        public AwarenessEventType EventType;
    }

    private enum OffMeshLinkType
    {
        Drop,
        ClimbLedge_Easy,
        ClimbLedge_Hard
    }

    public enum EquippedWeaponType
    {
        None,
        LightMelee,
        LightPistol
    }

    public enum MeleeAttackType
    {
        Light01,
        Light02
    }

    [System.Serializable]
    public class AttackTransformManipulator
    {
        [Range(0, 1)] public float RotationStart;
        [Range(0, 1)] public float RotationEnd;
        public float RotationSpeed;
        public AnimationCurve CurveRotation;

        [Space]

        [Range(0, 1)] public float MovementStart;
        [Range(0, 1)] public float MovementEnd;
        public float MovementSpeed;
        public AnimationCurve CurveMovement;

        public void ManipulateTransform(float stateTime, Transform transform, Transform rotationTarget)
        {
            Vector3 _targetDir = rotationTarget.position - transform.position;

            if (stateTime >= RotationStart && stateTime <= RotationEnd)
            {
                float _normalizedRotation = (stateTime - RotationStart) / ((RotationEnd - RotationStart));
                float _rotationAmount = CurveRotation.Evaluate(_normalizedRotation) * RotationSpeed * Time.deltaTime * GameTime.GameTimeScale;

                transform.forward = Vector3.Lerp(transform.forward, _targetDir.normalized, _rotationAmount);
                transform.forward = new Vector3(transform.forward.x, 0, transform.forward.z);
            }

            if (stateTime >= MovementStart && stateTime <= MovementEnd)
            {
                float _normalizedMovement = (stateTime - MovementStart) / ((MovementEnd - MovementStart));
                float _movementAmount = CurveMovement.Evaluate(_normalizedMovement) * MovementSpeed * Time.deltaTime * GameTime.GameTimeScale;

                if (_targetDir.magnitude > 1.3f)
                {
                    transform.position += transform.forward * _movementAmount * (Mathf.Clamp01(_targetDir.magnitude - 1.0f));
                }
            }
        }
    }

    [HideInInspector] public EnemyEntitySettings BaseSettings;
    [HideInInspector] public DialogueAIBarkContainer BarkContainer;

    [Space]

    [HideInInspector] public bool AIEnabled = true;
    [HideInInspector] public bool AnimationLock = false;

    [HideInInspector] public bool HasLightMelee = false;
    [HideInInspector] public bool HasLightPistol = false;

    [HideInInspector, NonSerialized] public int CurrentHealth = 100;
    [HideInInspector, NonSerialized] public int InstanceID;
    [HideInInspector, NonSerialized] public int FramesFromLastNavigationUpdate = 0;

    [HideInInspector, NonSerialized] public float AggressionLevel = 0;
    [HideInInspector, NonSerialized] public float PlayerVisionDetection = 0;
    [HideInInspector, NonSerialized] public float AggressionIncreaseTimeStamp = 0;
    [HideInInspector, NonSerialized] public float CurrentRotationSpeed = 0.0f;
    [HideInInspector, NonSerialized] public float DistanceToTarget = 300;
    [HideInInspector, NonSerialized] public float AngleToTarget = 0;
    [HideInInspector, NonSerialized] public float TimeSpentInCurrentState = 0.0f;
    [HideInInspector, NonSerialized] public float CurrentAnimationStateTimeNormalized = 0;
    [HideInInspector, NonSerialized] public float AttackCooldown = 0;
    [HideInInspector, NonSerialized] public float AttackCooldownTimerMultiplier = 1.0f;
    [HideInInspector, NonSerialized] public float CurrentAttackCooldownCost = 0;
    [HideInInspector, NonSerialized] public float TimeSpentInvestigating = 0;
    [HideInInspector, NonSerialized] public float TimeSpentInCurrentWaypoint = 0;

    [HideInInspector, NonSerialized] public bool HasLineOfSightToTarget = false;
    [HideInInspector, NonSerialized] public bool MovingToCover = false;
    [HideInInspector, NonSerialized] public bool Investigating = false;

    [HideInInspector, NonSerialized] public Vector3 InvestigationTargetPoint;

    [HideInInspector, NonSerialized] public EquippedWeaponType CurrentEquippedWeaponType = EquippedWeaponType.None;
    [HideInInspector, NonSerialized] public EquippedWeaponType NextEquippedWeaponType = EquippedWeaponType.None;
    [HideInInspector, NonSerialized] public NavigationTarget NavTarget = NavigationTarget.None;
    [HideInInspector, NonSerialized] public AwarenessEventType InvestigationEventType = AwarenessEventType.None;

    [HideInInspector, NonSerialized] public CoverPointOctree.CoverPoint CurrentCoverPoint;
    [HideInInspector, NonSerialized] public CoverWeightSettings CurrentCoverWeightSettings;
    [HideInInspector, NonSerialized] public EnvironmentProbeSettings CurrentProbeSettings;
    [HideInInspector, NonSerialized] public FlankPathWeightSettings CurrentFlankWeightSettings;

    [HideInInspector] public UnityEvent OnActionChanged;

    [HideInInspector, NonSerialized] public Transform CombatTarget = null;
    [HideInInspector, NonSerialized] public GameObject EquippedWeapon = null;

    [HideInInspector, NonSerialized] public AIComponent AIComponent;
    [HideInInspector, NonSerialized] public AIMovementComponent MovementComponent;
    [HideInInspector, NonSerialized] public AIVisionComponent VisionComponent;
    [HideInInspector, NonSerialized] public AIHearingComponent HearingComponent;
    [HideInInspector, NonSerialized] public EnemyUIComponent UIComponent;
    [HideInInspector, NonSerialized] public EnemyVFXComponent VFXComponent;
    [HideInInspector, NonSerialized] public DialogueSpeaker DialogueSpeaker;
    [HideInInspector, NonSerialized] public EnvironmentProbeComponent EnvironmentProbeComponent;

    [HideInInspector] public WaypointCollection PatrolWaypointRoute;
    [HideInInspector] public Waypoint CurrentWaypoint;

    [Header("Object References")]
    [SerializeField] private Animator m_animator;
    [SerializeField] private Transform m_rootBone;
    [SerializeField] private ParticleSystem m_particlesDeath;

    [Header("Light Melee")]
    [SerializeField] private GameObject m_lightMelee_Object;
    [SerializeField] private Transform m_lightMelee_Socket_Hand;
    [SerializeField] private Transform m_lightMelee_Socket_Back;

    [Header("Light Pistol")]
    [SerializeField] private GameObject m_lightPistol_Object;
    [SerializeField] private Transform m_lightPistol_Socket_Hand;
    [SerializeField] private Transform m_lightPistol_Socket_Back;
    [SerializeField] private float m_lightPistolAttackCost = 4.0f;

    private string m_currentAnimState_Layer0 = "";
    private string m_currentAnimState_Layer1 = "";

    private EnemyHitColliderManager m_hitColliderManager = null;
    private MeleeWeaponCollisionComponent m_lightMeleeWeaponCollision = null;
    private PhysicsObjectComponent m_lightMeleePhysicsComponent = null;
    private WeaponComponent m_lightPistolComponent = null;

    private Coroutine m_routineOffmeshAnimation = null;
    private Coroutine m_routineTakedown = null;
    private Coroutine m_routineDeath = null;

    private Coroutine m_routineEquipWeapon = null;
    private Coroutine m_routineMeleeAttack = null;

    private Coroutine m_routineAimProjectile = null;
    private Coroutine m_routineShootProjectile = null;

    private List<MeleeAttackRoutineInfo> m_currentAttackRoutines = new List<MeleeAttackRoutineInfo>();

    private void Awake()
    {
        InstanceID = gameObject.GetInstanceID();

        GameState.GetInstance().RegisterEnemyEntity(this);

        AIComponent = GetComponent<AIComponent>();
        VisionComponent = GetComponent<AIVisionComponent>();
        HearingComponent = GetComponent<AIHearingComponent>();
        VFXComponent = GetComponent<EnemyVFXComponent>();
        m_hitColliderManager = GetComponent<EnemyHitColliderManager>();
        DialogueSpeaker = GetComponent<DialogueSpeaker>();

        AggressionLevel = 0;
        CurrentHealth = BaseSettings.MaxHealth;
    }

    private void Start()
    {
        AINavigationManager.GetInstance().RegisterEnemy(this);

        var _self = this;
        m_hitColliderManager.RegisterEntity(ref _self);
        m_hitColliderManager.SetActive(true);

        if (HasLightMelee)
        {
            if (m_lightMelee_Object)
            {
                m_lightMeleeWeaponCollision = m_lightMelee_Object.GetComponent<MeleeWeaponCollisionComponent>();
                m_lightMeleeWeaponCollision.Owner = gameObject;

                m_lightMeleePhysicsComponent = m_lightMelee_Object.GetComponent<PhysicsObjectComponent>();
                if (m_lightMeleePhysicsComponent) m_lightMeleePhysicsComponent.SetEnabled(false);
            }

            m_lightMelee_Object.transform.parent = m_lightMelee_Socket_Back;
            m_lightMelee_Object.transform.localPosition = Vector3.zero;
            m_lightMelee_Object.transform.localRotation = Quaternion.identity;
            m_lightMelee_Object.SetActive(true);
        }

        if (HasLightPistol)
        {
            m_lightPistol_Object.transform.parent = m_lightPistol_Socket_Back;
            m_lightPistol_Object.transform.localPosition = Vector3.zero;
            m_lightPistol_Object.transform.localRotation = Quaternion.identity;
            m_lightPistol_Object.SetActive(true);

            m_lightPistolComponent = m_lightPistol_Object.GetComponent<WeaponComponent>();
        }
    }

    private void Update()
    {
        float _currentAggression = AggressionLevel;

        bool _lineOfSight = VisionComponent.CalculateVisionToPlayer();
        PlayerVisionDetection += VisionComponent.GetAggressionIncrease();
        PlayerVisionDetection = Mathf.Clamp(PlayerVisionDetection, 0, 1);

        if (AggressionLevel < PlayerVisionDetection)
        {
            AggressionLevel = PlayerVisionDetection;
        }

        if (_lineOfSight 
            && (AIComponent.GroupInstance == null || AIComponent.GroupInstance.Leader == AIComponent))
        {
            if (AggressionLevel > BaseSettings.AggressionThreshold_Investigation)
            {
                if ((int)InvestigationEventType <= (int)AwarenessEventType.LineOfSight)
                {
                    if (CombatTarget == null) CombatTarget = GameState.GetInstance().PlayerObject.transform;

                    InvestigationTargetPoint = CombatTarget.position;
                    InvestigationEventType = AwarenessEventType.LineOfSight;
                    TimeSpentInvestigating = 0.0f;

                    if (AIComponent.GroupInstance == null)
                    {
                        AIComponent.GroupInstance = AIGroupManager.GetIntance().GetPooledGroupInstance();
                        AIComponent.GroupInstance.InitializeGroup(AIComponent);
                    }
                }
            }
        }

        if (HearingComponent.PriorityHeardEvent != null)
        {
            if (AggressionLevel < 1.0f)
            {
                if (HearingComponent.PriorityHeardEvent.Hearable.AwarenessIncreaseAggressionLimit > AggressionLevel)
                {
                    AggressionLevel += HearingComponent.PriorityHeardEvent.GetAwarenessIncrease(transform.position);
                    AggressionLevel = Mathf.Clamp(AggressionLevel, 0, HearingComponent.PriorityHeardEvent.Hearable.AwarenessIncreaseAggressionLimit);
                }
            }

            if (AggressionLevel > BaseSettings.AggressionThreshold_Investigation 
                && (AIComponent.GroupInstance == null || AIComponent.GroupInstance.Leader == AIComponent))
            {
                if ((int)InvestigationEventType <= (int)HearingComponent.PriorityHeardEvent.Hearable.EventType)
                {
                    if (HearingComponent.PriorityHeardEvent.Hearable.HasPointOnNavmesh)
                    {
                        InvestigationTargetPoint = HearingComponent.PriorityHeardEvent.Hearable.PointOnNavmesh;
                        InvestigationEventType = HearingComponent.PriorityHeardEvent.Hearable.EventType;
                        TimeSpentInvestigating = 0.0f;

                        if (AIComponent.GroupInstance == null)
                        {
                            AIComponent.GroupInstance = AIGroupManager.GetIntance().GetPooledGroupInstance();
                            AIComponent.GroupInstance.InitializeGroup(AIComponent);
                        }
                    }
                }
            }

            HearingComponent.PriorityHeardEvent.LinkedComponents.Remove(HearingComponent);
            HearingComponent.PriorityHeardEvent = null;
        }

        if (_currentAggression < AggressionLevel)
        {
            AggressionIncreaseTimeStamp = Time.time;
        }
        else
        {
            if ( (Time.time - AggressionIncreaseTimeStamp) > BaseSettings.AggressionDecayTimeStationary)
            {
                AggressionLevel -= Time.deltaTime * GameTime.GameTimeScale * BaseSettings.AggressionDecreseSpeed;
            }
        }

        AggressionLevel = Mathf.Clamp01(AggressionLevel);

        if (AttackCooldown > 0)
        {
            AttackCooldown -= Time.deltaTime * GameTime.GameTimeScale * AttackCooldownTimerMultiplier;

            if (AttackCooldown < 0)
            {
                AttackCooldown = 0;
            }
        }
    }

    private void LateUpdate()
    {
        float _movementSpeedNormalized = MovementComponent.GetMovementSpeedNormalized();
        m_animator.SetFloat("Float_MovementSpeedNormalized", _movementSpeedNormalized);
        m_animator.SetFloat("Float_AnimSpeedMultiplier", 1 + (_movementSpeedNormalized * 1.7f));
    }

    public void CalculateUtilityAIValues()
    {
        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        CurrentAnimationStateTimeNormalized = _stateInfo.normalizedTime;

        if (CombatTarget)
        {
            Vector3 _targetPos = CombatTarget.position;
            DistanceToTarget = Vector3.Distance(_targetPos, transform.position);

            _targetPos.y = transform.position.y;

            AngleToTarget = Mathf.Abs(Vector3.Angle(transform.forward, _targetPos - transform.position));

            HasLineOfSightToTarget = VisionComponent.HasLineOfSightToTarget(CombatTarget.gameObject);
        }
    }

    public bool DealDamage(float damage)
    {
        if (CurrentHealth <= 0) return false;

        CurrentHealth -= (int)damage;
        if (CurrentHealth <= 0)
        {
            TriggerDeath();
            return true;
        }

        return false;
    }

    public void Takedown(Vector3 movePosition)
    {
        AIEnabled = false;
        MovementComponent.Enabled = false;

        m_lightMeleeWeaponCollision.CalculateCollision = false;
        stopAndResetRoutines();

        m_hitColliderManager.SetActive(false);
        m_animator.SetLayerWeight(1, 0.0f);

        switch (UIComponent.CurrentTakedownType)
        {
            case PlayerTakedownManager.TakedownType.air:
                m_routineTakedown = StartCoroutine(routine_AirTakedown(movePosition));
                break;

            case PlayerTakedownManager.TakedownType.behindGrabLightMeleeWeapon:
                m_routineTakedown = StartCoroutine(routine_TakedownBehindGrabLightMeleeWeapon(movePosition));
                break;
        }
    }

    public void TriggerParry()
    {
        m_lightMeleeWeaponCollision.CalculateCollision = false;
        stopAndResetRoutines();
        StartCoroutine(routine_Parried());
    }

    public void TriggerDeath()
    {
        AIEnabled = false;
        MovementComponent.Enabled = false;
        m_lightMeleeWeaponCollision.CalculateCollision = false;
        m_hitColliderManager.SetActive(true);
        m_routineDeath = StartCoroutine(routineDeath());
    }

    public void TakeCoverAnimation()
    {
        if (CurrentCoverPoint != null)
        {
            transform.forward = CurrentCoverPoint.Normal * -1;
            m_animator.SetLayerWeight(1, 0.0f);

            if (CurrentCoverPoint.IsHalfCover)
            {
                startAnimation(BaseSettings.AnimState_TakeCover_Half, 0, 0.7f);
            }
            else
            {
                startAnimation(BaseSettings.AnimState_TakeCover_Full, 0, 0.7f);
            }
        }
        else
        {
            idleAnimation();
        }
    }

    public void RotateTowardsTarget()
    {
        startAnimation(BaseSettings.AnimState_Idle, 0, 0.2f);
        m_animator.SetLayerWeight(1, 1);
    }

    public void EquipWeapon()
    {
        if (m_routineEquipWeapon == null)
        {
            m_routineEquipWeapon = StartCoroutine(routine_EquipWeapon(NextEquippedWeaponType));
        }
    }

    public void MeleeAttack(MeleeAttackType attackType, bool canInterruptMelee)
    {
        m_animator.SetLayerWeight(1, 0.0f);

        if (m_routineMeleeAttack == null || canInterruptMelee)
        {
            if (m_routineMeleeAttack != null)
            {
                for (int i = m_currentAttackRoutines.Count; i --> 0;)
                {
                    if (m_currentAttackRoutines[i].AttackType != attackType)
                    {
                        if (m_currentAttackRoutines[i].Routine != null)
                        {
                            StopCoroutine(m_currentAttackRoutines[i].Routine);
                        }

                        m_currentAttackRoutines.RemoveAt(i);
                    }
                }
            }

            m_routineMeleeAttack = StartCoroutine(routine_MeleeAttack(attackType));
            m_currentAttackRoutines.Add(new MeleeAttackRoutineInfo(attackType, m_routineMeleeAttack));
        }
    }

    public void AimAndShootProjectile(float stopDistanceSqrMag)
    {
        if (m_routineAimProjectile == null)
        {
            m_routineAimProjectile = StartCoroutine(routine_AimProjectile());
        }
        else
        {
            if (Vector3.SqrMagnitude(transform.position - CombatTarget.position) < stopDistanceSqrMag)
            {
                StopCoroutine(m_routineAimProjectile);
            }
        }
    }

    public void MovementAnimationLogic()
    {
        m_animator.SetLayerWeight(1, 0.35f);

        if (MovementComponent.GetMovementSpeedNormalized() > 0.1f)
        {
            movementAnimation();
        }
        else
        {
            idleAnimation();
        }
    }

    public bool IsCurrentAnimationState(string name, int layer)
    {
        switch (layer)
        {
            case 0: if (m_currentAnimState_Layer0 == name) return true; break;
            case 1: if (m_currentAnimState_Layer1 == name) return true; break;
        }

        return false;
    }

    public Transform GetCriticalBodyPart()
    {
        return m_hitColliderManager.GetCriticalBodyPartTransform();
    }

    private void idleAnimation()
    {
        startAnimation(BaseSettings.AnimState_Idle, 0, 0.2f);
        m_animator.SetLayerWeight(1, 1);

        switch (CurrentEquippedWeaponType)
        {
            case EquippedWeaponType.LightMelee:
                startAnimation(BaseSettings.AnimState_LightMelee_Idle, 1, 0);
                break;

            default:
                startAnimation("Empty", 1, 0);
                break;
        }
    }

    private void movementAnimation()
    {
        m_animator.SetLayerWeight(1, 0.35f);
        startAnimation(BaseSettings.AnimState_Move, 0, 0.2f);
    }

    private void startAnimation(string animState, int layer, float crossfadeTime)
    {
        switch (layer)
        {
            case 0:

                if (m_currentAnimState_Layer0 == animState) return;

                if (crossfadeTime == 0.0f) m_animator.Play(animState, layer);
                else m_animator.CrossFadeInFixedTime(animState, crossfadeTime, layer);

                m_currentAnimState_Layer0 = animState;

                break;

            case 1:

                if (m_currentAnimState_Layer1 == animState) return;

                if (crossfadeTime == 0.0f) m_animator.Play(animState, layer);
                else m_animator.CrossFadeInFixedTime(animState, crossfadeTime, layer);

                m_currentAnimState_Layer1 = animState;

                break;

            case 2:

                if (crossfadeTime == 0.0f) m_animator.Play(animState, layer);
                else m_animator.CrossFadeInFixedTime(animState, crossfadeTime, layer);

                break;
        }
    }

    private void stopAndResetRoutines()
    {
        StopAllCoroutines();

        m_routineAimProjectile = null;
        m_routineDeath = null;
        m_routineEquipWeapon = null;
        m_routineMeleeAttack = null;
        m_routineOffmeshAnimation = null;
        m_routineShootProjectile = null;
        m_routineTakedown = null;
    }

    private IEnumerator routine_OffMeshTransition()
    {
        //AnimationLock = true;

        //NavigationMeshAgent.updateRotation = false;

        //OffMeshLinkData _linkData = NavigationMeshAgent.currentOffMeshLinkData;
        //OffMeshLinkType _linkType = OffMeshLinkType.Drop;

        //Vector3 _toEndPos = _linkData.endPos - _linkData.startPos;

        //Vector3 _toEndPosHorizontal = _toEndPos;
        //_toEndPosHorizontal.y = 0;
        //_toEndPosHorizontal.Normalize();

        //Vector3 _startPos = transform.position;
        //Vector3 _climbAnimStartPos = _linkData.endPos - _toEndPosHorizontal * 0.79f - Vector3.up * 1.85f;

        //if (_linkData.endPos.y > _linkData.startPos.y)
        //{
        //    _linkType = OffMeshLinkType.ClimbLedge_Hard;
        //    startAnimation(BaseSettings.AnimState_Climb_Hard_Begin, 0, 0.15f);
        //}

        //switch (_linkType)
        //{
        //    case OffMeshLinkType.ClimbLedge_Hard:

        //        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        //        while (_stateInfo.IsName(BaseSettings.AnimState_Climb_Hard_Begin) == false)
        //        {
        //            yield return new WaitForEndOfFrame();
        //            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        //        }

        //        while (_stateInfo.IsName(BaseSettings.AnimState_Climb_Hard_Begin) && _stateInfo.normalizedTime < 1.0f)
        //        {
        //            transform.position = Vector3.Lerp(_startPos, _climbAnimStartPos, BaseSettings.Curve_ClimbHardBeginMovement.Evaluate(_stateInfo.normalizedTime) );
        //            transform.forward = Vector3.Lerp(transform.forward, _toEndPosHorizontal, _stateInfo.normalizedTime);

        //            yield return new WaitForEndOfFrame();
        //            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        //        }

        //        startAnimation(BaseSettings.AnimState_Climb_Hard, 0, 0.05f);

        //        _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        //        while (_stateInfo.IsName(BaseSettings.AnimState_Climb_Hard) == false)
        //        {
        //            yield return new WaitForEndOfFrame();
        //            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        //        }

        //        while (_stateInfo.IsName(BaseSettings.AnimState_Climb_Hard) && _stateInfo.normalizedTime < 1.0f)
        //        {
        //            transform.LookAt(_linkData.endPos);

        //            yield return new WaitForEndOfFrame();
        //            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        //        }

        //        NavigationMeshAgent.CompleteOffMeshLink();
        //        transform.position = m_rootBone.position;
        //        m_animator.Play(BaseSettings.AnimState_Idle, 0);
        //        yield return new WaitForEndOfFrame();

        //        break;

        //    case OffMeshLinkType.Drop:

        //        transform.position = _linkData.endPos;
        //        NavigationMeshAgent.CompleteOffMeshLink();

        //        break;
        //}

        //NavigationMeshAgent.updateRotation = true;

        //m_routineOffmeshAnimation = null;
        //AnimationLock = false;

        yield return null;
    }

    private IEnumerator routine_AirTakedown(Vector3 movePosition)
    {
        AnimationLock = true;

        startAnimation(BaseSettings.AnimState_TakedownAirTarget, 0, 0.05f);

        Transform _player = GameState.GetInstance().PlayerObject.transform;
        Vector3 _startPosition = transform.position;
        Quaternion _startRotation = transform.rotation;

        float _timer = 0.0f;
        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale * 2;

            transform.position = Vector3.Lerp(_startPosition, movePosition, _timer);

            Vector3 _lookDir = _player.position - transform.position;
            _lookDir.y = 0;
            _lookDir.Normalize();

            Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
            transform.rotation = Quaternion.Lerp(_startRotation, _targetDirection, _timer);

            yield return new WaitForEndOfFrame();
        }

        transform.position = movePosition;

        AnimationLock = false;
    }

    private IEnumerator routine_TakedownBehindGrabLightMeleeWeapon(Vector3 movePosition)
    {
        AnimationLock = true;

        startAnimation(BaseSettings.AnimState_TakedownBehindGrabLightMeleeWeapon_Target, 0, 0.05f);

        Transform _player = GameState.GetInstance().PlayerObject.transform;
        Vector3 _startPosition = transform.position;
        Quaternion _startRotation = transform.rotation;

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(BaseSettings.AnimState_TakedownBehindGrabLightMeleeWeapon_Target) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        while (_stateInfo.IsName(BaseSettings.AnimState_TakedownBehindGrabLightMeleeWeapon_Target) && _stateInfo.normalizedTime < 1.0f)
        {
            if (_stateInfo.normalizedTime < 0.1f)
            {
                transform.position = Vector3.Lerp(_startPosition, movePosition, _stateInfo.normalizedTime * 10);
                Vector3 _lookDir = _player.position - transform.position;
                _lookDir.y = 0;
                _lookDir *= -1;
                _lookDir.Normalize();

                Quaternion _targetDirection = Quaternion.LookRotation(_lookDir);
                transform.rotation = Quaternion.Lerp(_startRotation, _targetDirection, _stateInfo.normalizedTime * 10);
            }
            else
            {
                transform.position = movePosition;
            }

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        AnimationLock = false;
    }

    private IEnumerator routineDeath()
    {
        m_particlesDeath.Play();

        bool _particlesStopped = false;

        float _speed = 0.65f;

        float _timer = 0.0f;
        while (_timer < 1.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale * _speed;

            VFXComponent.SetDeathSliceAmount(_timer, true);

            if (_particlesStopped == false && _timer > _speed)
            {
                _particlesStopped = true;
                m_particlesDeath.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }

            yield return new WaitForEndOfFrame();
        }

        VFXComponent.SetDeathSliceAmount(2);

        m_lightMelee_Object.transform.parent = null;
        if (m_lightMeleePhysicsComponent)
        {
            m_lightMeleePhysicsComponent.SetEnabled(true);
        }

        m_routineDeath = null;
    }

    private IEnumerator routine_EquipWeapon(EquippedWeaponType weaponType)
    {
        AnimationLock = true;

        switch (CurrentEquippedWeaponType)
        {
            case EquippedWeaponType.LightMelee:

                StartCoroutine(routine_Unequip_LightMelee());
                while (CurrentEquippedWeaponType != EquippedWeaponType.None)
                {
                    yield return new WaitForEndOfFrame();
                }

                break;

            case EquippedWeaponType.LightPistol:

                StartCoroutine(routine_Unequip_LightPistol());
                while (CurrentEquippedWeaponType != EquippedWeaponType.None)
                {
                    yield return new WaitForEndOfFrame();
                }

                break;
        }

        switch (weaponType)
        {
            case EquippedWeaponType.LightMelee:

                m_animator.SetLayerWeight(1, 0f);
                startAnimation(BaseSettings.AnimState_LightMelee_Equip, 0, 0.08f);

                AnimatorStateInfo _stateInfo0 = m_animator.GetCurrentAnimatorStateInfo(0);
                while (_stateInfo0.IsName(BaseSettings.AnimState_LightMelee_Equip) == false)
                {
                    yield return new WaitForEndOfFrame();
                    _stateInfo0 = m_animator.GetCurrentAnimatorStateInfo(0);
                }

                while (_stateInfo0.IsName(BaseSettings.AnimState_LightMelee_Equip) && _stateInfo0.normalizedTime < 1.0f)
                {
                    if (_stateInfo0.normalizedTime >= 0.4f && _stateInfo0.normalizedTime <= 0.6f)
                    {
                        float _normalizedTransition = (_stateInfo0.normalizedTime - 0.4f) / 0.2f;
                        m_lightMelee_Object.transform.position = Vector3.Lerp(m_lightMelee_Socket_Back.position, m_lightMelee_Socket_Hand.position, _normalizedTransition);
                        m_lightMelee_Object.transform.rotation = Quaternion.Lerp(m_lightMelee_Socket_Back.rotation, m_lightMelee_Socket_Hand.rotation, _normalizedTransition);
                    }
                    else
                    {
                        if (_stateInfo0.normalizedTime < 0.4f)
                        {
                            m_lightMelee_Object.transform.parent = m_lightMelee_Socket_Back;
                        }
                        else
                        {
                            m_lightMelee_Object.transform.parent = m_lightMelee_Socket_Hand;
                        }

                        m_lightMelee_Object.transform.localPosition = Vector3.zero;
                        m_lightMelee_Object.transform.localRotation = Quaternion.identity;
                    }

                    yield return new WaitForEndOfFrame();
                    _stateInfo0 = m_animator.GetCurrentAnimatorStateInfo(0);
                }

                EquippedWeapon = m_lightMelee_Object;
                CurrentEquippedWeaponType = EquippedWeaponType.LightMelee;

                break;

            case EquippedWeaponType.LightPistol:

                m_animator.SetLayerWeight(1, 0f);
                startAnimation(BaseSettings.AnimState_LightPistol_Equip, 0, 0.08f);

                AnimatorStateInfo _stateInfo1 = m_animator.GetCurrentAnimatorStateInfo(0);
                while (_stateInfo1.IsName(BaseSettings.AnimState_LightPistol_Equip) == false)
                {
                    yield return new WaitForEndOfFrame();
                    _stateInfo1 = m_animator.GetCurrentAnimatorStateInfo(0);
                }

                while (_stateInfo1.IsName(BaseSettings.AnimState_LightPistol_Equip) && _stateInfo1.normalizedTime < 1.0f)
                {
                    if (_stateInfo1.normalizedTime >= 0.4f && _stateInfo1.normalizedTime <= 0.6f)
                    {
                        float _normalizedTransition = (_stateInfo1.normalizedTime - 0.4f) / 0.2f;
                        m_lightPistol_Object.transform.position = Vector3.Lerp(m_lightPistol_Socket_Back.position, m_lightPistol_Socket_Hand.position, _normalizedTransition);
                        m_lightPistol_Object.transform.rotation = Quaternion.Lerp(m_lightPistol_Socket_Back.rotation, m_lightPistol_Socket_Hand.rotation, _normalizedTransition);
                    }
                    else
                    {
                        if (_stateInfo1.normalizedTime < 0.4f)
                        {
                            m_lightPistol_Object.transform.parent = m_lightPistol_Socket_Back;
                        }
                        else
                        {
                            m_lightPistol_Object.transform.parent = m_lightPistol_Socket_Hand;
                        }

                        m_lightPistol_Object.transform.localPosition = Vector3.zero;
                        m_lightPistol_Object.transform.localRotation = Quaternion.identity;
                    }

                    yield return new WaitForEndOfFrame();
                    _stateInfo1 = m_animator.GetCurrentAnimatorStateInfo(0);
                }

                EquippedWeapon = m_lightPistol_Object;
                CurrentEquippedWeaponType = EquippedWeaponType.LightPistol;

                break;

            default:
                break;
        }

        AnimationLock = false;
        m_routineEquipWeapon = null;

        idleAnimation();
    }

    private IEnumerator routine_MeleeAttack(MeleeAttackType attackType)
    {
        AnimationLock = true;

        //NavigationMeshAgent.enabled = false;

        float _collisionStartTime = 0;
        float _collisionEndtime = 1;

        string _animState = BaseSettings.AnimState_LightMelee_Attack_Light_01;
        AttackTransformManipulator _transformManipulator = null;

        switch (attackType)
        {
            case MeleeAttackType.Light01:
                _animState = BaseSettings.AnimState_LightMelee_Attack_Light_01;
                _transformManipulator = BaseSettings.AttackTransformManipulator_LightMelee_01;

                _collisionStartTime = 0.3f;
                _collisionEndtime = 0.42f;
                break;

            case MeleeAttackType.Light02:
                _animState = BaseSettings.AnimState_LightMelee_Attack_Light_02;
                _transformManipulator = BaseSettings.AttackTransformManipulator_LightMelee_02;

                _collisionStartTime = 0.2f;
                _collisionEndtime = 0.4f;
                break;
        }

        startAnimation(_animState, 0, 0.1f);

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(_animState) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        m_lightMeleeWeaponCollision.CalculateCollision = true;

        while (_stateInfo.IsName(_animState) && _stateInfo.normalizedTime < 1.0f)
        {
            _transformManipulator.ManipulateTransform(_stateInfo.normalizedTime, transform, CombatTarget);

            if (_stateInfo.normalizedTime >= _collisionStartTime &&
                _stateInfo.normalizedTime <= _collisionEndtime)
            {
                m_lightMeleeWeaponCollision.CalculateCollision = true;
            }
            else
            {
                m_lightMeleeWeaponCollision.CalculateCollision = false;
            }

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        m_routineMeleeAttack = null;
        AnimationLock = false;

        //NavigationMeshAgent.enabled = true;

        idleAnimation();
    }

    private IEnumerator routine_Parried()
    {
        AnimationLock = true;

        string _animState = string.Empty;

        switch (CurrentEquippedWeaponType)
        {
            case EquippedWeaponType.LightMelee:
                _animState = BaseSettings.AnimState_LightMelee_Parried;
                break;
        }

        if (_animState != string.Empty)
        {
            m_animator.SetLayerWeight(1, 0f);
            startAnimation(_animState, 0, 0.08f);

            AnimatorStateInfo _stateInfo0 = m_animator.GetCurrentAnimatorStateInfo(0);
            while (_stateInfo0.IsName(_animState) == false)
            {
                yield return new WaitForEndOfFrame();
                _stateInfo0 = m_animator.GetCurrentAnimatorStateInfo(0);
            }

            while (_stateInfo0.IsName(_animState) && _stateInfo0.normalizedTime < 1.0f)
            {
                yield return new WaitForEndOfFrame();
                _stateInfo0 = m_animator.GetCurrentAnimatorStateInfo(0);
            }
        }

        AnimationLock = false;
    }

    private IEnumerator routine_AimProjectile()
    {
        AnimationLock = true;

        int _numberOfShots = UnityEngine.Random.Range(1, 3);
        float _timer = 0.0f;
        float _aimTime = UnityEngine.Random.Range(0.7f, 0.9f);

        startAnimation(BaseSettings.AnimState_LightPistol_Aim_Layer0, 0, 0.2f);
        startAnimation(BaseSettings.AnimState_LightPistol_Aim_Layer1, 1, 0.2f);

        m_animator.SetLayerWeight(1, 1.0f);

        for (int i = 0; i < _numberOfShots; i++)
        {
            while (_timer < _aimTime)
            {
                _timer += Time.deltaTime * GameTime.GameTimeScale;

                Vector3 _targetDir = CombatTarget.position - transform.position;
                _targetDir.y = 0;
                _targetDir.Normalize();

                transform.forward = Vector3.RotateTowards(transform.forward, _targetDir, Time.deltaTime * GameTime.GameTimeScale * CurrentRotationSpeed, 0);

                yield return null;
            }

            m_routineShootProjectile = StartCoroutine(routine_ShootProjectile());
            while (m_routineShootProjectile != null)
            {
                yield return null;
            }

            _timer = 0.0f;
            _aimTime = UnityEngine.Random.Range(0.2f, 0.4f);
        }

        AnimationLock = false;
        m_routineAimProjectile = null;
    }

    private IEnumerator routine_ShootProjectile()
    {
        float _timer = 0.0f;
        float _windupTime = 0.6f;

        while (_timer < _windupTime)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale;

            Vector3 _targetDir = CombatTarget.position - transform.position;
            _targetDir.y = 0;
            _targetDir.Normalize();

            transform.forward = Vector3.RotateTowards(transform.forward, _targetDir, Time.deltaTime * GameTime.GameTimeScale * CurrentRotationSpeed * 0.05f, 0);

            yield return null;
        }

        startAnimation(BaseSettings.AnimState_LightPistol_Recoil, 2, 0);

        WeaponComponent.FireParameters _parameters;
        _parameters.AimTarget = transform.position + transform.forward * 50.0f + Vector3.up * 1.5f;
        _parameters.TriggerStunState = false;
        _parameters.WaitOneFrameForVFX = false;
        _parameters.OverrideHitTarget = null;

        m_lightPistolComponent.Fire(_parameters);

        AttackCooldown += m_lightPistolAttackCost;

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(2);
        while (_stateInfo.IsName(BaseSettings.AnimState_LightPistol_Recoil) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(2);
        }

        while (_stateInfo.IsName(BaseSettings.AnimState_LightPistol_Recoil) && _stateInfo.normalizedTime < 1.0f)
        {
            Vector3 _targetDir = CombatTarget.position - transform.position;
            _targetDir.y = 0;
            _targetDir.Normalize();

            transform.forward = Vector3.RotateTowards(transform.forward, _targetDir, Time.deltaTime * GameTime.GameTimeScale * CurrentRotationSpeed * 0.3f, 0);

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(2);
        }

        m_routineShootProjectile = null;

        if (m_routineAimProjectile == null)
        {
            AnimationLock = false;
        }
    }

    private IEnumerator routine_Unequip_LightMelee()
    {
        m_animator.SetLayerWeight(1, 0f);
        startAnimation(BaseSettings.AnimState_LightMelee_Unequip, 0, 0.2f);

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(BaseSettings.AnimState_LightMelee_Unequip) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        while (_stateInfo.IsName(BaseSettings.AnimState_LightMelee_Unequip) && _stateInfo.normalizedTime < 1.0f)
        {
            if (_stateInfo.normalizedTime >= 0.4f && _stateInfo.normalizedTime <= 0.6f)
            {
                float _normalizedTransition = (_stateInfo.normalizedTime - 0.4f) / 0.2f;
                m_lightMelee_Object.transform.position = Vector3.Lerp(m_lightMelee_Socket_Hand.position, m_lightMelee_Socket_Back.position, _normalizedTransition);
                m_lightMelee_Object.transform.rotation = Quaternion.Lerp(m_lightMelee_Socket_Hand.rotation, m_lightMelee_Socket_Back.rotation, _normalizedTransition);
            }
            else
            {
                if (_stateInfo.normalizedTime < 0.4f)
                {
                    m_lightMelee_Object.transform.parent = m_lightMelee_Socket_Hand;
                }
                else
                {
                    m_lightMelee_Object.transform.parent = m_lightMelee_Socket_Back;
                }

                m_lightMelee_Object.transform.localPosition = Vector3.zero;
                m_lightMelee_Object.transform.localRotation = Quaternion.identity;
            }

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        EquippedWeapon = null;
        CurrentEquippedWeaponType = EquippedWeaponType.None;
    }

    private IEnumerator routine_Unequip_LightPistol()
    {
        m_animator.SetLayerWeight(1, 0f);
        startAnimation(BaseSettings.AnimState_LightPistol_Unequip, 0, 0.2f);

        AnimatorStateInfo _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        while (_stateInfo.IsName(BaseSettings.AnimState_LightPistol_Unequip) == false)
        {
            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        while (_stateInfo.IsName(BaseSettings.AnimState_LightPistol_Unequip) && _stateInfo.normalizedTime < 1.0f)
        {
            if (_stateInfo.normalizedTime >= 0.4f && _stateInfo.normalizedTime <= 0.6f)
            {
                float _normalizedTransition = (_stateInfo.normalizedTime - 0.4f) / 0.2f;
                m_lightPistol_Object.transform.position = Vector3.Lerp(m_lightPistol_Socket_Hand.position, m_lightPistol_Socket_Back.position, _normalizedTransition);
                m_lightPistol_Object.transform.rotation = Quaternion.Lerp(m_lightPistol_Socket_Hand.rotation, m_lightPistol_Socket_Back.rotation, _normalizedTransition);
            }
            else
            {
                if (_stateInfo.normalizedTime < 0.4f)
                {
                    m_lightPistol_Object.transform.parent = m_lightPistol_Socket_Hand;
                }
                else
                {
                    m_lightPistol_Object.transform.parent = m_lightPistol_Socket_Back;
                }

                m_lightPistol_Object.transform.localPosition = Vector3.zero;
                m_lightPistol_Object.transform.localRotation = Quaternion.identity;
            }

            yield return new WaitForEndOfFrame();
            _stateInfo = m_animator.GetCurrentAnimatorStateInfo(0);
        }

        EquippedWeapon = null;
        CurrentEquippedWeaponType = EquippedWeaponType.None;
    }
}
