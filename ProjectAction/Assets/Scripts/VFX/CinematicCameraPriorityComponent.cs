﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinematicCameraPriorityComponent : MonoBehaviour
{
    private CinemachineVirtualCamera m_cam;

    private void Awake()
    {
        m_cam = GetComponent<CinemachineVirtualCamera>();
    }

    public void CameraLive()
    {
        m_cam.MoveToTopOfPrioritySubqueue();
    }
}
