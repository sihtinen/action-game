﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CinematicController : MonoBehaviour
{
    public bool ControlGameObjectState = false;

    public bool IsTakedownCinematic;
    public PlayerTakedownManager.TakedownType TakedownType;

    private PlayableDirector m_director;

    private void Awake()
    {
        m_director = GetComponent<PlayableDirector>();
    }

    private IEnumerator Start()
    {
        yield return null;

        if (IsTakedownCinematic)
        {
            switch (TakedownType)
            {
                case PlayerTakedownManager.TakedownType.behindGrabLightMeleeWeapon:
                    GameState.GetInstance().PlayerObject.GetComponentInChildren<PlayerAnimationController>().Cinematic_TakedownBehindGrabLightMeleeWeapon = this;
                    break;
            }
        }

        SetActive(false);
    }

    public void SetActive(bool newState)
    {
        if (ControlGameObjectState) gameObject.SetActive(newState);

        if (newState)
        {
            m_director.timeUpdateMode = DirectorUpdateMode.Manual;
            m_director.time = 0.0f;
            m_director.Play(m_director.playableAsset, DirectorWrapMode.None);
            m_director.playableGraph.GetRootPlayable(0).SetSpeed(0.0f);
        }
        else
        {
            SetTimelinePosition(1.0f);
            m_director.Stop();
        }
    }

    public void SetTimelinePosition(float newPos)
    {
        m_director.time = newPos;
        m_director.Evaluate();
    }
}
