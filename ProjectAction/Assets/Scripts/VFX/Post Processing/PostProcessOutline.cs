﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(PostProcessOutlineRenderer), PostProcessEvent.BeforeStack, "Sihtinen/Post Process Outline")]
public sealed class PostProcessOutline : PostProcessEffectSettings
{
    public ColorParameter Color = new ColorParameter { };
    public FloatParameter ColorIntensity = new FloatParameter { };

    public FloatParameter Scale = new FloatParameter { value = 1 };
    public FloatParameter DepthThreshold = new FloatParameter { value = 0.2f };
    [Range(0, 1)] public FloatParameter NormalThreshold = new FloatParameter { value = 0.4f };
    [Range(0, 1)] public FloatParameter DepthNormalThreshold = new FloatParameter { value = 0.5f };
    public FloatParameter DepthNormalThresholdScale = new FloatParameter { value = 7 };
}

public sealed class PostProcessOutlineRenderer : PostProcessEffectRenderer<PostProcessOutline>
{
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Sihtinen/Post Process Outline"));

        sheet.properties.SetColor("_Color", settings.Color);
        sheet.properties.SetFloat("_ColorIntensity", settings.ColorIntensity);

        sheet.properties.SetFloat("_Scale", settings.Scale);
        sheet.properties.SetFloat("_DepthThreshold", settings.DepthThreshold);
        sheet.properties.SetFloat("_NormalThreshold", settings.NormalThreshold);
        sheet.properties.SetFloat("_DepthNormalThreshold", settings.DepthNormalThreshold);
        sheet.properties.SetFloat("_DepthNormalThresholdScale", settings.DepthNormalThresholdScale);

        Matrix4x4 clipToView = GL.GetGPUProjectionMatrix(context.camera.projectionMatrix, true).inverse;
        sheet.properties.SetMatrix("_ClipToView", clipToView);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}