﻿using UnityEngine;

public class RenderReplacementShaderToTexture : MonoBehaviour
{
    [SerializeField]
    Shader replacementShader;

    [SerializeField]
    RenderTextureFormat renderTextureFormat = RenderTextureFormat.ARGB32;

    [SerializeField]
    FilterMode filterMode = FilterMode.Point;

    [SerializeField]
    int renderTextureDepth = 24;

    [SerializeField]
    CameraClearFlags cameraClearFlags = CameraClearFlags.Color;

    [SerializeField]
    Color background = Color.black;

    [SerializeField]
    string targetTexture = "_RenderTexture";

    private RenderTexture renderTexture;
    private Camera m_camera;

    private void Awake()
    {
        m_camera = new Camera();
    }

    private void Start()
    {
        return;

        Camera thisCamera = GetComponent<Camera>();

        // Create a render texture matching the main camera's current dimensions.
        renderTexture = new RenderTexture(thisCamera.pixelWidth, thisCamera.pixelHeight, renderTextureDepth, renderTextureFormat);
        renderTexture.filterMode = filterMode;
        // Surface the render texture as a global variable, available to all shaders.
        Shader.SetGlobalTexture(targetTexture, renderTexture);

        // Setup a copy of the camera to render the scene using the normals shader.
        GameObject copy = new GameObject("Camera" + targetTexture);
        m_camera = copy.AddComponent<Camera>();
        m_camera.CopyFrom(thisCamera);
        m_camera.transform.SetParent(transform);
        m_camera.targetTexture = renderTexture;
        m_camera.SetReplacementShader(replacementShader, "RenderType");
        m_camera.depth = thisCamera.depth - 1;
        m_camera.clearFlags = cameraClearFlags;
        m_camera.backgroundColor = background;
    }
}
