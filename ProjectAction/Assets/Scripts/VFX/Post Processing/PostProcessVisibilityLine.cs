﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostProcessVisibilityLine : MonoBehaviour
{
    [SerializeField] private Material m_material;

    private Camera m_mainCamera;
    private Transform m_playerTransform;
    private PlayerVisibilityComponent m_playerVisibility;

    private void Awake()
    {
        m_mainCamera = GetComponent<Camera>();
        m_mainCamera.depthTextureMode = DepthTextureMode.Depth | DepthTextureMode.DepthNormals;
    }

    private void Start()
    {
        m_playerTransform = GameState.GetInstance().PlayerObject.transform;
        m_playerVisibility = m_playerTransform.GetComponent<PlayerVisibilityComponent>();
    }

    [ImageEffectOpaque]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        m_material.SetVector("_PlayerWorldPosition", m_playerTransform.position);
        m_material.SetFloat("_VisibilityRange", m_playerVisibility.GetCurrentVisibilityDistance());

        Matrix4x4 _viewToWorld = m_mainCamera.cameraToWorldMatrix;
        m_material.SetMatrix("_ViewToWorld", _viewToWorld);

        RaycastCornerBlit(source, destination, m_material);
    }

    void RaycastCornerBlit(RenderTexture source, RenderTexture dest, Material mat)
    {
        // Compute Frustum Corners
        float camFar = m_mainCamera.farClipPlane;
        float camFov = m_mainCamera.fieldOfView;
        float camAspect = m_mainCamera.aspect;

        float fovWHalf = camFov * 0.5f;

        Vector3 toRight = m_mainCamera.transform.right * Mathf.Tan(fovWHalf * Mathf.Deg2Rad) * camAspect;
        Vector3 toTop = m_mainCamera.transform.up * Mathf.Tan(fovWHalf * Mathf.Deg2Rad);

        Vector3 topLeft = (m_mainCamera.transform.forward - toRight + toTop);
        float camScale = topLeft.magnitude * camFar;

        topLeft.Normalize();
        topLeft *= camScale;

        Vector3 topRight = (m_mainCamera.transform.forward + toRight + toTop);
        topRight.Normalize();
        topRight *= camScale;

        Vector3 bottomRight = (m_mainCamera.transform.forward + toRight - toTop);
        bottomRight.Normalize();
        bottomRight *= camScale;

        Vector3 bottomLeft = (m_mainCamera.transform.forward - toRight - toTop);
        bottomLeft.Normalize();
        bottomLeft *= camScale;

        // Custom Blit, encoding Frustum Corners as additional Texture Coordinates
        RenderTexture.active = dest;
        mat.SetTexture("_MainTex", source);

        GL.PushMatrix();
        GL.LoadOrtho();

        mat.SetPass(0);

        GL.Begin(GL.QUADS);

        GL.MultiTexCoord2(0, 0.0f, 0.0f);
        GL.MultiTexCoord(1, bottomLeft);
        GL.Vertex3(0.0f, 0.0f, 0.0f);

        GL.MultiTexCoord2(0, 1.0f, 0.0f);
        GL.MultiTexCoord(1, bottomRight);
        GL.Vertex3(1.0f, 0.0f, 0.0f);

        GL.MultiTexCoord2(0, 1.0f, 1.0f);
        GL.MultiTexCoord(1, topRight);
        GL.Vertex3(1.0f, 1.0f, 0.0f);

        GL.MultiTexCoord2(0, 0.0f, 1.0f);
        GL.MultiTexCoord(1, topLeft);
        GL.Vertex3(0.0f, 1.0f, 0.0f);

        GL.End();
        GL.PopMatrix();
    }
}
