﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_ProjectileTrail : MonoBehaviour
{
    private TrailRenderer m_trail = null;
    private ProjectileBase m_projectileBase = null;

    private void Awake()
    {
        m_trail = GetComponent<TrailRenderer>();
        m_projectileBase = GetComponentInParent<ProjectileBase>();

        m_trail.enabled = false;
    }

    public void OnActivate()
    {
        m_trail.enabled = true;
    }

    public void OnDeactivate()
    {
        m_trail.enabled = false;
    }
}
