﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_ProjectileParticles : MonoBehaviour
{
    [SerializeField] private int m_amountOfParticles = 30;

    private ParticleSystem m_particles = null;
    private ProjectileBase m_projectileBase = null;

    private void Awake()
    {
        m_particles = GetComponent<ParticleSystem>();
        m_projectileBase = GetComponentInParent<ProjectileBase>();

        m_projectileBase.ProjectileHit.AddListener(OnHit);
    }

    public void OnActivate()
    {
        m_particles.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }

    public void OnHit()
    {
        transform.up = m_projectileBase.HitNormal;

        var _emitParams = new ParticleSystem.EmitParams();
        m_particles.Emit(_emitParams, m_amountOfParticles);
    }

    public void OnDeactivate()
    {
        m_particles.Emit(m_amountOfParticles);
    }
}
