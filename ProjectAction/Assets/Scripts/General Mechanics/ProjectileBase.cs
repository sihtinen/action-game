﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileBase : MonoBehaviour
{
    public struct FireParameters
    {
        public Vector3 StartPosition;
        public Vector3 Direction;
        public GameObject OverrideObject;
    }

    public enum ProjectileType
    {
        LightPistol
    }

    public bool Alive = false;
    public ProjectileType MyProjectileType;
    public LayerMask CollisionLayers;

    public float Damage = 30;
    public float Speed = 60;
    public float Gravity = 0.0f;

    [HideInInspector] public Vector3 StartPosition;
    [HideInInspector] public Vector3 CurrentVelocity;
    [HideInInspector] public Vector3 HitNormal;

    [SerializeField] private HearableEvent m_hearableEvent_Hit;

    [Space]

    public UnityEvent ProjectileActive;
    public UnityEvent ProjectileDeactive;
    public UnityEvent ProjectileHit;

    private bool m_updatedThisFrame = false;
    private ObjectPool m_objectPool = null;
    private Coroutine m_routineUpdatePosition = null;

    public void Fire(FireParameters parameters)
    {
        m_objectPool = ObjectPool.GetInstance();

        StartPosition = parameters.StartPosition;
        transform.position = StartPosition;
        transform.LookAt(StartPosition + parameters.Direction);

        gameObject.SetActive(true);

        CurrentVelocity = parameters.Direction * Speed;
        Alive = true;

        m_routineUpdatePosition = StartCoroutine(routine_UpdatePosition(parameters));

        ProjectileActive.Invoke();
    }

    private IEnumerator routine_UpdatePosition(FireParameters parameters)
    {
        float _timer = 0.0f;
        while (_timer < 3.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale;

            if (Gravity != 0.0f)
                CurrentVelocity -= Vector3.up * Gravity;

            Vector3 _direction = CurrentVelocity.normalized;
            Vector3 _goDistance = _direction * Speed * Time.deltaTime;
            Vector3 _targetPosition = transform.position + _goDistance;

            int _hits = Physics.RaycastNonAlloc(
                transform.position,
                _direction,
                m_objectPool.RaycastHits,
                _goDistance.magnitude,
                CollisionLayers,
                QueryTriggerInteraction.Ignore);

            if (_hits > 0)
            {
                RaycastHit _closest = m_objectPool.RaycastHits.GetClosestHit(_hits);
                HitNormal = _closest.normal;

                bool _hit = true;

                if (parameters.OverrideObject != null)
                {
                    if (_closest.collider.gameObject != parameters.OverrideObject)
                    {
                        _hit = false;
                    }
                }

                if (_hit)
                {
                    transform.position = _closest.point;
                    _timer = 3.0f;

                    EnemyHitCollider _enemyCollider = _closest.collider.gameObject.GetComponent<EnemyHitCollider>();
                    if (_enemyCollider)
                    {
                        _enemyCollider.ProjectileHit(Damage, _closest.point, _direction);

                        Collider _coll = _closest.collider;
                        Vector3 _exitPoint = _coll.ClosestPoint(_closest.point + _direction * 1.0f);

                        m_objectPool.SpawnProjectileCharacterHitParticles(_exitPoint, _direction);
                        GameState.GetInstance().OnPlayerAttackHit.Invoke(new GameState.PlayerAttackHitInfo());
                    }

                    if (_closest.collider.gameObject.layer == LayerMask.NameToLayer("Default"))
                    {
                        Rigidbody _rigid = _closest.collider.gameObject.GetComponent<Rigidbody>();

                        if (_rigid)
                        {
                            _rigid.AddForceAtPosition(CurrentVelocity * Time.fixedDeltaTime, _closest.point, ForceMode.Impulse);
                        }
                    }

                    m_hearableEvent_Hit.Origin = _closest.point;
                    HearableEventManager.GetInstance().TriggerHearableEvent(m_hearableEvent_Hit);

                    ProjectileHit.Invoke();
                }
            }
            else
            {
                transform.LookAt(transform.position + _direction);
                transform.position = _targetPosition;
            }

            yield return null;
        }

        // GIVE TIME FOR VFX

        _timer = 0.0f;
        while (_timer < 2.0f)
        {
            _timer += Time.deltaTime * GameTime.GameTimeScale;
            yield return null;
        }

        m_routineUpdatePosition = null;
        Alive = false;

        ProjectileDeactive.Invoke();

        gameObject.SetActive(false);
    }
}
