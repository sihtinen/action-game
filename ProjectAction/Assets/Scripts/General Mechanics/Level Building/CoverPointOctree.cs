﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CoverPointOctree : MonoBehaviour
{
    private static CoverPointOctree m_instance = null;
    public static CoverPointOctree GetInstance() { return m_instance; }

    private void Awake()
    {
        m_instance = this;
        NearbyPoints = new List<CoverPoint>();
    }

    [System.Serializable]
    public class CoverPoint
    {
        public CoverPoint(Vector3 position, Vector3 normal)
        {
            Position = position;
            Normal = normal;
        }

        public float GetDotProductToPosition(Vector3 position)
        {
            Vector3 _dir = position - Position;
            return Vector3.Dot(Normal, _dir.normalized);
        }

        public float GetScore(
            Vector3 sourcePosition,
            Vector3 combatTargetPosition,
            Vector3 combatDirectionPerpendicular,
            ref CoverWeightSettings settings)
        {
            Vector3 _toEntity = sourcePosition - Position;
            Vector3 _toCombatTarget = Position - combatTargetPosition;

            float _dot_ToCombatTarget = Vector3.Dot(Normal, _toCombatTarget.normalized) * settings.Weight_DotProduct_Cover;
            float _dotScore_ToCombatTarget = _dot_ToCombatTarget * settings.Weight_DotProduct_Cover;

            float _dotScore_CombatDirection = Mathf.Abs(Vector3.Dot(Normal, combatDirectionPerpendicular));
            _dotScore_CombatDirection *= settings.Weight_DotProduct_ToCombatDirection;

            float _sqrDist = _toEntity.sqrMagnitude;
            _sqrDist = Mathf.Clamp(_sqrDist, settings.MinimumDistance_FromSelfSqrMag, settings.MaximumDistance_FromSelfSqrMag);
            float _pointOnCurve = settings.Curve_DistanceFromSelf.Evaluate((_sqrDist / settings.MaximumDistance_FromSelfSqrMag));
            float _distanceScore_FromSelf = _pointOnCurve * settings.Weight_DistanceFromSelf;

            _sqrDist = _toCombatTarget.sqrMagnitude;
            _sqrDist = Mathf.Clamp(_sqrDist, settings.MinimumDistance_FromTargetSqrMag, settings.MaximumDistance_FromTargetSqrMag);
            _pointOnCurve = settings.Curve_DistanceFromCombatTarget.Evaluate((_sqrDist / settings.MaximumDistance_FromTargetSqrMag));
            float _distanceScore_FromTarget = _pointOnCurve * settings.Weight_DistanceFromTarget;

            float _coverScore = 0.0f;
            if (IsHalfCover) _coverScore = 1.0f * settings.Weight_IsHalfCover;

            float _lineOfSightScore = 0.0f;
            if (settings.CalculateLineOfSight)
            {
                Vector3 _raycastEnd = combatTargetPosition;
                _raycastEnd.y += settings.LineOfSightHeight;

                if (IsHalfCover)
                {
                    Vector3 _raycastStart = Position;
                    _raycastStart.y += settings.LineOfSightHeight;

                    Vector3 _diff = _raycastEnd - _raycastStart;

                    int _hits = Physics.RaycastNonAlloc(
                        _raycastStart,
                        _diff.normalized,
                        RaycastHits,
                        _diff.magnitude,
                        MovementLayer,
                        QueryTriggerInteraction.Ignore);

                    if (_hits == 0)
                        _lineOfSightScore = settings.Weight_LineOfSight;
                }
                else
                {
                    Vector3 _coverNormalPerpendicular = Vector3.Cross(Vector3.up, Normal).normalized;

                    for (int j = 0; j < 2; j++)
                    {
                        Vector3 _raycastStart = Position;
                        _raycastStart.y += settings.LineOfSightHeight;

                        if (j == 0)
                            _raycastStart += _coverNormalPerpendicular * settings.LineOfSight_FullCover_SideOffset;
                        else
                            _raycastStart -= _coverNormalPerpendicular * settings.LineOfSight_FullCover_SideOffset;

                        Vector3 _diff = _raycastEnd - _raycastStart;
                        int _hits = Physics.RaycastNonAlloc(
                            _raycastStart,
                            _diff.normalized,
                            RaycastHits,
                            _diff.magnitude,
                            MovementLayer,
                            QueryTriggerInteraction.Ignore);

                        if (_hits == 0)
                        {
                            _lineOfSightScore = settings.Weight_LineOfSight;
                            break;
                        }
                    }
                }
            }

            float _finalScore =
                _dotScore_ToCombatTarget
                + _dotScore_CombatDirection
                + _distanceScore_FromSelf
                + _distanceScore_FromTarget
                + _coverScore
                + _lineOfSightScore;

            float _maximumScore =
                settings.Weight_DotProduct_Cover
                + settings.Weight_DotProduct_ToCombatDirection
                + settings.Weight_DistanceFromSelf
                + settings.Weight_DistanceFromTarget
                + settings.Weight_IsHalfCover
                + settings.Weight_LineOfSight;

            return Mathf.Clamp01(_finalScore / _maximumScore);
        }

        public void SetOccupied(bool newState)
        {
            m_isOccupied = newState;

            if (OccupiedNeighbours == null) OccupiedNeighbours = new List<CoverPoint>();

            for (int i = 0; i < OccupiedNeighbours.Count; i++)
            {
                if (OccupiedNeighbours[i] == null) continue;
                OccupiedNeighbours[i].m_isOccupied = m_isOccupied;
            }

            if (m_isOccupied == false)
            {
                OccupiedNeighbours.Clear();
            }
        }

        public bool IsOccupied()
        {
            return m_isOccupied;
        }

        public Vector3 Position;
        public Vector3 Normal;

        private bool m_isOccupied = false;
        public bool IsHalfCover = false;
        public bool NeedsRecalculation = false;

        [System.NonSerialized] public List<CoverPoint> ConnectCheckCompleted = new List<CoverPoint>();
        [System.NonSerialized] public List<CoverPoint> OccupiedNeighbours = new List<CoverPoint>();
    }

    [SerializeField] private CoverPointSceneAsset m_pointsAsset;

    [Space]

    [SerializeField] private float m_minimumAgentHeight = 1.8f;
    [SerializeField, Range(0, 90)] private float m_maximumGroundAngle = 45;
    [SerializeField] private float m_pointStepDistance = 0.7f;
    [SerializeField] private float m_pointAdditionalDistance = 2.0f;

    [Space]

    [SerializeField] private float m_halfCoverHeight = 0.85f;
    [SerializeField] private float m_fullCoverHeight = 1.6f;
    [SerializeField] private float m_connectMaxDistanceBetweenPrimaryPoints = 6.0f;
    [SerializeField] private float m_connectStepDistance = 1.5f;
    [SerializeField] private float m_connectMaxHeightDifference = 0.5f;
    [SerializeField] private float m_duplicateCheckDistance = 0.3f;

    [SerializeField] private LayerMask m_movementLayer;

    [System.NonSerialized] public List<CoverPoint> NearbyPoints;

    private NavMeshPath m_path = null;
    private NavMeshHit m_navMeshHit;
    [HideInInspector, System.NonSerialized] public static RaycastHit[] RaycastHits = new RaycastHit[10];
    [HideInInspector, System.NonSerialized] public static LayerMask MovementLayer;

#if UNITY_EDITOR

    [Header("Editor Only")]

    [SerializeField] private bool m_recalculate = true;
    [SerializeField] private bool m_drawDebug;

    private Coroutine m_currentRoutine = null;


#endif

    private void Start()
    {
        MovementLayer = m_movementLayer;

#if UNITY_EDITOR
        if (m_recalculate)
        {
            m_path = new NavMeshPath();
            GenerateOctree();
        }
        else
        {
            if (m_pointsAsset != null)
                m_pointsAsset.LoadAndSetupOctree();
        }
#else
        if (m_pointsAsset != null)
        {
            m_pointsAsset.LoadAndSetupOctree();
        }
#endif
    }

    public CoverPoint GetValidCoverPoint(ref CoverWeightSettings weightSettings, Vector3 searchPosition, Vector3 entityPosition, Vector3 combatTargetPosition)
    { 
        if (m_pointsAsset == null) return null;
        if (m_pointsAsset.CoverPoints == null || m_pointsAsset.CoverPoints.Count == 0) return null;

        NearbyPoints.Clear();
        bool _results = m_pointsAsset.CoverPoints.GetNearbyNonAlloc(searchPosition, weightSettings.SearchDistance, NearbyPoints);

        CoverPoint _result = null;

        Vector3 _combatDirection = combatTargetPosition - entityPosition;
        Vector3 _combatDirectionPerpendicular = Vector3.Cross(Vector3.up, _combatDirection.normalized);

        if (_results)
        {
            float _bestScore = 0;

            for (int i = 0; i < NearbyPoints.Count; i++)
            {
                CoverPoint _current = NearbyPoints[i];

                if (_current == null || _current.IsOccupied())
                {
                    continue;
                }

                Vector3 _toEntity = _current.Position - entityPosition;
                if (_toEntity.sqrMagnitude < weightSettings.MinimumDistance_FromSelfSqrMag)
                {
                    continue;
                }

                Vector3 _toCombatTargetFromCover = _current.Position - combatTargetPosition;
                if (_toCombatTargetFromCover.sqrMagnitude < weightSettings.MinimumDistance_FromTargetSqrMag)
                {
                    continue;
                }

                float _finalScore = _current.GetScore(
                    entityPosition,
                    combatTargetPosition,
                    _combatDirectionPerpendicular,
                    ref weightSettings);

                if (_finalScore > _bestScore)
                {
                    _bestScore = _finalScore;
                    _result = _current;
                }
            }
        }

        if (_result != null)
        {
            NearbyPoints.Clear();
            _results = m_pointsAsset.CoverPoints.GetNearbyNonAlloc(_result.Position, 3, NearbyPoints);
            if (_results)
            {
                _result.OccupiedNeighbours = NearbyPoints;
            }

            _result.SetOccupied(true);
        }

        return _result;
    }

    public List<CoverPoint> GetClosestCoverPoints(Vector3 position, float radius)
    {
        List<CoverPoint> _results = new List<CoverPoint>();
        bool _found = m_pointsAsset.CoverPoints.GetNearbyNonAlloc(position, radius, NearbyPoints);

        if (_found)
        {
            _results = NearbyPoints;
        }

        return _results;
    }

#if UNITY_EDITOR

    public void GenerateOctree()
    {
        if (m_pointsAsset == null)
        {
            string _sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            string _assetName = _sceneName + "_CoverPoints";
            string _filePath = "Assets/Presets/Cover Point Assets/" + _assetName + ".asset";

            m_pointsAsset = AssetDatabase.LoadAssetAtPath<CoverPointSceneAsset>(_filePath);
            if (m_pointsAsset == null)
            {
                Debug.Log("Creating new Cover Points Asset for " + _sceneName, gameObject);

                m_pointsAsset = ScriptableObject.CreateInstance<CoverPointSceneAsset>();
                m_pointsAsset.name = _assetName;
                AssetDatabase.CreateAsset(m_pointsAsset, _filePath);
            }
            else
            {
                Debug.Log("Loaded Cover Points Asset for " + _sceneName + ", please assign the correct reference", gameObject);
            }
        }

        m_pointsAsset.AllPointsList = null;
        m_pointsAsset.CoverPoints = new PointOctree<CoverPoint>(200, Vector3.zero, 5.0f);

        StartCoroutine(routine_generateOctree(/*mesh*/));
    }

    private IEnumerator routine_generateOctree(/*Mesh mesh*/)
    {
        m_currentRoutine = StartCoroutine(routine_scanObjectBounds());
        while (m_currentRoutine != null) yield return null;

        m_currentRoutine = StartCoroutine(routine_recalculateBadCoverPoints());
        while (m_currentRoutine != null) yield return null;

        m_currentRoutine = StartCoroutine(routine_removeDuplicatePoints());
        while (m_currentRoutine != null) yield return null;

        m_currentRoutine = StartCoroutine(routine_calculateCoverHeight());
        while (m_currentRoutine != null) yield return null;

        m_pointsAsset.SaveAsset();

        Debug.Log("Cover point generation completed");
    }

    private IEnumerator routine_scanObjectBounds()
    {
        NavMesh.SamplePosition(Vector3.zero, out m_navMeshHit, 5.0f, NavMesh.AllAreas);
        Vector3 _connectPos = m_navMeshHit.position;

        Object[] _allObjects = FindObjectsOfType(typeof(GameObject));

        GameObject[] _staticObjects;
        _staticObjects = new GameObject[_allObjects.Length];

        int _counter = 0;

        foreach (GameObject go in _allObjects)
        {
            if (go.CompareTag("IgnoreCoverPointCheck")) continue;

            StaticEditorFlags _flags = GameObjectUtility.GetStaticEditorFlags(go);

            if ((_flags & StaticEditorFlags.NavigationStatic) != 0)
            {
                if (go.GetComponent<Renderer>() != null)
                {
                    _staticObjects[_counter] = go;
                    _counter++;
                }
            }
        }

        for (int i = 0; i < _staticObjects.Length; i++)
        {
            if (_staticObjects[i] == null) continue;

            Bounds _bounds = _staticObjects[i].GetComponent<Renderer>().bounds;

            Vector3 _startPos = _bounds.center
                - Vector3.right * ((_bounds.size.x + (m_pointAdditionalDistance * 0.5f)) * 0.5f)
                - Vector3.forward * ((_bounds.size.z + (m_pointAdditionalDistance * 0.5f)) * 0.5f)
                + Vector3.up * ((_bounds.size.y + (m_pointAdditionalDistance * 0.5f)) * 0.5f);

            int _stepsX = (int)((_bounds.size.x + m_pointAdditionalDistance) / m_pointStepDistance);
            int _stepsY = (int)((_bounds.size.y + m_pointAdditionalDistance) / m_pointStepDistance);
            int _stepsZ = (int)((_bounds.size.z + m_pointAdditionalDistance) / m_pointStepDistance);

            for (int x = 0; x < _stepsX; x++)
            {
                for (int z = 0; z < _stepsZ; z++)
                {
                    List<float> _groundHeights = new List<float>();
                    List<RaycastHit> _groundHits = new List<RaycastHit>();

                    for (int y = 0; y < _stepsY; y++)
                    {
                        Vector3 _currentPos = _startPos
                            + Vector3.right * x * m_pointStepDistance
                            + Vector3.forward * z * m_pointStepDistance
                            - Vector3.up * y * m_pointStepDistance;

                        int _hits = Physics.RaycastNonAlloc(
                            _currentPos,
                            Vector3.down,
                            RaycastHits,
                            m_pointStepDistance,
                            m_movementLayer,
                            QueryTriggerInteraction.Ignore);

                        if (_hits == 0) continue;

                        RaycastHit _closest = RaycastHits.GetClosestHit(_hits);
                        if (_groundHeights.Contains(_closest.point.y) == false)
                        {
                            _groundHeights.Add(_closest.point.y);
                            _groundHits.Add(_closest);
                        }
                    }

                    List<RaycastHit> _filteredList = new List<RaycastHit>();
                    for (int j = 0; j < _groundHits.Count; j++)
                    {
                        int _hits = Physics.RaycastNonAlloc(
                            _groundHits[j].point,
                            Vector3.up,
                            RaycastHits,
                            m_minimumAgentHeight,
                            m_movementLayer,
                            QueryTriggerInteraction.Ignore);

                        if (_hits > 0)
                        {
                            if (m_drawDebug)
                                drawShape(_groundHits[j].point, Color.red, 0.3f);

                            continue;
                        }

                        _filteredList.Add(_groundHits[j]);
                    }

                    // Angle filtering
                    for (int j = _filteredList.Count; j --> 0;)
                    {
                        if (_filteredList[j].normal != Vector3.up)
                        {
                            Vector3 _horizontal = _filteredList[j].normal;
                            _horizontal.y = 0;

                            if (Mathf.Abs(Vector3.Angle(_filteredList[j].normal, _horizontal)) < m_maximumGroundAngle)
                            {
                                if (m_drawDebug)
                                    drawShape(_filteredList[j].point, Color.yellow, 0.15f);

                                _filteredList.RemoveAt(j);
                                continue;
                            }
                        }

                        drawShape(_filteredList[j].point, Color.green, 0.15f);
                    }

                    //Navmesh filtering
                    for (int j = _filteredList.Count; j-- > 0;)
                    {
                        bool _hit = NavMesh.SamplePosition(_filteredList[j].point, out m_navMeshHit, m_pointStepDistance, NavMesh.AllAreas);
                        if (_hit)
                        {
                            Vector3 _pointOnNavmesh = m_navMeshHit.position;

                            _hit = NavMesh.FindClosestEdge(_pointOnNavmesh, out m_navMeshHit, NavMesh.AllAreas);
                            if (_hit)
                            {
                                NavMesh.CalculatePath(_connectPos, m_navMeshHit.position, NavMesh.AllAreas, m_path);
                                if (m_path.status == NavMeshPathStatus.PathComplete)
                                {
                                    CoverPoint _newPoint = new CoverPoint(m_navMeshHit.position, m_navMeshHit.normal);
                                    _newPoint.NeedsRecalculation = true;
                                    m_pointsAsset.CoverPoints.Add(_newPoint, _newPoint.Position);
                                }
                            }
                        }
                    }
                }

                yield return null;
            }

            yield return null;
        }

        m_currentRoutine = null;
    }

    private IEnumerator routine_generateCoverPointsFromMeshVertices(Mesh mesh)
    {
        int _current = 0;
        NavMesh.SamplePosition(Vector3.zero, out m_navMeshHit, 5.0f, NavMesh.AllAreas);
        Vector3 _connectPos = m_navMeshHit.position;

        while (_current < mesh.vertexCount)
        {
            Vector3 _vertexPosition = mesh.vertices[_current];

            NavMesh.CalculatePath(_connectPos, _vertexPosition, NavMesh.AllAreas, m_path);
            if (m_path.status == NavMeshPathStatus.PathComplete)
            {
                NavMesh.FindClosestEdge(_vertexPosition, out m_navMeshHit, NavMesh.AllAreas);
                if (m_navMeshHit.hit)
                {
                    CoverPoint _newPoint = new CoverPoint(_vertexPosition, m_navMeshHit.normal);

                    int _hits = Physics.RaycastNonAlloc(
                        _vertexPosition + Vector3.up * 0.2f,
                        -m_navMeshHit.normal,
                        RaycastHits,
                        0.6f,
                        m_movementLayer,
                        QueryTriggerInteraction.Ignore);

                    if (_hits > 0)
                    {
                        RaycastHit _closest = RaycastHits.GetClosestHit(_hits);
                        _newPoint.Position = _closest.point + _closest.normal * 0.25f;
                        _newPoint.Position.y = _vertexPosition.y;
                        _newPoint.Normal = _closest.normal;
                    }
                    else
                    {
                        _newPoint.NeedsRecalculation = true;
                    }

                    m_pointsAsset.CoverPoints.Add(_newPoint, _newPoint.Position);
                }
            }

            _current++;

            if (_current % 10 == 0)
            {
                yield return null;
            }
        }

        Destroy(mesh);

        m_currentRoutine = null;
    }

    private IEnumerator routine_recalculateBadCoverPoints()
    {
        int _current = 0;
        ICollection<CoverPoint> _coverPoints = m_pointsAsset.CoverPoints.GetAll();
        foreach (CoverPoint point in _coverPoints)
        {
            if (point.NeedsRecalculation == false) continue;

            int _hits = Physics.RaycastNonAlloc(
                point.Position + Vector3.up * 0.2f,
                Vector3.down,
                RaycastHits,
                0.4f,
                m_movementLayer,
                QueryTriggerInteraction.Ignore);

            if (_hits == 0)
            {
                m_pointsAsset.CoverPoints.Remove(point, point.Position);
                continue;
            }

            RaycastHit _groundHit = RaycastHits.GetClosestHit(_hits);

            float _bestScore = 0;
            CoverPoint _newPoint = new CoverPoint(Vector3.zero, Vector3.zero);

            int _steps = 36;
            for (int i = 0; i < _steps; i++)
            {
                float _angle = (((float)i / (float)_steps) * 360.0f) * Mathf.Deg2Rad;
                Vector3 _angleDirection = new Vector3(Mathf.Cos(_angle), 0, Mathf.Sin(_angle));
                Vector3 _pointOnPlane = Vector3.ProjectOnPlane(_angleDirection, _groundHit.normal);

                _hits = Physics.RaycastNonAlloc(
                    _groundHit.point + _groundHit.normal * 0.2f,
                    _pointOnPlane,
                    RaycastHits,
                    1.0f,
                    m_movementLayer,
                    QueryTriggerInteraction.Ignore);

                if (_hits > 0)
                {
                    RaycastHit _closest = RaycastHits.GetClosestHit(_hits);
                    float _currentDot = Vector3.Dot(point.Normal, _closest.normal);

                    float _currentScore = _currentDot - _closest.distance;

                    if (_currentScore > _bestScore)
                    {
                        _bestScore = _currentScore;

                        float _oldY = point.Position.y;
                        _newPoint.Position = _closest.point + _closest.normal * 0.25f;
                        _newPoint.Position.y = _oldY;

                        _newPoint.Normal = _closest.normal;
                        _newPoint.Normal.y = 0;
                        _newPoint.Normal.Normalize();

                        _newPoint.NeedsRecalculation = false;
                        _newPoint.ConnectCheckCompleted = point.ConnectCheckCompleted;
                    }
                }
                else
                {
                    if (m_drawDebug)
                        Debug.DrawLine(_groundHit.point + _groundHit.normal * 0.2f, _groundHit.point + _groundHit.normal * 0.2f + _pointOnPlane, Color.white, 0.3f);
                }
            }

            m_pointsAsset.CoverPoints.Remove(point, point.Position);
            if (_bestScore > 0)
            {
                m_pointsAsset.CoverPoints.Add(_newPoint, _newPoint.Position);
            }

            _current++;
            if (_current % 20 == 0)
            {
                yield return null;
            }
        }

        m_currentRoutine = null;
    }

    private IEnumerator routine_calculateNeighbourLinks()
    {
        float _sqrStepDistance = m_connectStepDistance * m_connectStepDistance;
        List<CoverPoint> _pointsToAdd = new List<CoverPoint>();

        int _current = 0;
        ICollection<CoverPoint> _coverPoints = m_pointsAsset.CoverPoints.GetAll();
        foreach (CoverPoint point in _coverPoints)
        {
            bool _foundNeighbors = m_pointsAsset.CoverPoints.GetNearbyNonAlloc(point.Position, m_connectMaxDistanceBetweenPrimaryPoints, NearbyPoints);
            if (_foundNeighbors)
            {
                for (int i = 0; i < NearbyPoints.Count; i++)
                {
                    if (NearbyPoints[i] == null)
                    {
                        break;
                    }

                    if (NearbyPoints[i].ConnectCheckCompleted.Contains(point) || point.ConnectCheckCompleted.Contains(NearbyPoints[i]))
                    {
                        continue;
                    }

                    NearbyPoints[i].ConnectCheckCompleted.Add(point);
                    point.ConnectCheckCompleted.Add(NearbyPoints[i]);

                    Vector3 _diff = NearbyPoints[i].Position - point.Position;
                    if (Mathf.Abs(_diff.y) > m_connectMaxHeightDifference)
                    {
                        continue;
                    }

                    int _hits = Physics.RaycastNonAlloc(
                        point.Position,
                        _diff.normalized,
                        RaycastHits,
                        _diff.magnitude,
                        m_movementLayer,
                        QueryTriggerInteraction.Ignore);

                    if (_hits > 0)
                    {
                        continue;
                    }

                    Vector3 _halfWayPoint = point.Position + _diff * 0.5f;

                    Vector3 _averageInvertedNormal = (point.Normal + NearbyPoints[i].Normal) / -2;
                    _averageInvertedNormal.Normalize();

                    _hits = Physics.RaycastNonAlloc(
                        _halfWayPoint,
                        _averageInvertedNormal,
                        RaycastHits,
                        0.26f,
                        m_movementLayer,
                        QueryTriggerInteraction.Ignore);

                    if (_hits == 0)
                    {
                        continue;
                    }

                    Vector3 _currentPos = point.Position;
                    if (Vector3.SqrMagnitude(_currentPos - NearbyPoints[i].Position) < _sqrStepDistance) continue;

                    RaycastHit _closest = RaycastHits.GetClosestHit(_hits);
                    Vector3 _normal = _closest.normal;
                    bool _continue = true;

                    while (_continue == true)
                    {
                        _currentPos += _diff.normalized * m_connectStepDistance;

                        if (Vector3.SqrMagnitude(_currentPos - NearbyPoints[i].Position) < _sqrStepDistance)
                        {
                            _continue = false;
                        }

                        _hits = Physics.RaycastNonAlloc(
                            _currentPos,
                            -_normal,
                            RaycastHits,
                            0.26f,
                            m_movementLayer,
                            QueryTriggerInteraction.Ignore);

                        if (_hits == 0)
                        {
                            break;
                        }

                        _closest = RaycastHits.GetClosestHit(_hits);
                        Vector3 _fullCoverPos = _currentPos + Vector3.up * m_fullCoverHeight;

                        _hits = Physics.RaycastNonAlloc(
                            _fullCoverPos,
                            -_normal,
                            RaycastHits,
                            0.4f,
                            m_movementLayer,
                            QueryTriggerInteraction.Ignore);

                        CoverPoint _newPoint = new CoverPoint(_currentPos, _closest.normal);

                        if (_hits == 0)
                        {
                            _newPoint.IsHalfCover = true;
                        }
                        else
                        {
                            _newPoint.IsHalfCover = false;
                        }

                        _pointsToAdd.Add(_newPoint);
                    }
                }
            }

            _current++;
            if (_current % 10 == 0)
            {
                yield return null;
            }
        }

        for (int i = 0; i < _pointsToAdd.Count; i++)
        {
            m_pointsAsset.CoverPoints.Add(_pointsToAdd[i], _pointsToAdd[i].Position);
        }

        m_currentRoutine = null;
    }

    private IEnumerator routine_removeDuplicatePoints()
    {
        int _current = 0;
        ICollection<CoverPoint> _coverPoints = m_pointsAsset.CoverPoints.GetAll();
        foreach (CoverPoint point in _coverPoints)
        {
            bool _duplicatesFound = m_pointsAsset.CoverPoints.GetNearbyNonAlloc(point.Position, m_duplicateCheckDistance, NearbyPoints);
            if (_duplicatesFound)
            {
                Vector3 _averagePosition = point.Position;
                int _pointCount = 1;

                for (int i = 0; i < NearbyPoints.Count; i++)
                {
                    if (NearbyPoints[i] == null)
                    {
                        continue;
                    }

                    if (point.Normal == NearbyPoints[i].Normal)
                    {
                        _averagePosition += NearbyPoints[i].Position;
                        _pointCount++;

                        m_pointsAsset.CoverPoints.Remove(NearbyPoints[i], NearbyPoints[i].Position);
                    }
                }

                if (_pointCount > 1)
                {
                    _averagePosition /= _pointCount;
                    CoverPoint _newPoint = new CoverPoint(_averagePosition, point.Normal);
                    point.Normal.y = 0;
                    point.Normal.Normalize();

                    _newPoint.NeedsRecalculation = false;
                    _newPoint.ConnectCheckCompleted = point.ConnectCheckCompleted;
                    _newPoint.IsHalfCover = point.IsHalfCover;

                    m_pointsAsset.CoverPoints.Remove(point);
                    m_pointsAsset.CoverPoints.Add(_newPoint, _newPoint.Position);
                }
            }

            _current++;
            if (_current % 20 == 0)
            {
                yield return null;
            }
        }

        m_currentRoutine = null;
    }

    private IEnumerator routine_calculateCoverHeight()
    {
        int _current = 0;
        ICollection<CoverPoint> _coverPoints = m_pointsAsset.CoverPoints.GetAll();
        foreach (CoverPoint point in _coverPoints)
        {
            Vector3 _fullCoverPos = point.Position + Vector3.up * m_fullCoverHeight;

            int _hits = Physics.RaycastNonAlloc(
                _fullCoverPos,
                -point.Normal,
                RaycastHits,
                1.0f,
                m_movementLayer,
                QueryTriggerInteraction.Ignore);

            if (_hits > 0)
            {
                point.IsHalfCover = false;
                continue;
            }
            else
            {
                point.IsHalfCover = true;
            }

            _current++;
            if (_current % 30 == 0)
            {
                yield return null;
            }
        }

        m_currentRoutine = null;
    }

    private void drawShape(Vector3 pos, Color color, float size)
    {
        Vector3 _xStart = pos - Vector3.right * (size * 0.5f);
        Vector3 _xEnd = pos + Vector3.right * (size * 0.5f);
        Vector3 _yStart = pos - Vector3.up * (size * 0.5f * 0.2f);
        Vector3 _yEnd = pos + Vector3.up * (size * 0.5f * 0.2f);
        Vector3 _zStart = pos - Vector3.forward * (size * 0.5f);
        Vector3 _zEnd = pos + Vector3.forward * (size * 0.5f);

        Debug.DrawLine(_xStart, _xEnd, color, 60.0f);
        Debug.DrawLine(_yStart, _yEnd, color, 60.0f);
        Debug.DrawLine(_zStart, _zEnd, color, 60.0f);
    }

    void OnDrawGizmos()
    {
        if (m_drawDebug == false) return;
        if (m_pointsAsset == null) return;
        if (m_pointsAsset.CoverPoints != null)
        {
            ICollection<CoverPoint> _coverPoints = m_pointsAsset.CoverPoints.GetAll();
            foreach (CoverPoint point in _coverPoints)
            {
                Vector3 _fullCoverPoint = point.Position + Vector3.up * m_fullCoverHeight;

                if (point.NeedsRecalculation == false)
                {
                    Vector3 _halfCoverPoint = point.Position + Vector3.up * m_halfCoverHeight;

                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(point.Position, _fullCoverPoint);

                    Gizmos.color = Color.yellow;
                    Gizmos.DrawLine(point.Position, point.Position + point.Normal);

                    if (point.IsHalfCover)
                    {
                        Gizmos.color = Color.cyan;
                        Gizmos.DrawLine(_halfCoverPoint, _halfCoverPoint - point.Normal);
                    }
                }
                else
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(point.Position, _fullCoverPoint);
                    Gizmos.DrawLine(point.Position, point.Position + point.Normal);
                }
            }
        }
    }

#endif
}
