﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Cover Points/New Cover Point Weight Settings", fileName = "New Cover Point Weight Settings")]
public class CoverWeightSettings : ScriptableObject
{
    public float SearchDistance = 15;

    public AnimationCurve Curve_DistanceFromCombatTarget;
    public float MinimumDistance_FromTargetSqrMag = 16;
    public float MaximumDistance_FromTargetSqrMag = 1200;

    public AnimationCurve Curve_DistanceFromSelf;
    public float MinimumDistance_FromSelfSqrMag = 16;
    public float MaximumDistance_FromSelfSqrMag = 1200;

    [Space]

    public float Weight_DotProduct_Cover;
    public float Weight_DotProduct_ToCombatDirection;
    public float Weight_DistanceFromSelf;
    public float Weight_DistanceFromTarget;
    public float Weight_IsHalfCover;

    [Space]

    public bool CalculateLineOfSight = false;
    public float Weight_LineOfSight = 0;
    public float LineOfSightHeight = 1.5f;
    public float LineOfSight_FullCover_SideOffset = 1.0f;
}
