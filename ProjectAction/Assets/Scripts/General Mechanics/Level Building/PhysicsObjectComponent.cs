﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObjectComponent : MonoBehaviour
{
    public bool PhysicsEnabled = false;
    public bool IgnorePlayerCollider = false;

    [SerializeField] private Collider[] m_colliders;

    private Rigidbody m_rigidbody = null;

    private void Start()
    {
        updateComponents();
    }

    public void SetEnabled(bool newState)
    {
        PhysicsEnabled = newState;
        updateComponents();
    }

    private void updateComponents()
    {
        if (m_rigidbody == null) m_rigidbody = GetComponent<Rigidbody>();
        m_rigidbody.isKinematic = !PhysicsEnabled;

        Collider _playerCollider = GameState.GetInstance().PlayerObject.GetComponent<Collider>();

        if (m_colliders != null)
        {
            for (int i = 0; i < m_colliders.Length; i++)
            {
                if (m_colliders[i] == null) continue;
                m_colliders[i].enabled = PhysicsEnabled;

                if (PhysicsEnabled && IgnorePlayerCollider)
                {
                    Physics.IgnoreCollision(m_colliders[i], _playerCollider, true);
                }
            }
        }
    }
}
