﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshRendererGroupComponent : MonoBehaviour
{
    [SerializeField] private Material m_material;
    [SerializeField] private bool m_useSecondaryMaterial = false;
    [SerializeField] private Material m_materialSecondary;

    [SerializeField] UnityEngine.Rendering.ShadowCastingMode m_shadowCastingMode;

    public void UpdateMeshRenderers()
    {
        foreach (MeshRenderer rend in GetComponentsInChildren<MeshRenderer>())
        {
            if (m_useSecondaryMaterial)
            {
                Material[] _newMats = new Material[2];
                _newMats[0] = m_material;
                _newMats[1] = m_materialSecondary;

                rend.materials = _newMats;
            }
            else
            {
                Material[] _newMats = new Material[1];
                _newMats[0] = m_material;

                rend.materials = _newMats;
            }

            rend.shadowCastingMode = m_shadowCastingMode;
        }
    }
}
