﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Cover Points/New Cover Point Scene Asset", fileName = "[SCENE NAME]_CoverPoints")]
public class CoverPointSceneAsset : ScriptableObject
{
    [System.NonSerialized] public PointOctree<CoverPointOctree.CoverPoint> CoverPoints;
    public List<CoverPointOctree.CoverPoint> AllPointsList;

#if UNITY_EDITOR
    public void SaveAsset()
    {
        AllPointsList = new List<CoverPointOctree.CoverPoint>();

        ICollection<CoverPointOctree.CoverPoint> _coverPoints = CoverPoints.GetAll();
        foreach (CoverPointOctree.CoverPoint point in _coverPoints)
        {
            AllPointsList.Add(point);
        }

        UnityEditor.EditorUtility.SetDirty(this);
        UnityEditor.AssetDatabase.SaveAssets();
    }
#endif

    public void LoadAndSetupOctree()
    {
        CoverPoints = new PointOctree<CoverPointOctree.CoverPoint>(200, Vector3.zero, 5.0f);
        for (int i = 0; i < AllPointsList.Count; i++)
        {
            CoverPoints.Add(AllPointsList[i], AllPointsList[i].Position);
        }
    }
}
