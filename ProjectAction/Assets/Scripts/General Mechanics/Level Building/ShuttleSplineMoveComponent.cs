﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;

public class ShuttleSplineMoveComponent : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField, Range(0, 1)] private float m_initialPosition = 0.5f;
    public float MoveSpeed = 0.03f;
    public float MoveSpeedMultiplier = 1.0f;
    public int Direction = 1;

    [SerializeField] private float m_rotationPosOffset = 0.07f;
    [SerializeField] private float m_followCartOffset = -0.02f;

    [SerializeField] private float m_positionMoveSharpness = 5;
    [SerializeField] private float m_rotationMoveSharpness = 5;

    [Header("Object References")]
    [SerializeField] private BezierSpline m_splinePath;
    [SerializeField] private List<Transform> m_followCarts = new List<Transform>();

    private float m_currentSplinePosition = 0.5f;

    private void Awake()
    {
        m_currentSplinePosition = m_initialPosition;

        for (int i = 0; i < m_followCarts.Count; i++)
        {
            m_followCarts[i].parent = null;
        }
    }

    private void Update()
    {
        if (m_splinePath == null) return;
        else if (GameTime.GameTimeScale == 0.0f) return;

        m_currentSplinePosition += Time.deltaTime * GameTime.GameTimeScale * MoveSpeed * MoveSpeedMultiplier * Direction;
        if (m_currentSplinePosition > 1.0f) m_currentSplinePosition -= 1.0f;
        else if (m_currentSplinePosition < 0.0f) m_currentSplinePosition += 1.0f;

        float _rotPos = m_currentSplinePosition + (m_rotationPosOffset * Direction);
        if (_rotPos > 1.0f) _rotPos -= 1.0f;
        else if (_rotPos < 0.0f) _rotPos += 1.0f;

        float _moveAmount = Time.deltaTime * GameTime.GameTimeScale * m_positionMoveSharpness;
        float _rotationAmount = Time.deltaTime * GameTime.GameTimeScale * m_rotationMoveSharpness;

        transform.position = Vector3.Lerp(transform.position, m_splinePath.GetPoint(m_currentSplinePosition), _moveAmount);
        transform.right = Vector3.Lerp(transform.right, m_splinePath.GetTangent(_rotPos), _rotationAmount);

        if (m_followCarts.Count > 0)
        {
            float _addedPos = m_currentSplinePosition - (m_followCartOffset * Direction);

            for (int i = 0; i < m_followCarts.Count; i++)
            {
                _rotPos = _addedPos + (m_rotationPosOffset * Direction);
                if (_rotPos > 1.0f) _rotPos -= 1.0f;
                else if (_rotPos < 0.0f) _rotPos += 1.0f;

                m_followCarts[i].position = Vector3.Lerp(m_followCarts[i].position, m_splinePath.GetPoint(_addedPos), _moveAmount);
                m_followCarts[i].right = Vector3.Lerp(m_followCarts[i].right, m_splinePath.GetTangent(_rotPos), _rotationAmount);

                _addedPos -= (m_followCartOffset * Direction);
            }
        }
    }
}
