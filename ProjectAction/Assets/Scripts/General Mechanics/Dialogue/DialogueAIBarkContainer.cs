﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Dialogue/New AI Bark Container", fileName = "New AI Bark Container")]
public class DialogueAIBarkContainer : ScriptableObject
{
    public DialogueLineContainer InvestigationStartLines;
    public DialogueLineContainer InvestigationEndLines;
    public DialogueLineContainer CombatStartLines;
}
