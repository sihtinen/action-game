﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Dialogue/New Dialogue Line Container", fileName = "New Dialogue Line Container")]
public class DialogueLineContainer : ScriptableObject
{
    public List<string> Lines = new List<string>();

    private static System.Random m_random = null;

    public string GetRandomLine()
    {
        if (m_random == null) m_random = new System.Random();

        int _index = m_random.Next(Lines.Count);
        return Lines[_index];
    }
}
