﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueSpeaker : MonoBehaviour
{
    [SerializeField] private Transform PivotPoint;
    [SerializeField] private Vector3 PositionOffset;

    private static Transform m_mainCameraTransform = null;
    private Coroutine m_routineCurrentLine = null;

    public void SpeakLine()
    {
        if (m_routineCurrentLine != null) StopCoroutine(routine_currentLine());
        m_routineCurrentLine = StartCoroutine(routine_currentLine());
    }

    private IEnumerator routine_currentLine()
    {
        yield return null;
    }
}
