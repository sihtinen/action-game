﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightLevelMeasureMaster : MonoBehaviour
{
    [SerializeField] private LightLevelMeasureComponent m_measureComponentDown;
    [SerializeField] private LightLevelMeasureComponent m_measureComponentUp;

    [Header("UI")]
    [SerializeField] private Slider m_slider;
    [SerializeField] private Image m_progressBarImage;

    [HideInInspector] public float CurrentAverageLightLevel;
    [HideInInspector] public float CurrentAverageLightLevelNormalized;

    private Color m_color = new Color(1, 1, 1, 1);
    private float m_targetLightLevel;

    private void Start()
    {
        GameState.GetInstance().PlayerObject.GetComponent<PlayerVisibilityComponent>().LightMeasureMaster = this;
    }

    private void Update()
    {
        float _current = CurrentAverageLightLevel;
        CurrentAverageLightLevel = Mathf.Lerp(CurrentAverageLightLevel, m_targetLightLevel, Time.deltaTime * GameTime.GameTimeScale * 10);
        CurrentAverageLightLevelNormalized = CurrentAverageLightLevel / 255;

        if (_current != CurrentAverageLightLevel)
        {
            m_slider.value = CurrentAverageLightLevel;

            m_color.r = CurrentAverageLightLevelNormalized;
            m_color.g = CurrentAverageLightLevelNormalized;
            m_color.b = CurrentAverageLightLevelNormalized;

            m_progressBarImage.color = m_color;
        }
    }

    public void OnLightLevelUpdate()
    {
        m_targetLightLevel = Mathf.Max(m_measureComponentDown.CurrentLightLevel, m_measureComponentUp.CurrentLightLevel);
    }
}
