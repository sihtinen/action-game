﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightLevelMeasureComponent : MonoBehaviour
{
    [SerializeField] private int m_renderTextureSize = 32;

    [SerializeField] private float m_verticalOffset = 0.2f;
    [SerializeField, Min(0)] private float m_renderFrequency = 0.2f;

    [Header("Object References")]
    [SerializeField] private Camera m_camera;

    [HideInInspector] public float CurrentLightLevel;

    private Coroutine m_routineRender = null;
    private Transform m_playerTransform = null;
    private LightLevelMeasureMaster m_lightLevelMaster = null;

    private RenderTexture m_renderTexture = null;
    private Texture2D m_colorTexture = null;
    private Rect m_readRect;

    private void Awake()
    {
        m_renderTexture = new RenderTexture(m_renderTextureSize, m_renderTextureSize, 16, RenderTextureFormat.ARGB32);
        m_renderTexture.antiAliasing = 1;
        m_renderTexture.filterMode = FilterMode.Point;
        m_renderTexture.Create();

        m_colorTexture = new Texture2D(m_renderTextureSize, m_renderTextureSize, TextureFormat.RGB24, false);
        m_colorTexture.anisoLevel = 0;
        m_colorTexture.filterMode = FilterMode.Point;

        m_readRect = new Rect(0, 0, m_renderTextureSize, m_renderTextureSize);

        m_camera.targetTexture = m_renderTexture;
        m_camera.forceIntoRenderTexture = true;
    }

    private void Start()
    {
        m_lightLevelMaster = GetComponentInParent<LightLevelMeasureMaster>();
        m_playerTransform = GameState.GetInstance().PlayerObject.transform;
        m_routineRender = StartCoroutine(routine_Render());
    }

    private IEnumerator routine_Render()
    {
        float _timer = 0.0f;
        bool _running = true;
        while (_running)
        {
            render();

            while (_timer < m_renderFrequency)
            {
                _timer += Time.deltaTime * GameTime.GameTimeScale;
                yield return null;
            }

            _timer = 0.0f;
        }
    }

    private void render()
    {
        transform.position = m_playerTransform.position + Vector3.up * m_verticalOffset;

        m_camera.enabled = true;
        m_camera.Render();
        m_camera.enabled = false;

        RenderTexture _previous = RenderTexture.active;
        RenderTexture.active = m_renderTexture;
        m_colorTexture.ReadPixels(m_readRect, 0, 0);
        m_colorTexture.Apply();
        RenderTexture.active = _previous;

        float _total = 0;
        Color32 _currentColor;

        for (int i = 0; i < m_renderTextureSize; i++)
        {
            for (int j = 0; j < m_renderTextureSize; j++)
            {
                _currentColor = m_colorTexture.GetPixel(i, j);
                float _averageColor = (_currentColor.r + _currentColor.g + _currentColor.b) / 3;
                _total += _averageColor;
            }
        }

        CurrentLightLevel = _total / (m_renderTextureSize * m_renderTextureSize);

        if (m_lightLevelMaster) m_lightLevelMaster.OnLightLevelUpdate();
    }
}
