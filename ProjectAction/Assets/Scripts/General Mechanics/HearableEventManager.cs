﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AwarenessEventType
{
    None = 0,
    ProjectileShoot = 30,
    ProjectileHit = 20,
    Takedown = 25,
    Movement = 10,
    OnAirLanding = 15,
    LineOfSight = 50
}

[System.Serializable]
public struct HearableEvent
{
    public AwarenessEventType EventType;
    public float SqrRadius;

    public float AwarenessIncrease;
    public float AwarenessIncreaseAggressionLimit;
    public AnimationCurve CurveAwarenessIncrease;

    [HideInInspector] public bool HasPointOnNavmesh;
    [HideInInspector] public Vector3 Origin;
    [HideInInspector] public Vector3 PointOnNavmesh;
}

public class HearableEventInstance
{
    public HearableEvent Hearable;
    public List<AIHearingComponent> LinkedComponents = new List<AIHearingComponent>();

    public float GetAwarenessIncrease(Vector3 listenPosition)
    {
        Vector3 _diff = listenPosition - Hearable.Origin;
        float _result = 0.0f;

        switch (Hearable.EventType)
        {
            case AwarenessEventType.Movement:
                _result = Hearable.CurveAwarenessIncrease.Evaluate(_diff.sqrMagnitude / Hearable.SqrRadius) * Hearable.AwarenessIncrease * Time.deltaTime * GameTime.GameTimeScale;
                break;

            default:
                _result = Hearable.CurveAwarenessIncrease.Evaluate(_diff.sqrMagnitude / Hearable.SqrRadius) * Hearable.AwarenessIncrease;
                break;
        }

        return _result;
    }
}

public class HearableEventManager : MonoBehaviour
{
    private static HearableEventManager m_instance = null;
    public static HearableEventManager GetInstance() { return m_instance; }

    private List<AIHearingComponent> m_registeredEntities = new List<AIHearingComponent>();
    private List<HearableEventInstance> m_eventInstances = new List<HearableEventInstance>();

    [SerializeField] private float m_eventTravelDelay = 0.02f;

    private NavMeshHit m_navMeshHit;

    private void Awake()
    {
        m_instance = this;
    }

    public void RegisterEntity(AIHearingComponent component)
    {
        if (m_registeredEntities.Contains(component) == false)
            m_registeredEntities.Add(component);
    }

    public void UnregisterEntity(AIHearingComponent component)
    {
        if (m_registeredEntities.Contains(component))
            m_registeredEntities.Remove(component);
    }

    public void TriggerHearableEvent(HearableEvent hearable)
    {
        hearable.HasPointOnNavmesh = NavMesh.SamplePosition(hearable.Origin, out m_navMeshHit, 3, NavMesh.AllAreas);
        if (hearable.HasPointOnNavmesh)
        {
            hearable.PointOnNavmesh = m_navMeshHit.position;
        }

        HearableEventInstance _instance = new HearableEventInstance();
        _instance.Hearable = hearable;
        m_eventInstances.Add(_instance);

        for (int i = 0; i < m_registeredEntities.Count; i++)
        {
            float _sqrDist = (m_registeredEntities[i].transform.position - hearable.Origin).sqrMagnitude;

            if (_sqrDist < hearable.SqrRadius)
            {
                m_registeredEntities[i].OnEventHeard(_instance);
            }
        }
    }

    private void LateUpdate()
    {
        for (int i = m_eventInstances.Count; i --> 0;)
        {
            if (m_eventInstances[i].LinkedComponents.Count == 0)
            {
                m_eventInstances[i] = null;
                m_eventInstances.RemoveAt(i);
            }
        }
    }
}
