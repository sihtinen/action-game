﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;

public class Editor_WaypointWindow : EditorWindow
{
    [MenuItem("Tools/Waypoint Editor")]
    public static void Open()
    {
        GetWindow<Editor_WaypointWindow>();
    }

    public Transform WaypointRoot = null;

    [Range(0, 3)]public float NavmeshSearchRadius = 3;

    private Waypoint m_selectedWaypoint = null;
    private Vector3 m_previousWaypointPosition = Vector3.zero;

    private WaypointCollection m_waypointCollection = null;
    private NavMeshHit m_navmeshHit = new NavMeshHit();

    public void OnInspectorUpdate()
    {
        Repaint();
    }

    private void OnGUI()
    {
        SerializedObject _obj = new SerializedObject(this);
        EditorGUILayout.PropertyField(_obj.FindProperty("WaypointRoot"));

        if (WaypointRoot == null)
        {
            EditorGUILayout.HelpBox("Select a root object for waypoints", MessageType.Warning);
            _obj.ApplyModifiedProperties();
            return;
        }

        WaypointCollection _collection = WaypointRoot.GetComponent<WaypointCollection>();
        if (_collection == null)
        {
            EditorGUILayout.HelpBox("Waypoint Collection -component not found", MessageType.Warning);
            
            if (GUILayout.Button("Add Waypoint Collection -script"))
            {
                WaypointRoot.gameObject.AddComponent<WaypointCollection>();
            }

            _obj.ApplyModifiedProperties();
            return;
        }

        m_waypointCollection = _collection;

        EditorGUILayout.BeginVertical("box");
        drawButtons();
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical("box");
        drawSettings(_obj);
        EditorGUILayout.EndVertical();

        _obj.ApplyModifiedProperties();
    }

    private void drawButtons()
    {
        if (GUILayout.Button("Create Waypoint"))
        {
            createWaypoint();
        }

        if (Selection.activeGameObject != null)
        {
            Waypoint _selectedWaypoint = Selection.activeGameObject.GetComponent<Waypoint>();

            if (_selectedWaypoint)
            {
                m_selectedWaypoint = _selectedWaypoint;
                calculateNavmeshPosition();

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Selection: " + _selectedWaypoint.name);

                if (GUILayout.Button("Create Waypoint After Selected"))
                {
                    createWaypointAfterSelected();
                }
                if (GUILayout.Button("Create Waypoint Before Selected"))
                {
                    createWaypointBeforeSelected();
                }
                if (GUILayout.Button("Destroy Selected"))
                {
                    destroySelectedWaypoint();
                }
            }
        }
    }

    private void createWaypoint()
    {
        GameObject _newObject = new GameObject("Waypoint " + m_waypointCollection.Waypoints.Count);
        Waypoint _waypoint = _newObject.AddComponent<Waypoint>();

        _waypoint.Collection = m_waypointCollection;
        _waypoint.transform.position = WaypointRoot.transform.position;

        if (m_waypointCollection.Waypoints.Count > 0)
        {
            Waypoint _previous = m_waypointCollection.Waypoints[m_waypointCollection.Waypoints.Count - 1];

            _waypoint.From = _previous;
            _previous.To = _waypoint;

            _waypoint.transform.position = _previous.transform.position;
            _waypoint.transform.forward = _previous.transform.forward;
        }

        m_waypointCollection.Waypoints.Add(_waypoint);
        _newObject.transform.parent = WaypointRoot;

        Selection.activeGameObject = _waypoint.gameObject;
    }

    private void createWaypointAfterSelected()
    {
        GameObject _newObject = new GameObject("Waypoint " + m_waypointCollection.Waypoints.Count);
        Waypoint _waypoint = _newObject.AddComponent<Waypoint>();
        _waypoint.Collection = m_waypointCollection;

        if (m_selectedWaypoint.To)
        {
            m_selectedWaypoint.To.From = _waypoint;
            _waypoint.To = m_selectedWaypoint.To;

            _waypoint.transform.position = Vector3.Lerp(m_selectedWaypoint.transform.position, m_selectedWaypoint.To.transform.position, 0.5f);
        }
        else
        {
            _waypoint.transform.position = m_selectedWaypoint.transform.position;
        }

        _waypoint.From = m_selectedWaypoint;
        m_selectedWaypoint.To = _waypoint;

        m_waypointCollection.Waypoints.Add(_waypoint);
        _newObject.transform.parent = WaypointRoot;

        Selection.activeGameObject = _waypoint.gameObject;
    }

    private void createWaypointBeforeSelected()
    {
        GameObject _newObject = new GameObject("Waypoint " + m_waypointCollection.Waypoints.Count);
        Waypoint _waypoint = _newObject.AddComponent<Waypoint>();
        _waypoint.Collection = m_waypointCollection;

        if (m_selectedWaypoint.From)
        {
            m_selectedWaypoint.From.To = _waypoint;
            _waypoint.From = m_selectedWaypoint.From;

            _waypoint.transform.position = Vector3.Lerp(m_selectedWaypoint.transform.position, m_selectedWaypoint.From.transform.position, 0.5f);
        }
        else
        {
            _waypoint.transform.position = m_selectedWaypoint.transform.position;
        }

        _waypoint.To = m_selectedWaypoint;
        m_selectedWaypoint.From = _waypoint;

        m_waypointCollection.Waypoints.Add(_waypoint);
        _newObject.transform.parent = WaypointRoot;

        Selection.activeGameObject = _waypoint.gameObject;
    }

    private void destroySelectedWaypoint()
    {
        m_selectedWaypoint.OnDestroy_Logic();
        DestroyImmediate(m_selectedWaypoint.gameObject);
    }

    private void drawSettings(SerializedObject serializedObject)
    {
        EditorGUILayout.LabelField("Settings");
        EditorGUILayout.PropertyField(serializedObject.FindProperty("NavmeshSearchRadius"));
    }

    private void calculateNavmeshPosition()
    {
        if (m_selectedWaypoint.transform.position == m_previousWaypointPosition)
        {
            return;
        }

        m_selectedWaypoint.PointOnNavmeshFound = NavMesh.SamplePosition(m_selectedWaypoint.transform.position, out m_navmeshHit, NavmeshSearchRadius, NavMesh.AllAreas);
        if (m_selectedWaypoint.PointOnNavmeshFound)
        {
            m_selectedWaypoint.PointOnNavmesh = m_navmeshHit.position;
        }

        m_previousWaypointPosition = m_selectedWaypoint.transform.position;
    }
}
