﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad()]
public class Editor_WaypointGizmos
{
    [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Pickable)]
    public static void OnDrawSceneGizmos(Waypoint waypoint, GizmoType gizmoType)
    {
        if (waypoint.Collection && waypoint.Collection.DrawGizmos == false) return;

        switch (waypoint.WaypointAction)
        {
            case Waypoint.WaypointActionType.Movement:
                Gizmos.color = Color.yellow;
                break;

            case Waypoint.WaypointActionType.Idle:
                Gizmos.color = Color.Lerp(Color.white, Color.blue, 0.7f);
                break;

            case Waypoint.WaypointActionType.PerformAction:
                Gizmos.color = Color.Lerp(Color.cyan, Color.red, 0.7f);
                break;
        }

        if ((gizmoType & GizmoType.Selected) != 0)
        {
            Gizmos.color *= 0.6f;
            Gizmos.DrawSphere(waypoint.transform.position, 0.12f);
        }
        else
        {
            Gizmos.DrawSphere(waypoint.transform.position, 0.23f);
        }

        if (waypoint.PointOnNavmeshFound)
        {
            Gizmos.color = Color.Lerp(Color.white, Color.blue, 0.3f);
            Gizmos.DrawWireSphere(waypoint.PointOnNavmesh, 0.1f);
        }

        Gizmos.color = Color.Lerp(Color.red, Color.yellow, 0.6f);
        Gizmos.color *= 0.82f;

        if (waypoint.To == null) return;

        Vector3 _from = waypoint.transform.position + Vector3.up * 0.3f;
        Vector3 _to = waypoint.To.transform.position + Vector3.up * 0.3f;

        Vector3 _direction = _to - _from;
        Vector3 _directionLeft = Vector3.Cross(_direction, Vector3.up).normalized;
        _direction.Normalize();

        Vector3 _arrowPosLeft = _to + _directionLeft * 0.11f - _direction * 0.75f;
        Vector3 _arrowPosRight = _to - _directionLeft * 0.11f - _direction * 0.75f;

        Gizmos.DrawLine(_from, _to);
        Gizmos.DrawLine(_to, _arrowPosLeft);
        Gizmos.DrawLine(_to, _arrowPosRight);
        Gizmos.DrawLine(_arrowPosLeft, _arrowPosRight);
    }
}
