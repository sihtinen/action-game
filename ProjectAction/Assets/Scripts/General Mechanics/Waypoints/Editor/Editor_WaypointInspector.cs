﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Waypoint))]
public class Editor_WaypointInspector : Editor
{
    private Waypoint m_target = null;

    private void OnEnable()
    {
        m_target = (Waypoint)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        drawBaseSettings(serializedObject);

        EditorGUILayout.Space();

        drawConnectionSettings(serializedObject);

        serializedObject.ApplyModifiedProperties();
    }

    private void drawBaseSettings(SerializedObject serializedObject)
    {
        EditorGUILayout.BeginVertical("box");

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Waypoint type", GUILayout.Width(100));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("WaypointAction"), GUIContent.none);
        EditorGUILayout.EndHorizontal();

        switch (m_target.WaypointAction)
        {
            case Waypoint.WaypointActionType.Idle:

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Wait time", GUILayout.Width(100));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("IdleWaitTime"), GUIContent.none);
                EditorGUILayout.EndHorizontal();

                break;

            case Waypoint.WaypointActionType.PerformAction:

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Wait time", GUILayout.Width(100));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("IdleWaitTime"), GUIContent.none);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Look target", GUILayout.Width(100));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("LookTarget"), GUIContent.none);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Move target", GUILayout.Width(100));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("MoveTarget"), GUIContent.none);
                EditorGUILayout.EndHorizontal();

                break;

            default:
                break;
        }

        EditorGUILayout.EndVertical();
    }

    private void drawConnectionSettings(SerializedObject serializedObject)
    {
        EditorGUILayout.BeginVertical("box");

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("From", GUILayout.Width(100));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("From"), GUIContent.none);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("To", GUILayout.Width(100));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("To"), GUIContent.none);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }
}
