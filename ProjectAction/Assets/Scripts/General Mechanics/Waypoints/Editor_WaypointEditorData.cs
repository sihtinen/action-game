﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

[InitializeOnLoad]
public static class Editor_WaypointEditorData
{
    public static PlayModeStateChange PlayState;
    public static List<WaypointCollection> WaypointCollectionsInScene = new List<WaypointCollection>();

    static Editor_WaypointEditorData()
    {
        EditorApplication.playModeStateChanged += LogPlayModeState;
    }

    private static void LogPlayModeState(PlayModeStateChange state)
    {
        PlayState = state;
    }
}

#endif
