﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class WaypointCollection : MonoBehaviour
{
    public List<Waypoint> Waypoints = new List<Waypoint>();
    public bool DrawGizmos = true;

    public Waypoint FindClosestWaypoint(Vector3 searchPosition, bool ignoreMovementNodes)
    {
        Waypoint _result = null;

        if (Waypoints.Count > 0)
        {
            _result = Waypoints[0];

            if (Waypoints.Count > 1)
            {
                float _sqrDist = Vector3.SqrMagnitude(Waypoints[0].GetPosition() - searchPosition);
                float _closestDistance = _sqrDist;
                for (int i = 1; i < Waypoints.Count; i++)
                {
                    if (ignoreMovementNodes)
                    {
                        if (Waypoints[i].WaypointAction == Waypoint.WaypointActionType.Movement)
                        {
                            continue;
                        }
                    }

                    _sqrDist = Vector3.SqrMagnitude(Waypoints[i].GetPosition() - searchPosition);
                    if (_sqrDist < _closestDistance)
                    {
                        _result = Waypoints[i];
                        _closestDistance = _sqrDist;
                    }
                }
            }
        }

        return _result;
    }

#if UNITY_EDITOR
    private void OnEnable()
    {
        if (Editor_WaypointEditorData.WaypointCollectionsInScene.Contains(this) == false)
        {
            Editor_WaypointEditorData.WaypointCollectionsInScene.Add(this);
        }
    }

    private void OnDestroy()
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode == false &&
            Editor_WaypointEditorData.PlayState == PlayModeStateChange.EnteredEditMode)
        {

        }
        else
        {
            return;
        }

        if (Editor_WaypointEditorData.WaypointCollectionsInScene.Contains(this))
        {
            Editor_WaypointEditorData.WaypointCollectionsInScene.Remove(this);
        }
    }
#endif
}
