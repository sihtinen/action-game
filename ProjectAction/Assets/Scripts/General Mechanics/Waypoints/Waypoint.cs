﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class Waypoint : MonoBehaviour
{
    public enum WaypointActionType
    {
        Movement,
        Idle,
        PerformAction
    }

    public Waypoint From;
    public Waypoint To;
    public WaypointCollection Collection;

    public bool PointOnNavmeshFound = false;
    public Vector3 PointOnNavmesh;

    public WaypointActionType WaypointAction;

    [Min(0)] public float IdleWaitTime = 8;

    public Transform LookTarget;
    public Transform MoveTarget;

    public Vector3 GetPosition()
    {
        if (PointOnNavmeshFound)
        {
            return PointOnNavmesh;
        }
        else
        {
            return transform.position;
        }
    }

    public void OnDestroy_Logic()
    {
        if (To != null &&
            From != null)
        {
            To.From = From;
            From.To = To;
        }

        for (int i = Collection.Waypoints.Count; i-- > 0;)
        {
            if (Collection.Waypoints[i] == this)
            {
                Collection.Waypoints.RemoveAt(i);
            }
        }
    }

    private void OnDestroy()
    {
#if UNITY_EDITOR
        if (!EditorApplication.isPlayingOrWillChangePlaymode &&
            Editor_WaypointEditorData.PlayState == PlayModeStateChange.EnteredEditMode)
        {
            
        }
        else
        {
            return;
        }
#endif

        OnDestroy_Logic();
    }
}
