﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

public class WeaponComponent : MonoBehaviour
{
    public struct FireParameters
    {
        public Vector3 AimTarget;
        public bool WaitOneFrameForVFX;
        public bool TriggerStunState;
        public GameObject OverrideHitTarget;
    }

    [Header("Recoil")]
    public PlayerAimManager.RecoilImpulse RecoilImpulse;

    [Header("Screen Shake")]
    [SerializeField] private float m_screenShakeIntensity_Min = 1;
    [SerializeField] private float m_screenShakeIntensity_Max = 1;

    [Header("Projectile")]
    [SerializeField] private ProjectileBase.ProjectileType m_projectileType;

    [Header("Object References")]
    [SerializeField] private Transform m_muzzlePosition;
    [SerializeField] private CinemachineImpulseSource m_impulseSource;

    [Header("Events")]
    [SerializeField] private HearableEvent m_hearableEvent_Fire;

    [Space]

    public UnityEvent OnFire;

    public Vector3 GetMuzzlePosition()
    {
        return m_muzzlePosition.position;
    }

    public void Fire(FireParameters parameters)
    {
        if (parameters.WaitOneFrameForVFX)
        {
            StartCoroutine(routine_VFXOneFrameDelay(parameters));
        }
        else
        {
            fire(parameters);
        }
    }

    private void fire(FireParameters parameters)
    {
        Vector3 _random = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));
        _random.Normalize();

        m_impulseSource.GenerateImpulse(_random * Mathf.Lerp(m_screenShakeIntensity_Min, m_screenShakeIntensity_Max, Random.Range(0, 1)));

        ProjectileBase _projectileInstance = ObjectPool.GetInstance().GetProjectile(m_projectileType);
        if (_projectileInstance)
        {
            Vector3 _direction = parameters.AimTarget - m_muzzlePosition.position;
            _direction.Normalize();

            ProjectileBase.FireParameters _parameters;
            _parameters.StartPosition = GetMuzzlePosition();
            _parameters.Direction = _direction;
            _parameters.OverrideObject = parameters.OverrideHitTarget;

            _projectileInstance.Fire(_parameters);
        }

        m_hearableEvent_Fire.Origin = m_muzzlePosition.position;
        HearableEventManager.GetInstance().TriggerHearableEvent(m_hearableEvent_Fire);

        OnFire.Invoke();
    }

    private IEnumerator routine_VFXOneFrameDelay(FireParameters parameters)
    {
        yield return null;
        fire(parameters);
    }
}
