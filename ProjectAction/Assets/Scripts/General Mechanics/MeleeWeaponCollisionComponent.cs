﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeaponCollisionComponent : MonoBehaviour
{
    public bool CalculateCollision = false;
    public GameObject Owner;

    [SerializeField, Range(1, 10)] private int m_betweenPointsIterations = 3;
    [SerializeField] private Transform[] m_collisionVertices;
    [SerializeField] private LayerMask m_damageLayer;

    private bool m_activePreviousFrame = false;
    private Vector3[,] m_previousCollisionPoints;
    private List<MonoBehaviour> m_registeredTargets = new List<MonoBehaviour>();

    private static RaycastHit[] m_hits = new RaycastHit[100];

    private void Awake()
    {
        m_previousCollisionPoints = new Vector3[m_collisionVertices.Length - 1, m_betweenPointsIterations + 1];
    }

    private void Update()
    {
        if (CalculateCollision == false)
        {
            m_activePreviousFrame = false;
            m_registeredTargets.Clear();
            return;
        }

        for (int i = 0; i < (m_collisionVertices.Length - 1); i++)
        {
            Vector3 _point01 = m_collisionVertices[i].position;
            Vector3 _point02 = m_collisionVertices[i + 1].position;

            for (int j = 0; j < m_betweenPointsIterations; j++)
            {
                float _normalized = (float)j / (float)m_betweenPointsIterations;
                Vector3 _currentMidPoint = Vector3.Lerp(_point01, _point02, _normalized);

                if (m_activePreviousFrame)
                {
                    Vector3 _rayStart = m_previousCollisionPoints[i, j];
                    Vector3 _rayEnd = _currentMidPoint;
                    Vector3 _diff = _rayEnd - _rayStart;

                    int _hits = Physics.RaycastNonAlloc(
                        _rayStart,
                        _diff.normalized,
                        m_hits,
                        _diff.magnitude,
                        m_damageLayer,
                        QueryTriggerInteraction.Collide);

                    if (_hits > 0)
                    {
                        RaycastHit _closest = m_hits.GetClosestHit(_hits, null);

                        PlayerHurtbox _playerHurtbox = _closest.collider.gameObject.GetComponent<PlayerHurtbox>();
                        if (_playerHurtbox)
                        {
                            _playerHurtbox.DealDamage(0, Owner.transform);
                            m_registeredTargets.Add(_playerHurtbox);
                        }
                    }

                    Debug.DrawLine(m_previousCollisionPoints[i, j], _currentMidPoint, Color.red, 0.6f);
                }

                m_previousCollisionPoints[i, j] = _currentMidPoint;
            }
        }

        m_activePreviousFrame = true;
    }
}
