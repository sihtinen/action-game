﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    private static ObjectPool m_instance = null;
    public static ObjectPool GetInstance() { return m_instance; }

    [HideInInspector] public RaycastHit[] RaycastHits = new RaycastHit[100];

    [SerializeField] private ProjectileBase m_lightPistolProjectilePrefab;
    [SerializeField] private GameObject m_characterHitParticlesPrefab;
    [SerializeField] private int m_lightPistolProjectilePoolSize = 100;

    private List<ProjectileBase> m_projectilePool;
    private List<GameObject> m_characterHitParticlesPool;

    private void Awake()
    {
        m_instance = this;

        if (m_lightPistolProjectilePrefab)
        {
            m_projectilePool = new List<ProjectileBase>(m_lightPistolProjectilePoolSize);
            m_characterHitParticlesPool = new List<GameObject>(m_lightPistolProjectilePoolSize);

            Transform _lightPistolProjectileParent = new GameObject().transform;
            _lightPistolProjectileParent.name = "Light Pistol Projectile Pool";

            for (int i = 0; i < m_lightPistolProjectilePoolSize; i++)
            {
                ProjectileBase _current = Instantiate(m_lightPistolProjectilePrefab.gameObject, _lightPistolProjectileParent).GetComponent<ProjectileBase>();
                _current.gameObject.SetActive(false);
                m_projectilePool.Add(_current);

                GameObject _newCharacterHitParticle = Instantiate(m_characterHitParticlesPrefab, _lightPistolProjectileParent);
                _newCharacterHitParticle.SetActive(false);
                m_characterHitParticlesPool.Add(_newCharacterHitParticle);
            }
        }
    }

    public ProjectileBase GetProjectile(ProjectileBase.ProjectileType projectileType)
    {
        ProjectileBase _result = null;
        for (int i = 0; i < m_projectilePool.Count; i++)
        {
            if (m_projectilePool[i].MyProjectileType != projectileType) continue;
            if (m_projectilePool[i].Alive == false)
            {
                _result = m_projectilePool[i];
                break;
            }
        }

        return _result;
    }

    public void SpawnProjectileCharacterHitParticles(Vector3 position, Vector3 hitNormal)
    {
        GameObject _result = null;
        for (int i = 0; i < m_characterHitParticlesPool.Count; i++)
        {
            if (m_characterHitParticlesPool[i].activeInHierarchy == false)
            {
                _result = m_characterHitParticlesPool[i];
                break;
            }
        }

        if (_result)
        {
            _result.transform.position = position;
            _result.transform.up = hitNormal;
            _result.SetActive(true);
            //_result.GetComponent<ParticleSystem>().Play();
        }
    }
}
