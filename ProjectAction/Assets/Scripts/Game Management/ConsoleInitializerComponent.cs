﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Popcron.Console;

[Category("Time")]
public class Commands
{
    [Command("set time scale")]
    public static void SetGameTimeScale(float newTimeScale)
    {
        if (GameTime.GetInstance())
            GameTime.GetInstance().OverrideGameTimeScale = newTimeScale;
    }

    [Command("set frame rate cap")]
    [Alias("set fps cap")]
    public static void SetFrameRateCap(int newFrameRateCap)
    {
        if (GameTime.GetInstance())
            GameTime.GetInstance().FrameRateCap = newFrameRateCap;
    }
}

public class ConsoleInitializerComponent : MonoBehaviour
{
    [SerializeField] private List<KeyCode> m_consoleToggleInputs = new List<KeyCode>();

    private void Start()
    {
        Console.Initialize(m_consoleToggleInputs);
        Console.Open = false;
    }

    public bool IsConsoleOpen()
    {
        return Console.Open;
    }
}
