﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameState : MonoBehaviour
{
    public struct PlayerAttackHitInfo
    {

    }

    public class PlayerAttackHitEvent : UnityEvent<PlayerAttackHitInfo> { }

    private static GameState m_instance = null;
    public static GameState GetInstance() { return m_instance; }

    public GameObject PlayerObject = null;
    public UI_MainCanvasComponent UI_MainCanvas = null;
    public List<EnemyEntity> EnemyEntities = new List<EnemyEntity>();

    public PlayerAttackHitEvent OnPlayerAttackHit;

    private void Awake()
    {
        m_instance = this;
        OnPlayerAttackHit = new PlayerAttackHitEvent();
    }

    public void RegisterEnemyEntity(EnemyEntity entity)
    {
        if (EnemyEntities.Contains(entity) == false)
        {
            EnemyEntities.Add(entity);
        }
    }
}
