﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTime : MonoBehaviour
{
    [System.Serializable]
    public class TimeScaleManipulatorParameters
    {
        public AnimationCurve Curve;
        public int Priority = 0;
        public float Duration = 1.0f;
        public float LowestTimeScale = 0;
    }

    private class TimeScaleManipulator
    {
        public TimeScaleManipulator(MonoBehaviour source, AnimationCurve curve, int priority, float duration, float lowestTimeScale)
        {
            Source = source;

            Curve = curve;
            Priority = priority;
            Duration = duration;
            LowestTimeScale = lowestTimeScale;

            CurrentTime = 0.0f;
        }

        public TimeScaleManipulator(MonoBehaviour source, ref TimeScaleManipulatorParameters parameters)
        {
            Source = source;

            Curve = parameters.Curve;
            Priority = parameters.Priority;
            Duration = parameters.Duration;
            LowestTimeScale = parameters.LowestTimeScale;

            CurrentTime = 0.0f;
        }

        public MonoBehaviour Source;
        public AnimationCurve Curve;
        public int Priority;
        public float Duration;
        public float LowestTimeScale;

        public float CurrentTime;
    }

    private static GameTime m_instance = null;
    public static GameTime GetInstance() { return m_instance; }

    public static float GameTimeScale = 1.0f;
    public float OverrideGameTimeScale = 1.0f;

    [SerializeField] private bool m_enableFrameRateCap = true;
    [Range(30, 300)] public int FrameRateCap = 144;

    private List<TimeScaleManipulator> m_timeScaleManipulators = null;
    private PlayerMovementManager m_movementManager = null;
    private PlayerAimManager m_aimManager = null;
    private ConsoleInitializerComponent m_consoleComponent = null;

    private int m_currentFrameRate = -1;

    private void Awake()
    {
        m_instance = this;

        m_timeScaleManipulators = new List<TimeScaleManipulator>();

        if (m_enableFrameRateCap)
        {
            m_currentFrameRate = FrameRateCap;
            Application.targetFrameRate = m_currentFrameRate;
        }
    }

    private void Start()
    {
        m_consoleComponent = GetComponent<ConsoleInitializerComponent>();
        m_movementManager = GameState.GetInstance().PlayerObject.GetComponent<PlayerMovementManager>();
        m_aimManager = GameState.GetInstance().PlayerObject.GetComponent<PlayerAimManager>();
    }

    private void Update()
    {
        if (m_enableFrameRateCap)
        {
            if (m_currentFrameRate != FrameRateCap)
            {
                m_currentFrameRate = FrameRateCap;
                Application.targetFrameRate = m_currentFrameRate;
            }
        }

        calculateTimeScale();
    }

    public void TriggerHitFreeze(MonoBehaviour source, int priority, float duration, float lowestTimeScale, ref AnimationCurve curve)
    {
        for (int i = 0; i < m_timeScaleManipulators.Count; i++)
        {
            if (m_timeScaleManipulators[i].Source == source)
            {
                m_timeScaleManipulators.RemoveAt(i);
                break;
            }
        }

        TimeScaleManipulator _newManipulator = new TimeScaleManipulator(
            source,
            curve,
            priority,
            duration,
            lowestTimeScale);

        m_timeScaleManipulators.Add(_newManipulator);
    }

    public void TriggerHitFreeze(MonoBehaviour source, ref TimeScaleManipulatorParameters parameters)
    {
        for (int i = 0; i < m_timeScaleManipulators.Count; i++)
        {
            if (m_timeScaleManipulators[i].Source == source)
            {
                m_timeScaleManipulators.RemoveAt(i);
                break;
            }
        }

        TimeScaleManipulator _newManipulator = new TimeScaleManipulator(source, ref parameters);

        m_timeScaleManipulators.Add(_newManipulator);
    }

    private void calculateTimeScale()
    {
        if (m_timeScaleManipulators.Count > 0)
        {
            TimeScaleManipulator _target = null;
            for (int i = m_timeScaleManipulators.Count; i-- > 0;)
            {
                m_timeScaleManipulators[i].CurrentTime += Time.unscaledDeltaTime;
                if (m_timeScaleManipulators[i].CurrentTime > m_timeScaleManipulators[i].Duration)
                {
                    m_timeScaleManipulators.RemoveAt(i);
                    continue;
                }

                if (_target == null || m_timeScaleManipulators[i].Priority > _target.Priority)
                {
                    _target = m_timeScaleManipulators[i];
                }
            }

            if (_target != null)
            {
                float _normalized = _target.CurrentTime / _target.Duration;
                Time.timeScale = Mathf.Lerp(_target.LowestTimeScale, 1.0f, _target.Curve.Evaluate(_normalized));
            }
            else
            {
                Time.timeScale = 1.0f;
            }
        }
        else
        {
            Time.timeScale = 1.0f;

            if (m_movementManager.HasGround() == false && m_aimManager.IsAiming())
            {
                Time.timeScale = 0.2f;
            }
        }

        if (m_consoleComponent.IsConsoleOpen())
        {
            Time.timeScale = 0.0f;
            GameTimeScale = 0.0f;
        }
        else
        {
            GameTimeScale = 1.0f;
            Time.timeScale *= OverrideGameTimeScale;
        }
    }

    private void OnDestroy()
    {
        Time.timeScale = 1.0f;
        GameTimeScale = 1.0f;
    }

    public bool IsConsoleOpen()
    {
        return m_consoleComponent.IsConsoleOpen();
    }
}
